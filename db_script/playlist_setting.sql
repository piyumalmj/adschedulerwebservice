INSERT INTO `pages` (`page_name`) VALUES ('PlaylistSetting');
INSERT INTO `authorities` (`authority`, `username`) VALUES ('ROLE_PlaylistSetting', 'SU');

CREATE TABLE advert_setting (
  id int(11) NOT NULL AUTO_INCREMENT,
  max_limit int(11) NOT NULL,
  max_slots int(11) NOT NULL,
  min_limit int(11) NOT NULL,
  channel_id int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhtqbpmwnf8lo6frvk38gtnql6` (`channel_id`)
);