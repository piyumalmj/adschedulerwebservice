/*
-- Query: SELECT * FROM vcl_schedule_db.product
LIMIT 0, 50000

-- Date: 2019-08-29 16:27
*/
INSERT INTO `product` (`id`,`name`) VALUES (1,'COUGH & COLD REMEDIES');
INSERT INTO `product` (`id`,`name`) VALUES (2,'BODY WASH');
INSERT INTO `product` (`id`,`name`) VALUES (3,'CORPORATE');
INSERT INTO `product` (`id`,`name`) VALUES (4,'FACE WASH');
INSERT INTO `product` (`id`,`name`) VALUES (5,'WOMEN\'S RANGE');
INSERT INTO `product` (`id`,`name`) VALUES (6,'WOMEN\'S CREAMS/LOTION');
INSERT INTO `product` (`id`,`name`) VALUES (7,'LUBRICANTS');
INSERT INTO `product` (`id`,`name`) VALUES (8,'PUBLICATION');
INSERT INTO `product` (`id`,`name`) VALUES (9,'AIRLINE');
INSERT INTO `product` (`id`,`name`) VALUES (10,'AIR CONDITIONERS');
INSERT INTO `product` (`id`,`name`) VALUES (11,'MOTORCYCLES');
INSERT INTO `product` (`id`,`name`) VALUES (12,'AUDIO EQUIPMENT');
