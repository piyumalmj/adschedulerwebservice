import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by dperera on 23/10/2017.
 */
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setMaxInactiveInterval(30 * 60);  //maximum session inactive time 30 minutes
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }
}
