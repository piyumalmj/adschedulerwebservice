import com.vclabs.nash.configuration.ServletErrorFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;

/**
 * Created by dperera on 14/08/2017.
 */
public class ApplicationInitializer implements WebApplicationInitializer {

	private static final int MAX_UPLOAD_SIZE = 1000 * 1024 * 1024;

	@Override
	public void onStartup(ServletContext container) throws ServletException {

		container.setInitParameter("spring.profiles.active", "dev");
		// Create the 'root' Spring application context
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(RootApplicationConfiguration.class);

		// Manage the life-cycle of the root application context
		container.addListener(new ContextLoaderListener(rootContext));
		container.addListener(new SessionListener());
		// Create the dispatcher servlet's Spring application context
		AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();

		container.addFilter("errorHandlerFilter", ServletErrorFilter.class)
				.addMappingForUrlPatterns(null, false, "/*");
		// Register and map the dispatcher servlet
		ServletRegistration.Dynamic dispatcher = container.addServlet(
				"dispatcher", new DispatcherServlet(dispatcherContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(RootApplicationConfiguration.TMP_UPLOAD_FOLDER_PATH,
				MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 5, MAX_UPLOAD_SIZE / 20);
		dispatcher.setMultipartConfig(multipartConfigElement);
	}
}
