package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.RevenueLineClientDTO;
import com.vclabs.nash.dashboard.dto.RevenueLineDbDTO;
import com.vclabs.nash.dashboard.dto.RevenueLinePieChartDTO;
import com.vclabs.nash.dashboard.dto.RevenueLineUserDTO;
import com.vclabs.nash.dashboard.service.RevenueLineDashboardWidgetService;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Nalaka on 2019-01-18.
 */
@Service
public class RevenueLineDashboardWidgetServiceImpl implements RevenueLineDashboardWidgetService {

    static double totalBudget = 0.0;

    @Autowired
    private WorkOrderService workOrderService;

    @Autowired
    private DashboardManager dashboardManager;

    @Async
    @Override
    public void setRevenueLineDashboardData() {
        List<RevenueLineDbDTO> allWorkOrdeList = workOrderService.findAllWorkOrderForRevenueLine();

        totalBudget = allWorkOrdeList.stream().mapToDouble(o -> o.getTotalBudget()).sum();

        HashMap<String, RevenueLinePieChartDTO> pieChartData = getPiechartData(allWorkOrdeList);

        HashMap<String, List<RevenueLineDbDTO>> clientDetailMap = addToClientMap(allWorkOrdeList);
        List<RevenueLineClientDTO> revenueLineClientDTOS = addToUserMap(clientDetailMap);
        dashboardManager.convertAndSend(NashDMConverterName.REVENUE_LINE_DASHBOARD, "dashboard.revenueLineDashboard.public", revenueLineClientDTOS, pieChartData);
    }

    private HashMap<String, List<RevenueLineDbDTO>> addToClientMap(List<RevenueLineDbDTO> revenueData){
        HashMap<String, List<RevenueLineDbDTO>> clientMap = new HashMap<>();
        List<RevenueLineDbDTO> clientData = new ArrayList<>();
        for(RevenueLineDbDTO dbData : revenueData){
            if(clientMap.containsKey(dbData.getClientName())){
                clientData = clientMap.get(dbData.getClientName());
                clientData.add(dbData);
            }else {
                clientData = new ArrayList<>();
                clientData.add(dbData);
                clientMap.put(dbData.getClientName(), clientData);
            }
        }
        return clientMap;
    }

    private List<RevenueLineClientDTO> addToUserMap(HashMap<String, List<RevenueLineDbDTO>> clientMap) {
        Iterator iterator = clientMap.entrySet().iterator();
        List<RevenueLineClientDTO> clientDataSet = new ArrayList<>();
        List<RevenueLineUserDTO> userDataList = new ArrayList<>();
        int id = 0;
        while (iterator.hasNext()){
            id++;
            double budget = 0.0;
            Map.Entry pair = (Map.Entry)iterator.next();
            List<RevenueLineDbDTO> list = (List<RevenueLineDbDTO>)pair.getValue();

            RevenueLineClientDTO clientData = new RevenueLineClientDTO();
            clientData.setId(id);
            clientData.setName(pair.getKey().toString());

            HashMap<String, List<RevenueLineUserDTO>> userMap = new HashMap<>();
            for (RevenueLineDbDTO data : list){
                RevenueLineUserDTO userData = new RevenueLineUserDTO();
                userData.setName(data.getUser());
                userData.setBudget(data.getTotalBudget());

                if(userMap.containsKey(data.getUser())){
                    userDataList = userMap.get(data.getUser());
                    userDataList.add(userData);
                }else {
                    userDataList = new ArrayList<>();
                    userDataList.add(userData);
                    userMap.put(data.getUser(), userDataList);
                }
                budget =+ data.getTotalBudget();
            }
            Iterator iterator1 = userMap.entrySet().iterator();
            List<RevenueLineUserDTO> users = new ArrayList<>();
            while (iterator1.hasNext()){
                Map.Entry pair1 = (Map.Entry)iterator1.next();
                users.addAll((List<RevenueLineUserDTO>)pair1.getValue());
            }
            double totalBudgetOfUser = users.stream().mapToDouble(o -> o.getBudget()).sum();
            for (RevenueLineUserDTO  user : users ){
                int percentage = 100;
                if(totalBudgetOfUser > 0){
                    Double percentageCal = (user.getBudget()*100/totalBudgetOfUser);
                    percentage = percentageCal.intValue();
                }
                user.setPercentage((long) percentage);
            }
            clientData.setValue((long) budget);
            clientData.setUserList(users);
            double percentage = 100.00;

            if (totalBudget > 0){
                percentage = (budget/totalBudget)*100;
            }
            clientData.setPercentage(percentage);
            clientDataSet.add(clientData);
        }
        return clientDataSet;
    }

    private HashMap<String, RevenueLinePieChartDTO> getPiechartData(List<RevenueLineDbDTO> allWorkOrder) {
        HashMap<String, RevenueLinePieChartDTO> pieChartDTOHashMap = new HashMap<>();
        for (RevenueLineDbDTO revenueLineDbDTO : allWorkOrder) {
            RevenueLinePieChartDTO pieChartDTO = pieChartDTOHashMap.get(revenueLineDbDTO.getWoType());
            if (pieChartDTO == null) {
                pieChartDTO = new RevenueLinePieChartDTO();
                pieChartDTO.setName(revenueLineDbDTO.getWoType());
                pieChartDTO.addBudget(revenueLineDbDTO.getTotalBudget());
                pieChartDTO.setPercentage((pieChartDTO.getBudget() / totalBudget) * 100);
                pieChartDTOHashMap.put(revenueLineDbDTO.getWoType(), pieChartDTO);
            } else {
                pieChartDTO.addBudget(revenueLineDbDTO.getTotalBudget());
                pieChartDTO.setPercentage((pieChartDTO.getBudget() / totalBudget) * 100);
            }
        }

        return pieChartDTOHashMap;
    }

}
