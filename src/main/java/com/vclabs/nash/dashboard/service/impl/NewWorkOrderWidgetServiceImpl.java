package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.NewWorkOrderWidgetService;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 26/11/2018
 */
@Service
public class NewWorkOrderWidgetServiceImpl implements NewWorkOrderWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setNewWorkOrderData(){
        List<String> sellers = workOrderService.findNewWorkOrderSellers();

        for (String seller : sellers){
            List<WorkOrder> workOrders = workOrderService.findNewWorkOrders(seller);
            dashboardManager.convertAndSend(NashDMConverterName.NEW_WORK_ORDER,"dashboard.newWorkOrder."+seller, workOrders);
        }
    }

}
