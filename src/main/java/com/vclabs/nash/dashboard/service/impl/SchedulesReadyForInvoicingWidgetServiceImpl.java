package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.SchedulesReadyForInvoicingDto;
import com.vclabs.nash.dashboard.service.SchedulesReadyForInvoicingWidgetService;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 04/12/2018
 */
@Service
public class SchedulesReadyForInvoicingWidgetServiceImpl implements SchedulesReadyForInvoicingWidgetService{

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setSchedulesReadyForInvoicingData() {
        List<SchedulesReadyForInvoicingDto> workOrders = workOrderService.findSchedulesReadyForInvoicing();
        dashboardManager.convertAndSend(NashDMConverterName.SCHEDULES_READY_FOR_INVOICING,"dashboard.schedulesReadyForInvoicing.public", workOrders);

    }

}
