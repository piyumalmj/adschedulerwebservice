package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import com.vclabs.nash.dashboard.service.IngestStatusWidgetService;
import com.vclabs.nash.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sanduni on 06/12/2018
 */
@Service
public class IngestStatusWidgetServiceImpl implements IngestStatusWidgetService {

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private DashboardManager dashboardManager;

    @Async
    @Override
    public void setIngestStatusWidgetData() {
        List<IngestStatusWidgetDto> dataSet = advertisementService.findAllAdvertisementsForIngestStatusWidget();
        dashboardManager.convertAndSend(NashDMConverterName.INGEST_STATUS, "dashboard.ingestStatus.public", dataSet);
    }
}
