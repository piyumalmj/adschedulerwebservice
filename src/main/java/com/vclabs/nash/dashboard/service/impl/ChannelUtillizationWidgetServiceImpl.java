package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.ChannelUtillizationWidgetService;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Nalaka on 2018-11-21.
 */
@Service
@Transactional("main")
public class ChannelUtillizationWidgetServiceImpl implements ChannelUtillizationWidgetService {

    @Autowired
    private DashboardManager dashboardManager;
    @Autowired
    private NashInventoryConsumptionService nashInventoryConsumptionService;

    @Async
    @Override
    public void setChannelUtilizationData(){
        dashboardManager.convertAndSend(NashDMConverterName.CHANNEL_UTILIZATION,"dashboard.channelUtilization.public",nashInventoryConsumptionService.findChannelUtillizationDetails());
    }
}


