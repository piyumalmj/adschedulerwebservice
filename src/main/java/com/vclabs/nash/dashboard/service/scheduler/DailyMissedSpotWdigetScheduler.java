package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.DailyMissedSpotUpdateWidgetService;
import com.vclabs.nash.dashboard.service.TotalDailyMissedSpotUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class DailyMissedSpotWdigetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(DailyMissedSpotWdigetScheduler.class);

    @Autowired
    private DailyMissedSpotUpdateWidgetService dailyMissedSpotUpdateWidgetService;

    @Autowired
    private TotalDailyMissedSpotUpdateService totalDailyMissedSpotUpdateService;

    //@Scheduled(cron = "${cron.daily.total.missed.spot.widget}")
    public void dailyMissedSpotUpdateWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("Daily Missed Spot Update Schedule started");

        dailyMissedSpotUpdateWidgetService.setDailyMissedSpotData();
        totalDailyMissedSpotUpdateService.setDailyMissedSpotUpdateData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("Daily Missed Spot Update Schedule finished");
    }
}
