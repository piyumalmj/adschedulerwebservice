package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.PriorityAdvertisementWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Nalaka on 2019-02-14.
 */
@Component
public class PriorityAdvertisementWidgetSchedule {

    private static final Logger logger = LoggerFactory.getLogger(PriorityAdvertisementWidgetSchedule.class);

    @Autowired
    private PriorityAdvertisementWidgetService priorityAdvertisementWidgetService;

    //@Scheduled(cron = "${cron.priority.advertisement.widget}")
    public void priorityAdvertisementWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("priorityAdvertisementWidgetService Update Schedule started");

        priorityAdvertisementWidgetService.setPriorityAdvertisementWidgetData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("priorityAdvertisementWidgetService Update Schedule finished");
    }
}
