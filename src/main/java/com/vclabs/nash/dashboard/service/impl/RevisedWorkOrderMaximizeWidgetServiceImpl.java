package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.RevisedWorkOrderMaximizeWidgetService;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 27/11/2018
 */
@Service
public class RevisedWorkOrderMaximizeWidgetServiceImpl implements RevisedWorkOrderMaximizeWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setRevisedWorkOrderData(){
        List<WorkOrder> workOrders = workOrderService.findRevisedWorkOrders();
        List<WorkOrder> currentWorkOrders = new ArrayList<>();

        if(workOrders.size() > 0){
            String seller = workOrders.get(0).getSeller();
            for (WorkOrder workOrder : workOrders){
                if((workOrder.getSeller()).equals(seller)){
                    currentWorkOrders.add(workOrder);
                }else{
                    dashboardManager.convertAndSend(NashDMConverterName.REVISED_WORK_ORDER,"dashboard.revisedWorkOrder." + seller, currentWorkOrders);
                    seller = workOrder.getSeller();
                    currentWorkOrders.clear();
                    currentWorkOrders.add(workOrder);
                }

            }
            dashboardManager.convertAndSend(NashDMConverterName.REVISED_WORK_ORDER,"dashboard.revisedWorkOrder." + seller, currentWorkOrders);
        }

    }
}
