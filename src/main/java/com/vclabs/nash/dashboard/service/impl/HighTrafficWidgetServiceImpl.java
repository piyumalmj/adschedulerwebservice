package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import com.vclabs.nash.dashboard.service.HighTrafficWidgetService;
import com.vclabs.nash.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 27/11/2018
 */
@Service
public class HighTrafficWidgetServiceImpl implements HighTrafficWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private SchedulerService schedulerService;

    @Async
    @Override
    public void setHighTrafficData(){
        List<HightTrafficChannelsWidgetDto> channels = schedulerService.findHighTrafficChannelsOfTheHour();
        dashboardManager.convertAndSend(NashDMConverterName.HIGH_TRAFFIC_CHANNELS,"dashboard.highTrafficChannels.public", channels);
    }
}
