package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyRecordedRevenueDto;
import com.vclabs.nash.dashboard.service.MyRecordedRevenueWidgetService;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 04/12/2018
 */
@Service
public class MyRecordedRevenueWidgetServiceImpl implements MyRecordedRevenueWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setMyRecordedRevenueData() {
        List<String> sellers = workOrderService.findAllSellers();

        for (String seller : sellers){
            List<MyRecordedRevenueDto> recordedRevenue = workOrderService.getMyRecordedRevenue(seller);
            dashboardManager.convertAndSend(NashDMConverterName.MY_RECORDED_REVENUE,"dashboard.myRecordedRevenue."+seller, recordedRevenue);
        }
    }
}
