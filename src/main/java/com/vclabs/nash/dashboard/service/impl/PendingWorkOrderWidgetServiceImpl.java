package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PendingWOCountByAutoStatusDto;
import com.vclabs.nash.dashboard.service.PendingWorkOrderWidgetService;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sanduni on 29/11/2018
 */
@Service
public class PendingWorkOrderWidgetServiceImpl implements PendingWorkOrderWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setPendingWorkOrderData() {

        List<PendingWOCountByAutoStatusDto> woCountByAutoStatusDtos = workOrderService.findAllPendingWOAutoStatusCount();
        HashMap<String, List<PendingWOCountByAutoStatusDto>> woByAutoStatus = addWOAutoMap(woCountByAutoStatusDtos);

        dashboardManager.convertAndSend(NashDMConverterName.PENDING_WORK_ORDER,"dashboard.pendingWorkOrders.public", woByAutoStatus);

    }

    private HashMap<String, List<PendingWOCountByAutoStatusDto>> addWOAutoMap(List<PendingWOCountByAutoStatusDto> allWorkOrder) {
        HashMap<String, List<PendingWOCountByAutoStatusDto>> woMap = new HashMap<>();
        List<PendingWOCountByAutoStatusDto> woList = new ArrayList<>();
        for (PendingWOCountByAutoStatusDto data : allWorkOrder) {
            if(woMap.containsKey(data.getRevenueMonth())){
                woList.add(data);
                woMap.get(data.getRevenueMonth()).add(data);
            }else {
                List<PendingWOCountByAutoStatusDto> newWoList = new ArrayList<>();
                newWoList.add(data);
                woMap.put(data.getRevenueMonth(), newWoList);
            }
        }

        return woMap;
    }

}
