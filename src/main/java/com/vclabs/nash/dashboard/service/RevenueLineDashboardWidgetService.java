package com.vclabs.nash.dashboard.service;

/**
 * Created by Nalaka on 2019-01-18.
 */
public interface RevenueLineDashboardWidgetService {

    void setRevenueLineDashboardData();
}
