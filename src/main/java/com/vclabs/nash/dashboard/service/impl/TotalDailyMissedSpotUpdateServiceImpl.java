package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.TotalDailyMissedSpotUpdateService;
import com.vclabs.nash.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Created by Sanduni on 31/12/2018
 */
@Service
public class TotalDailyMissedSpotUpdateServiceImpl implements TotalDailyMissedSpotUpdateService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private SchedulerService schedulerService;

    @Async
    @Override
    public void setDailyMissedSpotUpdateData() {
        int missedSpots = schedulerService.findCurrentDateAllMissedSpot().size();
        int allSpots = schedulerService.findCurrentDateAllValidSpot().size();
        int percentageOfMissedSpots = 0;

        if(allSpots > 0){
            percentageOfMissedSpots = Integer.valueOf(missedSpots * 100 / allSpots);
        }
        dashboardManager.convertAndSend(NashDMConverterName.TOTAL_DAILY_MISSED_SPOT_UPDATE, "dashboard.dailyMissedSpotUpdateTotal.public", percentageOfMissedSpots);
    }
}
