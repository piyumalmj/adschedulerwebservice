package com.vclabs.nash.dashboard.service;

/**
 * Created by Sanduni on 29/11/2018
 */
public interface PendingWorkOrderWidgetService {

    void setPendingWorkOrderData();
}
