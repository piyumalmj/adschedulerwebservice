package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.MyToDoListWidgetService;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nalaka on 2018-11-08.
 */
@Service
@Transactional("main")
public class MyToDoListWidgetServiceImpl implements MyToDoListWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setMyToDoListData() {
        List<String> userList = workOrderService.findAllSellers();
        for (String user : userList) {
            dashboardManager.convertAndSend(NashDMConverterName.MY_TO_DO_LIST, "dashboard.myToDoList." + user, workOrderService.findPenddingWorkOrderByUser(user));
        }
    }
}
