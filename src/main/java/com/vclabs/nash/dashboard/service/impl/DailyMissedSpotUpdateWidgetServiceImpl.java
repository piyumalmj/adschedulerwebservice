package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.DailyMissedSpotDTO;
import com.vclabs.nash.dashboard.service.DailyMissedSpotUpdateWidgetService;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Nalaka on 2018-12-31.
 */
@Service
@Transactional("main")
public class DailyMissedSpotUpdateWidgetServiceImpl implements DailyMissedSpotUpdateWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    SchedulerService schedulerService;

    @Async
    @Override
    public void setDailyMissedSpotData() {

        HashMap map = new HashMap();
        List<ScheduleDef> allValidSpotList = schedulerService.findAllScheduleSpots();

        for (ScheduleDef scheduleDef : allValidSpotList) {
            DailyMissedSpotDTO dailyMissedSpotDTO = (DailyMissedSpotDTO) map.get(scheduleDef.getChannelid().getChannelid());
            if (dailyMissedSpotDTO == null) {
                DailyMissedSpotDTO dailyMissedSpot = new DailyMissedSpotDTO();
                dailyMissedSpot.setChannelId(scheduleDef.getChannelid().getChannelid());
                dailyMissedSpot.setChannelName(String.valueOf(scheduleDef.getChannelid().getChannelname()));
                if (scheduleDef.getStatus().equals("5")) {
                    dailyMissedSpot.getMissedSpotList().add(scheduleDef);
                } else {
                    dailyMissedSpot.getScheduleSpotList().add(scheduleDef);
                }
                map.put(scheduleDef.getChannelid().getChannelid(), dailyMissedSpot);
            } else {
                if (scheduleDef.getStatus().equals("5")) {
                    dailyMissedSpotDTO.getMissedSpotList().add(scheduleDef);
                } else {
                    dailyMissedSpotDTO.getScheduleSpotList().add(scheduleDef);
                }
            }
        }

        for (Object key : map.keySet()) {
            int channelId = (int) key;
            List<ScheduleDef> airedSpotList = schedulerService.findCurrentDateAllAiredSpot(channelId);
            DailyMissedSpotDTO temDto = (DailyMissedSpotDTO) map.get(channelId);
            int missedPresentage = temDto.getMissedSpotList().size() * 100 / (airedSpotList.size() + temDto.getScheduleSpotList().size() + temDto.getMissedSpotList().size());
            temDto.setMissedSpotPresentage(missedPresentage);
            // int schedulePresentage = temDto.getScheduleSpotList().size() * 100 / (airedSpotList.size() + temDto.getScheduleSpotList().size() + temDto.getMissedSpotList().size());
            int schedulePresentage = 100 - missedPresentage;
            temDto.setScheduledSpotPresentage(schedulePresentage);
        }

        dashboardManager.convertAndSend(NashDMConverterName.DAILY_MISSED_SPOT_UPDATE, "dashboard.dailyMissedSpotUpdate.public", map);

    }
}
