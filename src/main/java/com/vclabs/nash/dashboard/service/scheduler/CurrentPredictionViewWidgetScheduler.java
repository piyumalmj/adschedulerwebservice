package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.CurrentPredictionViewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 13/02/2019
 */
@Component
public class CurrentPredictionViewWidgetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(CurrentPredictionViewWidgetScheduler.class);

    @Autowired
    private CurrentPredictionViewService currentPredictionViewService;

    //@Scheduled(cron = "${cron.current.prediction.view.widget}")
    public void setCurrentPredictionView(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("Current Prediction View Schedule started");

        currentPredictionViewService.setCurrentPredictionViewData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("Current Prediction View Schedule finished");
    }
}
