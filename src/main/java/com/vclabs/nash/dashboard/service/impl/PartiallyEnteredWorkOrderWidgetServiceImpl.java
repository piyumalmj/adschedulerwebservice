package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PartiallyEnteredWODto;
import com.vclabs.nash.dashboard.service.PartiallyEnteredWorkOrderWidgetService;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 27/11/2018
 */
@Service
public class PartiallyEnteredWorkOrderWidgetServiceImpl implements PartiallyEnteredWorkOrderWidgetService {

    @Value("${dashboard.record.limit}")
    private int RECORD_LIMIT;

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setPartiallyEnteredWOData() {
        List<PartiallyEnteredWODto> workOrders = workOrderService.findPartiallyEnteredWOs();
        List<PartiallyEnteredWODto> currentWorkOrders = new ArrayList<>();

        if(workOrders.size() > 0){
            String seller = workOrders.get(0).getSeller();
            for (PartiallyEnteredWODto workOrder : workOrders){
                if((workOrder.getSeller()).equals(seller)){
                    if(currentWorkOrders.size()<=RECORD_LIMIT) {
                        currentWorkOrders.add(workOrder);
                    }
                }else{
                    dashboardManager.convertAndSend(NashDMConverterName.PARTIALLY_ENTERED_WORK_ORDER,"dashboard.partiallyEnteredWorkOrder." + seller, currentWorkOrders);
                    seller = workOrder.getSeller();
                    currentWorkOrders.clear();
                }

            }
            dashboardManager.convertAndSend(NashDMConverterName.PARTIALLY_ENTERED_WORK_ORDER,"dashboard.partiallyEnteredWorkOrder." + seller, currentWorkOrders);
        }

    }
}
