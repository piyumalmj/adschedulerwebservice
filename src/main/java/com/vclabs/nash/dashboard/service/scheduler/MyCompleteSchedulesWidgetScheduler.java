package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.MyCompleteSchedulesWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class MyCompleteSchedulesWidgetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(MyCompleteSchedulesWidgetScheduler.class);

    @Autowired
    private MyCompleteSchedulesWidgetService myCompleteSchedulesWidgetService;

    //@Scheduled(cron = "${cron.my.complete.schedule.widget}")
    public void myCompleteSchedulesWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myCompleteSchedules Update Schedule started");

        myCompleteSchedulesWidgetService.setMyCompleteSchedulesData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myCompleteSchedules Update Schedule finished");
    }
}
