package com.vclabs.nash.dashboard.service;

/**
 * Created by Sanduni on 07/12/2018
 */
public interface PriorityAdvertisementWidgetService {

    void setPriorityAdvertisementWidgetData();
}
