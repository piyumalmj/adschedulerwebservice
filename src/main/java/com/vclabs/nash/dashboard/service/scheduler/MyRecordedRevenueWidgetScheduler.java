package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.MyRecordedRevenueWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class MyRecordedRevenueWidgetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(MyRecordedRevenueWidgetScheduler.class);

    @Autowired
    private MyRecordedRevenueWidgetService myRecordedRevenueWidgetService;

    //@Scheduled(cron = "${cron.my.recorded.revenue.widget}")
    public void myRecordedRevenueWdigetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myRecordedRevenue Update Schedule started");

        myRecordedRevenueWidgetService.setMyRecordedRevenueData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myRecordedRevenue Update Schedule finished");
    }
}
