package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.HighTrafficWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class HighTrafficChannelsWdigetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(HighTrafficChannelsWdigetScheduler.class);

    @Autowired
    private HighTrafficWidgetService highTrafficWidgetService;

    //@Scheduled(cron = "${cron.high.traffic.channels.widget}")
    public void highTrafficChannelsWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("highTrafficChannels Update Schedule started");

        highTrafficWidgetService.setHighTrafficData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("highTrafficChannels Update Schedule finished");
    }
}
