package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 28/11/2018
 */
public class HightTrafficChannelsWidgetDto {

    private BigInteger spotCount;

    private Integer channelId;

    private String channelName;

    public HightTrafficChannelsWidgetDto(BigInteger spotCount, Integer channelId, String channelName) {
        this.spotCount = spotCount;
        this.channelId = channelId;
        this.channelName = channelName;
    }

    public BigInteger getSpotCount() {
        return spotCount;
    }

    public void setSpotCount(BigInteger spotCount) {
        this.spotCount = spotCount;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
