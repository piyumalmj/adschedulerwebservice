package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 29/11/2018
 */
public class PendingWOCountByAutoStatusDto {

    private BigInteger woCount;

    private String revenueMonth;

    private int autoStatus;

    private Integer woId;

    private Integer scheduleId;

    private String client;

    public PendingWOCountByAutoStatusDto(BigInteger woCount, String revenueMonth, int autoStatus, Integer woId, Integer scheduleId, String client) {
        this.woCount = woCount;
        this.revenueMonth = revenueMonth;
        this.autoStatus = autoStatus;
        this.woId = woId;
        this.scheduleId = scheduleId;
        this.client = client;
    }

    public BigInteger getWoCount() {
        return woCount;
    }

    public void setWoCount(BigInteger woCount) {
        this.woCount = woCount;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public int getAutoStatus() {
        return autoStatus;
    }

    public void setAutoStatus(int autoStatus) {
        this.autoStatus = autoStatus;
    }

    public Integer getWoId() {
        return woId;
    }

    public void setWoId(Integer woId) {
        this.woId = woId;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
