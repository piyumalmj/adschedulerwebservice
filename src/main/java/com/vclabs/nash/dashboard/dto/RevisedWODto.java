package com.vclabs.nash.dashboard.dto;

/**
 * Created by Sanduni on 13/11/2018
 */
public class RevisedWODto {

    private Integer woId;

    private String client;

    private int spots;

    public Integer getWoId() {
        return woId;
    }

    public void setWoId(Integer woId) {
        this.woId = woId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public int getSpots() {
        return spots;
    }

    public void setSpots(int spots) {
        this.spots = spots;
    }
}
