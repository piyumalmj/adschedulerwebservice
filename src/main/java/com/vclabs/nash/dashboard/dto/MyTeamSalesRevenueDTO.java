package com.vclabs.nash.dashboard.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-13.
 */
public class MyTeamSalesRevenueDTO {

    private String month;

    private List<MyTeamSalesRevenueMemberDTO> memberLis = new ArrayList<>();

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<MyTeamSalesRevenueMemberDTO> getMemberLis() {
        return memberLis;
    }

    public void setMemberLis(List<MyTeamSalesRevenueMemberDTO> memberLis) {
        this.memberLis = memberLis;
    }
}
