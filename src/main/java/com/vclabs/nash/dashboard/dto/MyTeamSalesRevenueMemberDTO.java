package com.vclabs.nash.dashboard.dto;

import java.math.BigDecimal;

/**
 * Created by Nalaka on 2019-01-13.
 */
public class MyTeamSalesRevenueMemberDTO {

    private String meName;

    private long amount;

    public String getMeName() {
        return meName;
    }

    public void setMeName(String meName) {
        this.meName = meName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
