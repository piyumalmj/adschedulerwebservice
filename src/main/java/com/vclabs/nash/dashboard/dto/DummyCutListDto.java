package com.vclabs.nash.dashboard.dto;

import com.vclabs.nash.model.entity.ClientDetails;

import java.util.Date;

/**
 * Created by Sanduni on 09/11/2018
 */
public class DummyCutListDto {

    private Integer cutId;

    private ClientDetails client;

    private Date createDate;

    private Date expireDate;

    private int spots;

    private String user;

    public Integer getCutId() {
        return cutId;
    }

    public void setCutId(Integer cutId) {
        this.cutId = cutId;
    }

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public int getSpots() {
        return spots;
    }

    public void setSpots(int spots) {
        this.spots = spots;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
