package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 05/12/2018
 */
public class MyCompletedSchedulesDto {

    private BigInteger completedSchedules;

    private String client;

    private String seller;

    public MyCompletedSchedulesDto(BigInteger completedSchedules, String client, String seller) {
        this.completedSchedules = completedSchedules;
        this.client = client;
        this.seller = seller;
    }

    public BigInteger getCompletedSchedules() {
        return completedSchedules;
    }

    public void setCompletedSchedules(BigInteger completedSchedules) {
        this.completedSchedules = completedSchedules;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }
}
