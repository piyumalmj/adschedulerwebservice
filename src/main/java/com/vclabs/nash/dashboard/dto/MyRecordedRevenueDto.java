package com.vclabs.nash.dashboard.dto;

/**
 * Created by Sanduni on 03/12/2018
 */
public class MyRecordedRevenueDto {

    String revenueMonth;

    Double totalBudget;

    public MyRecordedRevenueDto(String revenueMonth, Double totalBudget) {
        this.revenueMonth = revenueMonth;
        this.totalBudget = totalBudget;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public Double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(Double totalBudget) {
        this.totalBudget = totalBudget;
    }
}
