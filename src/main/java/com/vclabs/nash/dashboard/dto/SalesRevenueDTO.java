package com.vclabs.nash.dashboard.dto;

import java.util.Date;

/**
 * Created by Nalaka on 2019-01-13.
 */
public class SalesRevenueDTO {

    private int workOredrId;

    private String user;

    private String revenueMonth;

    private double totalBudget;

    private Date startdate;

    private Date enddate;

    public SalesRevenueDTO(int workOredrId, String user, String revenueMonth, double totalBudget, Date startdate, Date enddate) {
        this.workOredrId = workOredrId;
        this.user = user;
        this.revenueMonth = revenueMonth;
        this.totalBudget = totalBudget;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public int getWorkOredrId() {
        return workOredrId;
    }

    public void setWorkOredrId(int workOredrId) {
        this.workOredrId = workOredrId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
}
