package com.vclabs.nash.dashboard.dto;

/**
 * Created by Sanduni on 07/12/2018
 */
public class PriorityAdvertisementWidgetDto {

    private Integer advertId;

    private String advertName;

    private int status;

    private int priorityNo;

    private String timeBelt;

    private String channel;

    public PriorityAdvertisementWidgetDto(Integer advertId, String advertName, int status, int priorityNo, String timeBelt, String channel) {
        this.advertId = advertId;
        this.advertName = advertName;
        this.status = status;
        this.priorityNo = priorityNo;
        this.timeBelt = timeBelt;
        this.channel = channel;
    }

    public Integer getAdvertId() {
        return advertId;
    }

    public void setAdvertId(Integer advertId) {
        this.advertId = advertId;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPriorityNo() {
        return priorityNo;
    }

    public void setPriorityNo(int priorityNo) {
        this.priorityNo = priorityNo;
    }

    public String getTimeBelt() {
        return timeBelt;
    }

    public void setTimeBelt(String timeBelt) {
        this.timeBelt = timeBelt;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
