package com.vclabs.nash.dashboard.dto;

import java.util.Date;

/**
 * Created by Nalaka on 2019-01-18.
 */
public class RevenueLineDbDTO {
    private String user;
    private Date startDate;
    private Date endDate;
    private double totalBudget;
    private String woType;
    private String clientName;

    public RevenueLineDbDTO(String user, Date startDate, Date endDate, double totalBudget, String woType, String clientName) {
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalBudget = totalBudget;
        this.woType = woType;
        this.clientName = clientName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
    }

    public String getWoType() {
        return woType;
    }

    public void setWoType(String woType) {
        this.woType = woType;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
