package com.vclabs.nash.dashboard.dto;

/**
 * Created by Sanduni on 25/01/2019
 */
public class CurrentPredictionViewDto {

    private long id;

    private String icon;

    private long count;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
