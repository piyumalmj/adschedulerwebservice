package com.vclabs.nash.dashboard.dto;

import java.util.Date;

/**
 * Created by Sanduni on 04/12/2018
 */
public class SchedulesReadyForInvoicingDto {

    Integer woId;

    String client;

    Date endDate;

    String seller;

    public SchedulesReadyForInvoicingDto(Integer woId, String client, Date endDate, String seller) {
        this.woId = woId;
        this.client = client;
        this.endDate = endDate;
        this.seller = seller;
    }

    public Integer getWoId() {
        return woId;
    }

    public void setWoId(Integer woId) {
        this.woId = woId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }
}
