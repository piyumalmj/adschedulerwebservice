package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 25/01/2019
 */
public class ChannelPredictionDto {

    private int id;

    private String name;

    private BigInteger count;

    public ChannelPredictionDto(int id, String name, BigInteger count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getCount() {
        return count;
    }

    public void setCount(BigInteger count) {
        this.count = count;
    }
}
