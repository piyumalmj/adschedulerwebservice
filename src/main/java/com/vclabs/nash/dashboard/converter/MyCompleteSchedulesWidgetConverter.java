package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.MCSTableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.WOTableRow;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyCompletedSchedulesDto;
import com.vclabs.nash.model.entity.WorkOrder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sanduni on 05/12/2018
 */
@Component
public class MyCompleteSchedulesWidgetConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.MY_COMPLETED_SCHEDULES);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<MyCompletedSchedulesDto> workOrders = (List<MyCompletedSchedulesDto>) objects[0];
        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        List<MCSTableRow> tableRows = new ArrayList<>();

        tableWidgetDM.setTitle("My Complete Schedules");
        tableWidgetDM.setWidgetId("myCompleteSchedules");

        for (MyCompletedSchedulesDto wo : workOrders){
            MCSTableRow tableRow = new MCSTableRow();
            tableRow.setC1(wo.getClient());
            tableRow.setC2(wo.getCompletedSchedules().toString());
            tableRows.add(tableRow);
        }
        List<String> tableHeaders = Arrays.asList("Client", "Completed Schedules");
        tableWidgetDM.setTableHeaders(tableHeaders);
        tableWidgetDM.setRows(tableRows);
        return tableWidgetDM;
    }
}
