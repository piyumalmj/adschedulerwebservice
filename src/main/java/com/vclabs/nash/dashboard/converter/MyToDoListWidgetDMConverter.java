package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.list_widget.ListItem;
import com.vclabs.dashboard.core.data.model.list_widget.ListWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.model.entity.WorkOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-11-07.
 */
@Component
public class MyToDoListWidgetDMConverter extends WidgetDMConverter {

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.MY_TO_DO_LIST);
    }

    @Override
    public ListWidgetDM convert(Object... objects) {
        ListWidgetDM dataModel = new ListWidgetDM();
        List<WorkOrder> workOrders = (List<WorkOrder>) objects[0];
        List<ListItem> listItems = new ArrayList<>();
        for (WorkOrder workOrder : workOrders) {
            ListItem listItem = new ListItem();
            listItem.setId(workOrder.getWorkorderid());
            listItem.setText(workOrder.getOrdername());
            listItem.setUrl(UPDATE_WO_URL+workOrder.getWorkorderid());
            listItems.add(listItem);
        }

      //  dataModel.setCount(workOrders.size());
        dataModel.setWidgetId("myToDoList");
        dataModel.setTitle("My To Do List");
        dataModel.setSubTitle("My Pending Work Orders");
        dataModel.setList(listItems);
        return dataModel;
    }
}