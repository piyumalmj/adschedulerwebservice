package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.SCTableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.model.entity.ServerDetails;
import com.vclabs.nash.service.ChannelDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-10.
 */
@Component
public class ServerControlWidgetConverter extends WidgetDMConverter {

    @Autowired
    ChannelDetailService channelDetailService;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.SERVER_CONTROL);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<ServerDetails> serverDetailsList = (List<ServerDetails>) objects[0];
        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        tableWidgetDM.setTitle("Server Control");
        tableWidgetDM.setWidgetId("serverControl");

        List<SCTableRow> scTableRowsList = new ArrayList<>();
        for (ServerDetails serverDetails1 : serverDetailsList) {
            if(serverDetails1.getChannelDetails()!=null) {
                SCTableRow scTableRow = new SCTableRow();
                scTableRow.setId(String.valueOf(serverDetails1.getServerId()));
                scTableRow.setC1(channelDetailService.getChannel(serverDetails1.getChannelDetails()).getChannelname());
                scTableRow.setC2("" + serverDetails1.getCardPort());
                if (serverDetails1.getStatus() == 0) {
                    scTableRow.setBgColor("#EA5E56");
                } else {
                    scTableRow.setBgColor("#93CB50");
                }
                scTableRowsList.add(scTableRow);
            }
        }

        List<String> tableHeaders = Arrays.asList("Channel Name", "Port No");
        tableWidgetDM.setTableHeaders(tableHeaders);
        tableWidgetDM.setRows(scTableRowsList);
        return tableWidgetDM;
    }
}
