package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.hbar_pie_widget.BarChartData;
import com.vclabs.dashboard.core.data.model.hbar_pie_widget.DataRow;
import com.vclabs.dashboard.core.data.model.hbar_pie_widget.HBarPieChartWidgetDM;
import com.vclabs.dashboard.core.data.model.hbar_pie_widget.PieChartData;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.RevenueLineClientDTO;
import com.vclabs.nash.dashboard.dto.RevenueLinePieChartDTO;
import com.vclabs.nash.dashboard.dto.RevenueLineUserDTO;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by Nalaka on 2019-01-17.
 */
@Component
public class RevenueLineDashboardConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.REVENUE_LINE_DASHBOARD);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        long totalRevenue = 0;
        List<RevenueLineClientDTO> clientDTOList = (List<RevenueLineClientDTO>) objects[0];
        HashMap<String, RevenueLinePieChartDTO> pieChartDataList = (HashMap<String, RevenueLinePieChartDTO>) objects[1];
        HBarPieChartWidgetDM widget = new HBarPieChartWidgetDM();
        widget.setTitle("Revenue Line Dashboard");
        widget.setWidgetId("revenueLineDashboard");
        widget.setBottomText("TOTAL REVENUE ");
        widget.setUnitText("LKR");

        List<String> labels = new ArrayList<>();//Arrays.asList("Sports Events", "Adhub", "General");
        List<String> backgroundColor = Arrays.asList("#D766A8", "#E2E220", "#E1A5CB","#D766A8", "#E2E220", "#E1A5CB","#D766A8", "#E2E220", "#E1A5CB");
        List<Double> data = new ArrayList<>();// Arrays.asList(50.0, 170.0, 33.0);

        Iterator iterator = pieChartDataList.entrySet().iterator();
        labels.clear();
        data.clear();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry)iterator.next();
            RevenueLinePieChartDTO pieChartData = (RevenueLinePieChartDTO)pair.getValue();
            labels.add(pieChartData.getName());
            data.add(pieChartData.getPercentage());
        }

        PieChartData pieChartData = new PieChartData();
        pieChartData.setId(1);
        pieChartData.setLabels(labels);
        pieChartData.setBackgroundColor(backgroundColor);
        pieChartData.setData(data);

        List<BarChartData> barChartDataList = new ArrayList<>();

        for (RevenueLineClientDTO revenueLineClientDTO : clientDTOList) {
            BarChartData barChartData = new BarChartData();
            barChartData.setId(revenueLineClientDTO.getId());
            barChartData.setName(revenueLineClientDTO.getName());
            barChartData.setPercentage(revenueLineClientDTO.getPercentage());
            barChartData.setValue(revenueLineClientDTO.getValue());
            totalRevenue += revenueLineClientDTO.getValue();
            //set color
            List<DataRow> dataRows = new ArrayList();

            for (RevenueLineUserDTO revenueLineUserDTO : revenueLineClientDTO.getUserList()) {
                DataRow dataRow = new DataRow();
                dataRow.setName(revenueLineUserDTO.getName());
                dataRow.setPercentage(revenueLineUserDTO.getPercentage());
                dataRows.add(dataRow);
            }
            barChartData.setDataRows(dataRows);
            barChartDataList.add(barChartData);
        }
        pieChartData.setTotal(totalRevenue);
        widget.setBarChartData(barChartDataList);
        widget.setPieChartData(pieChartData);
        return widget;
    }
}
