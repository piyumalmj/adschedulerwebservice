package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
//import com.vclabs.dashboard.core.data.model.hbar_widget.HBar;
import com.vclabs.dashboard.core.data.model.hbar_widget.*;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.ChannelUtillizationDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-11-21.
 */
@Component
public class ChannelUtilizationConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.CHANNEL_UTILIZATION);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        HBarChartWidgetDM hBarChartWidgetDM = new HBarChartWidgetDM();
        List<ChannelUtillizationDto> channelUtillizationDtoList = (List<ChannelUtillizationDto>) objects[0];
        List<HBarRow> chartData = new ArrayList<>();
        hBarChartWidgetDM.setTitle("Channel Utilization");
        hBarChartWidgetDM.setWidgetId("channelUtilization");

        for (ChannelUtillizationDto channelUtillizationDto : channelUtillizationDtoList) {
            HBarRow hBar = new HBarRow();
            hBar.setId(channelUtillizationDto.getChannel().getChannelid());
            hBar.setName(channelUtillizationDto.getChannel().getChannelname());

            List<HBarColumn> hBarColumnList = new ArrayList<>();

            HBarColumn hBarColumnOne = new HBarColumn();
            hBarColumnOne.setColor("#4FCDE8");
            hBarColumnOne.setValue(channelUtillizationDto.getSpentTimePercentage());
            hBarColumnList.add(hBarColumnOne);

            HBarColumn hBarColumnTwo = new HBarColumn();
            hBarColumnTwo.setColor("#B8EA72");
            hBarColumnTwo.setValue(channelUtillizationDto.getRemainingTimePercentage());
            hBarColumnList.add(hBarColumnTwo);

            hBar.setHbar(hBarColumnList);

            chartData.add(hBar);
        }
        hBarChartWidgetDM.setChartData(chartData);
        HBarChartConfig config = new HBarChartConfig();
        List<LegendItem> itemsList = new ArrayList<>();

        LegendItem itemOne = new LegendItem();
        itemOne.setColor("#4FCDE8");
        itemOne.setLabel("Used");
        itemsList.add(itemOne);

        LegendItem itemTwo = new LegendItem();
        itemTwo.setColor("#B8EA72");
        itemTwo.setLabel("Available");
        itemsList.add(itemTwo);

        config.setLegend(itemsList);

        hBarChartWidgetDM.setChartConfig(config);
        return hBarChartWidgetDM;
    }
}
