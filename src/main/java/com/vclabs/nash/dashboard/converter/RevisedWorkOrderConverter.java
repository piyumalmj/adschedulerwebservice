package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.Url;
import com.vclabs.dashboard.core.data.model.table_widget.TableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.WOTableRow;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.ClientDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 27/11/2018
 */
@Component
public class RevisedWorkOrderConverter extends WidgetDMConverter{

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    @Autowired
    private ClientDetailService clientDetailService;

    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDAO;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.REVISED_WORK_ORDER);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        List<WorkOrder> newWorkOrderList = (List<WorkOrder>) objects[0];
        List<WOTableRow> tableRows = new ArrayList<>();
        tableWidgetDM.setTitle("Revised WO");
        tableWidgetDM.setWidgetId("Revised WO");

        for (WorkOrder newWorkOrder : newWorkOrderList) {
            WOTableRow tableRow = new WOTableRow();
            tableRow.setId(newWorkOrder.getWorkorderid().toString());
            int spots = workOrderChannelListDAO.findSpotCountByWorkOrder(newWorkOrder.getWorkorderid());
            Url url = new Url();
            url.setUrl(UPDATE_WO_URL + newWorkOrder.getWorkorderid().toString());
            url.setLabel("#"+newWorkOrder.getWorkorderid().toString());
            tableRow.setC1(url);
            tableRow.setId(newWorkOrder.getWorkorderid().toString());
            tableRow.setC2(clientDetailService.getSelectedClient(newWorkOrder.getClient()).getClientname());
            tableRow.setC3(String.valueOf(spots));
            tableRows.add(tableRow);
        }
        tableWidgetDM.setRows(tableRows);

        return tableWidgetDM;
    }
}
