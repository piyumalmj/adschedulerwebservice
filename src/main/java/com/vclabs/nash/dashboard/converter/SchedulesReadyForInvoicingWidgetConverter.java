package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.Url;
import com.vclabs.dashboard.core.data.model.table_widget.SRITableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.SchedulesReadyForInvoicingDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sanduni on 04/12/2018
 */
@Component
public class SchedulesReadyForInvoicingWidgetConverter extends WidgetDMConverter{

    @Value("${nash.url.update.schedule}")
    private String UPDATE_SCHEDULE_URL;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.SCHEDULES_READY_FOR_INVOICING);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<SchedulesReadyForInvoicingDto> workOrders = (List<SchedulesReadyForInvoicingDto>) objects[0];

        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        List<SRITableRow> tableRows = new ArrayList<>();
        tableWidgetDM.setWidgetId("schedulesReadyForInvoicing");
        tableWidgetDM.setTitle("Schedules Ready For Invoicing");

        for (SchedulesReadyForInvoicingDto wo : workOrders){
            SRITableRow tableRow = new SRITableRow();
            if(wo.getWoId()!=null) {
                tableRow.setId(wo.getWoId().toString());
            }else {
                tableRow.setId("-999");
            }
            Url url = new Url();
            url.setUrl( UPDATE_SCHEDULE_URL );
            if(wo.getWoId()!=null) {
                url.setLabel(wo.getWoId().toString());
            }else{
                url.setLabel("-999");
            }
            tableRow.setC1(url);
            tableRow.setC2(wo.getClient());
            tableRow.setC3(new SimpleDateFormat("yy-MM-dd").format(wo.getEndDate()).toString());
            tableRow.setC4(wo.getSeller());

            tableRows.add(tableRow);
        }
        List<String> tableHeaders = Arrays.asList("Schedule No", "Client", "End Date", "ME", "");
        tableWidgetDM.setTableHeaders(tableHeaders);
        tableWidgetDM.setRows(tableRows);
        return tableWidgetDM;
    }
}
