package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.TableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.model.view.reporting.DummyCutView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Nalaka on 2018-12-21.
 */
@Component
public class DummyCutListWidgetConverter extends WidgetDMConverter {

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    @Value("${dashboard.record.limit}")
    private int RECORD_LIMIT;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.DUMMY_CUT_LIST);
    }

    @Override
    public BasicWidgetDM convert(Object... parameters) {

        List<DummyCutView> dummyCutViewList = (List<DummyCutView>) parameters[0];

        Collections.sort(dummyCutViewList, new Comparator<DummyCutView>() {
            @Override
            public int compare(DummyCutView o1, DummyCutView o2) {
                return o2.getCutID()-o1.getCutID();
            }
        });

        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        tableWidgetDM.setTitle("Dummy Cut");
        tableWidgetDM.setWidgetId("dummyCutList");
        List<String> tableHeaders = new ArrayList<>();
        tableHeaders.add("Cut#");
        tableHeaders.add("Client");
        tableHeaders.add("Date");
        tableHeaders.add("Spots");
        tableHeaders.add("ME");
        tableWidgetDM.setTableHeaders(tableHeaders);

        List<HashMap> tableRows = new ArrayList<>();
        int count=0;
        for (DummyCutView dummyCutView : dummyCutViewList) {
            if(count<RECORD_LIMIT) {
                HashMap row = new HashMap();
                row.put("id", dummyCutView.getCutID() + "");

                HashMap firstrow = new HashMap();
                firstrow.put("label", dummyCutView.getCutID());
                firstrow.put("url", UPDATE_WO_URL + dummyCutView.getWoId());
                row.put("c1", firstrow);
                row.put("c2", dummyCutView.getClientName());
                row.put("c3", dummyCutView.getAiringDateRange());
                row.put("c4", String.valueOf(dummyCutView.getNumberOfSpot()));
                row.put("c5", dummyCutView.getMe());
                tableRows.add(row);
                count++;
            }else {
                break;
            }
        }
        tableWidgetDM.setRows(tableRows);
        return tableWidgetDM;
    }
}
