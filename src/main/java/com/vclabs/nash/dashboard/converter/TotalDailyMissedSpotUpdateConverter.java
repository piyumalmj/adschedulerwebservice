package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.TextLine;
import com.vclabs.dashboard.core.data.model.donut_widget.multi_chart.ChartGroup;
import com.vclabs.dashboard.core.data.model.donut_widget.multi_chart.DonutChart;
import com.vclabs.dashboard.core.data.model.donut_widget.single_chart.SingleDonutChartWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.DailyMissedSpotsDto;
import com.vclabs.nash.model.entity.ScheduleDef;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 31/12/2018
 */
@Component
public class TotalDailyMissedSpotUpdateConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.TOTAL_DAILY_MISSED_SPOT_UPDATE);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        int missedSpots = (int) objects[0];
        SingleDonutChartWidgetDM singleDonutChartWidgetDM = new SingleDonutChartWidgetDM();
        DonutChart chart = new DonutChart();
        TextLine centerText = new TextLine();
        centerText.setLabel("Total");
        centerText.setValue(String.valueOf(missedSpots));


        List<ChartGroup> chartGroups = new ArrayList<>();
        ChartGroup group1 = new ChartGroup();
        group1.setBgColor("#A3E25F");
        group1.setType("Schedule Spots");
        group1.setValue(100 - missedSpots);
        chartGroups.add(group1);

        ChartGroup group2 = new ChartGroup();
        group2.setBgColor("#E56051");
        group2.setType("Missed Spots");
        group2.setValue(missedSpots);
        chartGroups.add(group2);

        chart.setGroups(chartGroups);

        singleDonutChartWidgetDM.setCenterText(centerText);
        singleDonutChartWidgetDM.setChart(chart);
        singleDonutChartWidgetDM.setTitle("Daily Missed Spot Update");
        singleDonutChartWidgetDM.setWidgetId("dailyMissedSpotUpdate");
        return singleDonutChartWidgetDM;
    }
}
