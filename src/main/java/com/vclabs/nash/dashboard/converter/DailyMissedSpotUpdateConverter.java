package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.donut_widget.multi_chart.*;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.DailyMissedSpotDTO;
import com.vclabs.nash.model.entity.ScheduleDef;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Nalaka on 2018-12-28.
 */
@Component
public class DailyMissedSpotUpdateConverter  extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.DAILY_MISSED_SPOT_UPDATE);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        HashMap map = (HashMap) objects[0];

        DMSWidgetDM dmsWidgetDM = new DMSWidgetDM();
        dmsWidgetDM.setTitle("Daily Missed Spot Update");
        dmsWidgetDM.setWidgetId("dailyMissedSpotUpdate");

        List<DonutChart> charts = new ArrayList<>();

        for (Object dailyMissedSpotObject : map.values()) {
            DailyMissedSpotDTO dailyMissedSpotDTO = (DailyMissedSpotDTO) dailyMissedSpotObject;

            DonutChart donutChart = new DonutChart();
            donutChart.setId(dailyMissedSpotDTO.getChannelId());
            donutChart.setName(dailyMissedSpotDTO.getChannelName());

            List<ChartGroup> groups = new ArrayList<>();

            ChartGroup scheduleGroup = new ChartGroup();
            scheduleGroup.setBgColor("#A3E25F");
            scheduleGroup.setValue(dailyMissedSpotDTO.getScheduledSpotPresentage());
            scheduleGroup.setType("Schedule Spots");

            List<DetailRow> scheduleDetails = new ArrayList<>();

            for (ScheduleDef scheduleDef : dailyMissedSpotDTO.getScheduleSpotList()) {
                DetailRow detailRow = new DetailRow();
                detailRow.setC1(scheduleDef.getActualstarttime().toString());
                detailRow.setC2(scheduleDef.getActualendtime().toString());
                detailRow.setC3(scheduleDef.getAdvertid().getAdvertname());
                detailRow.setC4("No comment");
                detailRow.setC5(scheduleDef.getAdvertid().getAdverttype());
                scheduleDetails.add(detailRow);
            }

            scheduleGroup.setDetails(scheduleDetails);
            groups.add(scheduleGroup);

            ChartGroup missedGroup = new ChartGroup();
            missedGroup.setBgColor("#E56051");
            missedGroup.setValue(dailyMissedSpotDTO.getMissedSpotPresentage());
            missedGroup.setType("Missed Spots");

            List<DetailRow> missedDetails = new ArrayList<>();

            for (ScheduleDef scheduleDef : dailyMissedSpotDTO.getMissedSpotList()) {
                DetailRow detailRow = new DetailRow();
                detailRow.setC1(scheduleDef.getActualstarttime().toString());
                detailRow.setC2(scheduleDef.getActualendtime().toString());
                detailRow.setC3(scheduleDef.getAdvertid().getAdvertname());
                detailRow.setC4("No comment");
                detailRow.setC5(scheduleDef.getAdvertid().getAdverttype());
                missedDetails.add(detailRow);
            }
            missedGroup.setDetails(missedDetails);
            groups.add(missedGroup);
            donutChart.setGroups(groups);
            charts.add(donutChart);
        }

        List<String> tableHeaders = new ArrayList<>();
        tableHeaders.add("Time");
        tableHeaders.add("Name");
        tableHeaders.add("Comment");

        ModalConfig config = new ModalConfig();
        config.setTableHeaders(tableHeaders);
        config.setDdAllOption("All Types ...");

        dmsWidgetDM.setCharts(charts);
        dmsWidgetDM.setModalConfig(config);
        return dmsWidgetDM;
    }
}
