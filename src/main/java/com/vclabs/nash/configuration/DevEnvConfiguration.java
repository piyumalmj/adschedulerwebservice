package com.vclabs.nash.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by dperera on 13/11/2017.
 */

@Configuration
@Profile("dev")
@PropertySource({ "classpath:alert.properties", "classpath:application.properties", "classpath:filePath.properties", "classpath:ops-url.properties", "classpath:schedule.properties" })
public class DevEnvConfiguration {


}
