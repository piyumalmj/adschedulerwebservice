package com.vclabs.nash.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.NestedServletException;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by dperera on 19/10/2017.
 */
public class ServletErrorFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServletErrorFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            chain.doFilter(request, response);
        } catch (NestedServletException ex) {
            //no need to log or handle
        }
    }

    @Override
    public void destroy() {

    }
}
