package com.vclabs.nash.configuration;

import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by dperera on 15/05/2018.
 */
public class Sha1PasswordEncoder implements PasswordEncoder {

    private MessageDigestPasswordEncoder shaEncoder;

    public Sha1PasswordEncoder(MessageDigestPasswordEncoder shaEncoder) {
        this.shaEncoder = shaEncoder;
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return shaEncoder.encodePassword(rawPassword.toString(), null);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return shaEncoder.isPasswordValid(encodedPassword, rawPassword.toString(), null);
    }
}
