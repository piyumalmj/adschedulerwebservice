package com.vclabs.nash.model.daoimpl.inventoryprediction;

import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionDailyDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
@Repository
public class InventoryPredictionDailyDAOImpl extends VCLSessionFactory implements InventoryPredictionDailyDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryPredictionDailyDAOImpl.class);

    @Override
    public InventoryDailyPrediction save(InventoryDailyPrediction inventoryDailyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.saveOrUpdate(inventoryDailyPrediction);
            LOGGER.debug("Successfully saved/updated InventoryDailyPrediction");
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryDailyPrediction;
    }

    @Override
    public List<InventoryDailyPrediction> save(List<InventoryDailyPrediction> inventoryDailyPredictions){
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.save(inventoryDailyPredictions);
            LOGGER.debug("Successfully saved/updated InventoryDailyPrediction");
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryDailyPredictions;
    }

    @Override
    public InventoryDailyPrediction update(InventoryDailyPrediction inventoryDailyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.update(inventoryDailyPrediction);
            LOGGER.debug("Successfully updated InventoryDailyPrediction");
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryDailyPrediction;
    }

    @Override
    public List<InventoryDailyPrediction> findAll(){
        Session session = this.getSessionFactory().getCurrentSession();
        List<InventoryDailyPrediction> inventoryDailyPredictions = session.createCriteria(InventoryDailyPrediction.class).list();
        return inventoryDailyPredictions;
    }

    @Override
    public List<InventoryDailyPrediction> findLatestPredictions(Date startingDate, Date lastDate) {
        String sql = "select * from inventory_daily_prediction idp where idp.dateAndTime between ?1 and ?2";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryDailyPrediction.class);
        query.setParameter(1, startingDate);
        query.setParameter(2, lastDate);
        List<InventoryDailyPrediction> result = query.getResultList();
        return result;
    }

    @Override
    public InventoryDailyPrediction findById(long id){
        Session session = this.getSessionFactory().getCurrentSession();
        InventoryDailyPrediction dailyPrediction = session.find(InventoryDailyPrediction.class, id);
        return  dailyPrediction;
    }

    @Override
    public List<InventoryDailyPrediction> findUnProcessedPredictions() {
        String sql = "select idp.* from inventory_daily_prediction idp left join prediction_result_meta prm on idp.predictionResultMetaId_id = prm.id " +
                "where prm.isProcess = 0";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryDailyPrediction.class);
        List<InventoryDailyPrediction> result = query.getResultList();
        return result;
    }

}
