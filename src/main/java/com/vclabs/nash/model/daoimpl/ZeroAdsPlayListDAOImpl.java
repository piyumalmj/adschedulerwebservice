package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.ZeroAdsPlayListDAO;
import com.vclabs.nash.model.entity.ZeroAdsPlayList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-18.
 */
@Repository
public class ZeroAdsPlayListDAOImpl extends VCLSessionFactory implements ZeroAdsPlayListDAO {

    @Override
    public Boolean saveZeroAdsPlayList(ZeroAdsPlayList model) {

        Session session = this.getSessionFactory().getCurrentSession();
        session.save(model);
        return true;

    }

    @Override
    public Boolean updateZeroAdsPlayList(ZeroAdsPlayList model) {

        Session session = this.getSessionFactory().getCurrentSession();
        session.update(model);
        return true;

    }

    @Override
    public List<ZeroAdsPlayList> getPlayListByChannel(int channelId) {

        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(ZeroAdsPlayList.class);
        criteria.add(Restrictions.eq("channelid.channelid", channelId));
        criteria.add(Restrictions.eq("advetStatus", ZeroAdsPlayList.Status.ACTIVATE));
        criteria.createAlias("advertid", "advert");
        criteria.addOrder(Order.asc("orderNumber"));
        ArrayList<ZeroAdsPlayList> zeroAdsPlayList = (ArrayList<ZeroAdsPlayList>) criteria.list();

        return zeroAdsPlayList;
    }

    @Override
    public List<ZeroAdsPlayList> getPlayListByAdvert(int advertId) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(ZeroAdsPlayList.class);
        criteria.add(Restrictions.eq("advertid.advertid", advertId));
        criteria.add(Restrictions.eq("advetStatus", ZeroAdsPlayList.Status.ACTIVATE));
        criteria.createAlias("advertid", "advert");
        criteria.addOrder(Order.asc("orderNumber"));
        ArrayList<ZeroAdsPlayList> zeroAdsPlayList = (ArrayList<ZeroAdsPlayList>) criteria.list();

        return zeroAdsPlayList;
    }

    @Override
    public List<ZeroAdsPlayList> getValidManualPlayListByChannel(int channelId) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(ZeroAdsPlayList.class);
        criteria.add(Restrictions.eq("channelid.channelid", channelId));
        criteria.add(Restrictions.eq("advetStatus", ZeroAdsPlayList.Status.ACTIVATE));
        criteria.createAlias("advertid", "advert");
        criteria.add(Restrictions.eq("advert.status", 1));
        criteria.addOrder(Order.asc("orderNumber"));
        ArrayList<ZeroAdsPlayList> zeroAdsPlayList = (ArrayList<ZeroAdsPlayList>) criteria.list();

        return zeroAdsPlayList;
    }
}
