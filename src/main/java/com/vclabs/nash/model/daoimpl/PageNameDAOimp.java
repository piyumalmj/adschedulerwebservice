/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.PageNameDAO;
import com.vclabs.nash.model.entity.PageName;

/**
 *
 * @author user
 */
@Repository
public class PageNameDAOimp extends VCLSessionFactory implements PageNameDAO {

    private static final Logger logger = LoggerFactory.getLogger(PageNameDAOimp.class);

    @Override
    public List<PageName> getAllPages() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            List<PageName> pageNameList = null;
            Criteria criteria = session.createCriteria(PageName.class);
            pageNameList = (ArrayList<PageName>) criteria.list();
            return pageNameList;
        } catch (Exception e) {
            logger.debug("Exception in PageNameDAOimp in getAllPages() : {}", e.getMessage());
            throw e;
        }
    }

}
