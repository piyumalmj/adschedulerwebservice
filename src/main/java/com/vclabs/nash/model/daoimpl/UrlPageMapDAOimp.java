/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.UrlPageMapDAO;
import com.vclabs.nash.model.entity.UrlPageMap;

/**
 *
 * @author user
 */
@Repository
public class UrlPageMapDAOimp extends VCLSessionFactory implements UrlPageMapDAO {

    @Override
    public List<UrlPageMap> getSeletedPageURL(int pageID,String acce) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();
            
            Criteria criteria = session.createCriteria(UrlPageMap.class);
            criteria.add(Restrictions.eq("pageId.pageId", pageID));
            criteria.add(Restrictions.eq("accpermission", acce));
            List<UrlPageMap> urlPagMapList = (ArrayList<UrlPageMap>) criteria.list();
            return urlPagMapList;

        } catch (Exception e) {
            throw e;
        } finally {
            session.close();
        }
    }

}
