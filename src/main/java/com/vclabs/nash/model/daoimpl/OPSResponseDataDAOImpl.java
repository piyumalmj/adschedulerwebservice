package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.OPSResponseDataDAO;
import com.vclabs.nash.model.entity.OPSResponseData;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created by Nalaka on 2018-09-25.
 */
@Repository
public class OPSResponseDataDAOImpl extends VCLSessionFactory implements OPSResponseDataDAO {

    @Override
    public OPSResponseData save(OPSResponseData oPSResponseData) {

        Session session = this.getSessionFactory().getCurrentSession();
        session.save(oPSResponseData);
        return oPSResponseData;
    }

    @Override
    public OPSResponseData findById(long id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(OPSResponseData.class);
        criteria.add(Restrictions.eq("id", id));
        OPSResponseData oPSResponseData = (OPSResponseData) criteria.uniqueResult();
        return oPSResponseData;
    }

    @Override
    public OPSResponseData update(OPSResponseData oPSResponseData) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(oPSResponseData);
        return oPSResponseData;
    }

}