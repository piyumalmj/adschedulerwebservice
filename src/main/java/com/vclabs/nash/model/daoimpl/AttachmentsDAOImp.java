/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.AttachmentsDAO;
import com.vclabs.nash.model.entity.Attachments;

/**
 *
 * @author Sanira Nanayakkara
 */
@Repository
public class AttachmentsDAOImp extends VCLSessionFactory implements AttachmentsDAO{

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentsDAOImp.class);

    @Override
    public int insertAttachment(Attachments attachement) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();

            Transaction tx = session.beginTransaction();
            session.save(attachement);
            session.flush();
            tx.commit();

            return attachement.getAttachmentsId();

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp while trying to insertAttachment", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public boolean updateAttachment(Attachments attachement) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();

            Transaction tx = session.beginTransaction();
            session.saveOrUpdate(attachement);
            session.flush();
            tx.commit();

            return true;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp in updateAttachment : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public boolean desableAttachment(Attachments attachement) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            
            attachement.setEnabled(false);
            session.saveOrUpdate(attachement);
            session.flush();
            tx.commit();

            return true;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp while trying to desableAttachment", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public boolean removeAttachment(Attachments attachement) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();

            Transaction tx = session.beginTransaction();
            session.delete(attachement);
            session.flush();
            tx.commit();

            return true;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp in removeAttachment : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Attachments getAttachmentbyId(int id) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();

            Attachments attachement = null;

            Criteria criteria = session.createCriteria(Attachments.class);
            criteria.add(Restrictions.eq("attachmentsId", id));
            Object obj = criteria.uniqueResult();
            
            if(obj != null)
                attachement = (Attachments)obj; 
            return attachement;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp while trying to getAttachmentbyId", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Attachments> getAllAttachmentList() {
        Session session = null;
        try {
            session = getSessionFactory().openSession();

            List<Attachments> attachement = null;

            Criteria criteria = session.createCriteria(Attachments.class);
            Object obj = criteria.list();
            
            if(obj != null)
                attachement = (List<Attachments>)obj; 
            return attachement;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp in getAllAttachmentList : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Attachments> getAttachmentList(String table, int id) {
        try {
            Session session = getSessionFactory().getCurrentSession();
            List<Attachments> attachement = null;
            Criteria criteria = session.createCriteria(Attachments.class);
            criteria.add(Restrictions.eq("referenceTable", table));
            criteria.add(Restrictions.eq("referenceId", id));
            criteria.add(Restrictions.eq("enabled", true));
            Object obj = criteria.list();
            
            if(obj != null)
                attachement = (List<Attachments>)obj; 
            return attachement;

        } catch (Exception e) {
            LOGGER.debug("Exception in AttachmentsDAOImp while trying to getAttachmentList", e.getMessage());
            throw e;
        }
    }

}
