/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.RemainingSpotBankDAO;
import com.vclabs.nash.model.entity.RemainingSpotBank;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author user
 */
@Repository
public class RemainingSpotBankDAOImp extends VCLSessionFactory implements RemainingSpotBankDAO {

    private static final Logger logger = LoggerFactory.getLogger(RemainingSpotBankDAOImp.class);

    @Override
    @Transactional
    public Boolean saveORUpdateRemainingSpot(RemainingSpotBank model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in RemainingSpotBankDAOImp in saveORUpdateRemainingSpot() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public RemainingSpotBank getSetectedRemainingSpot(int workOrderID, int advertID) {
        RemainingSpotBank spotBank = null;
        try {
            Session session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(RemainingSpotBank.class);
            criteria.add(Restrictions.eq("advertid.advertid", advertID));
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderID));
            spotBank = (RemainingSpotBank) criteria.uniqueResult();
            session.close();
        } catch (Exception e) {
            logger.debug("Exception in RemainingSpotBankDAOImp in getSetectedRemainingSpot() : {}", e.getMessage());
            throw e;
        }
        return spotBank;
    }
}
