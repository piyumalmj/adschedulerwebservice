/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.List;

import com.vclabs.nash.model.entity.SecurityUsersEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.AuthorityDAO;
import com.vclabs.nash.model.entity.SecurityAuthoritiesEntity;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class AuthorityDAOimp extends VCLSessionFactory implements AuthorityDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityDAOimp.class);

    @Override
    public List<SecurityAuthoritiesEntity> getAuthority(String userName) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityAuthoritiesEntity.class);
            criteria.add(Restrictions.eq("username.username", userName));
            criteria.addOrder(Order.asc("authority"));
            List<SecurityAuthoritiesEntity> authorityList = (List<SecurityAuthoritiesEntity>) criteria.list();
            return authorityList;
        } catch (Exception e) {
            LOGGER.debug("Exception in AuthorityDAOimp in getAuthority : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteAuthority(SecurityAuthoritiesEntity model) {
//        try (Session session = this.getSessionFactory().openSession()) {
//            Transaction tx = session.beginTransaction();
//            session.delete(model);
//            tx.commit();
//            return true;
//        } catch (Exception e) {
//            LOGGER.debug("Exception in AuthorityDAOimp in deleteAuthority : ", e.getMessage());
//            throw e;
//        }
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in AuthorityDAOimp in deleteAuthority : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean saveAuthority(SecurityAuthoritiesEntity model) {
        try (Session session = this.getSessionFactory().openSession()){
            Transaction tx = session.beginTransaction();
            Query query = session.createSQLQuery("INSERT INTO authorities (authority, username) VALUES (:authority, :username)");
            query.setParameter("authority", model.getAuthority());
            query.setParameter("username", model.getUsername().getUsername());
            query.executeUpdate();
            tx.commit();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in AuthorityDAOimp in saveAuthority : ", e.getMessage());
            throw e;
        }
    }

}
