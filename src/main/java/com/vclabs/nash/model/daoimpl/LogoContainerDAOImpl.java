/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.LogoContainerDAO;
import com.vclabs.nash.model.entity.LogoContainer;
import com.vclabs.nash.model.entity.LogoContainerMap;

/**
 * @author Sanira Nanayakkara
 */
@Repository
public class LogoContainerDAOImpl extends VCLSessionFactory implements LogoContainerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceDAOimp.class);

    @Override
    public ArrayList<LogoContainer> getLogoContainerList() {
        ArrayList<LogoContainer> ret = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(LogoContainer.class);
            ret = (ArrayList<LogoContainer>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in getLogoContainerList : ", e.getMessage());
            throw e;
        }
        return ret;
    }

    @Override
    public Boolean updateLogoContainer(LogoContainer model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in updateLogoContainer : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean insertLogoContainer(LogoContainer model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in insertLogoContainer : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<LogoContainerMap> getLogoListbyId(int id) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(LogoContainerMap.class);
            criteria.add(Restrictions.eq("logoContainerId", id));
            return (List<LogoContainerMap>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in getLogoListbyId : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean insertLogoContainerMap(LogoContainerMap map) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(map);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in insertLogoContainerMap : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean removeLogoContainerMap(LogoContainerMap map) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(map);
            return true;
        }catch (Exception e) {
            LOGGER.debug("Exception in LogoContainerDAOImpl in removeLogoContainerMap : ", e.getMessage());
            throw e;
        }
    }

}
