/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.entity.TVCDurationSpotCount;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author user
 */
@Repository
public class WorkOrderChannelListDAOimp extends VCLSessionFactory implements WorkOrderChannelListDAO {

    private static final Logger logger = LoggerFactory.getLogger(WorkOrderChannelListDAOimp.class);

    @Override
    public List<WorkOrderChannelList> getSelectedWorkOrderSpotCount(int workOrderId, int channelId) {

        List<WorkOrderChannelList> workOrderChanneList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrderChannelList.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            workOrderChanneList = (ArrayList<WorkOrderChannelList>) criteria.list();
            return workOrderChanneList;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in getSelectedWorkOrderSpotCount:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<WorkOrderChannelList> getSelectedWorkOrderSpotCount(int workOrderId) {

        List<WorkOrderChannelList> workOrderChanneList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrderChannelList.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.createAlias("channelid", "chanId");
            criteria.addOrder(Order.asc("chanId.channelname"));
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            workOrderChanneList = (ArrayList<WorkOrderChannelList>) criteria.list();
            return workOrderChanneList;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in getSelectedWorkOrderSpotCount:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getSelectedOrderChannelCount(int workOrderId) {
        try {
            long channelCount = 0;
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrderChannelList.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.setProjection(Projections.rowCount());
            channelCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + channelCount);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in getSelectedOrderChannelCount:{}", e.getMessage());
            throw e;
        }
    }

    //WorkOrderChannelList method
    @Override
    public WorkOrderChannelList getSelectedOrderedChannelListDetail(int Orderedchannellistid) {
        WorkOrderChannelList orderChannel = null;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(WorkOrderChannelList.class);
            criteria.add(Restrictions.eq("orderedchannellistid", Orderedchannellistid));
            orderChannel = (WorkOrderChannelList) criteria.uniqueResult();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in getSelectedOrderedChannelListDetail:{}", e.getMessage());
            throw e;
        }finally {
            if(session != null){
                session.close();
            }
        }
        return orderChannel;
    }

    @Override
    public Boolean setWorkOrderChannelList(WorkOrderChannelList model) {
        try {
            //logger.info("INSERT INTO `vcl_schedule_db`.`work_order_channel_list` (`Work_order_id`, `Channel_id`, `Numbert_of_spot`, `Bonus_spot`, `Tvc_package_value`, `Crowler_b_spots`, `Crowler_spots`, `Crowler_package_value`, `Logo_b_spots`, `Logo_spots`, `Logo_package_value`, `L_sqeeze_b_spots`, `L_sqeeze_spots`, `L_sqeez_package_value`, `V_sqeeze_b_spots`, `V_sqeez_spots`, `V_sqeez_package_value`, `Package_value`, `Avarage_package_value`) VALUES (" + model.getWorkorderid().getWorkorderid() + ", " + model.getChannelid().getChannelid() + ", " + model.getNumbertofspot() + ", " + model.getBonusspot() + ", " + model.getTvcpackagevalue() + ", " + model.getCrowlerbspots() + ", " + model.getCrowlerspots() + ", " + model.getCrowlerpackagevalue() + ", " + model.getLogobspots() + ", " + model.getLogospots() + ", " + model.getLogopackagevalue() + ", " + model.getLsqeezebspots() + ", " + model.getLsqeezespots() + ", " + model.getLsqeezpackagevalue() + ", " + model.getVsqeezebspots() + ", " + model.getVsqeezspots() + ", " + model.getVsqeezpackagevalue() + ", " + model.getPackagevalue() + ", " + model.getAvaragepackagevalue() + ");");
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in setWorkOrderChannelList:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updateWorkOrderChannelList(WorkOrderChannelList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in updateWorkOrderChannelList:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteWorkOrderChannelList(WorkOrderChannelList model) {
        try {
            //logger.info("DELETE FROM `vcl_schedule_db`.`work_order_channel_list` WHERE `Ordered_channel_list_id`=" + model.getOrderedchannellistid() + ");");
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in deleteWorkOrderChannelList:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean saveTVCDurationSpotCount(TVCDurationSpotCount spotCount) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(spotCount);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in saveTVCDurationSpotCount:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updateTVCDurationSpotCount(TVCDurationSpotCount spotCount) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(spotCount);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in updateTVCDurationSpotCount:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteTVCDurationSpotCount(TVCDurationSpotCount spotCount) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(spotCount);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in deleteTVCDurationSpotCount:{}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<WorkOrderChannelList> getSelectedChannelWODetails(int channelId) {
        List<WorkOrderChannelList> orderChannel = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrderChannelList.class);
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            orderChannel = (List<WorkOrderChannelList>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderChannelListDAOimp in getSelectedChannelWODetails:{}", e.getMessage());
            throw e;
        }
        return orderChannel;
    }

    @Override
    @Transactional
    public int findSpotCountByWorkOrder(int workOrderId){
        String sql = String.format("select Numbert_of_spot from work_order_channel_list where Work_order_id = %s", "" + workOrderId);
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql);
        List<Integer> spots = (query.getResultList());
        return spots.stream().mapToInt(Integer::intValue).sum();
    }
}
