package com.vclabs.nash.model.daoimpl.inventory;

import com.vclabs.nash.model.dao.inventory.InventoryDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryPriceRange;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nalaka on 2018-09-27.
 */
@Repository
public class InventoryDAOImpl extends VCLSessionFactory implements InventoryDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryDAOImpl.class);

    @Override
    @Transactional
    public Inventory save(Inventory inventory) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(inventory);
        return inventory;
    }

    @Override
    public InventoryRow save(InventoryRow inventoryRow) {
        Session session = this.getSessionFactory().getCurrentSession();
        try{
            session.saveOrUpdate(inventoryRow);
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryRow;
    }

    @Override
    @Transactional
    public InventoryPriceRange save(InventoryPriceRange inventoryPriceRange) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(inventoryPriceRange);
        return inventoryPriceRange;
    }

    @Override
    @Transactional
    public Inventory findByChannelId(int channelId) {

        String sql = "select i from Inventory i where i.channel.id = :cid and i.date is null";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(sql, Inventory.class);
        query.setParameter("cid", channelId);
        Optional<Inventory> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    @Transactional
    public Inventory findByChannelIdAndDate(int channelId, Date date) {

        String sql = "select inv from Inventory inv where inv.channel.channelid = :cid and inv.date = :idate";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(sql, Inventory.class);
        query.setParameter("cid", channelId);
        query.setParameter("idate", date);
        Optional<Inventory> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public int isExistInventory(int channelId, Date date) {
        return 0;
    }

    @Override
    @Transactional
    public List<Date> findInventoryExistDates(int channelId, Date from, Date to) {
        String sql = "select date from inventory where inventory.channelId = ?1 and inventory.date >= ?2 and inventory.date <=?3 order by inventory.date";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        query.setParameter(1, channelId);
        query.setParameter(2, from);
        query.setParameter(3, to);
        List<Date> result = query.getResultList();
        return result.isEmpty() ?  new ArrayList<>() : result;
    }
}
