package com.vclabs.nash.model.daoimpl.product;

import com.vclabs.nash.model.dao.product.BrandDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.Attachments;
import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.entity.product.Product;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Created by dperera on 29/08/2019.
 */
@Repository
public class BrandDAOImpl extends VCLSessionFactory implements BrandDAO {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BrandDAOImpl.class);

    @Override
    public void save(Brand brand) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(brand);
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while saving brand {}", brand.getName());
        }
    }

    @Override
    public Brand findByName(String name) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select * from brand where name = ?1";
        Query query = session.createNativeQuery(sqlString, Brand.class);
        query.setParameter(1, name);
        Optional<Brand> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public List<Brand> findAll() {
        Session session = null;
        try {
            session = getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Brand.class);
            criteria.addOrder(Order.asc("name"));
            return criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while getting all brands {} ", e.getMessage());
            return null;
        }
    }
}
