package com.vclabs.nash.model.daoimpl;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package schedule.model.dao;
//
//import java.util.Date;
//import java.util.List;
//import org.hibernate.Criteria;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//import org.hibernate.criterion.Order;
//import org.hibernate.criterion.Restrictions;
//import org.springframework.stereotype.Repository;
//import com.vclabs.nash.model.entity.PriorityDef;
//
///**
// *
// * @author Sanira Nanayakkara
// */
//@Repository
//public class SchedulePriorityMapDAOImpl extends VCLSessionFactory implements SchedulePriorityMapDAO{
//
//    @Override
//    public Boolean saveSchedulePriorityMap(SchedulePriorityMap model) {
//        Session session = null;
//        try {
//            session = this.getSessionFactory().openSession();
//            Transaction tx = session.beginTransaction();
//            session.save(model);
//
//            session.flush();
//            tx.commit();
//            return true;
//
//        } catch (Exception e) {
//            throw e;
//        }
//        finally{           
//            session.close();
//        }
//    }
//
//    @Override
//    public Boolean updateSchedulePriorityMap(SchedulePriorityMap model) {
//        Session session = null;
//        try {
//            session = this.getSessionFactory().openSession();
//            Transaction tx = session.beginTransaction();
//            session.saveOrUpdate(model);
//
//            session.flush();
//            tx.commit();
//            return true;
//
//        } catch (Exception e) {
//            throw e;
//        }
//        finally{           
//            session.close();
//        }
//    }
//
//    @Override
//    public Boolean removeSchedulePriorityMap(SchedulePriorityMap model) {
//        Session session = null;
//        try {
//            session = this.getSessionFactory().openSession();
//            Transaction tx = session.beginTransaction();
//            session.delete(model);
//
//            session.flush();
//            tx.commit();
//            return true;
//
//        } catch (Exception e) {
//            throw e;
//        }
//        finally{           
//            session.close();
//        }
//    }
//
//    @Override
//    public List<SchedulePriorityMap> getSchedulePriorityMapList(PriorityDef priorityDf, Date dtStartDate, Date dtEndDate) {
//        try {
//            Session session = this.getSessionFactory().getCurrentSession();
//
//            Criteria criteria = session.createCriteria(SchedulePriorityMap.class);
//            criteria.add(Restrictions.eq("priority", priorityDf));
//            criteria.add(Restrictions.between("date", dtStartDate, dtEndDate));
//            criteria.addOrder(Order.asc("date"));
//            
//            List<SchedulePriorityMap> ret = (List<SchedulePriorityMap>)criteria.list();            
//            return ret;
//
//        } catch (Exception e) {
//            throw e;
//        }
//    }
//
//}
