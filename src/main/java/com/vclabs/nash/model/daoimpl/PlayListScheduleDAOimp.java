/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.math.BigInteger;
import java.util.*;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.PlayListScheduleDAO;
import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.model.entity.PlayListHistory;
import com.vclabs.nash.model.process.OneDayScheduleInfo;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class PlayListScheduleDAOimp extends VCLSessionFactory implements PlayListScheduleDAO {

    private static final Logger logger = LoggerFactory.getLogger(PlayListScheduleDAOimp.class);

    @Override
    @Transactional
    public List<PlayList> getPlayList() {

        List<PlayList> playList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PlayList.class);
            playList = (List<PlayList>) criteria.list();
            return playList;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getPlayListCount(){
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery("select count(*) from play_list");
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue();
    }

    @Override
    public List<PlayList> getPlayLists(int limit, int offset) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery("select * from play_list limit ?1 offset ?2", PlayList.class);
        query.setParameter(1, limit);
        query.setParameter(2, offset);
        return query.getResultList();
    }

    @Override
    @Transactional
    public List<PlayList> getPlayList(Date selectDate) {

        List<PlayList> playList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("date", selectDate));
            criteria.addOrder(Order.asc("channel"));
            criteria.addOrder(Order.asc("scheduleStartTime"));
            playList = (List<PlayList>) criteria.list();
            return playList;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public List<PlayList> getPlayList(int workOrder) {
        List<PlayList> playList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("workOrder.workorderid", workOrder));
            criteria.addOrder(Order.asc("channel"));
            criteria.addOrder(Order.asc("scheduleStartTime"));
            playList = (List<PlayList>) criteria.list();
            return playList;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean saveAndUpdatePlayList(PlayList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            if(model.getPlaylistId() == null){  //to avoid duplicate schedule in the play list
                String sqlString = "delete from play_list where Schedule_id = ?1 ";
                Query query = session.createNativeQuery(sqlString);
                query.setParameter(1, model.getSchedule().getScheduleid());
                query.executeUpdate();
            }
            session.saveOrUpdate(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean deletePlayList(PlayList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean deletePlayLists(List<Integer> playLists) {
        Session session = null;
        try {
            session = this.getSessionFactory().getCurrentSession();
            String sqlString = "delete from play_list where Playlist_id in ?1";
            Query query = session.createNativeQuery(sqlString);
            query.setParameter(1, playLists);
            query.executeUpdate();
            logger.debug("Deleted play lists - " + Arrays.toString(playLists.toArray()));
            return true;
        }
        catch (Exception e){
            logger.debug("Exception has occurred while deleting playLists: {}", Arrays.toString(playLists.toArray()),
                    e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean savePlayListHistory(PlayListHistory model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean savePlayListHistory(List<PlayListHistory> models) {
        Session session = null;
        Transaction tx = null;
        try {
            session = this.getSessionFactory().openSession();
            tx = session.beginTransaction();
            for(PlayListHistory plh : models){
                logger.info("Save " + plh.toString());
                session.saveOrUpdate(plh);
            }
            session.flush();
            tx.commit();
        }catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            throw e;
        }finally {
            session.close();
        }
        return true;
    }

    @Override
    public List<PlayList> getOneDaySchedules(OneDayScheduleInfo scheduleInfo) {

        List<PlayList> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Date dtStartTime = new Date();
            Date dtEndTime = new Date();

            Calendar cal_date = Calendar.getInstance();
            cal_date.setTime(dtStartTime);
            cal_date.set(Calendar.HOUR_OF_DAY, 0);
            cal_date.set(Calendar.MINUTE, 0);
            cal_date.set(Calendar.SECOND, 0);
            dtStartTime = cal_date.getTime();

            Calendar cal_time = Calendar.getInstance();
            cal_time.setTime(dtEndTime);
            cal_date.set(Calendar.HOUR_OF_DAY, 23);
            cal_date.set(Calendar.MINUTE, 59);
            cal_date.set(Calendar.SECOND, 59);
            dtEndTime = cal_date.getTime();;

            Criteria criteria = session.createCriteria(PlayList.class);
            //criteria.add(Restrictions.between("date", dtStartTime, dtEndTime));
            criteria.add(Restrictions.ne("comment", "filler_generated"));
            criteria.add(Restrictions.ne("status", "6"));

            if (scheduleInfo.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channel.channelid", scheduleInfo.getChannelId()));
            }

            if (scheduleInfo.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workOrder.workorderid", scheduleInfo.getWorkOrderId()));
            }

            if (scheduleInfo.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("advert.advertid", scheduleInfo.getAdvertId()));
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                criteria.createAlias("advert", "_advert").add(Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType()));
            }
            criteria.add(Restrictions.and(Restrictions.ge("date", scheduleInfo.getDate()), Restrictions.le("date", dtEndTime)));
            if (scheduleInfo.getEndHour() != -111 && scheduleInfo.getStartHour() != -111) {
                criteria.add(Restrictions.and(Restrictions.ge("scheduleHour", scheduleInfo.getStartHour()), (Restrictions.le("scheduleHour", scheduleInfo.getEndHour()))));
            }

            criteria.addOrder(Order.asc("playCluster"));
            criteria.addOrder(Order.asc("playOrder"));
            criteria.addOrder(Order.desc("scheduleHour"));
            criteria.addOrder(Order.desc("scheduleStartTime"));
            criteria.addOrder(Order.desc("scheduleEndTime"));

            scheduleList = (ArrayList<PlayList>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PlayList> getOneDaySchedulesView(OneDayScheduleInfo scheduleInfo) {
        List<PlayList> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Date dtStartTime = new Date();
            Date dtEndTime = new Date();

            Calendar cal_date = Calendar.getInstance();
            cal_date.setTime(dtStartTime);
            cal_date.set(Calendar.HOUR_OF_DAY, 0);
            cal_date.set(Calendar.MINUTE, 0);
            cal_date.set(Calendar.SECOND, 0);
            dtStartTime = cal_date.getTime();

            Calendar cal_time = Calendar.getInstance();
            cal_time.setTime(dtEndTime);
            cal_date.set(Calendar.HOUR_OF_DAY, 23);
            cal_date.set(Calendar.MINUTE, 59);
            cal_date.set(Calendar.SECOND, 59);
            dtEndTime = cal_date.getTime();
            ;

            Criteria criteria = session.createCriteria(PlayList.class);
            //criteria.add(Restrictions.between("date", dtStartTime, dtEndTime));
            criteria.add(Restrictions.ne("comment", "filler_generated"));
            criteria.add(Restrictions.ne("status", "6"));

            if (scheduleInfo.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channel.channelid", scheduleInfo.getChannelId()));
            }

            if (scheduleInfo.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workOrder.workorderid", scheduleInfo.getWorkOrderId()));
            }

            if (scheduleInfo.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("advert.advertid", scheduleInfo.getAdvertId()));
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                if (scheduleInfo.getAdvertType().equals("ADVERT")) {
                    // criteria.add(Restrictions.eq("_advert.adverttype","FILLER"));
                    criteria.createAlias("advert", "_advert").add(Restrictions.or(Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType()), Restrictions.eq("_advert.adverttype", "FILLER")));
                } else {
                    criteria.createAlias("advert", "_advert").add(Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType()));
                }
            }
            criteria.add(Restrictions.and(Restrictions.ge("date", scheduleInfo.getDate()), Restrictions.le("date", dtEndTime)));
            if (scheduleInfo.getEndHour() != -111 && scheduleInfo.getStartHour() != -111) {
                criteria.add(Restrictions.and(Restrictions.ge("scheduleHour", scheduleInfo.getStartHour()), (Restrictions.le("scheduleHour", scheduleInfo.getEndHour()))));
            }

            criteria.addOrder(Order.asc("playCluster"));
            criteria.addOrder(Order.asc("playOrder"));
            criteria.addOrder(Order.desc("scheduleHour"));
            criteria.addOrder(Order.desc("scheduleStartTime"));
            criteria.addOrder(Order.desc("scheduleEndTime"));

            scheduleList = (ArrayList<PlayList>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PlayList> getSchedulForTR(int woID, int channelID, Date from, Date to) {

        List<PlayList> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            //criteria.add(Restrictions.between("date", dtStartTime, dtEndTime));
            criteria.add(Restrictions.ne("comment", "filler_generated"));
            criteria.add(Restrictions.ne("status", "6"));

            if (channelID != -111) {
                criteria.add(Restrictions.eq("channel.channelid", channelID));
            }

            if (woID != -111) {
                criteria.add(Restrictions.eq("workOrder.workorderid", woID));
            }

            criteria.addOrder(Order.asc("playCluster"));
            criteria.addOrder(Order.asc("playOrder"));

            scheduleList = (ArrayList<PlayList>) criteria.list();
            return scheduleList;

        } catch (Exception e) {
            logger.debug("Exception in PlayListScheduleDAOimpl: ", e.getMessage());
            throw e;
        }
    }

}
