package com.vclabs.nash.model.daoimpl.teams;

import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.model.dao.Teams.TeamDetailDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.teams.TeamDetail;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 03/01/2019
 */
@Repository
public class TeamDetailDAOImpl extends VCLSessionFactory implements TeamDetailDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamDetailDAOImpl.class);

    @Override
    @Transactional
    public TeamDetail save(TeamDetail teamDetail) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(teamDetail);
        return teamDetail;
    }

    @Override
    @Transactional
    public TeamDetail findById(Integer id) {
        Session session = this.getSessionFactory().getCurrentSession();
        TeamDetail teamDetail = session.find(TeamDetail.class, id);
        return teamDetail;
    }

    @Override
    @Transactional
    public List<TeamDetail> findAll() {
        Session session = this.getSessionFactory().getCurrentSession();
//        Criteria criteria = session.createCriteria(TeamDetail.class);
//        List<TeamDetail> teams = (ArrayList<TeamDetail>) criteria.list();
        Query query = session.createNativeQuery("SELECT * FROM team_detail", TeamDetail.class);
        List<TeamDetail> teams = query.getResultList();
        return teams;
    }

    @Override
    public List<TeamDetail> findByTeamName(String teamName) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery("select * from team_detail where name=?1", TeamDetail.class);
        query.setParameter(1,teamName);
        return query.getResultList();
    }

    @Override
    @Transactional
    public List<String> findAllMembersOfTeam(Integer teamId) {
        List<String> members = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("select tm.username from team_detail td left join team_Members tm " +
                    "on td.id = tm.id where td.id = ?1 order by tm.username");
            query.setParameter(1, teamId);
            members = query.getResultList();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
        return members;
    }

    @Override
    @Transactional
    public List<String> findAllAdminsOfTeam(Integer teamId) {
        List<String> admins = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("select ta.username from team_detail td left join team_Admins ta " +
                    "on td.id = ta.id where td.id = ?1 order by ta.username");
            query.setParameter(1, teamId);
            admins = query.getResultList();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
        return admins;
    }

    @Override
    @Transactional
    public Boolean deleteMember(Integer teamId, String username) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("delete from team_Members where id = ?1 and username = ?2");
            query.setParameter(1, teamId);
            query.setParameter(2, username);
            LOGGER.debug("Team member {} removed successfully from team id {}",username,teamId);
            int executeUpdate =query.executeUpdate();
            return true;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean deleteAdmin(Integer teamId, String username) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("delete from team_Admins where id = ?1 and username = ?2");
            query.setParameter(1, teamId);
            query.setParameter(2, username);
            LOGGER.debug("Team admin {} removed successfully from team id {}",username,teamId);
            query.executeUpdate();
            return true;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public List<MyTeamToDoListDto> getTeamMembersWODetails() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("select tm.id as teamId, tm.username as teamMember, wo.Work_order_id as woId, wo.Order_name as woName " +
                    "from team_Members tm " +
                    "left join work_order wo\n" +
                    "on tm.username = wo.Seller " +
                    "where (wo.autoStatus = 0 or wo.manualStatus = 0) " +
                    "order by tm.id asc, tm.username asc", "myTeamToDoListMapping");
            List<MyTeamToDoListDto> result = query.getResultList();
            return result;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

}
