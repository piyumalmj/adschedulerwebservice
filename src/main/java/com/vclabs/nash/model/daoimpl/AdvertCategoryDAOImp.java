/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.Advertisement.AdvertCategoryDAO;
import com.vclabs.nash.model.entity.AdvertCategories;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class AdvertCategoryDAOImp extends VCLSessionFactory implements AdvertCategoryDAO {

    static final Logger logger = LoggerFactory.getLogger(AdvertCategoryDAOImp.class);

    @Override
    public List<AdvertCategories> getAllAdvertCategories() {
        ArrayList<AdvertCategories> advertCategory = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(AdvertCategories.class);
            advertCategory = (ArrayList<AdvertCategories>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in AdvertCategoryDAOImp while getting advert categories  : {}", e.getMessage());
            throw e;
        }
        return advertCategory;
    }

    @Override
    public Boolean saveAdvertCategories(AdvertCategories model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertCategoryDAOImp while saving advert categories  : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updateAdvertCategories(AdvertCategories model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertCategoryDAOImp in updateAdvertCategories  : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<AdvertCategories> getAdvertCategories(String categoryName) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery("select * from advertisement_categories where category = ?1", AdvertCategories.class);
        query.setParameter(1, categoryName);
        return query.getResultList();
    }
}
