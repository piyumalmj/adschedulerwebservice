/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;

import com.vclabs.nash.model.dao.TimeSlotScheduleMapDAO;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nalaka
 * @since 27-02-2018
 */
@Repository
public class TimeSlotScheduleMapDAOImp extends VCLSessionFactory implements TimeSlotScheduleMapDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeSlotScheduleMapDAOImp.class);

    @Override
    public List<TimeSlotScheduleMap> getSelectedWorkOrderTimeSlot(int workOrder) {
        List<TimeSlotScheduleMap> timeSlotMap = null;

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("workOrderId", workOrder));
            criteria.createAlias("scheduleid", "schedule");
            criteria.add(Restrictions.ne("schedule.status", "6"));
            timeSlotMap = (ArrayList<TimeSlotScheduleMap>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in TimeSlotScheduleMapDAOImpl in getSelectedWorkOrderTimeSlot", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return timeSlotMap;
    }

    @Override
    public TimeSlotScheduleMap getSelectedTimeSlot(int scheduleID) {

        TimeSlotScheduleMap model = new TimeSlotScheduleMap();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("scheduleid.scheduleid", scheduleID));
            model = (TimeSlotScheduleMap) criteria.uniqueResult();
            return model;
        } catch (Exception e) {
            LOGGER.debug("Exception in TimeSlotScheduleMapDAOImpl in getSelectedTimeSlot", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

}
