/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.process.AdvertFilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

import com.vclabs.nash.model.process.dto.AdvertisementDropDownDataDto;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 *
 * @author user
 */
@Repository
public class AdvertisementDAOImpl extends VCLSessionFactory implements AdvertisementDAO {

    private static final Logger logger = LoggerFactory.getLogger(AdvertisementDAOImpl.class);

    @Value("${dashboard.record.limit}")
    private int RECORD_LIMIT;

    @Override
    public Advertisement getSelectedAdvertisement(int advertisementId) {
        Advertisement adver = null;
        Session session = null;
        try {

            session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.eq("advertid", advertisementId));
            adver = (Advertisement) criteria.uniqueResult();
            return adver;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getSelectedAdvertisement : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Advertisement getAdvertisementForAudit(int advertisementId) {
        Advertisement adver = null;
        Session session = null;
        try {

            //For the update advertisement
            //For the audt, can't be used current session

            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.eq("advertid", advertisementId));
            adver = (Advertisement) criteria.uniqueResult();
            return adver;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementForAudit : {}", e.getMessage());
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    @Transactional
    public Boolean updateAdvertisement(Advertisement model) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (model.getAdvertid() == null) {
                model.setCreateDate(dateFormat.parse(dateFormat.format(model.getCreateDate())));
                model.setExpireDate(dateFormat.parse(dateFormat.format(model.getExpireDate())));
            }
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return true;
        } catch (ParseException e) {
            logger.debug("ParseException in AdvertisementDAOImpl in updateAdvertisement : {}", e.getMessage());
            return false;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in updateAdvertisement : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Advertisement> getFillerAdvertisements() {
        ArrayList<Advertisement> ret = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.eq("adverttype", "FILLER"));
            criteria.add(Restrictions.eq("isFileAvailable", true));
            criteria.add(Restrictions.eq("Enabled", 1));
            ret = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getFillerAdvertisements : {}", e.getMessage());
        }
        return ret;
    }

    @Override
    public List<Advertisement> getFilterAdvertisements(String clientId, String AdvertCatogory, String advertName, String advertId) {
        ArrayList<Advertisement> ret = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.eq("status", 1));
            criteria.add(Restrictions.eq("Enabled", 1));

            if (!AdvertCatogory.equals(
                    "-111")) {
                criteria.add(Restrictions.eq("adverttype", AdvertCatogory));
            }

            if (!advertId.equals("-111")) {
                int advert_id = Integer.parseInt(advertId);
                criteria.add(Restrictions.eq("advertid", advert_id));
            }

            if (!clientId.equals(
                    "-111")) {
                int clien_id = Integer.parseInt(clientId);
                criteria.createAlias("client", "clid");
                criteria.add(Restrictions.eq("clid.clientid", clien_id));
            }

            if (!advertName.equals(
                    "-111")) {
                criteria.add(Restrictions.like("advertname", advertName, MatchMode.ANYWHERE));
            }

            criteria.addOrder(Order.desc("advertid"));

            ret = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getFilterAdvertisements : {}", e.getMessage());
            throw e;
        }
        return ret;
    }

    /*Admin user use this method. Admin user want view all Advertisement (Include enable Advert)*/
    @Override
    @Transactional
    public List<Advertisement> getAdvertisementAll() {
        ArrayList<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.ne("status", 3));
            criteria.addOrder(Order.desc("advertid"));
            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementAll : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public List<Advertisement> getAdvertisementAllValid() {
        ArrayList<Advertisement> advertList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.eq("Enabled", 1));
            criteria.add(Restrictions.eq("isFileAvailable", true));

            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementAllValid : {}", e.getMessage());
            throw e;
        }

        return advertList;
    }

    @Override
    public List<Advertisement> getAdvertisementList(int client) {

        ArrayList<Advertisement> advertList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.eq("Enabled", 1));
            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementList : {}", e.getMessage());
            throw e;
        }

        return advertList;
    }

    @Override
    public List<Advertisement> getAdvertisementListValid(int client) {

        ArrayList<Advertisement> advertList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.eq("Enabled", 1));
            criteria.add(Restrictions.eq("isFileAvailable", true));

            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementListValid : {}", e.getMessage());
            throw e;
        }

        return advertList;
    }

    @Override
    public Boolean saveAdvertisement(Advertisement model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in saveAdvertisement : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteAdvertisement(Advertisement model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in deleteAdvertisement : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<AdvertisementDropDownDataDto> getAdvertisementAllOrderByName() {
        try {
            String sql = String.format("select Advert_id as advertid, Advert_name as advertname, Advert_type from advertisement_def where enabled = 1 order by Advert_name asc");
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "AdvertisementDropDownDataMapping");
            List<AdvertisementDropDownDataDto> listData = query.getResultList();
            return listData;
        }catch (Exception e){
            logger.debug("Exception in getAdvertisementAllOrderByName: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<Advertisement> getClientAdvertisement(int clientOne, int clientTwo) {
        ArrayList<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class
            );
            criteria.add(Restrictions.eq("Enabled", 1));
            if (clientTwo == -1 || clientOne
                    == -1) {
                if (clientOne != -1) {
                    criteria.add(Restrictions.eq("client.clientid", clientOne));
                } else {
                    criteria.add(Restrictions.eq("client.clientid", clientTwo));
                }
            } else {
                criteria.add(Restrictions.disjunction()
                        .add(Restrictions.eq("client.clientid", clientOne))
                        .add(Restrictions.eq("client.clientid", clientTwo))
                );
            }
            criteria.addOrder(Order.desc("advertid"));
            advertList = (ArrayList<Advertisement>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getClientAdvertisement : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public List<Advertisement> getLimitedAdvert(int size, int offset, AdvertFilter advertFilter) {
        ArrayList<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.and(Restrictions.ne("advertid", 1), Restrictions.ne("advertid", 0)));
            criteria.add(Restrictions.ne("status", 3));
            if (!advertFilter.getAdvertId().equals("-111")) {
                int advertId = Integer.parseInt(advertFilter.getAdvertId());
                criteria.add(Restrictions.eq("advertid", advertId));
            }
            if (!advertFilter.getAdvertCategory().equals("-111")) {
                criteria.add(Restrictions.eq("adverttype", advertFilter.getAdvertCategory()));
            }
            if (!advertFilter.getClient().equals("-111")) {
                int clien_id = Integer.parseInt(advertFilter.getClient());
                criteria.createAlias("client", "clid");
                criteria.add(Restrictions.eq("clid.clientid", clien_id));
            }

            if (!advertFilter.getAdvertName().equals("-111")) {
                criteria.add(Restrictions.like("advertname", advertFilter.getAdvertName(), MatchMode.ANYWHERE));
            }
            criteria.addOrder(Order.desc("advertid"));
            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getLimitedAdvert : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public int getLimitedAdvertCount(AdvertFilter advertFilter) {
        long advertCount = 0;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.ne("status", 3));
            if (!advertFilter.getAdvertId().equals("-111")) {
                int advertId = Integer.parseInt(advertFilter.getAdvertId());
                criteria.add(Restrictions.eq("advertid", advertId));
            }
            if (!advertFilter.getAdvertCategory().equals("-111")) {
                criteria.add(Restrictions.eq("adverttype", advertFilter.getAdvertCategory()));
            }
            if (!advertFilter.getClient().equals("-111")) {
                int clien_id = Integer.parseInt(advertFilter.getClient());
                criteria.createAlias("client", "clid");
                criteria.add(Restrictions.eq("clid.clientid", clien_id));
            }

            if (!advertFilter.getAdvertName().equals("-111")) {
                criteria.add(Restrictions.like("advertname", advertFilter.getAdvertName(), MatchMode.ANYWHERE));
            }
            criteria.setProjection(Projections.rowCount());
            advertCount = (long) criteria.uniqueResult();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getLimitedAdvertCount : {}", e.getMessage());
            throw e;
        }
        return Integer.parseInt("" + advertCount);
    }

    @Override
    public List<Advertisement> getDummyCuts() {
        List<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("SELECT * FROM advertisement_def where enabled=1 and File_Available=false and status!=3", Advertisement.class);
            advertList = query.getResultList();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getDummyCuts : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public List<Advertisement> getAdvertisementByMultyIDSAndClient(List<Integer> cutIds, List<Integer> clientList) {
        ArrayList<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            if (!cutIds.isEmpty()) {
                criteria.add(Property.forName("advertid").in(cutIds));
            }
            if (!clientList.isEmpty()) {
                criteria.add(Property.forName("client.clientid").in(clientList));
            }
            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementByMultyIDSAndClient : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public List<Integer> getAdvertisementIDList() {

        List<Integer> advertIdList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.addOrder(Order.desc("advertid"));
            criteria.setProjection(Projections.property("advertid"));
            advertIdList = (List<Integer>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getAdvertisementIDList : {}", e.getMessage());
            throw e;
        }
        return advertIdList;
    }

    @Override
    public List<Advertisement> getDeletedAdvertList() {
        ArrayList<Advertisement> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(Advertisement.class);
            criteria.add(Restrictions.eq("status", 3));
            criteria.addOrder(Order.desc("advertid"));
            advertList = (ArrayList<Advertisement>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getDeletedAdvertList : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    /**************for Dashboard***********/////////////
    @Override
    public List<Advertisement> getDummyCutsForDashboard() {
        try {
            String sql = String.format("select * from advertisement_def ad where (ad.status = 1 or ad.status = 2) and ad.enabled = 1 and ad.File_Available = 0 order by ad.Advert_id desc limit " + RECORD_LIMIT);
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, Advertisement.class);
            List<Advertisement> dummyCuts = query.getResultList();
            return dummyCuts;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImple getDummyCutsForDashboard: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public Integer getSpotCountOfDummyCutsForDashboard(Integer advertId) {
        try {
            String sql = String.format("select count(*) from schedule_def sd left join advertisement_def ad on sd.Advert_id = ad.Advert_id where sd.status != 6 and ad.enabled = 1 and ad.File_Available = 0 and ad.Advert_id=%d", advertId);
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql);
            Integer spots = ((Number) query.getSingleResult()).intValue();
            return spots;
        }catch (Exception e){
            logger.debug("Exception in AdvertisementDAOImple getDummyCutsForDashboard: {}", e.getMessage());
            return 0;
        }
    }

    /******For IngestStatusWidget*******/
    @Override
    public List<IngestStatusWidgetDto> findAllAdvertsForIngestStatusWidget() {
        try {
            String sql = "select ad.Advert_id as cutId, cd.Client_name as client, ad.status as status from advertisement_def ad left join client_details cd on ad.Client = cd.Client_id order by ad.Advert_id desc limit "+RECORD_LIMIT;
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "ingestStatusWidgetMapping");
            List<IngestStatusWidgetDto> result = query.getResultList();
            return result;
        }catch (Exception e){
            logger.debug("Exception in AdvertisementDAOImple findAllAdvertsForIngestStatusWidget: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<PriorityAdvertisementWidgetDto> findPriorityAdvertisementsDetails() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String sql = "select distinct ad.Advert_id as advertId, ad.Advert_name as advertName, ad.status as status, pd.priority as priorityNo, CONCAT_WS('-', time(tb.start_time), time(tb.end_time)) as timeBelt , c.Channel_name as channel from advertisement_def ad left join schedule_def sd on ad.Advert_id = sd.Advert_id left join channel_detail c on sd.Channel_id = c.Channel_id left join priority_def pd on pd.priority_id = sd.Priority_id left join time_belts tb on pd.timebelt_id = tb.time_belt_id where sd.Priority_id > 0 and sd.Date ='" + dateFormat.format(new Date()) + "'";
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "priorityAdvertisementWidgetMapping");
            List<PriorityAdvertisementWidgetDto> result = query.getResultList();
            return result;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImple findPriorityAdvertisementsDetails: {}", e.getMessage());
            return null;
        }
    }

}
