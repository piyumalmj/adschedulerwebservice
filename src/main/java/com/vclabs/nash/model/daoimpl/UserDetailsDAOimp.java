/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.entity.SecurityAuthoritiesEntity;
import com.vclabs.nash.model.entity.SecurityUsersEntity;
import com.vclabs.nash.model.dao.UserDetailsDAO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
@Repository
public class UserDetailsDAOimp extends VCLSessionFactory implements UserDetailsDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsDAOimp.class);

    @Override
    public ArrayList<SecurityUsersEntity> getUserDetailsList(String userName, String passWord) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.and(Restrictions.eq("username", userName), Restrictions.eq("password", passWord)));
            ArrayList<SecurityUsersEntity> userdetails = (ArrayList<SecurityUsersEntity>) criteria.list();
            return userdetails;
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public ArrayList<SecurityUsersEntity> getUserValidation(String userName) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.eq("username", userName));
            ArrayList<SecurityUsersEntity> userdetails = (ArrayList<SecurityUsersEntity>) criteria.list();
            return userdetails;
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public void setUserDetails(SecurityUsersEntity userDetails) {
        try(Session session = this.getSessionFactory().openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(userDetails);
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public void updateUserDetails(SecurityUsersEntity userDetails) {
        try (Session session = this.getSessionFactory().openSession()){
            Transaction tx = session.beginTransaction();
            session.update(userDetails);
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public void updateHasTeamTrue(List<String> username) {
        String sql="update users set hasTeam = 1 where username in (?1)";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        query.setParameter(1,username);
        query.executeUpdate();
    }

    @Override
    public void updateHasTeamFalse(List<String> username) {
        String sql="update users set hasTeam = 0 where username in (?1)";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        query.setParameter(1,username);
        query.executeUpdate();
    }

    @Override
    public void setAuthority(SecurityAuthoritiesEntity auth) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(auth);
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<SecurityUsersEntity> getAllUser() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.ne("username", "SU"));
            List<SecurityUsersEntity> userdetails = (List<SecurityUsersEntity>) criteria.list();
            return userdetails;
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public SecurityUsersEntity getUserDetails(String userName) {
        SecurityUsersEntity user = null;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.eq("username", userName));
            user = (SecurityUsersEntity) criteria.uniqueResult();
            return user;
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : {}", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public SecurityUsersEntity getUserDetails(String userName, int enabled) {
        SecurityUsersEntity user = null;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.eq("username", userName));
            criteria.add(Restrictions.eq("enabled", (short)enabled));
            user = (SecurityUsersEntity) criteria.uniqueResult();
            return user;
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public ArrayList<SecurityUsersEntity> getUserList(String groupName) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.eq("createWorkOrder", Short.parseShort("1")));
            criteria.add(Restrictions.eq("enabled", Short.parseShort("1")));
            return (ArrayList<SecurityUsersEntity>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<SecurityUsersEntity> getUserListForCheck(String groupName) {
        try {
            Session session  = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
            criteria.add(Restrictions.eq("usergroup", groupName));
            criteria.add(Restrictions.eq("enabled", Short.parseShort("1")));
            return (List<SecurityUsersEntity>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in UserDetailsDAOimp : ", e.getMessage());
            throw e;
        }
    }

    /**
     * Returns authorities of the user
     * @param username
     * @return list of authorities
     */
    @Override
    public List<SecurityAuthoritiesEntity> getSecurityAuthoritiesByUserName(String username) {
        Session session = null;
        try{
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(SecurityAuthoritiesEntity.class);
            criteria.add(Restrictions.eq("username.username", username));
            return  (ArrayList<SecurityAuthoritiesEntity>) criteria.list();
        }catch (Exception e){
            LOGGER.debug("Exception in UserDetailsDAOimp : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<SecurityUsersEntity> getTeamlessUser() {
        Session session  = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(SecurityUsersEntity.class);
        criteria.add(Restrictions.eq("hasTeam", false));
        ArrayList<SecurityUsersEntity> userdetails = (ArrayList<SecurityUsersEntity>) criteria.list();
        return userdetails;
    }
}
