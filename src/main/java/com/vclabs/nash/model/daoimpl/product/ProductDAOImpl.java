package com.vclabs.nash.model.daoimpl.product;

import com.vclabs.nash.model.dao.product.ProductDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.product.Product;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Created by dperera on 29/08/2019.
 */
@Repository
public class ProductDAOImpl extends VCLSessionFactory implements ProductDAO {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ProductDAOImpl.class);

    @Override
    public void save(Product product) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(product);
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while saving product {}", product.getName());
        }
    }

    @Override
    public Product findByName(String name) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            String sqlString = "select * from product where name = ?1";
            Query query = session.createNativeQuery(sqlString, Product.class);
            query.setParameter(1, name);
            Optional<Product> result = query.getResultList().stream().findFirst();
            return result.isPresent() ? result.get() : null;
        }catch (Exception e){
            LOGGER.debug("Exception has occurred while getting product by name {} ", name);
        }
        return null;
    }

    @Override
    public List<Product> findAll() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            String sqlString = "select * from product order by name";
            Query query = session.createNativeQuery(sqlString, Product.class);
            return  query.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while getting all brands {} ", e.getMessage());
        }
        return null;
    }
}
