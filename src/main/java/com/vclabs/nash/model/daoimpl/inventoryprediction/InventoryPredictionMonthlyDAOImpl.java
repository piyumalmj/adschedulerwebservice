package com.vclabs.nash.model.daoimpl.inventoryprediction;

import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionMonthlyDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
@Repository
public class InventoryPredictionMonthlyDAOImpl extends VCLSessionFactory implements InventoryPredictionMonthlyDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryPredictionMonthlyDAOImpl.class);

    @Override
    public InventoryMonthlyPrediction save(InventoryMonthlyPrediction inventoryMonthlyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try{
            session.saveOrUpdate(inventoryMonthlyPrediction);
            LOGGER.debug("InventoryMonthlyPrediction is saved/updated successfully");
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryMonthlyPrediction;
    }

    @Override
    public InventoryMonthlyPrediction update(InventoryMonthlyPrediction inventoryMonthlyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.update(inventoryMonthlyPrediction);
            LOGGER.debug("Successfully updated InventoryMonthlyPrediction");
        }catch (Exception e){
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryMonthlyPrediction;
    }

    @Override
    public List<InventoryMonthlyPrediction> findAll() {
        Session session = this.getSessionFactory().getCurrentSession();
        List<InventoryMonthlyPrediction> inventoryDailyPredictions = session.createCriteria(InventoryMonthlyPrediction.class).list();
        return inventoryDailyPredictions;
    }

    @Override
    public List<InventoryMonthlyPrediction> findLatestMonthlyPrediction() {
        String sql = "SELECT imp.* FROM inventory_monthly_prediction imp left join prediction_result_meta prm on imp.predictionResultMetaId_id=prm.id where prm.isProcess=false";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryMonthlyPrediction.class);
        return query.getResultList();
    }


    @Override
    public List<InventoryMonthlyPrediction> findLatestPredictions(Date startingDate, Date lastDate) {
        String sql = "select * from inventory_daily_prediction idp where idp.dateAndTime between ?1 and ?2";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryMonthlyPrediction.class);
        query.setParameter(1, startingDate);
        query.setParameter(2, lastDate);
        List<InventoryMonthlyPrediction> result = query.getResultList();
        return result;
    }
}
