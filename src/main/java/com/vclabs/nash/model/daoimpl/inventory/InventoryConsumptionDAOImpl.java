package com.vclabs.nash.model.daoimpl.inventory;

import com.vclabs.nash.model.dao.inventory.InventoryConsumptionDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryConsumption;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Optional;

/**
 * Created by Sanduni on 03/10/2018
 */
@Repository
public class InventoryConsumptionDAOImpl extends VCLSessionFactory implements InventoryConsumptionDAO{

    @Override
    @Transactional
    public InventoryConsumption findByInventoryRowId(long inventoryRowId){
        String sql = "select ic from InventoryConsumption ic where ic.inventoryRow.id = :irid";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(sql, InventoryConsumption.class);
        query.setParameter("irid", inventoryRowId);
        Optional<InventoryConsumption> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public InventoryConsumption saveAndFlush(InventoryConsumption inventoryConsumption) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(inventoryConsumption);
        session.flush();
        return inventoryConsumption;
    }
}
