/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.entity.GeneralAudit;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
@Repository
public class GeneralAuditDAOimp extends VCLSessionFactory implements GeneralAuditDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralAuditDAOimp.class);

    @Override
    public Boolean saveGeneralAudit(GeneralAudit model) {

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.saveOrUpdate(model);
            session.flush();
            tx.commit();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in GeneralAuditDAOImpl in saveGeneralAudit", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<GeneralAudit> getSelectedData(String tableName, int recodeID) {
        Session session = null;
        Transaction tx = null;
        List<GeneralAudit> auditList = new ArrayList<>();
        try {
            session = this.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(GeneralAudit.class);
            criteria.add(Restrictions.eq("tablename", tableName));
            criteria.add(Restrictions.eq("recordId", "" + recodeID));
            criteria.addOrder(Order.asc("general_auditId"));

            auditList = (ArrayList<GeneralAudit>) criteria.list();

            tx.commit();
            return auditList;

        } catch (Exception e) {
            LOGGER.debug("Exception in GeneralAuditDAOImpl in getSelectedData", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<GeneralAudit> getAdvertDeleteDetails() {
        List<GeneralAudit> audit = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(GeneralAudit.class);
            criteria.add(Restrictions.eq("tablename", "advertisement_def"));
            criteria.add(Restrictions.eq("field", "status"));
            criteria.add(Restrictions.eq("newData", "3"));
            criteria.addOrder(Order.asc("general_auditId"));
            audit = (ArrayList<GeneralAudit>) criteria.list();
            return audit;
        } catch (Exception e) {
            LOGGER.debug("Exception in GeneralAuditDAOImpl in getAdvertDeleteDetails", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<GeneralAudit> getSelectedSchedulesRecodes(ArrayList<String> recodeIDs) {
        Session session = null;
        Transaction tx = null;
        List<GeneralAudit> auditList = new ArrayList<>();
        try {
            session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(GeneralAudit.class);
            criteria.add(Restrictions.eq("tablename", "schedule_def"));
            criteria.add(Property.forName("recordId").in(recodeIDs));
            criteria.addOrder(Order.asc("dateAndTime"));
            criteria.addOrder(Order.asc("recordId"));

            auditList = (ArrayList<GeneralAudit>) criteria.list();

            //tx.commit();
            return auditList;

        } catch (Exception e) {
            LOGGER.debug("Exception in GeneralAuditDAOImpl in getSelectedSchedulesRecodes", e.getMessage());
            throw e;
        }
    }

}
