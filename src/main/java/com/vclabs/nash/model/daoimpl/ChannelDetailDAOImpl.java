/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.ChannelDetailDAO;

import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ServerDetails;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author user
 */
@Repository
public class ChannelDetailDAOImpl extends VCLSessionFactory implements ChannelDetailDAO {

    private static final Logger logger = LoggerFactory.getLogger(ChannelDetailDAOImpl.class);

    @Override
    @Transactional
    public List<ChannelDetails> getChannelList() {

        ArrayList<ChannelDetails> channelList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            criteria.add(Restrictions.eq("enabled", true));
            channelList = (ArrayList<ChannelDetails>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return channelList;
    }

    @Override
    public ChannelDetails getChannel(int _channelID) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            ChannelDetails channelDetails = null;
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            criteria.add(Restrictions.eq("channelid", _channelID));
            channelDetails = (ChannelDetails) criteria.uniqueResult();
            return channelDetails;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ChannelDetails getChannelByOpsChannelId(int opsChannelId) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(ChannelDetails.class);
        criteria.add(Restrictions.eq("opsChannelId", opsChannelId));
        ChannelDetails channelDetails = (ChannelDetails) criteria.uniqueResult();
        return channelDetails;
    }

    @Override
    public int saveChannel(ChannelDetails model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model.getChannelid();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int save(ChannelDetails model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return model.getChannelid();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Boolean updateChannel(ChannelDetails model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            if (model.getServerDetails() != null) {
                model.setServerDetails((ServerDetails) session.load(ServerDetails.class, model.getServerDetails().getServerId()));
            }
            session.update(model);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<ChannelDetails> getChannelListOrderByName() {
        ArrayList<ChannelDetails> channelList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            criteria.add(Restrictions.eq("enabled", true));
            criteria.addOrder(Order.asc("channelname"));
            channelList = (ArrayList<ChannelDetails>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return channelList;
    }

    @Override
    public List<ChannelDetails> getAllChannelList() {
        ArrayList<ChannelDetails> channelList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            channelList = (ArrayList<ChannelDetails>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return channelList;
    }

    @Override
    public List<ChannelDetails> getAllManualChannelList() {
        ArrayList<ChannelDetails> channelList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            criteria.add(Restrictions.eq("enabled", true));
            criteria.add(Restrictions.eq("isManual", true));
            criteria.addOrder(Order.asc("channelname"));
            channelList = (ArrayList<ChannelDetails>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return channelList;
    }

    @Override
    public List<ChannelDetails> getAllAutoChannelList() {
        ArrayList<ChannelDetails> channelList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ChannelDetails.class);
            criteria.add(Restrictions.eq("enabled", true));
            criteria.add(Restrictions.eq("isManual", false));
            criteria.addOrder(Order.asc("channelname"));
            channelList = (ArrayList<ChannelDetails>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return channelList;
    }

}
