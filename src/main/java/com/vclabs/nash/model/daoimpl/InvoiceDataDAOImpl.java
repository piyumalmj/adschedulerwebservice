/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.daoimpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.InvoiceDataDAO;
import com.vclabs.nash.model.entity.InvoiceData;

import java.util.List;

/**
 *
 * @author hp
 */
@Repository
public class InvoiceDataDAOImpl extends VCLSessionFactory implements InvoiceDataDAO {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceDataDAOImpl.class);

    @Override
    public InvoiceData getInvoiceData(int invoiceId) {
        InvoiceData invoiceData = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(InvoiceData.class);
            criteria.add(Restrictions.eq("invoiceId.invoiceid", invoiceId));
            invoiceData = (InvoiceData) criteria.uniqueResult();
        } catch (Exception e) {
            logger.debug("Exception in InvoiceDataDAOImpl in getInvoiceData() : ", e.getMessage());
            throw e;
        }
        return invoiceData;
    }

    @Override
    public InvoiceData saveInvoiceData(InvoiceData invoice) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(invoice);
            return invoice;
        } catch (Exception e) {
            logger.debug("Exception in InvoiceDataDAOImpl in saveInvoiceData() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public InvoiceData updateInvoiceData(InvoiceData invoice) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(invoice);
            session.flush();
            return invoice;
        } catch (Exception e) {
            logger.debug("Exception in InvoiceDataDAOImpl in updateInvoiceData() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public InvoiceData getSelectedInvoiceData(int invoiceDataId) {
        InvoiceData invoiceData = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(InvoiceData.class);
            criteria.add(Restrictions.eq("invoiceDataId", invoiceDataId));
            invoiceData = (InvoiceData) criteria.uniqueResult();
        } catch (Exception e) {
            logger.debug("Exception in InvoiceDataDAOImpl in getSelectedInvoiceData() : ", e.getMessage());
            throw e;
        }
        return invoiceData;
    }

}
