/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.PlayHistoryDAO;
import com.vclabs.nash.model.entity.PlayListHistory;
import com.vclabs.nash.model.process.OneDayScheduleInfo;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class PlayHistoryDAOimp extends VCLSessionFactory implements PlayHistoryDAO {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PlayHistoryDAOimp.class);


    @Override
    public List<PlayListHistory> getOneSchedule(OneDayScheduleInfo scheduleInfo) {
        List<PlayListHistory> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PlayListHistory.class);
            criteria.add(Restrictions.eq("date", scheduleInfo.getDate()));
            criteria.add(Restrictions.ne("comment", "filler_generated"));
            criteria.add(Restrictions.ne("status", "6"));

            if (scheduleInfo.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channelid", scheduleInfo.getChannelId()));
            }

            if (scheduleInfo.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workorderid", scheduleInfo.getWorkOrderId()));
            }

            if (scheduleInfo.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("advertid", scheduleInfo.getAdvertId()));
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                criteria.createAlias("scheduleid", "schedule");
                criteria.createAlias("schedule.advertid", "_advert");
                if(scheduleInfo.getAdvertType().equals("ADVERT")){
                    criteria.add(Restrictions.or(Restrictions.eq("_advert.adverttype", "FILLER"),Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType())));
                }else{
                    criteria.add(Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType()));
                }
            }
            if (scheduleInfo.getEndHour() != -111 && scheduleInfo.getStartHour() != -111) {
                criteria.add(Restrictions.and(Restrictions.ge("schedulehour", scheduleInfo.getStartHour()), (Restrictions.le("schedulehour", scheduleInfo.getEndHour()))));
            }
            criteria.addOrder(Order.asc("playcluster"));
            criteria.addOrder(Order.asc("playorder"));
            scheduleList = (ArrayList<PlayListHistory>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            logger.debug("Exception in PlayHistoryDAOimp in getOneSchedule() : ", e.getMessage());
            throw e;
        }
    }

    /*@Override
    public List<PlayListHistory> getOneSchedule(OneDayScheduleInfo scheduleInfo) {
        List<PlayListHistory> playListHistories = new ArrayList<>();
        try {
            String sql = "SELECT * FROM play_list_history where Date = ?1 and Comment != ?2 and Status != ?3 ";

            if (scheduleInfo.getChannelId() != -111) {
                sql = sql + "and Channel_id = " + scheduleInfo.getChannelId() + " ";
            }

            if (scheduleInfo.getWorkOrderId() != -111) {
                sql = sql + "and Work_order_id = " + scheduleInfo.getWorkOrderId() + " ";
            }

            if (scheduleInfo.getAdvertId() != -111) {
                sql = sql + "and Advert_id = " + scheduleInfo.getAdvertId() + " ";
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                if(scheduleInfo.getAdvertType().equals("ADVERT")){
                    criteria.add(Restrictions.or(Restrictions.eq("_advert.adverttype", "FILLER"),Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType())));
                    sql = sql + "and Advert_id = " + scheduleInfo.getAdvertId() + " ";
                }else{
                    criteria.add(Restrictions.eq("_advert.adverttype", scheduleInfo.getAdvertType()));
                }
            }
            if (scheduleInfo.getEndHour() != -111 && scheduleInfo.getStartHour() != -111) {
                criteria.add(Restrictions.and(Restrictions.ge("schedulehour", scheduleInfo.getStartHour()), (Restrictions.le("schedulehour", scheduleInfo.getEndHour()))));
            }
            sql = sql + "order by Play_cluster asc, Play_order asc";
            //criteria.addOrder(Order.asc("playcluster"));
            //criteria.addOrder(Order.asc("playorder"));

            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, PlayListHistory.class);

            query.setParameter(1, scheduleInfo.getDate());
            query.setParameter(2, "filler_generated");
            query.setParameter( 3, 6);
            playListHistories = query.getResultList();
        } catch (Exception e) {
                logger.debug("Exception in getOneDaySchedules : {}", e.getMessage());
                throw e;
        }
        return playListHistories;
    }*/

    @Override
    public List<PlayListHistory> getMissedSpot() {
        List<PlayListHistory> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PlayListHistory.class);

            criteria.add(Restrictions.or(Restrictions.eq("status", "5"), Restrictions.eq("status", "8"), Restrictions.eq("status", "9")));

//            criteria.addOrder(Order.asc("channelid"));
//            criteria.addOrder(Order.asc("schedulestarttime"));
//            criteria.addOrder(Order.asc("scheduleendtime"));
//            criteria.addOrder(Order.asc("advertid"));
//            criteria.addOrder(Order.asc("playcluster"));
//            criteria.addOrder(Order.asc("playorder"));
            scheduleList = (ArrayList<PlayListHistory>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            logger.debug("Exception in PlayHistoryDAOimp in getMissedSpot() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public PlayListHistory getSelectedData(int ScheduleId, String data) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date selectedDate = dateFormat.parse(data);
            Session session = this.getSessionFactory().getCurrentSession();
            String sqlString = "select * from play_list_history where Schedule_id = ?1 and Date = ?2 order by Play_list_id desc  limit 1";
            Query query = session.createNativeQuery(sqlString, PlayListHistory.class);
            query.setParameter(1, ScheduleId);
            query.setParameter(2, selectedDate);
            Optional<PlayListHistory> result = query.getResultList().stream().findFirst();
            return result.isPresent() ? result.get() : null;
           /* Criteria criteria = session.createCriteria(PlayListHistory.class);
            criteria.add(Restrictions.eq("date", selectedDate));
            criteria.add(Restrictions.eq("scheduleid.scheduleid", ScheduleId));
            Object result = criteria.uniqueResult();
            if(result != null){
                playListHistory = (PlayListHistory) result;
            }*/
        } catch (Exception e) {
            logger.debug("Exception in PlayHistoryDAOimp in getSelectedData()[ scheduleId : {} ]", ScheduleId);
        }
        return null;
    }


}
