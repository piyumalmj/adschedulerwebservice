/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 *
 * @author user
 */
public class WorkOrderView implements Comparable<WorkOrderView> {

    private Integer workorderid;
    private int agent;
    private int client;
    private String ordername;
    private String scheduleRef;
    private String seller;
    private String spotCount;
    private String woType;
    private String renMonth;
    private String aired;
    private String commission;
    private double amount;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date date;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date startdate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date enddate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date lastModifydate;
    private String status;
    private int permissionstatus;
    private int channelCount;
    private int advertCount;
    private int billingStatus;
    private Boolean notMatchWithBudget = true;

    public Integer getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public int getPermissionstatus() {
        return permissionstatus;
    }

    public void setPermissionstatus(int permissionstatus) {
        this.permissionstatus = permissionstatus;
    }

    public int getChannelCount() {
        return channelCount;
    }

    public void setChannelCount(int channelCount) {
        this.channelCount = channelCount;
    }

    public int getAdvertCount() {
        return advertCount;
    }

    public void setAdvertCount(int advertCount) {
        this.advertCount = advertCount;
    }

    public int getBillingStatus() {
        return billingStatus;
    }

    public void setBillingStatus(int billingStatus) {
        this.billingStatus = billingStatus;
    }

    public Boolean getNotMatchWithBudget() {
        return notMatchWithBudget;
    }

    public void setNotMatchWithBudget(Boolean notMatchWithBudget) {
        this.notMatchWithBudget = notMatchWithBudget;
    }

    public int getAgent() {
        return agent;
    }

    public void setAgent(int agent) {
        this.agent = agent;
    }

    public String getScheduleRef() {
        return scheduleRef;
    }

    public void setScheduleRef(String scheduleRef) {
        this.scheduleRef = scheduleRef;
    }

    public String getSpotCount() {
        return spotCount;
    }

    public void setSpotCount(String spotCount) {
        this.spotCount = spotCount;
    }

    public Date getLastModifydate() {
        return lastModifydate;
    }

    public void setLastModifydate(Date lastModifydate) {
        this.lastModifydate = lastModifydate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWoType() {
        return woType;
    }

    public void setWoType(String woType) {
        this.woType = woType;
    }

    @Override
    public int compareTo(WorkOrderView o) {
        return o.getWorkorderid() - this.getWorkorderid();
    }

    public String getRenMonth() {
        return renMonth;
    }

    public void setRenMonth(String renMonth) {
        this.renMonth = renMonth;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAired() {
        return aired;
    }

    public void setAired(String aired) {
        this.aired = aired;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

}
