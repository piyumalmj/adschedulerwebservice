/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view;

import java.util.ArrayList;
import java.util.List;
import com.vclabs.nash.model.process.RemainingSpotModel;

/**
 *
 * @author user
 */
public class RemainingSpotView {

    private int remainingDuration = 0;
    private List<RemainingSpotModel> remainList = new ArrayList<>();
    private List<RemainingSpotModel> totalAdvertCount = new ArrayList<>();

    public List<RemainingSpotModel> getTotalAdvertCount() {
        return totalAdvertCount;
    }

    public void setTotalAdvertCount(List<RemainingSpotModel> totalAdvertCount) {
        this.totalAdvertCount = totalAdvertCount;
    }

    public RemainingSpotView() {
    }

    public int getRemainingDuration() {
        return remainingDuration;
    }

    public void setRemainingDuration(int remainingDuration) {
        this.remainingDuration = remainingDuration;
    }

    public List<RemainingSpotModel> getRemainList() {
        return remainList;
    }

    public void setRemainList(List<RemainingSpotModel> remainList) {
        this.remainList = remainList;
    }

    public void addDuration(int duration) {
        remainingDuration += duration;
    }

    public void addTotalAdvertCount(int channelID, int duration) {

        if (totalAdvertCount.isEmpty()) {
            RemainingSpotModel model = new RemainingSpotModel();
            model.setAdvertDuration(duration);
            model.addOne();
            model.setChannelID(channelID);
            totalAdvertCount.add(model);
        } else {
            Boolean newAdvert = true;
            for (RemainingSpotModel dataModel : totalAdvertCount) {
                if (dataModel.getAdvertDuration() == duration && dataModel.getChannelID() == channelID) {
                    dataModel.addOne();
                    newAdvert = false;
                }
            }
            if (newAdvert) {
                RemainingSpotModel model = new RemainingSpotModel();
                model.setAdvertDuration(duration);
                model.addOne();
                model.setChannelID(channelID);
                totalAdvertCount.add(model);
            }
        }

    }
}
