/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vclabs.nash.model.view.CustomDateSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author hp
 */
public class DummyCutView implements Comparable<DummyCutView> {

    private int woId;
    private String clientName = "";
    private String product = "";
    private String me = "";
    private int cutID;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date createDate;
    private int duration;
    private String language;
    private int numberOfSpot;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date airingStartDate;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date airingEndtDate;
    private String airingDateRange = "";

    public DummyCutView() {
    }

    public int getWoId() {
        return woId;
    }

    public void setWoId(int woId) {
        this.woId = woId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getMe() {
        return me;
    }

    public void setMe(String me) {
        this.me = me;
    }

    public int getCutID() {
        return cutID;
    }

    public void setCutID(int cutID) {
        this.cutID = cutID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getNumberOfSpot() {
        return numberOfSpot;
    }

    public void setNumberOfSpot(int numberOfSpot) {
        this.numberOfSpot = numberOfSpot;
    }

    public Date getAiringStartDate() {
        return airingStartDate;
    }

    public void setAiringStartDate(Date airingStartDate) {
        this.airingStartDate = airingStartDate;
    }

    public Date getAiringEndtDate() {
        return airingEndtDate;
    }

    public void setAiringEndtDate(Date airingEndtDate) {
        this.airingEndtDate = airingEndtDate;
    }

    public String getAiringDateRange() {
        return airingDateRange;
    }

    public void setAiringDateRange(String airingDateRange) {
        this.airingDateRange = airingDateRange;
    }

    public void setDateRang() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yy-MM-dd");
        this.airingDateRange = dateFormatter.format(this.airingStartDate) + " - " + dateFormatter.format(this.airingEndtDate);
    }

    @Override
    public int compareTo(DummyCutView o) {
        return this.cutID - o.cutID;
    }

}
