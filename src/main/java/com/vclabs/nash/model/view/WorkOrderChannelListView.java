/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import java.util.HashMap;

/**
 *
 * @author user
 */
public class WorkOrderChannelListView {

    private int orderedchannellistid;
    private int workorderid;
    private int channelid;
    private String channelname;

    private int numbertofspot;
    private int bonusSpots;
    private double tvcPackage;

    private int logoSpots;
    private int logo_B_spots;
    private double logoPackage;

    private int crowlerSpots;
    private int crowler_B_Spots;
    private double crowlerPackage;

    private int v_sqeezeSpots;
    private int v_sqeeze_B_Spots;
    private double vSqeezePackage;

    private int l_sqeezeSpots;
    private int l_sqeeze_B_Spots;
    private double lSqeezePackage;

    private double packageValue;
    private double averagePackageValue;
    
    private HashMap<String, Integer> tvcSpotCountList = new HashMap<>();
    private HashMap<String, Integer> tvcBonusSpotCountList = new HashMap<>();

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public int getOrderedchannellistid() {
        return orderedchannellistid;
    }

    public void setOrderedchannellistid(int orderedchannellistid) {
        this.orderedchannellistid = orderedchannellistid;
    }

    public int getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(int workorderid) {
        this.workorderid = workorderid;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public int getNumbertofspot() {
        return numbertofspot;
    }

    public void setNumbertofspot(int numbertofspot) {
        this.numbertofspot = numbertofspot;
    }

    public int getBonusSpots() {
        return bonusSpots;
    }

    public void setBonusSpots(int bonusSpots) {
        this.bonusSpots = bonusSpots;
    }

    public int getLogoSpots() {
        return logoSpots;
    }

    public void setLogoSpots(int logoSpots) {
        this.logoSpots = logoSpots;
    }

    public int getLogo_B_spots() {
        return logo_B_spots;
    }

    public void setLogo_B_spots(int logo_B_spots) {
        this.logo_B_spots = logo_B_spots;
    }

    public int getCrowlerSpots() {
        return crowlerSpots;
    }

    public void setCrowlerSpots(int crowlerSpots) {
        this.crowlerSpots = crowlerSpots;
    }

    public int getCrowler_B_Spots() {
        return crowler_B_Spots;
    }

    public void setCrowler_B_Spots(int crowler_B_Spots) {
        this.crowler_B_Spots = crowler_B_Spots;
    }

    public int getV_sqeezeSpots() {
        return v_sqeezeSpots;
    }

    public void setV_sqeezeSpots(int v_sqeezeSpots) {
        this.v_sqeezeSpots = v_sqeezeSpots;
    }

    public int getV_sqeeze_B_Spots() {
        return v_sqeeze_B_Spots;
    }

    public void setV_sqeeze_B_Spots(int v_sqeeze_B_Spots) {
        this.v_sqeeze_B_Spots = v_sqeeze_B_Spots;
    }

    public int getL_sqeezeSpots() {
        return l_sqeezeSpots;
    }

    public void setL_sqeezeSpots(int l_sqeezeSpots) {
        this.l_sqeezeSpots = l_sqeezeSpots;
    }

    public int getL_sqeeze_B_Spots() {
        return l_sqeeze_B_Spots;
    }

    public void setL_sqeeze_B_Spots(int l_sqeeze_B_Spots) {
        this.l_sqeeze_B_Spots = l_sqeeze_B_Spots;
    }

    public double getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(double packageValue) {
        this.packageValue = packageValue;
    }

    public double getAveragePackageValue() {
        return averagePackageValue;
    }

    public void setAveragePackageValue(double averagePackageValue) {
        this.averagePackageValue = averagePackageValue;
    }

    public double getTvcPackage() {
        return tvcPackage;
    }

    public void setTvcPackage(double tvcPachage) {
        this.tvcPackage = tvcPachage;
    }

    public double getLogoPackage() {
        return logoPackage;
    }

    public void setLogoPackage(double logoPackage) {
        this.logoPackage = logoPackage;
    }

    public double getCrowlerPackage() {
        return crowlerPackage;
    }

    public void setCrowlerPackage(double crowlerPackage) {
        this.crowlerPackage = crowlerPackage;
    }

    public double getvSqeezePackage() {
        return vSqeezePackage;
    }

    public void setvSqeezePackage(double vSqeezePackage) {
        this.vSqeezePackage = vSqeezePackage;
    }

    public double getlSqeezePackage() {
        return lSqeezePackage;
    }

    public void setlSqeezePackage(double lSqeezePackage) {
        this.lSqeezePackage = lSqeezePackage;
    }

    public HashMap<String, Integer> getTvcSpotCountList() {
        return tvcSpotCountList;
    }

    public void setTvcSpotCountList(HashMap<String, Integer> tvcSpotCountList) {
        this.tvcSpotCountList = tvcSpotCountList;
    }

    public HashMap<String, Integer> getTvcBonusSpotCountList() {
        return tvcBonusSpotCountList;
    }

    public void setTvcBonusSpotCountList(HashMap<String, Integer> tvcBonusSpotCountList) {
        this.tvcBonusSpotCountList = tvcBonusSpotCountList;
    }

}
