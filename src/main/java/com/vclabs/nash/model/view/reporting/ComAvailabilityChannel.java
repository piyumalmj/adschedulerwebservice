/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author hp
 */
public class ComAvailabilityChannel {

    private int channelId;
    private String channelName;
    private Date date = new Date();
    HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = new HashMap<Integer, ComAvailabilityHourDetails>();

    public ComAvailabilityChannel() {
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public HashMap<Integer, ComAvailabilityHourDetails> getHourDetailsMap() {
        return hourDetailsMap;
    }

    public void setHourDetailsMap(HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap) {
        this.hourDetailsMap = hourDetailsMap;
    }

}
