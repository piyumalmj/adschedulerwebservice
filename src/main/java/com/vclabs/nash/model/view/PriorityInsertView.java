/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */


package com.vclabs.nash.model.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Sanira Nanayakkara
 */
public class PriorityInsertView {
    private int iTimebeltID;
    private int iCluster;
    private int iWorkOrderID;
    private int iAdvertisementID;
    private int iPriority;    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dtStratDate;    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dtEndDate;
    private String stDays;
    private boolean overWrite;
    private int ichannelID;

    public int getiTimebeltID() {    
        return iTimebeltID;
    }
            
    public void setiTimebeltID(int iTimbeltID) {
        this.iTimebeltID = iTimbeltID;
    }

    public int getiCluster() {
        return iCluster;
    }

    public void setiCluster(int iCluster) {
        this.iCluster = iCluster;
    }

    public int getiWorkOrderID() {
        return iWorkOrderID;
    }

    public void setiWorkOrderID(int iWorkOrderID) {
        this.iWorkOrderID = iWorkOrderID;
    }

    public int getiAdvertisementID() {
        return iAdvertisementID;
    }

    public void setiAdvertisementID(int iAdvertisementID) {
        this.iAdvertisementID = iAdvertisementID;
    }

    public int getiPriority() {
        return iPriority;
    }

    public void setiPriority(int iPriority) {
        this.iPriority = iPriority;
    }

    public Date getDtStratDate() {
        return dtStratDate;
    }

    public void setDtStratDate(Date dtStratDate) {
        this.dtStratDate = dtStratDate;
    }

    public Date getDtEndDate() {
        return dtEndDate;
    }

    public void setDtEndDate(Date dtEndDate) {
        this.dtEndDate = dtEndDate;
    }

    public String getStDays() {
        return stDays;
    }

    public void setStDays(String stDays) {
        this.stDays = stDays;
    }

    public boolean isOverWrite() {
        return overWrite;
    }

    public void setOverWrite(boolean overWrite) {
        this.overWrite = overWrite;
    }

    public int getIchannelID() {
        return ichannelID;
    }

    public void setIchannelID(int ichannelID) {
        this.ichannelID = ichannelID;
    }
    
    
}
