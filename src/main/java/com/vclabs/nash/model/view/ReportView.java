/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

/**
 *
 * @author user
 */
public class ReportView implements Comparable<ReportView> {

    private String client;
    private String channelName;
    private int workOrderId;
    private String advertName;
    private String date;
    private String scheduleStartTime;
    private String scheduleEndTime;
    private String actualTime = "00:00:00";
    private String status;

    public ReportView() {
    }

    public ReportView(String client, String channelName, int workOrderId, String advertName, String date, String ScheduleStartTime, String actualTime, String status) {
        this.client = client;
        this.channelName = channelName;
        this.workOrderId = workOrderId;
        this.advertName = advertName;
        this.date = date;
        this.scheduleStartTime = ScheduleStartTime;
        this.actualTime = actualTime;
        this.status = status;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String ScheduleStartTime) {
        this.scheduleStartTime = ScheduleStartTime;
    }

    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String ScheduleEndTime) {
        this.scheduleEndTime = ScheduleEndTime;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compareTo(ReportView o) {
        String thisdate = this.actualTime.replace(":", "");
        int thisdateINT = Integer.parseInt(thisdate);
        String date = o.actualTime.replace(":", "");
        int dateINT = Integer.parseInt(date);
        return thisdateINT - dateINT;
    }

}
