/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hp
 */
public class ClientOrAgencyList {

    private String clientOrAgencyName;
    private int month;
    private String monthName;
    private double grandTotal = 0;
    private Map<Integer, ClientOrAgencyValue> clientAgencyList = new TreeMap();

    public ClientOrAgencyList() {

    }

    public String getClientOrAgencyName() {
        return clientOrAgencyName;
    }

    public void setClientOrAgencyName(String clientOrAgencyName) {
        this.clientOrAgencyName = clientOrAgencyName;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public double getGrendTotal() {
        return grandTotal;
    }

    public void addGrendTotal(double value) {
        this.grandTotal += value;
    }

    public void setGrendTotal(double grendTotal) {
        this.grandTotal = grendTotal;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Map<Integer, ClientOrAgencyValue> getClientAgencyList() {
        return clientAgencyList;
    }

    public void setClientAgencyList(Map<Integer, ClientOrAgencyValue> clientAgencyList) {
        this.clientAgencyList = clientAgencyList;
    }

    public void setGrandTotal() {
        for (Entry<Integer, ClientOrAgencyValue> entry : this.clientAgencyList.entrySet()) {
            this.grandTotal += entry.getValue().getValue();
        }
    }

}
