/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 *
 * @author user
 */
public class ScheduleHomeView implements Comparable<ScheduleHomeView> {

    private int workorderid;
    private String workOrderName;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date scheduleStartDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date scheduleEndDate;
    private int availableAdvert;
    private int permissionstatus;
    private int airSpots;
    private int billingStatus;

    public int getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(int workorderid) {
        this.workorderid = workorderid;
    }

    public String getWorkOrderName() {
        return workOrderName;
    }

    public void setWorkOrderName(String workOrderName) {
        this.workOrderName = workOrderName;
    }

    public Date getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Date scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public int getAvailableAdvert() {
        return availableAdvert;
    }

    public void setAvailableAdvert(int availableAdvert) {
        this.availableAdvert = availableAdvert;
    }

    public int getPermissionstatus() {
        return permissionstatus;
    }

    public void setPermissionstatus(int permissionstatus) {
        this.permissionstatus = permissionstatus;
    }

    public int getAirSpots() {
        return airSpots;
    }

    public void setAirSpots(int airSpots) {
        this.airSpots = airSpots;
    }

    public int getBillingStatus() {
        return billingStatus;
    }

    public void setBillingStatus(int billingStatus) {
        this.billingStatus = billingStatus;
    }

    @Override
    public int compareTo(ScheduleHomeView o) {
        return o.getWorkorderid() - this.getWorkorderid();
    }

}
