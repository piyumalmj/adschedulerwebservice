package com.vclabs.nash.model.view;

/**
 * @author Sanira Nanayakkara
 */
public class PreScheduleReplaceView {
    private int channelId;
    private String channelName;
    private long requaredDuration;
    private long availableDuration;
    private int totalReplaceCount;
    private int validReplaceCount;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public long getRequaredDuration() {
        return requaredDuration;
    }

    public void setRequaredDuration(long requaredDuration) {
        this.requaredDuration = requaredDuration;
    }

    public long getAvailableDuration() {
        return availableDuration;
    }

    public void setAvailableDuration(long availableDuration) {
        this.availableDuration = availableDuration;
    }

    public int getTotalReplaceCount() {
        return totalReplaceCount;
    }

    public void setTotalReplaceCount(int totalReplaceCount) {
        this.totalReplaceCount = totalReplaceCount;
    }

    public int getValidReplaceCount() {
        return validReplaceCount;
    }

    public void setValidReplaceCount(int validReplaceCount) {
        this.validReplaceCount = validReplaceCount;
    }
}
