package com.vclabs.nash.model.view;

/**
 * Created by Nalaka on 2019-04-08.
 */
public class MoshMsg {

    private int id;
    private Boolean status;

    public MoshMsg() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
