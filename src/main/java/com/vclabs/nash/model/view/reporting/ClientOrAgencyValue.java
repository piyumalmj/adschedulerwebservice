/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import java.math.BigDecimal;

/**
 *
 * @author hp
 */
public class ClientOrAgencyValue {

    private String clientOrAgencyName;
    private int year;
    private int month;
    private String monthName;
    private double value = 0;

    public ClientOrAgencyValue() {

    }

    public String getClientOrAgencyName() {
        return clientOrAgencyName;
    }

    public void setClientOrAgencyName(String clientOrAgencyName) {
        this.clientOrAgencyName = clientOrAgencyName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
        setMonthName(month);
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public double getValue() {
        return value;
    }

    public void addValue(double value) {
        this.value += value;
    }

//    public void addValue(BigDecimal value) {
//        this.value = this.value.add(value);
//    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setMonthName(int month) {
        switch (month) {
            case 1:
                this.monthName = "January";
                break;

            case 2:
                this.monthName = "February";
                break;

            case 3:
                this.monthName = "March";
                break;

            case 4:
                this.monthName = "April";
                break;

            case 5:
                this.monthName = "May";
                break;

            case 6:
                this.monthName = "June";
                break;
            case 7:
                this.monthName = "July";
                break;

            case 8:
                this.monthName = "August";
                break;

            case 9:
                this.monthName = "September";
                break;

            case 10:
                this.monthName = "October";
                break;

            case 11:
                this.monthName = "November";
                break;

            case 12:
                this.monthName = "December";
                break;
            default:
                this.monthName = "NO month";
                break;
        }
    }
}
