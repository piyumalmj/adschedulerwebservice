/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

/**
 *
 * @author hp
 */
public class ComAvailabilityHourDetails {

    private int hour = 0;
    private int advertCount = 0;//value=1
    private int advertDuration = 0;//value=2
    private int inventryUtilization = 0;//value=3
    private int actualUtilization = 0;//value=4
    private long totalScheduleTime = 0;

    private String time = "";

    private Boolean isRecordAvailable = Boolean.FALSE;

    public ComAvailabilityHourDetails() {
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getAdvertCount() {
        return advertCount;
    }

    public void setAdvertCount(int advertCount) {
        this.advertCount = advertCount;
    }

    public int getInventryUtilization() {
        return inventryUtilization;
    }

    public void setInventryUtilization(int inventryUtilization) {
        this.inventryUtilization = inventryUtilization;
    }

    public int getActualUtilization() {
        return actualUtilization;
    }

    public void setActualUtilization(int actualUtilization) {
        this.actualUtilization = actualUtilization;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getIsRecordAvailable() {
        return isRecordAvailable;
    }

    public void setIsRecordAvailable(Boolean isRecordAvailable) {
        this.isRecordAvailable = isRecordAvailable;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public void addAdvertCount() {
        advertCount++;
    }

    public void addAdvertDuration(int duration) {
        advertDuration += duration;
    }

    public long getTotalScheduleTime() {
        return totalScheduleTime;
    }

    public void setTotalScheduleTime(long totalScheduleTime) {
        this.totalScheduleTime = totalScheduleTime;
    }

    public void calulateTimeUtilization() {
        if (totalScheduleTime != 0) {
            Long utilizationPrecentage = (advertDuration * 100 / totalScheduleTime);
            inventryUtilization = utilizationPrecentage.intValue();
        }
    }

    public void calulateActualUtilization() {
        if (inventryUtilization != 0) {
            actualUtilization = 100 - inventryUtilization;
        }
    }

}
