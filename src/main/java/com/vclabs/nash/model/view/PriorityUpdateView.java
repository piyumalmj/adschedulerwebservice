/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;

/**
 * @author Sanira Nanayakkara
 */
public class PriorityUpdateView implements Comparable<PriorityUpdateView>{

    private long priorityId;
    private Integer timebeltId;
    private int cluster;
    private int priorityCount;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date startTime;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date endTime;

    public Integer getTimebeltId() {
        return timebeltId;
    }

    public void setTimebeltId(Integer timebeltId) {
        this.timebeltId = timebeltId;
    }

    public int getCluster() {
        return cluster;
    }

    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    public int getPriorityCount() {
        return priorityCount;
    }

    public void setPriorityCount(int priority) {
        this.priorityCount = priority;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    ///////////for groupby cluster in priorityservice class //////////////////////////////////
    @Override
    public int hashCode() {
        int hash = 0;
        hash += timebeltId;
        hash += cluster;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PriorityUpdateView)) {
            return false;
        }
        
        PriorityUpdateView other = (PriorityUpdateView) object;
        if ((this.timebeltId == other.timebeltId) && (this.cluster == other.cluster)) {
            return true;
        }
        return false;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int compareTo(PriorityUpdateView o) {
        if(this.timebeltId == o.timebeltId)
            return this.cluster - o.cluster;
        else
            return this.timebeltId - o.timebeltId;
    }

    public long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(long priorityId) {
        this.priorityId = priorityId;
    }
}
