/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
public class WorkOrderUpdateView {

    private int workOrderId;
    private String seller;
    private String agency;
    private String clientName;
    private String billingClient;
    private String agencyByName;
    private String clientByName;
    private String billingClientByName;
    private String productName;
    private String revenueMonth;
    private String revenueLine;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date startdate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date enddate;
    private String comment;
    private int b_spots_visibility;
    private double totalbudget;
    private int commission;
    private String woTyep;
    private String scheduleRef;
    private List<WorkOrderChannelListView> channelList;
    private int billStatus;
    private int workOrderStatus;
    private String manualStatus;

    public WorkOrderUpdateView(int workOrderId, String seller, String clientName, String productName, Date startdate, Date enddate, String comment, List<WorkOrderChannelListView> channelList) {
        this.workOrderId = workOrderId;
        this.seller = seller;
        this.clientName = clientName;
        this.productName = productName;
        this.startdate = startdate;
        this.enddate = enddate;
        this.comment = comment;
        this.channelList = channelList;
    }

    public WorkOrderUpdateView() {
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public int getB_spots_visibility() {
        return b_spots_visibility;
    }

    public void setB_spots_visibility(int b_spots_visibility) {
        this.b_spots_visibility = b_spots_visibility;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<WorkOrderChannelListView> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<WorkOrderChannelListView> channelList) {
        this.channelList = channelList;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agecny) {
        this.agency = agecny;
    }

    public String getBillingClient() {
        return billingClient;
    }

    public void setBillingClient(String billingClient) {
        this.billingClient = billingClient;
    }

    public double getTotalbudget() {
        return totalbudget;
    }

    public void setTotalbudget(double totalbudget) {
        this.totalbudget = totalbudget;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public String getWoTyep() {
        return woTyep;
    }

    public void setWoTyep(String woTyep) {
        this.woTyep = woTyep;
    }

    public String getScheduleRef() {
        return scheduleRef;
    }

    public void setScheduleRef(String scheduleRef) {
        this.scheduleRef = scheduleRef;
    }

    public int getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(int billStatus) {
        this.billStatus = billStatus;
    }

    public int getWorkOrderStatus() {
        return workOrderStatus;
    }

    public void setWorkOrderStatus(int workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    public String getAgencyByName() {
        return agencyByName;
    }

    public void setAgencyByName(String agencyByName) {
        this.agencyByName = agencyByName;
    }

    public String getClientByName() {
        return clientByName;
    }

    public void setClientByName(String clientByName) {
        this.clientByName = clientByName;
    }

    public String getBillingClientByName() {
        return billingClientByName;
    }

    public void setBillingClientByName(String billingClientByName) {
        this.billingClientByName = billingClientByName;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public String getManualStatus() {
        return manualStatus;
    }

    public void setManualStatus(String manualStatus) {
        this.manualStatus = manualStatus;
    }

    public String getRevenueLine() {
        return revenueLine;
    }

    public void setRevenueLine(String revenueLine) {
        this.revenueLine = revenueLine;
    }

}
