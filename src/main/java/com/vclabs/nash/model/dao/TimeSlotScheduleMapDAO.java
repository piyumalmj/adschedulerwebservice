/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.TimeSlotScheduleMap;

import java.util.List;

/**
 *
 * @author hp
 */
public interface TimeSlotScheduleMapDAO {

    List<TimeSlotScheduleMap> getSelectedWorkOrderTimeSlot(int workOrder);

    TimeSlotScheduleMap getSelectedTimeSlot(int scheduleID);
}
