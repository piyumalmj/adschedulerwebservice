package com.vclabs.nash.model.dao.product;

import com.vclabs.nash.model.entity.product.CodeMapping;

import java.util.List;

/**
 * Created by dperera on 29/08/2019.
 */
public interface CodeMappingDAO {

    CodeMapping save(CodeMapping codeMapping);

    List<CodeMapping> findAllCodeMapping(int size, int offset,String clientId, String productName, String productId);

    int findAllCodeMappingCount(String clientId, String productName, String productId);

    List<CodeMapping> findByClientId(long clientId);

    List<CodeMapping> findByCode(String code);

    CodeMapping findById(long id);
}
