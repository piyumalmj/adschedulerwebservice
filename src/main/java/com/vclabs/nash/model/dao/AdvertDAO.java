package com.vclabs.nash.model.dao;

import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;

import java.util.List;

/**
 * Created by Sanduni on 24/01/2019
 */
public interface AdvertDAO {

    List<ChannelPredictionDto> findAll();
}
