/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.PlayList;

import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
public interface PlayListDAO {

    List<PlayList> getPlayList(int _channelId, Date _date);

    List<PlayList> getScheduledPlayList(int _channelId, int _hour) throws Exception;

    List<PlayList> getScheduledPlayList(int _channelId, Date start_time, Date end_time, int _hour) throws Exception;

    List<PlayList> getPlayListAdvert(int _channelId, int _hour);

    List<PlayList> getPlayListAdvert(int _channelId, Date start_time, Date end_time, int _hour);

    List<PlayList> getPlayListCrawler(int _channelId, int _hour);

    List<PlayList> getPlayListCrawler(int _channelId, Date start_time, Date end_time, int _hour);

    List<PlayList> getPlayListLogo(int _channelId, int _hour);

    List<PlayList> getPlayListLogo(int _channelId, Date start_time, Date end_time, int _hour);

    PlayList getPlayList(int _playListId);

    boolean insertPlayList(PlayList _playList);

    boolean updatePlayList(PlayList _playList);

    boolean saveOrUpdate(PlayList playList);

    boolean updateSchedulesValidity(int _channelId, Date start_time, Date end_time, int hour);

    boolean clearTable();

    List<PlayList> getSelectedHourValidAdvert(int _channelId, int _hour);
}
