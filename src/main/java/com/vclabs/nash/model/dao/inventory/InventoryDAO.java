package com.vclabs.nash.model.dao.inventory;

import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryPriceRange;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */

public interface InventoryDAO {//

    Inventory save(Inventory inventory);

    InventoryRow save(InventoryRow inventoryRow);

    InventoryPriceRange save(InventoryPriceRange inventoryPriceRange);

    Inventory findByChannelId(int channelId);

    Inventory findByChannelIdAndDate(int channelId, Date date);

    int isExistInventory(int channelId, Date date);

    List<Date> findInventoryExistDates(int channelId, Date fromDate, Date toDate);
}
