package com.vclabs.nash.model.dao.Advertisement;

import com.vclabs.nash.model.entity.AdvertSetting;

import java.util.List;

/**
 * Created by dperera on 18/09/2019.
 */
public interface AdvertSettingDAO {

    AdvertSetting save(AdvertSetting advertSetting);

    boolean hasIntersection(AdvertSetting advertSetting);

    AdvertSetting finMaxAllowedSlots(int cid, int duration);

    List<AdvertSetting> finByChannelId(int cid);

    Boolean delete(AdvertSetting advertSetting);

}
