package com.vclabs.nash.model.dao.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 11/10/2018
 */
public interface CentralizedInventoryPredictionDAO {

    CentralizedInventoryPrediction save(CentralizedInventoryPrediction centralizedInventoryPrediction);

    CentralizedInventoryPrediction findCentralizedInventoryPrediction(Date dateAndTime, Integer channelId);

    List<CentralizedInventoryPrediction> findUpdatedInventory();

    List<CentralizedInventoryPrediction> findByDateAndChannel(Date date, Integer channelId);

}
