package com.vclabs.nash.model.dao.Teams;

import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.model.entity.teams.TeamDetail;

import java.util.List;

/**
 * Created by Sanduni on 03/01/2019
 */
public interface TeamDetailDAO {

    TeamDetail save(TeamDetail teamDetail);

    TeamDetail findById(Integer id);

    List<TeamDetail> findAll();

    List<TeamDetail> findByTeamName(String teamName);

    List<String> findAllMembersOfTeam(Integer teamId);

    List<String> findAllAdminsOfTeam(Integer teamId);

    Boolean deleteMember(Integer teamId, String username);

    Boolean deleteAdmin(Integer teamId, String username);

    List<MyTeamToDoListDto> getTeamMembersWODetails();
}
