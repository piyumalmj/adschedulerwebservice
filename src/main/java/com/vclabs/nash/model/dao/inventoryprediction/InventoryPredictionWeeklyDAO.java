package com.vclabs.nash.model.dao.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
public interface InventoryPredictionWeeklyDAO {

    InventoryWeeklyPrediction save(InventoryWeeklyPrediction inventoryWeeklyPrediction);

    InventoryWeeklyPrediction update(InventoryWeeklyPrediction inventoryWeeklyPrediction);

    List<InventoryWeeklyPrediction> findAll();

    List<InventoryWeeklyPrediction> findLatestWeeklyPrediction();

    List<InventoryWeeklyPrediction> findLatestPredictions(Date startingDate, Date lastDate);

}
