/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.dao;

import java.util.List;

import com.vclabs.nash.dashboard.dto.*;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.FiltersModel;
import com.vclabs.nash.model.process.dto.WODropDownDataDto;

/**
 *
 * @author user
 */
public interface WorkOrderDAO { //tested

    List<WorkOrder> getActiveWorkOrederList();

    List<WorkOrder> getWorkOrederList(String seller);

    List<WorkOrder> getAllWorkOrderList();

    List<WODropDownDataDto> getWODropDownData();

    List<WorkOrder> getWorkOrderListForChackStatus();

    List<Integer> getWorkOrderListForCheckStatus();

    int saveWorkOrder(WorkOrder model);

    Boolean upadteWorkOrder(WorkOrder model); //tested

    Boolean deleteWorkOrder(WorkOrder model);

    WorkOrder getSelectedWorkOrder(int workOrderId); //tested

    List<WorkOrder> getIsExistingWorkOrder(String refe); //tested

    List<WorkOrder> getIsExistingWorkOrder(String refe, int workOrderId); //tested

    int getAllWorkOrderCount(int woID, FiltersModel filter); //tested

    List<WorkOrder> getLimitedWorkOrderList(int size, int offset, int woID, FiltersModel filter); //tested

    List<WorkOrder> getLimitedWorkOrderListForSchedule(int size, int offset, int woID, FiltersModel filter); //tested

    List<WorkOrder> getLimitedWorkOrderListForBilling(int size, int offset, FiltersModel filter); //tested

    int getAllWorkOrderCountForSchedule(int woID, FiltersModel filter); //tested

    int getAllWorkOrderCountForBilling(FiltersModel filter); //tested

    /*--------------------------------Client Revenue Report-----------------------------------------*/
    List<WorkOrder> getSelectedClientWorkOrder(int clientId, List<Integer> clientIDs);

    /*--------------------------------Seller Report-----------------------------------------*/
    List<WorkOrder> getSelectedUserWorkOrederList(List<String> woType, List<String> meList, List<String> monthList, List<Integer> clientIntList, List<Integer> agencyIntList);

    /*----------------------------------Dashboard core---------------------------*/

    List<WorkOrder> findPenddingWorkOrderByUser(String user);

    List<WorkOrder> findPenddingWorkOrderByRevenueMonth(String month);

    List<PendingWOCountByAutoStatusDto> findAllPendingWOAutoStatusCount();

    List<PendingWOCountByManualStatusDto> findAllPendingWOManualStatusCount();

    List<WorkOrder> findCompletedSchedulesWorkOrderByUser(String user);

    List<WorkOrder> findNewWorkOrders(String seller);

    List<String> findNewWorkOrdersSellers();

    List<WorkOrder> findRevisedWorkOrders();

    List<SchedulesReadyForInvoicingDto> findSchedulesReadyForInvoicing();

    List<WorkOrder> findPartiallyEnteredWorkOrders();

    double getTotalRevenueForMonthBySeller(String month, String seller);

    List<MyRecordedRevenueDto> findMyRecordedRevenue(String userName);

    List<String> findAllSellers();

    List<MyCompletedSchedulesDto> findMyCompletedSchedules();

    List<SalesRevenueDTO> findSelecteUserAllWorkOrder(String user);

    List<RevenueLineDbDTO> findAllWorkOrderForRevenueLine();

}
