package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.OPSResponseData;

/**
 * Created by Nalaka on 2018-09-25.
 */
public interface OPSResponseDataDAO {

    OPSResponseData save(OPSResponseData oPSResponseData);

    OPSResponseData findById(long id);

    OPSResponseData update (OPSResponseData oPSResponseData);

}
