package com.vclabs.nash.model.dao.product;

import com.vclabs.nash.model.entity.product.Product;

import java.util.List;

/**
 * Created by dperera on 29/08/2019.
 */
public interface ProductDAO {

    void save(Product product);

    Product findByName(String name);

    List<Product> findAll();

}
