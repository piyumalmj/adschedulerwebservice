package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.ZeroAdsPlayList;

import java.util.List;

/**
 * Created by Nalaka on 2018-09-18.
 */
public interface ZeroAdsPlayListDAO {

    Boolean saveZeroAdsPlayList(ZeroAdsPlayList model);

    Boolean updateZeroAdsPlayList(ZeroAdsPlayList model);

    List<ZeroAdsPlayList> getPlayListByChannel(int channelId);

    List<ZeroAdsPlayList> getPlayListByAdvert(int advertId);

    List<ZeroAdsPlayList> getValidManualPlayListByChannel(int channelId);
}
