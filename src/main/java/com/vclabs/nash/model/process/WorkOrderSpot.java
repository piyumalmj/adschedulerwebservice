/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

import java.util.ArrayList;
import java.util.List;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.view.WorkOrderChannelScheduleView;

/**
 *
 * @author user
 */
public class WorkOrderSpot {

    private int workOrderId;
    private List<SpotValidationModel> validateList = new ArrayList<>();

    public WorkOrderSpot() {
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public List<SpotValidationModel> getValidateList() {
        return validateList;
    }

    public void setValidateList(List<SpotValidationModel> validateList) {
        this.validateList = validateList;
    }

    public void setChannelSpotCount(ScheduleDef model) {

        Boolean added = true;
        for (int i = 0; i < validateList.size(); i++) {
            if (validateList.get(i).getChannelID() == model.getChannelid().getChannelid()) {
                if (validateList.get(i).getAdvertDuration() == model.getAdvertid().getDuration()) {
                    validateList.get(i).addSpotForRE();
                    added = false;
                }
            }
        }

        if (added) {
            SpotValidationModel spotModel = new SpotValidationModel();
            spotModel.setChannelID(model.getChannelid().getChannelid());
            spotModel.setAdvertDuration(model.getAdvertid().getDuration());
            spotModel.addSpotForRE();
            validateList.add(spotModel);
        }
    }

    public void setChannelAvailabeSpot(WorkOrderChannelScheduleView model, WorkOrderChannelList breakDownModel) {
        for (int i = 0; i < breakDownModel.getTvcSpots().size(); i++) {
            boolean added = true;
            for (int j = 0; j < validateList.size(); j++) {
                if ((validateList.get(j).getAdvertDuration() == breakDownModel.getTvcSpots().get(i).getDuration()) && (validateList.get(j).getChannelID() == breakDownModel.getChannelid().getChannelid())) {
                    added = false;
                }
            }
            if (added) {
                SpotValidationModel spotModel = new SpotValidationModel();
                spotModel.setChannelID(breakDownModel.getChannelid().getChannelid());
                spotModel.setAdvertDuration(breakDownModel.getTvcSpots().get(i).getDuration());
                spotModel.setAvailableSpot(model.getTvcBreackdownMapAvailable().get(breakDownModel.getTvcSpots().get(i).getDuration()));
                spotModel.setTotalAvailableDuration(model.getTotalTVCDuration());
                validateList.add(spotModel);
            }
        }
    }
}
