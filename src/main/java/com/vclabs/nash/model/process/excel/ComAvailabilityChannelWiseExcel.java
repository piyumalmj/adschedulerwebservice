/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.view.reporting.ComAvailabilityChannel;
import com.vclabs.nash.model.view.reporting.ComAvailabilityHourDetails;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author hp
 */
@Component
public class ComAvailabilityChannelWiseExcel extends Common {

    private final String WEBDIRECTORY = "ComAvailabilityChannelWiseExcel";
    private final String FILEPATH = "ComAvailabilityChannelWiseReport.xls";

    public void generateComAvailabilityChannelWiseExcel(int value, HttpServletRequest request, Map<String, ComAvailabilityChannel> comAvailabilityChannelList) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("ComAvailabilityChannelWise");
        /*Create row*/
        HSSFRow channelRow = sheet.createRow((short) 0);
        channelRow.createCell(0).setCellValue("Channel:");
        channelRow.createCell(1).setCellValue(comAvailabilityChannelList.entrySet().iterator().next().getValue().getChannelName());

        HSSFRow rowhead = sheet.createRow((short) 1);
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead, comAvailabilityChannelList);

        ArrayList timeMap = getTimeBelt(comAvailabilityChannelList);
        int rowCount = 2;

        for (int i = 0; i < timeMap.size(); i++) {
            HSSFRow row = sheet.createRow((short) rowCount);
            row.createCell(0).setCellValue((String) timeMap.get(i));
            int colCount = 1;
            for (Map.Entry<String, ComAvailabilityChannel> entry : comAvailabilityChannelList.entrySet()) {
                HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = entry.getValue().getHourDetailsMap();
                String time = (String) timeMap.get(i);
                int hour = Integer.parseInt(time.split(":")[0]);
                ComAvailabilityHourDetails comAvailabilityHourDetails = hourDetailsMap.get(hour);
                switch (value) {
                    case 1:
                        row.createCell(colCount).setCellValue(comAvailabilityHourDetails.getAdvertCount());
                        break;
                    case 2:
                        row.createCell(colCount).setCellValue(comAvailabilityHourDetails.getAdvertDuration());
                        break;
                    case 3:
                        row.createCell(colCount).setCellValue(comAvailabilityHourDetails.getInventryUtilization());
                        break;
                    case 4:
                        row.createCell(colCount).setCellValue(comAvailabilityHourDetails.getActualUtilization());
                        break;
                    default:
                        row.createCell(colCount).setCellValue(comAvailabilityHourDetails.getAdvertCount());
                        break;
                }

                colCount++;
            }
            rowCount++;
        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead, Map<String, ComAvailabilityChannel> comAvailabilityChannelList) {
        rowhead.createCell(0).setCellValue("Time");

        int count = 1;
        if (!comAvailabilityChannelList.isEmpty()) {

            for (Map.Entry<String, ComAvailabilityChannel> entry : comAvailabilityChannelList.entrySet()) {
                rowhead.createCell(count).setCellValue(entry.getValue().getDate().toString());
                count++;
            }

        }

        return count;
    }

    public ArrayList getTimeBelt(Map<String, ComAvailabilityChannel> comAvailabilityChannelList) {
        ArrayList timeMap = new ArrayList();
        int count = 1;
        if (!comAvailabilityChannelList.isEmpty()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = comAvailabilityChannelList.entrySet().iterator().next().getValue().getHourDetailsMap();
            for (Map.Entry<Integer, ComAvailabilityHourDetails> entry : hourDetailsMap.entrySet()) {
                timeMap.add(entry.getValue().getTime());
            }

        }

        return timeMap;
    }

    public void downloadComAvailabilityChannelWiseExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
