/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
public class SAPData {

    private String salesOrderID = "";
    private String invoiceType = "";
    private String contactPerson_attn = "";
    private String endUser = "";
    private String materialDescription = "";
    private String conditionType_ZCTR = "";
    private String conditionType_Z001 = "";
    private String conditionType_ZNBT = "";
    private String billingDateFrom = "";
    private String billingDateTo = "";
    private String agencyName = "";
    private String agencyAddress = "";
    private String channel = "";
    private String productORBrand = "";
    private String clientName = "";
    private String mediaAgentName = "";
    /*|No of Spots|5s|10s|15s|20s|30s|60s|120s*/

    public SAPData() {
    }

    public String getSalesOrderID() {
        return salesOrderID;
    }

    public void setSalesOrderID(String salesOrderID) {
        this.salesOrderID = salesOrderID;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getContactPerson_attn() {
        return contactPerson_attn;
    }

    public void setContactPerson_attn(String contactPerson_attn) {
        this.contactPerson_attn = contactPerson_attn;
    }

    public String getEndUser() {
        return endUser;
    }

    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    public String getMaterialDescription() {
        return materialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    public String getConditionType_ZCTR() {
        return conditionType_ZCTR;
    }

    public void setConditionType_ZCTR(String conditionType_ZCTR) {
        this.conditionType_ZCTR = conditionType_ZCTR;
    }

    public String getConditionType_Z001() {
        return conditionType_Z001;
    }

    public void setConditionType_Z001(String conditionType_Z001) {
        this.conditionType_Z001 = conditionType_Z001;
    }

    public String getConditionType_ZNBT() {
        return conditionType_ZNBT;
    }

    public void setConditionType_ZNBT(String conditionType_ZNBT) {
        this.conditionType_ZNBT = conditionType_ZNBT;
    }

    public String getBillingDateFrom() {
        return billingDateFrom;
    }

    public void setBillingDateFrom(String billingDateFrom) {
        this.billingDateFrom = billingDateFrom;
    }

    public String getBillingDateTo() {
        return billingDateTo;
    }

    public void setBillingDateTo(String billingDateTo) {
        this.billingDateTo = billingDateTo;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyAddress() {
        return agencyAddress;
    }

    public void setAgencyAddress(String agencyAddress) {
        this.agencyAddress = agencyAddress;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProductORBrand() {
        return productORBrand;
    }

    public void setProductORBrand(String productORBrand) {
        this.productORBrand = productORBrand;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getMediaAgentName() {
        return mediaAgentName;
    }

    public void setMediaAgentName(String mediaAgentName) {
        this.mediaAgentName = mediaAgentName;
    }

    public String getSAPdata() {
        return getSalesOrderID() + "|" + getInvoiceType() + "|" + getContactPerson_attn() + "|" + getEndUser() + "|" + getMaterialDescription() + "|" + getConditionType_ZCTR() + "|" + getConditionType_Z001() + "|" + getConditionType_ZNBT() + "|" + getBillingDateFrom() + "|" + getBillingDateTo() + "|" + getAgencyName() + "|" + getAgencyAddress() + "|" + getChannel() + "|" + getProductORBrand() + "|" + getClientName() + "|" + getMediaAgentName();
    }
}
