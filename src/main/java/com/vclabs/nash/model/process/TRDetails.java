/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
public class TRDetails {

    private String regNO = "";
    private String product = "";
    private String scheduleRef = "";
    private String advertiser = "";
    private String agency = "";
    private String marketingEx = "";
    private String channel = "";

    public TRDetails() {
    }

    public String getRegNO() {
        return regNO;
    }

    public void setRegNO(String regNO) {
        this.regNO = regNO;
    }

    public String getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(String advertiser) {
        this.advertiser = advertiser;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getMarketingEx() {
        return marketingEx;
    }

    public void setMarketingEx(String marketingEx) {
        this.marketingEx = marketingEx;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getScheduleRef() {
        return scheduleRef;
    }

    public void setScheduleRef(String scheduleRef) {
        this.scheduleRef = scheduleRef;
    }

}
