package com.vclabs.nash.model.process;

/**
 * Created by Nalaka on 2018-09-18.
 */
public class ZeroAdsSettingInfo {
    private String id;
    private int channelId;
    private String pListType;

    public ZeroAdsSettingInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getpListType() {
        return pListType;
    }

    public void setpListType(String pListType) {
        this.pListType = pListType;
    }
}
