/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author hp
 */
public class MissedSpotFilters {

    private String fromDate;
    private String toDate;
    private String channelIDs;
    private String hour;
    private int advertID;
    private int[] clientID;

    public MissedSpotFilters() {
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getChannelIDs() {
        return channelIDs;
    }

    public void setChannelIDs(String channelIDs) {
        this.channelIDs = channelIDs;
    }

    public int getAdvertID() {
        return advertID;
    }

    public void setAdvertID(int advertID) {
        this.advertID = advertID;
    }

    public int[] getClientID() {
        return clientID;
    }

    public void setClientID(int[] clientID) {
        this.clientID = clientID;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }
}
