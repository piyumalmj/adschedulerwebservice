package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author Sanira Nanayakkara
 */
public class CopySchedulesInfo {
    @JsonProperty("workOrderId")
    private int workOrderID;    
    @JsonProperty("advertID")
    private int advertID;     
    @JsonProperty("channelID")
    private int channelID; 
    @JsonProperty("startTime")
    private String startTime; 
    @JsonProperty("endTime")
    private String endTime; 
    @JsonProperty("lstSelectedSchedules")
    private List<Integer> selectedSchedules;
    @JsonProperty("revenueMonth")
    private String revenueMonth;
    @JsonProperty("revenueLine")
    private String revenueLine;

    public int getWorkOrderID() {
        return workOrderID;
    }

    public void setWorkOrderID(int workOrderID) {
        this.workOrderID = workOrderID;
    }

    public List<Integer> getSelectedSchedules() {
        return selectedSchedules;
    }

    public void setSelectedSchedules(List<Integer> selectedSchedules) {
        this.selectedSchedules = selectedSchedules;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public String getRevenueLine() {
        return revenueLine;
    }

    public void setRevenueLine(String revenueLine) {
        this.revenueLine = revenueLine;
    }

    public int getAdvertID() {
        return advertID;
    }

    public void setAdvertID(int advertID) {
        this.advertID = advertID;
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
