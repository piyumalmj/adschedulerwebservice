/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.process.ScheduleItemModel;
import com.vclabs.nash.model.view.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.stereotype.Component;


import static org.apache.poi.hssf.record.ExtendedFormatRecord.CENTER;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.CellStyle.ALIGN_CENTER;

/**
 *
 * @author hp
 */
@Component
public class SchedulePrintExcel extends Common {

    private final String WEBDIRECTORY = "SchedulePrint";
    private final String FILEPATH = "ScheduleDetails.xls";

    private CellStyle headerCellStyle;

    private CellStyle cellStyle;

    private CellStyle tableHeaderStyle;

    public void generateScheduleExcel(HttpServletRequest request, WorkOrderScheduleView workOrderScheduleView, List<AdvertisementDefView> advertisementDefViewList, List<WorkOrderChannelScheduleView> workOrderChannelScheduleViewList, WorkOrderUpdateView workOrderUpdateView) throws Exception {

        try {
            List<ScheduleUpdateView> scheduleUpdateViewList = workOrderScheduleView.getTimebeltSchedules();

            /*create work book*/
            HSSFWorkbook workbook = new HSSFWorkbook();

            headerCellStyle = scheduletableHeader(workbook);
            cellStyle = setDataRowCellStyle(workbook);
            tableHeaderStyle = tableHeaderStyle(workbook);
            /*create new sheel*/
            HSSFSheet sheet = workbook.createSheet("Scheduler");

            int rowCount = 2;
            /*-------------------WorkOrder Details-------------------*/
            HSSFRow woRow1 = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;
            cell = woRow1.createCell(0);
            cell.setCellValue("Wo Id");
            cell.setCellStyle(headerCellStyle);
            woRow1.createCell(1).setCellValue(workOrderUpdateView.getWorkOrderId());

            cell = woRow1.createCell(3);
            cell.setCellValue("Marketing Ex");
            cell.setCellStyle(headerCellStyle);
            woRow1.createCell(4).setCellValue(workOrderUpdateView.getSeller());

            rowCount++;
            HSSFRow woRow2 = sheet.createRow((short) rowCount);
            rowCount++;
            cell = woRow2.createCell(0);
            cell.setCellValue("Wo type");
            cell.setCellStyle(headerCellStyle);
            woRow2.createCell(1).setCellValue(workOrderUpdateView.getWoTyep());

            cell = woRow2.createCell(3);
            cell.setCellValue("Schedule ref");
            cell.setCellStyle(headerCellStyle);
            woRow2.createCell(4).setCellValue(workOrderUpdateView.getScheduleRef());

            rowCount++;
            HSSFRow woRow3 = sheet.createRow((short) rowCount);
            rowCount++;
            cell = woRow3.createCell(0);
            cell.setCellValue("Product");
            cell.setCellStyle(headerCellStyle);
            woRow3.createCell(1).setCellValue(workOrderUpdateView.getProductName());

            cell = woRow3.createCell(3);
            cell.setCellValue("Commission");
            cell.setCellStyle(headerCellStyle);
            woRow3.createCell(4).setCellValue(workOrderUpdateView.getCommission() + "%");

            rowCount++;
            HSSFRow woRow4 = sheet.createRow((short) rowCount);
            rowCount++;
            cell = woRow4.createCell(0);
            cell.setCellValue("Start");
            cell.setCellStyle(headerCellStyle);
            woRow4.createCell(1).setCellValue(workOrderUpdateView.getStartdate().toString());

            cell = woRow4.createCell(3);
            cell.setCellValue("End");
            cell.setCellStyle(headerCellStyle);
            woRow4.createCell(4).setCellValue(workOrderUpdateView.getEnddate().toString());

            rowCount++;
            HSSFRow woRow5 = sheet.createRow((short) rowCount);
            rowCount++;
            cell = woRow5.createCell(0);
            cell.setCellValue("Agency");
            cell.setCellStyle(headerCellStyle);
            woRow5.createCell(1).setCellValue(workOrderUpdateView.getAgencyByName());

            cell = woRow5.createCell(3);
            cell.setCellValue("Client");
            cell.setCellStyle(headerCellStyle);
            woRow5.createCell(4).setCellValue(workOrderUpdateView.getClientByName());

            cell = woRow5.createCell(6);
            cell.setCellValue("Schedule value");
            cell.setCellStyle(headerCellStyle);
            woRow5.createCell(7).setCellValue(workOrderUpdateView.getTotalbudget());

            /*-------------------Channels spots table-------------------*/
            rowCount++;
            HSSFRow channelhead = sheet.createRow((short) rowCount);
            channelTableHeaderFields(channelhead);
            rowCount++;
            for (WorkOrderChannelScheduleView workOrderChannelScheduleView : workOrderChannelScheduleViewList) {

                if (workOrderChannelScheduleView.getTvcfspot() != 0) {

                    HashMap<Integer, Integer> tvcBreackdownMap = workOrderChannelScheduleView.getTvcBreackdownMap();
                    HashMap<Integer, Integer> tvcBreackdownMapAvailable = workOrderChannelScheduleView.getTvcBreackdownMapAvailable();

                    for (Entry<Integer, Integer> tvcmap : tvcBreackdownMap.entrySet()) {
                        HSSFRow row = sheet.createRow((short) (rowCount));
                        rowCount++;

                        setChannelTableValue(row, workOrderChannelScheduleView.getChannelName(), "TVC", "" + tvcmap.getKey(), tvcmap.getValue() - tvcBreackdownMapAvailable.get(tvcmap.getKey()) + "/" + tvcmap.getValue());

                    }
                }

                if (workOrderChannelScheduleView.getLogoSpots() != 0) {
                    HSSFRow row = sheet.createRow((short) (rowCount));
                    rowCount++;

                    setChannelTableValue(row, workOrderChannelScheduleView.getChannelName(), "Logo", "0", workOrderChannelScheduleView.getLogoSpots() - workOrderChannelScheduleView.getAvailableLogoSpots() + "/" + workOrderChannelScheduleView.getLogoSpots());
                }
                if (workOrderChannelScheduleView.getCrowlerSpots() != 0) {
                    HSSFRow row = sheet.createRow((short) (rowCount));
                    rowCount++;

                    setChannelTableValue(row, workOrderChannelScheduleView.getChannelName(), "Crowler", "0", workOrderChannelScheduleView.getCrowlerSpots() - workOrderChannelScheduleView.getAvailableCrowlerSpots() + "/" + workOrderChannelScheduleView.getCrowlerSpots());
                }
                if (workOrderChannelScheduleView.getL_sqeezeSpots() != 0) {
                    HSSFRow row = sheet.createRow((short) (rowCount));
                    rowCount++;

                    setChannelTableValue(row, workOrderChannelScheduleView.getChannelName(), "L Sqeeze", "0", workOrderChannelScheduleView.getL_sqeezeSpots() - workOrderChannelScheduleView.getAvailableL_SqeezeSpots() + "/" + workOrderChannelScheduleView.getL_sqeezeSpots());
                }
                if (workOrderChannelScheduleView.getV_sqeezeSpots() != 0) {
                    HSSFRow row = sheet.createRow((short) (rowCount));
                    rowCount++;

                    setChannelTableValue(row, workOrderChannelScheduleView.getChannelName(), "L Sqeeze", "0", workOrderChannelScheduleView.getV_sqeezeSpots() - workOrderChannelScheduleView.getAvailableV_SqeezeSpots() + "/" + workOrderChannelScheduleView.getV_sqeezeSpots());
                }
            }

            rowCount++;
            /*-------------------Advertisement table-------------------*/
            HSSFRow adverthead = sheet.createRow((short) rowCount);
            AdvertTableHeaderFields(adverthead);
            rowCount++;
            for (AdvertisementDefView advertisementDefView : advertisementDefViewList) {
                HSSFRow row = sheet.createRow((short) (rowCount));
                cell = row.createCell(0);
                cell.setCellValue(advertisementDefView.getAdvertId());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(1);
                cell.setCellValue(advertisementDefView.getAdvertName());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(2);
                cell.setCellValue(advertisementDefView.getDuration());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(3);
                cell.setCellValue(setAdvertType(advertisementDefView.getType()));
                cell.setCellStyle(cellStyle);
                rowCount++;
            }

            /*-------------------Schedule table-------------------*/
            rowCount++;
            /*Create row*/
            HSSFRow rowhead = sheet.createRow((short) rowCount);
            rowCount++;

            /*Set table header fields*/
            createExcelHeaderFields(rowhead, scheduleUpdateViewList);

            int colCount = 0;
            for (int i = 0; i < scheduleUpdateViewList.size(); i++) {
                ScheduleUpdateView scheduleUpdateView = scheduleUpdateViewList.get(i);
                HSSFRow row = sheet.createRow((short) (rowCount));
                cell = row.createCell(0);
                cell.setCellValue(scheduleUpdateView.getChannelName());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(1);
                cell.setCellValue(scheduleUpdateView.getAdvertName());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(2);
                cell.setCellValue(scheduleUpdateView.getAdverLang());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(3);
                cell.setCellValue(scheduleUpdateView.getAdvertDuration());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(4);
                cell.setCellValue(scheduleUpdateView.getRevenueline());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(5);
                cell.setCellValue(scheduleUpdateView.getSchedulestarttime().toString());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(6);
                cell.setCellValue(scheduleUpdateView.getScheduleendtime().toString());
                cell.setCellStyle(cellStyle);

                List<ScheduleItemModel> scheduleItemList = scheduleUpdateView.getScheduleItem();
                colCount = 7;
                for (int j = 0; j < scheduleItemList.size(); j++) {
                    ScheduleItemModel scheduleItemModel = scheduleItemList.get(j);

                    cell = row.createCell(colCount);
                    cell.setCellValue(scheduleItemModel.getAdvertCount());
                    cell.setCellStyle(cellStyle);

                    colCount++;
                }
                rowCount++;
            }
            aligncolumns(sheet, colCount);
            writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
        } catch (Exception e) {
            throw e;
        }
    }

    public int createExcelHeaderFields(HSSFRow rowhead, List<ScheduleUpdateView> scheduleList) {
        Cell cell;
        cell = rowhead.createCell(0);
        cell.setCellValue("Channel");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(1);
        cell.setCellValue("Advertisement");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(2);
        cell.setCellValue("Lan");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(3);
        cell.setCellValue("Duration");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(4);
        cell.setCellValue("Rev. line");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(5);
        cell.setCellValue("Start time");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(6);
        cell.setCellValue("End time");
        cell.setCellStyle(tableHeaderStyle);

        int colCount = 7;
        if (!scheduleList.isEmpty()) {
            List<ScheduleItemModel> scheduleItemList = scheduleList.get(0).getScheduleItem();
            for (ScheduleItemModel scheduleItemModel : scheduleItemList) {

                cell = rowhead.createCell(colCount);
                cell.setCellValue(scheduleItemModel.getDate());
                cell.setCellStyle(tableHeaderStyle);

                colCount++;
            }
        }

        return colCount;

    }

    public void setChannelTableValue(HSSFRow rowhead, String name, String type, String duration, String spot) {
        Cell cell;
        cell = rowhead.createCell(0);
        cell.setCellValue(name);
        cell.setCellStyle(cellStyle);

        cell = rowhead.createCell(1);
        cell.setCellValue(type);
        cell.setCellStyle(cellStyle);

        cell = rowhead.createCell(2);
        cell.setCellValue(duration);
        cell.setCellStyle(cellStyle);

        cell = rowhead.createCell(3);
        cell.setCellValue(spot);
        cell.setCellStyle(cellStyle);
    }

    public void channelTableHeaderFields(HSSFRow rowhead) {

        Cell cell;
        cell = rowhead.createCell(0);
        cell.setCellValue("Name");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(1);
        cell.setCellValue("Type");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(2);
        cell.setCellValue("Duration(Sec)");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(3);
        cell.setCellValue("Spot");
        cell.setCellStyle(tableHeaderStyle);
    }

    public void AdvertTableHeaderFields(HSSFRow rowhead) {
        Cell cell;
        cell = rowhead.createCell(0);
        cell.setCellValue("ID");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(1);
        cell.setCellValue("Name");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(2);
        cell.setCellValue("Duration(Sec)");
        cell.setCellStyle(tableHeaderStyle);

        cell = rowhead.createCell(3);
        cell.setCellValue("Type");
        cell.setCellStyle(tableHeaderStyle);
    }

    public String setAdvertType(String type) {
        switch (type) {
            case "ADVERT":
                return "TVC";

            case "CRAWLER":
                return "Crawler";

            case "LOGO":
                return "Logo";

            case "V_SHAPE":
                return "V squeeze";

            case "L_SHAPE":
                return "L squeeze";

            case "FILLER":
                return "Filler";

            case "SLIDE":
                return "Slide";

            default:
                return type;
        }
    }

    public void downloadSchedulePrintExcel(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }

    private CellStyle scheduletableHeader(HSSFWorkbook workBook) {
        CellStyle style;
        Font monthFont = workBook.createFont();
        monthFont.setFontHeightInPoints((short) 11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = workBook.createCellStyle();
        style.setAlignment(ALIGN_CENTER);
        style.setVerticalAlignment(CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern((short) 1);
        style.setWrapText(true);
        return style;
    }
}
