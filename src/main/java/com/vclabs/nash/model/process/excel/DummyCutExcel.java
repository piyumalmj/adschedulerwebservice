/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.view.reporting.DummyCutView;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author hp
 */
@Component
public class DummyCutExcel extends Common {

    private final String WEBDIRECTORY = "DummyCut";
    private final String FILEPATH = "DummyCutReport.xls";

    public void generateDummyCutExcel(HttpServletRequest request, List<DummyCutView> dummyCutList) throws Exception {

        try {
            /*create work book*/
            HSSFWorkbook workbook = new HSSFWorkbook();
            /*create new sheel*/
            HSSFSheet sheet = workbook.createSheet("Dummy Cut");
            /*Create row*/
            HSSFRow rowhead = sheet.createRow((short) 0);
            /*Set table header fields*/
            createExcelHeaderFields(rowhead);

            for (int i = 0; i < dummyCutList.size(); i++) {
                DummyCutView dummyCutView = dummyCutList.get(i);
                HSSFRow row = sheet.createRow((short) (i + 1));
                row.createCell(0).setCellValue("" + dummyCutView.getWoId());
                row.createCell(1).setCellValue(dummyCutView.getClientName());
                row.createCell(2).setCellValue(dummyCutView.getProduct());
                row.createCell(3).setCellValue(dummyCutView.getMe());
                row.createCell(4).setCellValue("" + dummyCutView.getCutID());
                row.createCell(5).setCellValue(dummyCutView.getCreateDate().toString());
                row.createCell(6).setCellValue("" + dummyCutView.getDuration());
                row.createCell(7).setCellValue(dummyCutView.getLanguage());
                row.createCell(8).setCellValue("" + dummyCutView.getNumberOfSpot());
                row.createCell(9).setCellValue(dummyCutView.getAiringDateRange());
            }
            aligncolumns(sheet, 10);
            writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
        } catch (Exception e) {
            throw e;
        }
    }

    public void createExcelHeaderFields(HSSFRow rowhead) {

        rowhead.createCell(0).setCellValue("WO Id");
        rowhead.createCell(1).setCellValue("Client Name");
        rowhead.createCell(2).setCellValue("Product");
        rowhead.createCell(3).setCellValue("ME");
        rowhead.createCell(4).setCellValue("Cut ID");
        rowhead.createCell(5).setCellValue("Create date");
        rowhead.createCell(6).setCellValue("Duration");
        rowhead.createCell(7).setCellValue("Language");
        rowhead.createCell(8).setCellValue("No of Spot");
        rowhead.createCell(9).setCellValue("Spot airing date range");

    }

    public void downloadDummyCutExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }

}
