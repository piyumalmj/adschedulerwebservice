/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.view.MissedSpotView;
import com.vclabs.nash.model.view.OnedayScheduleView;
import com.vclabs.nash.model.view.ScheduleAdvertView;

public class ExcelGenerater {

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;

    private static final Logger logger = LoggerFactory.getLogger(ExcelGenerater.class);

    public Boolean writeMissedSpotInExcel(List<MissedSpotView> itemList, HttpServletRequest request) throws Exception {
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("FirstSheet");

            HSSFRow rowhead = sheet.createRow((short) 0);
            rowhead.createCell(0).setCellValue("Original Time Rule");
            rowhead.createCell(1).setCellValue("Work Order Name");
            rowhead.createCell(2).setCellValue("Work Order ID");
            rowhead.createCell(3).setCellValue("Channel Name");
            rowhead.createCell(4).setCellValue("Client");
            rowhead.createCell(5).setCellValue("Cut ID");
            rowhead.createCell(6).setCellValue("Advertisement");
            rowhead.createCell(7).setCellValue("Status");
            rowhead.createCell(8).setCellValue("Type");
            rowhead.createCell(9).setCellValue("Actual Time Belt");
            rowhead.createCell(10).setCellValue("Date");
            rowhead.createCell(11).setCellValue("Schedule End Date");

            int count = 1;
            int woId = 0;
            WorkOrder workOrderId;
            String clientName = "";
            if (!itemList.isEmpty()) {
                for (int i = 0; i < itemList.size(); i++) {
                    for (int j = 0; j < itemList.get(i).getItemList().size(); j++) {
                        HSSFRow row = sheet.createRow((short) count);
                        row.createCell(0).setCellValue(itemList.get(i).getTimeBelt());
                        row.createCell(1).setCellValue(itemList.get(i).getItemList().get(j).getWorkOrderName());
                        row.createCell(2).setCellValue(itemList.get(i).getItemList().get(j).getWorkOrderId());
                        row.createCell(3).setCellValue(itemList.get(i).getItemList().get(j).getChannelName());
                        row.createCell(4).setCellValue(itemList.get(i).getItemList().get(j).getClient());
                        row.createCell(5).setCellValue(itemList.get(i).getItemList().get(j).getAdvertId());
                        row.createCell(6).setCellValue(itemList.get(i).getItemList().get(j).getAdvertNme());
                        row.createCell(7).setCellValue(itemList.get(i).getItemList().get(j).getStatus());
                        row.createCell(8).setCellValue(setAdvertType(itemList.get(i).getItemList().get(j).getAdvertType()));
                        row.createCell(9).setCellValue(itemList.get(i).getItemList().get(j).getActualTimeBelt());
                        row.createCell(10).setCellValue(itemList.get(i).getItemList().get(j).getSchedulDate());
                        row.createCell(11).setCellValue(itemList.get(i).getItemList().get(j).getScheduleEndDate().toString());
                        count++;
                    }
                }
            }
            FileOutputStream fileOut = new FileOutputStream(getFilePath(request));
            workbook.write(fileOut);
            fileOut.close();
            return true;

        } catch (Exception ex) {
            logger.debug("Exception in ExcelGenerater in writeMissedSpotInExcel : {}", ex.getMessage());
            throw ex;
        }
    }

    public String setAdvertType(String type) {
        switch (type) {
            case "ADVERT":
                return "TVC";

            case "CRAWLER":
                return "Crawler";

            case "LOGO":
                return "Logo";

            case "V_SHAPE":
                return "V squeeze";

            case "L_SHAPE":
                return "L squeeze";

            case "FILLER":
                return "Filler";

            case "SLIDE":
                return "Slide";

            default:
                return type;
        }
    }

    public String getFilePath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String webDirectory = "/Excell";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String filePath = "/MissedSpotExcell.xls";
        File pdf_file_path = new File(webServerPath + webDirectory + filePath);

        if (pdf_file_path.isFile()) {
            pdf_file_path.delete();
        }

        return pdf_file_path.toString();
    }

    /*---------------------------------------------------------------------------------------------------------------*/
    public Boolean generateTR(List<ScheduleDef> scheduleDefList, TRDetails model, HttpServletRequest request) throws Exception {
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = null;
            if (!scheduleDefList.isEmpty()) {
                model.setChannel(scheduleDefList.get(0).getChannelid().getChannelname());
                sheet = addSheet(workbook, model.getChannel());
            } else {
                sheet = workbook.createSheet("No data");
            }

            addHead(sheet, model);

            DateFormat dtFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            DateFormat tmFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);

            int count = 11;
            int spotCount = 1;
            for (ScheduleDef item : scheduleDefList) {
                if (model.getChannel().equals(item.getChannelid().getChannelname())) {
                    HSSFRow row = sheet.createRow((short) count);
                    row.createCell(0).setCellValue(dtFormat.format(item.getDate()));
                    if (item.getStatus().equals("2")) {
                        row.createCell(1).setCellValue(tmFormat.format(item.getActualstarttime()));
                    } else {
                        row.createCell(1).setCellValue("");
                    }
                    row.createCell(2).setCellValue(item.getAdvertid().getAdvertname());
                    row.createCell(3).setCellValue("" + item.getAdvertid().getDuration());
                    count++;
                    spotCount++;
                } else {
                    rowMarge(sheet, count, spotCount);
                    model.setChannel(item.getChannelid().getChannelname());
                    sheet = addSheet(workbook, model.getChannel());
                    addHead(sheet, model);
                    spotCount = 1;
                    count = 11;
                    HSSFRow row = sheet.createRow((short) count);
                    row.createCell(0).setCellValue(dtFormat.format(item.getDate()));
                    if (item.getStatus().equals("2")) {
                        row.createCell(1).setCellValue(tmFormat.format(item.getActualstarttime()));
                    } else {
                        row.createCell(1).setCellValue("");
                    }
                    row.createCell(2).setCellValue(item.getAdvertid().getAdvertname());
                    row.createCell(3).setCellValue("" + item.getAdvertid().getDuration());
                    spotCount++;
                    count++;
                }
            }
            rowMarge(sheet, count, spotCount);
            FileOutputStream fileOut = new FileOutputStream(getTransmissionPath(request));
            workbook.write(fileOut);
            fileOut.close();
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ExcelGenerater: {}", e.getMessage());
            throw e;
        }
    }

    public HSSFSheet addSheet(HSSFWorkbook workBook, String ChannelName) {
        HSSFSheet sheet = workBook.createSheet(ChannelName);
        return sheet;
    }

    public void addHead(HSSFSheet sheet, TRDetails model) {
        HSSFRow rowhead1 = sheet.createRow((short) 0);
        rowhead1.createCell(0).setCellValue("Dialog Television (Pvt) Ltd.");
        rowhead1.createCell(1).setCellValue("");
        rowhead1.createCell(2).setCellValue("");
        rowhead1.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));

        HSSFRow rowhead2 = sheet.createRow((short) 1);
        rowhead2.createCell(0).setCellValue("475 Union Place, Colombo 2");
        rowhead2.createCell(1).setCellValue("");
        rowhead2.createCell(2).setCellValue("");
        rowhead2.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 3));

        HSSFRow rowhead3 = sheet.createRow((short) 2);
        rowhead3.createCell(0).setCellValue("Transmission Certificate");
        rowhead3.createCell(1).setCellValue("");
        rowhead3.createCell(2).setCellValue("");
        rowhead3.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 3));

        HSSFRow rowhead4 = sheet.createRow((short) 3);
        rowhead4.createCell(0).setCellValue("Reg. No:");
        rowhead4.createCell(1).setCellValue("");
        rowhead4.createCell(2).setCellValue(model.getRegNO());
        rowhead4.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));

        HSSFRow rowhead5 = sheet.createRow((short) 4);
        rowhead5.createCell(0).setCellValue("Product:");
        rowhead5.createCell(1).setCellValue("");
        rowhead5.createCell(2).setCellValue(model.getProduct());
        rowhead5.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(4, 4, 2, 3));

        HSSFRow rowhead6 = sheet.createRow((short) 5);
        rowhead6.createCell(0).setCellValue("Schedule ref:");
        rowhead6.createCell(1).setCellValue("");
        rowhead6.createCell(2).setCellValue(model.getScheduleRef());
        rowhead6.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(5, 5, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(5, 5, 2, 3));

        HSSFRow rowhead7 = sheet.createRow((short) 6);
        rowhead7.createCell(0).setCellValue("Advertiser:");
        rowhead7.createCell(1).setCellValue("");
        rowhead7.createCell(2).setCellValue(model.getAdvertiser());
        rowhead7.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(6, 6, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(6, 6, 2, 3));

        HSSFRow rowhead8 = sheet.createRow((short) 7);
        rowhead8.createCell(0).setCellValue("Channel:");
        rowhead8.createCell(1).setCellValue("");
        rowhead8.createCell(2).setCellValue(model.getChannel());
        rowhead8.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(7, 7, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(7, 7, 2, 3));

        HSSFRow rowhead9 = sheet.createRow((short) 8);
        rowhead9.createCell(0).setCellValue("Agency/Agent:");
        rowhead9.createCell(1).setCellValue("");
        rowhead9.createCell(2).setCellValue(model.getAgency());
        rowhead9.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(8, 8, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(8, 8, 2, 3));

        HSSFRow rowhead10 = sheet.createRow((short) 9);
        rowhead10.createCell(0).setCellValue("Marketing Ex.:");
        rowhead10.createCell(1).setCellValue("");
        rowhead10.createCell(2).setCellValue(model.getMarketingEx());
        rowhead10.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(9, 9, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(9, 9, 2, 3));

        HSSFRow rowhead = sheet.createRow((short) 10);
        rowhead.createCell(0).setCellValue("Date");
        rowhead.createCell(1).setCellValue("Aired Time");
        rowhead.createCell(2).setCellValue("Product Name");
        rowhead.createCell(3).setCellValue("Duration");
    }

    public void rowMarge(HSSFSheet sheet, int count, int rowCount) {
        HSSFRow row1 = sheet.createRow((short) count);
        row1.createCell(0).setCellValue("Total No of Spots:");
        row1.createCell(1).setCellValue("");
        row1.createCell(2).setCellValue(rowCount - 1);
        row1.createCell(3).setCellValue("");
        sheet.addMergedRegion(new CellRangeAddress(count, count, 0, 1));
        sheet.addMergedRegion(new CellRangeAddress(count, count, 2, 3));
    }

    public String getTransmissionPath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String webDirectory = "/PDF";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String filePath = "/TransmissionReport.xls";
        File pdf_file_path = new File(webServerPath + webDirectory + filePath);

        if (pdf_file_path.isFile()) {
            pdf_file_path.delete();
        }

        return pdf_file_path.toString();
    }

    /*----------------------------------Oneday schedule------------------------------------*/
    public Boolean oneDayScheduleExcel(List<OnedayScheduleView> onedayScheduleList, HttpServletRequest request){

        try {
            int count = 1;
            SimpleDateFormat hourAndMinutesTime = new SimpleDateFormat("HH:mm");
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = addSheet(workbook, "One day schedule spot");
            HSSFRow row = sheet.createRow((short) 0);
            row.createCell(0).setCellValue("Time Belt");
            row.createCell(1).setCellValue("Channel Name");
            row.createCell(2).setCellValue("Client");
            row.createCell(3).setCellValue("Schedule time");
            row.createCell(4).setCellValue("Shifted from");
            row.createCell(5).setCellValue("Airde time");
            row.createCell(6).setCellValue("Duration");
            row.createCell(7).setCellValue("Cluster");
            row.createCell(8).setCellValue("Type");
            row.createCell(9).setCellValue("Status");

            for (OnedayScheduleView onedayScheduleView : onedayScheduleList) {

                for (ScheduleAdvertView scheduleAdvertView : onedayScheduleView.getScheduleItems()) {
                    HSSFRow row1 = sheet.createRow((short) count);
                    row1.createCell(0).setCellValue(hourAndMinutesTime.format(onedayScheduleView.getScheduleStartTime()) + "-" + hourAndMinutesTime.format(onedayScheduleView.getScheduleEndTime()));
                    row1.createCell(1).setCellValue(scheduleAdvertView.getChannelName());
                    row1.createCell(2).setCellValue(scheduleAdvertView.getClient());
                    row1.createCell(3).setCellValue(scheduleAdvertView.getStartAndEndTime());
                    row1.createCell(4).setCellValue(scheduleAdvertView.getInitialStartEndTime());
                    row1.createCell(5).setCellValue(scheduleAdvertView.getPalyTime());
                    row1.createCell(6).setCellValue(scheduleAdvertView.getDuration());
                    row1.createCell(7).setCellValue(scheduleAdvertView.getClusterNum());
                    row1.createCell(8).setCellValue(setAdvertType(scheduleAdvertView.getAdvertType()));
                    row1.createCell(9).setCellValue(scheduleAdvertView.getStatus());
                    count++;
                }

            }
            FileOutputStream fileOut = new FileOutputStream(getOneDaySchedulePath(request));
            workbook.write(fileOut);
            fileOut.close();
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ExcelGenerater: {}", e.getMessage());
            return false;
        }

    }

    public String getOneDaySchedulePath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String webDirectory = "/OneDay";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String filePath = "/OneDayScheduleExcell.xls";
        File pdf_file_path = new File(webServerPath + webDirectory + filePath);

        if (pdf_file_path.isFile()) {
            pdf_file_path.delete();
        }

        return pdf_file_path.toString();
    }

}
