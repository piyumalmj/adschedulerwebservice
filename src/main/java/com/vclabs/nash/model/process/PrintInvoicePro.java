/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.process;

import java.util.List;
import com.vclabs.nash.model.entity.InvoiceSpotRecord;
import com.vclabs.nash.model.entity.InvoiceTaxDetails;

/**
 *
 * @author user
 */
public class PrintInvoicePro {

    private int invoiceid = 1;
    private int workOrderId = 1;
    private int invoiceType = 1;
    private int commissionRate = 4;
    private double totalAmount = 1000;
    private int printStatus;
    private String workOrderPeriod = "";
    private String vatRegNum = "";

    private String date = "2016.11.01";

    private String client = "client";
    private String clientAddress = "client \n clientAddress \n clientAddress2";
    private String billingclient = "billingclient";
    private String billingclientAddress = "billingclient \n billingclientAddress \n billingclientAddress2";
    private String commissionclient = "commissionclient";
    private String commissionclientAddress = "commissionclient \n commissionclientAddress \n commissionclientAddress2";

    private String channelNames = "TLC,Star World";

    private List<InvoiceSpotRecord> spotRecord;
    private List<InvoiceTaxDetails> taxDetails;

    public PrintInvoicePro() {
    }

    public int getInvoiceid() {
        return invoiceid;
    }

    public void setInvoiceid(int invoiceid) {
        this.invoiceid = invoiceid;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public int getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(int invoiceType) {
        this.invoiceType = invoiceType;
    }

    public int getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(int commissionRate) {
        this.commissionRate = commissionRate;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getBillingclient() {
        return billingclient;
    }

    public void setBillingclient(String billingclient) {
        this.billingclient = billingclient;
    }

    public String getBillingclientAddress() {
        return billingclientAddress;
    }

    public void setBillingclientAddress(String billingclientAddress) {
        this.billingclientAddress = billingclientAddress;
    }

    public String getCommissionclient() {
        return commissionclient;
    }

    public void setCommissionclient(String commissionclient) {
        this.commissionclient = commissionclient;
    }

    public String getCommissionclientAddress() {
        return commissionclientAddress;
    }

    public void setCommissionclientAddress(String commissionclientAddress) {
        this.commissionclientAddress = commissionclientAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(String channelNames) {
        this.channelNames = channelNames;
    }

    public List<InvoiceSpotRecord> getSpotRecord() {
        return spotRecord;
    }

    public void setSpotRecord(List<InvoiceSpotRecord> spotRecord) {
        this.spotRecord = spotRecord;
    }

    public List<InvoiceTaxDetails> getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(List<InvoiceTaxDetails> taxDetails) {
        this.taxDetails = taxDetails;
    }

    public int getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(int printStatus) {
        this.printStatus = printStatus;
    }

    public String getWorkOrderPeriod() {
        return workOrderPeriod;
    }

    public void setWorkOrderPeriod(String workOrderPeriod) {
        this.workOrderPeriod = workOrderPeriod;
    }

    public String getVatRegNum() {
        return vatRegNum;
    }

    public void setVatRegNum(String vatRegNum) {
        this.vatRegNum = vatRegNum;
    }

}
