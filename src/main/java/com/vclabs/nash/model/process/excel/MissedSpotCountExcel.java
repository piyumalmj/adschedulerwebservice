/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;
import com.vclabs.nash.model.process.MissedSpotItemModel;

/**
 *
 * @author hp
 */
@Component
public class MissedSpotCountExcel extends Common {

    private final String WEBDIRECTORY = "MissedSpotCount";
    private final String FILEPATH = "MissedSpotCount.xls";

    private CellStyle cellStyle;

    private CellStyle tableHeaderStyle;

    public void generateMissedSpotCountReport(List<MissedSpotItemModel> missedSpotCoutList, HttpServletRequest request) throws Exception {

        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("MissedSpotCountReport");

        int rowCount = 1;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead);

        for (MissedSpotItemModel missedSpotItemModel : missedSpotCoutList) {
            int cellCount = 0;

            HSSFRow row = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getScheduleTimeBelt());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getChannelName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getSchedulDate());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getSchedulStartTime() + " - " + missedSpotItemModel.getSchedulEndTime());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            if (!missedSpotItemModel.getScheduleTimeBelt().equals(missedSpotItemModel.getShiftedTimeBelt())) {
                cell.setCellValue(missedSpotItemModel.getShiftedTimeBelt());
            } else {
                cell.setCellValue("");
            }
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getActualTimeBelt());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getAdvertNme());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getDuratuion());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getCluster());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(getAdvertType(missedSpotItemModel.getAdvertType()));
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getClient());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getAdvertId());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(missedSpotItemModel.getStatus());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue("");
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue("");
            cell.setCellStyle(cellStyle);

        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead) {
        int fieldCount = 0;

        Cell cell;
        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Time Belt");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Channel Name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Scheduled time");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Shifted time");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Aired time");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Advert Name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Duration");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Cluster");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Type");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Client");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("ID");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Status");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Comment");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Played User");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        return fieldCount;
    }

    private String getAdvertType(String type) {
        switch (type) {
            case "ADVERT":
                return "TVC";

            case "CRAWLER":
                return "Crawler";

            case "LOGO":
                return "Logo";

            case "V_SHAPE":
                return "V squeeze";

            case "L_SHAPE":
                return "L squeeze";

            case "FILLER":
                return "Filler";

            case "SLIDE":
                return "Slide";

            default:
                return type;

        }
    }

    public void downloadMissedSpotCountReportExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }

}
