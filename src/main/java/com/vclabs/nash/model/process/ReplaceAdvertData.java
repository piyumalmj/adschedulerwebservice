/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author user
 */
public class ReplaceAdvertData {
    
    @JsonProperty("workOrderId")
    private int workOrderID;    
    @JsonProperty("originalAdvertID")
    private int originalAdvertID;    
    @JsonProperty("lstReplaceAdvertID")
    private List<Integer> replaceAdvertID;  
    @JsonProperty("lstReplaceChannelID")
    private List<Integer> replaceChannelID;  
    @JsonProperty("startDate")
    private String scheduleStartDate;
    @JsonProperty("endDate")
    private String scheduleEndDate;
    @JsonProperty("replaceMode")
    private int replaceMode; /// 1-vertical ,2-horizontal
    @JsonProperty("isSelected")
    private boolean isSelected;
    @JsonProperty("lstSelectedSchedules")
    private List<Integer> selectedSchedules;

    public ReplaceAdvertData() {
    }

    public ReplaceAdvertData(int workOrderID, int originalAdvertID, List<Integer> replaceAdvertID) {
        this.workOrderID = workOrderID;
        this.originalAdvertID = originalAdvertID;
        this.replaceAdvertID = replaceAdvertID;
    }

    public int getWorkOrderID() {
        return workOrderID;
    }

    public void setWorkOrderID(int workOrderID) {
        this.workOrderID = workOrderID;
    }

    public int getOriginalAdvertID() {
        return originalAdvertID;
    }

    public void setOriginalAdvertID(int originalAdvertID) {
        this.originalAdvertID = originalAdvertID;
    }

    public List<Integer> getReplaceAdvertID() {
        return replaceAdvertID;
    }

    public void setReplaceAdvertID(List<Integer> replaceAdvertID) {
        this.replaceAdvertID = replaceAdvertID;
    }

    public String getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(String scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public String getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(String scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public int getReplaceMode() {
        return replaceMode;
    }

    public void setReplaceMode(int replaceMode) {
        this.replaceMode = replaceMode;
    }

    public List<Integer> getReplaceChannelID() {
        return replaceChannelID;
    }

    public void setReplaceChannelID(List<Integer> replaceChannelID) {
        this.replaceChannelID = replaceChannelID;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public List<Integer> getSelectedSchedules() {
        return selectedSchedules;
    }

    public void setSelectedSchedules(List<Integer> selectedSchedules) {
        this.selectedSchedules = selectedSchedules;
    }
    
}
