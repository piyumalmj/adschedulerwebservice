/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.view.reporting.ClientOrAgencyList;
import com.vclabs.nash.model.view.reporting.ClientOrAgencyValue;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;


/**
 *
 * @author hp
 */
@Component
public class ClientOrAgencyExcel extends Common {

    private final String WEBDIRECTORY_1 = "ClientRevenueExcel";
    private final String FILEPATH_1 = "ClientRevenueReport.xls";

    private final String WEBDIRECTORY_2 = "AgencyRevenueExcel";
    private final String FILEPATH_2 = "AgencyRevenueReport.xls";

    private CellStyle cellStyle;
    private CellStyle tableHeaderStyle;

    public void generateClientORAgencyRevenueExcel(Map<Integer, ClientOrAgencyList> clientAgencyList, HttpServletRequest request, Boolean isClient) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet;
        if (isClient) {
            sheet = workbook.createSheet("ClientRevenue");
        } else {
            sheet = workbook.createSheet("AgencyRevenue");
        }
        /*Create row*/
        int rowCount = 0;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead, clientAgencyList, isClient);

        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.DOWN);

        for (Map.Entry<Integer, ClientOrAgencyList> entry : clientAgencyList.entrySet()) {

            int cellCount = 0;
            Cell cell;
            HSSFRow row = sheet.createRow((short) rowCount);
            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(entry.getValue().getClientOrAgencyName());
            cell.setCellStyle(cellStyle);

            for (Map.Entry<Integer, ClientOrAgencyValue> clientValue : entry.getValue().getClientAgencyList().entrySet()) {
                cell = row.createCell(cellCount);
                cellCount++;
                cell.setCellValue(df.format(clientValue.getValue().getValue()) + "");
                cell.setCellStyle(cellStyle);
            }

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(df.format(entry.getValue().getGrandTotal()) + "");
            cell.setCellStyle(cellStyle);

            rowCount++;
        }
        aligncolumns(sheet, count);
        if (isClient) {
            writeFile(workbook, getFilePath(request, WEBDIRECTORY_1, FILEPATH_1, true));
        } else {
            writeFile(workbook, getFilePath(request, WEBDIRECTORY_2, FILEPATH_2, true));
        }

    }

    public int createExcelHeaderFields(HSSFRow rowhead, Map<Integer, ClientOrAgencyList> clientAgencyList, Boolean isClient) {
        int fieldCount = 1;
        Map<Integer, ClientOrAgencyValue> clientAgencyValueList = clientAgencyList.entrySet().iterator().next().getValue().getClientAgencyList();

        Cell cell;
        cell = rowhead.createCell(0);
        if (isClient) {
            cell.setCellValue("Client");
        } else {
            cell.setCellValue("Agency");
        }
        cell.setCellStyle(tableHeaderStyle);

        if (!clientAgencyValueList.isEmpty()) {

            for (Map.Entry<Integer, ClientOrAgencyValue> entry : clientAgencyValueList.entrySet()) {
                cell = rowhead.createCell(fieldCount);
                cell.setCellValue(entry.getValue().getMonthName());
                cell.setCellStyle(tableHeaderStyle);
                fieldCount++;
            }

        }
        cell = rowhead.createCell(fieldCount);
        fieldCount++;
        cell.setCellValue("Grand Total");
        cell.setCellStyle(tableHeaderStyle);

        return fieldCount;
    }

    public void downloadClientRevenueExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY_1, FILEPATH_1, false));
    }

    public void downloadAgencyRevenueExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY_2, FILEPATH_2, false));
    }
}
