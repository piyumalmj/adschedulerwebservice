package com.vclabs.nash.model.process;

import java.util.List;

public class OPSMainSchedule {
    private WorkOrderInfo workOrder;

    private List<OrderChannelInfo> channelList;

    private List<AdvertisementInfo> advertList;

    private List<ScheduleInfo> scheduleList;

    public WorkOrderInfo getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrderInfo workOrder) {
        this.workOrder = workOrder;
    }

    public List<OrderChannelInfo> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<OrderChannelInfo> channelList) {
        this.channelList = channelList;
    }

    public List<AdvertisementInfo> getAdvertList() {
        return advertList;
    }

    public void setAdvertList(List<AdvertisementInfo> advertList) {
        this.advertList = advertList;
    }

    public List<ScheduleInfo> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<ScheduleInfo> scheduleList) {
        this.scheduleList = scheduleList;
    }
}
