package com.vclabs.nash.model.process;

import java.util.HashMap;

/**
 * Created by Nalaka on 2019-03-21.
 */
public class ChannelSpotInfo {
    private String spotType;
    private int count = 0;

    private HashMap<String, Integer> tvcSpotCountList = new HashMap<String, Integer>();

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void addOne() {
        this.count++;
    }

    public HashMap<String, Integer> getTvcSpotCountList() {
        return tvcSpotCountList;
    }

    public void setTvcSpotCountList(HashMap<String, Integer> tvcSpotCountList) {
        this.tvcSpotCountList = tvcSpotCountList;
    }
}
