/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
public class SAPChannelSpotInfo {

    private String channelName;
    private double channelTotalAmount = 0;
    private double channelAiredTotalAmount = 0;

    public SAPChannelSpotInfo() {
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public double getChannelTotalAmount() {
        return channelTotalAmount;
    }

    public double getChannelAiredTotalAmount() {
        return channelAiredTotalAmount;
    }

    public void addTotalAmount(double amount) {
        channelTotalAmount += amount;
    }

    public void addTotalAiredAmount(double amount, int allSpot, int airedSpot) {
        channelAiredTotalAmount += (amount / allSpot) * airedSpot;
    }

}
