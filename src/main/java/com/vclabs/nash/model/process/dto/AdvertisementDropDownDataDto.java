package com.vclabs.nash.model.process.dto;

/**
 * Created by Sanduni on 02/01/2019
 */
public class AdvertisementDropDownDataDto {

    private Integer advertid;

    private String advertname;

    private String adverttype;

    public AdvertisementDropDownDataDto(Integer advertid, String advertname, String adverttype) {
        this.advertid = advertid;
        this.advertname = advertname;
        this.adverttype = adverttype;
    }

    public Integer getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Integer advertid) {
        this.advertid = advertid;
    }

    public String getAdvertname() {
        return advertname;
    }

    public void setAdvertname(String advertname) {
        this.advertname = advertname;
    }

    public String getAdverttype() {
        return adverttype;
    }

    public void setAdverttype(String adverttype) {
        this.adverttype = adverttype;
    }
}
