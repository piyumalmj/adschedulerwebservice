package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sanira Nanayakkara
 */
public class ClusterInfo {

    @JsonProperty("id")
    private int timebeltId;
    
    @JsonProperty("clusterCount")
    private int clusterCount;
    
    @JsonProperty("maxScheduleTime")
    private int maxScheduleTime;

    public int getTimebeltId() {
        return timebeltId;
    }

    public void setTimebeltId(int timebeltId) {
        this.timebeltId = timebeltId;
    }

    public int getClusterCount() {
        return clusterCount;
    }

    public void setClusterCount(int clusterCount) {
        this.clusterCount = clusterCount;
    }

    public int getMaxScheduleTime() {
        return maxScheduleTime;
    }

    public void setMaxScheduleTime(int maxScheduleTime) {
        this.maxScheduleTime = maxScheduleTime;
    }
}
