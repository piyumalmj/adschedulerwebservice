/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.view.reporting.ComAvailabilityChannel;
import com.vclabs.nash.model.view.reporting.ComAvailabilityHourDetails;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author hp
 */
@Component
public class ComAvailabilityHourlyExcel extends Common {

    private final String WEBDIRECTORY = "ComAvailabilityHourly";
    private final String FILEPATH = "ComAvailabilityHourlyReport.xls";

    public void generateComAvailabilityHourlyExcel(int value, HttpServletRequest request, Map<Integer, ComAvailabilityChannel> comAvailabilityChannelList) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("ComAvailabilityHourly");
        /*Create row*/
        HSSFRow rowhead = sheet.createRow((short) 0);
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead, comAvailabilityChannelList);
        int rowCount = 1;

        for (Entry<Integer, ComAvailabilityChannel> ComAvailabilityChannel : comAvailabilityChannelList.entrySet()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = ComAvailabilityChannel.getValue().getHourDetailsMap();

            HSSFRow row = sheet.createRow((short) rowCount);
            row.createCell(0).setCellValue(ComAvailabilityChannel.getValue().getChannelName());

            int colCount = 1;

            for (Entry<Integer, ComAvailabilityHourDetails> entry : hourDetailsMap.entrySet()) {

                switch (value) {
                    case 1:
                        row.createCell(colCount).setCellValue(entry.getValue().getAdvertCount());
                        break;
                    case 2:
                        row.createCell(colCount).setCellValue(entry.getValue().getAdvertDuration());
                        break;
                    case 3:
                        row.createCell(colCount).setCellValue(entry.getValue().getInventryUtilization());
                        break;
                    case 4:
                        row.createCell(colCount).setCellValue(entry.getValue().getActualUtilization());
                        break;
                    default:
                        row.createCell(colCount).setCellValue(entry.getValue().getAdvertCount());
                        break;
                }

                colCount++;
            }

            rowCount++;
        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead, Map<Integer, ComAvailabilityChannel> comAvailabilityChannelList) {
        rowhead.createCell(0).setCellValue("Channel");

        int count = 1;
        if (!comAvailabilityChannelList.isEmpty()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = comAvailabilityChannelList.entrySet().iterator().next().getValue().getHourDetailsMap();
            for (Entry<Integer, ComAvailabilityHourDetails> entry : hourDetailsMap.entrySet()) {
                ComAvailabilityHourDetails comAvailabilityHourDetails = hourDetailsMap.get(entry.getKey());
                rowhead.createCell(count).setCellValue(comAvailabilityHourDetails.getTime());
                count++;
            }

        }

        return count;
    }
    
    public void downloadComAvailabilityHourlyExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
