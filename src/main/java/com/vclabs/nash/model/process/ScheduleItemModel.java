/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.vclabs.nash.model.view.CustomTimeSerializer_mmss;

/**
 *
 * @author user
 */
public class ScheduleItemModel {

    private String date;
    private int advertCount;
    private int airedCount = 0;
    private List<Integer> scheduleid = new ArrayList<>();
    private int expire = 0; //1=expired 0=not expired

    private int utilizationCategory = 0;
    @JsonSerialize(using = CustomTimeSerializer_mmss.class)
    private Date totalAdvertTime;
    @JsonSerialize(using = CustomTimeSerializer_mmss.class)
    private Date utilizedAdvertTime;

    @JsonIgnore
    private double[] TimeBeltUtilizedCategory = new double[]{0.75, 1.0};

    public ScheduleItemModel() {
        totalAdvertTime = new Date(0);
        utilizedAdvertTime = new Date(0);
        advertCount = 0;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAdvertCount() {
        return advertCount;
    }

    public void setAdvertCount(int advertCount) {
        this.advertCount = advertCount;
    }

    public List<Integer> getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(List<Integer> scheduleid) {
        this.scheduleid = scheduleid;
    }

    public void addOne() {
        advertCount++;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public int getUtilizationCategory() {
        return utilizationCategory;
    }

    public void setUtilizationCategory(int utilizationCategory) {
        this.utilizationCategory = utilizationCategory;
    }

    public Date getTotalAdvertTime() {
        return totalAdvertTime;
    }

    public void setTotalAdvertTime(Date totalAdvertTime) {
        this.totalAdvertTime = totalAdvertTime;
    }

    public Date getUtilizedAdvertTime() {
        return utilizedAdvertTime;
    }

    public void setUtilizedAdvertTime(Date utilizedAdvertTime) {
        this.utilizedAdvertTime = utilizedAdvertTime;
    }

    public void addUtilizedAdvertTimeinSeconds(long seconds) {
        this.utilizedAdvertTime.setTime(this.utilizedAdvertTime.getTime() + seconds * 1000);

        double u_ratio = this.utilizedAdvertTime.getTime() / (double) this.totalAdvertTime.getTime();
        for (int i = 0; i < TimeBeltUtilizedCategory.length; i++) {
            if (u_ratio > TimeBeltUtilizedCategory[i]) {
                this.utilizationCategory = i + 1;
            }
        }
    }

    public int getAiredCount() {
        return airedCount;
    }

    public void setAiredCount(int airedCount) {
        this.airedCount = airedCount;
    }

    public void addAired() {
        this.airedCount++;
    }

}
