/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import java.util.Date;

/**
 *
 * @author user
 */
public class ScheduleModelPro {

    private int scheduleId;
    private int channelId;
    private int advertDuration;
    private int seaquenceNum;
    private int advertId;
    private Date initiateDate;
    private Date scheduledStartDate;
    private Date scheduledEndDate;
    private String commercialtype;

    public ScheduleModelPro() {

    }

    public ScheduleModelPro(int scheduleId, int advertDuration, int seaquenceNum, Date initiateDate, Date scheduledStartDate, Date scheduledEndDate, String commercialtype) {
        this.scheduleId = scheduleId;
        this.advertDuration = advertDuration;
        this.seaquenceNum = seaquenceNum;
        this.initiateDate = initiateDate;
        this.scheduledStartDate = scheduledStartDate;
        this.scheduledEndDate = scheduledEndDate;
        this.commercialtype = commercialtype;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public int getSeaquenceNum() {
        return seaquenceNum;
    }

    public void setSeaquenceNum(int seaquenceNum) {
        this.seaquenceNum = seaquenceNum;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public Date getInitiateDate() {
        return initiateDate;
    }

    public void setInitiateDate(Date initiateDate) {
        this.initiateDate = initiateDate;
    }

    public Date getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(Date scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }

    public Date getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(Date scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public String getCommercialtype() {
        return commercialtype;
    }

    public void setCommercialtype(String commercialtype) {
        this.commercialtype = commercialtype;
    }

}
