/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author user
 */
public class ChannelInfo {

    @JsonProperty("channelId")
    private int channelId;
    @JsonProperty("longName")
    private String longName;
    @JsonProperty("shortName")
    private String shortName;
    @JsonProperty("vFormat")
    private String vFormat;
    @JsonProperty("remarks")
    private String remarks;
    
    @JsonProperty("isManual")
    private boolean isManual;
    @JsonProperty("opsId")
    private int opsId;
    @JsonProperty("manualDelay")
    private int manualDelay;
    @JsonProperty("logoPosition")
    private String logoPosition;

    /**
     *
     */
    public ChannelInfo() {
    }

    /**
     *
     * @return
     */
    public String getLongName() {
        return longName;
    }

    /**
     *
     * @param longName
     */
    public void setLongName(String longName) {
        this.longName = longName;
    }

    /**
     *
     * @return
     */
    public String getShortName() {
        return shortName;
    }

    /**
     *
     * @param shortName
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     *
     * @return
     */
    public String getvFormat() {
        return vFormat;
    }

    /**
     *
     * @param vFormat
     */
    public void setvFormat(String vFormat) {
        this.vFormat = vFormat;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isIsManual() {
        return isManual;
    }

    public void setIsManual(boolean isManual) {
        this.isManual = isManual;
    }

    public int getManualDelay() {
        return manualDelay;
    }

    public void setManualDelay(int manualDelay) {
        this.manualDelay = manualDelay;
    }

    public String getLogoPosition() {
        return logoPosition;
    }

    public void setLogoPosition(String logoPosition) {
        this.logoPosition = logoPosition;
    }

    public int getOpsId() {
        return opsId;
    }

    public void setOpsId(int opsId) {
        this.opsId = opsId;
    }
}
