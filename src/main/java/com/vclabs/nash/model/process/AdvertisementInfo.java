/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author user
 */
public class AdvertisementInfo {

    @JsonProperty("advertId")
    private int advertid;

    private int dbAdvertid;

    @JsonProperty("advertName")
    private String advertname;

    @JsonProperty("advertPath")
    private String advertpath;

    @JsonProperty("advertType")
    private String adverttype;

    @JsonProperty("advertCategory")
    private String commercialcategory;

    @JsonProperty("duration")
    private int duration;

    @JsonProperty("expireDate")
    private String expireDate;

    @JsonProperty("suspendDate")
    private String suspendDate;

    @JsonProperty("language")
    private String language;

    @JsonProperty("vedioType")
    private String vedioType;

    @JsonProperty("with")
    private int width;

    @JsonProperty("hight")
    private int hight;

    @JsonProperty("x_position")
    private int xposition;

    @JsonProperty("y_position")
    private int yposition;

    @JsonProperty("client")
    private int client;

    @JsonProperty("logo_container")
    private int logoContainer = -1;

    @JsonProperty("codeMapping")
    private long codeMapping;

    public AdvertisementInfo() {

    }

    public int getAdvertid() {
        return advertid;
    }

    public void setAdvertid(int advertid) {
        this.advertid = advertid;
    }

    public String getAdvertname() {
        return advertname;
    }

    public void setAdvertname(String advertname) {
        this.advertname = advertname;
    }

    public String getAdvertpath() {
        return advertpath;
    }

    public void setAdvertpath(String advertpath) {
        this.advertpath = advertpath;
    }

    public String getAdverttype() {
        return adverttype;
    }

    public void setAdverttype(String adverttype) {
        this.adverttype = adverttype;
    }

    public String getCommercialcategory() {
        return commercialcategory;
    }

    public void setCommercialcategory(String commercialcategory) {
        this.commercialcategory = commercialcategory;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getVedioType() {
        return vedioType;
    }

    public void setVedioType(String vedioType) {
        this.vedioType = vedioType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public int getXposition() {
        return xposition;
    }

    public void setXposition(int xposition) {
        this.xposition = xposition;
    }

    public int getYposition() {
        return yposition;
    }

    public void setYposition(int yposition) {
        this.yposition = yposition;
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public int getLogoContainer() {
        return logoContainer;
    }

    public void setLogoContainer(int logoContainer) {
        this.logoContainer = logoContainer;
    }

    public String getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(String suspendDate) {
        this.suspendDate = suspendDate;
    }

    public int getDbAdvertid() {
        return dbAdvertid;
    }

    public void setDbAdvertid(int dbAdvertid) {
        this.dbAdvertid = dbAdvertid;
    }

    public long getCodeMapping() {
        return codeMapping;
    }

    public void setCodeMapping(long codeMapping) {
        this.codeMapping = codeMapping;
    }
}
