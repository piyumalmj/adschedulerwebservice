/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.vclabs.nash.model.entity.WorkOrder;

/**
 *
 * @author hp
 */
public class FiltersModel {

    private int workOrderId = 0;

    private String agent;
    private String client;
    private String product;
    private String scheduleRef;
    private String me;
    private String woType;
    private String invoiceType;
    private String revMonth;
    private int commission = -999;
    private int agencyclient = -999;

    private String start;
    private Date startDate;

    private String end;
    private Date endDate;

    private String modifyDateS;
    private Date modifyDate;

    private String status;
    private WorkOrder.AutoStatus autoStatus = WorkOrder.AutoStatus.Initial;
    private WorkOrder.ManualStatus manualStatus = WorkOrder.ManualStatus.Initial;
    private Boolean autoStatusB = false;
    private Boolean manualmaStatusB = false;

    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getScheduleRef() {
        return scheduleRef;
    }

    public void setScheduleRef(String scheduleRef) {
        this.scheduleRef = scheduleRef;
    }

    public String getMe() {
        return me;
    }

    public void setMe(String me) {
        this.me = me;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
        try {
            this.startDate = dateFormate.parse(this.start);
        } catch (Exception e) {
            this.start = "";
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
        try {
            this.endDate = dateFormate.parse(this.end);
        } catch (Exception e) {
            this.end = "";
        }
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SimpleDateFormat getDateFormate() {
        return dateFormate;
    }

    public void setDateFormate(SimpleDateFormat dateFormate) {
        this.dateFormate = dateFormate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        setAutoAndManualStatus(this.status);
    }

    public String getModifyDateS() {
        return modifyDateS;
    }

    public void setModifyDateS(String modifyDateS) {
        this.modifyDateS = modifyDateS;
        try {
            this.modifyDate = dateFormate.parse(this.modifyDateS);
        } catch (Exception e) {
            this.modifyDateS = "";
        }
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public WorkOrder.AutoStatus getAutoStatus() {
        return autoStatus;
    }

    public void setAutoStatus(WorkOrder.AutoStatus autoStatus) {
        this.autoStatus = autoStatus;
    }

    public WorkOrder.ManualStatus getManualStatus() {
        return manualStatus;
    }

    public void setManualStatus(WorkOrder.ManualStatus manualStatus) {
        this.manualStatus = manualStatus;
    }

    public Boolean getAutoStatusB() {
        return autoStatusB;
    }

    public void setAutoStatusB(Boolean autoStatusB) {
        this.autoStatusB = autoStatusB;
    }

    public Boolean getManualmaStatusB() {
        return manualmaStatusB;
    }

    public void setManualmaStatusB(Boolean manualmaStatusB) {
        this.manualmaStatusB = manualmaStatusB;
    }

    public String getWoType() {
        return woType;
    }

    public void setWoType(String woType) {
        if (!woType.equals("select")) {
            this.woType = woType;
        }
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        if (!invoiceType.equals("select")) {
            this.invoiceType = invoiceType;
            this.commission = Integer.parseInt(invoiceType.split("-")[0]);
            String agn = invoiceType.split("-")[1];
            if (agn.equals("Direct")) {
                this.agencyclient = 0;
            } else {
                this.agencyclient = 1;
            }
        }
    }

    public String getRevMonth() {
        return revMonth;
    }

    public void setRevMonth(String revMonth) {
        this.revMonth = revMonth;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public int getAgencyclient() {
        return agencyclient;
    }

    public void setAgencyclient(int agencyclient) {
        this.agencyclient = agencyclient;
    }

    private void setAutoAndManualStatus(String status) {
        if (status.equals("Initial") || status.equals("IncompleteSchedule") || status.equals("CompleteSchedule") || status.equals("CompleteAiring") || status.equals("CompleteInvoicing")) {

            switch (status) {
                case "IncompleteSchedule":
                    autoStatus = WorkOrder.AutoStatus.IncompleteSchedule;
                    break;

                case "CompleteSchedule":
                    autoStatus = WorkOrder.AutoStatus.CompleteSchedule;
                    break;

                case "CompleteAiring":
                    autoStatus = WorkOrder.AutoStatus.CompleteAiring;
                    break;

                case "CompleteInvoicing":
                    autoStatus = WorkOrder.AutoStatus.CompleteInvoicing;
                    break;

                default:
                    break;
            }

            this.autoStatusB = true;
        }

        if (status.equals("Initial") || status.equals("Revised") || status.equals("Hold") || status.equals("PaymentDone")) {
            switch (status) {
                case "Revised":
                    manualStatus = WorkOrder.ManualStatus.Revised;
                    break;

                case "Hold":
                    manualStatus = WorkOrder.ManualStatus.Hold;
                    break;

                case "PaymentDone":
                    manualStatus = WorkOrder.ManualStatus.PaymentDone;
                    break;

                default:
                    break;
            }

            this.manualmaStatusB = true;
        }
    }

}
