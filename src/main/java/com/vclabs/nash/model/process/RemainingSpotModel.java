/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
public class RemainingSpotModel {

    private int advertDuration;
    private int advetCount = 0;
    private int channelID;

    public RemainingSpotModel(int advertDuration, int remainingSec) {
        this.advertDuration = advertDuration;
        this.advetCount = remainingSec;
    }

    public RemainingSpotModel() {
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public int getAdvetCount() {
        return advetCount;
    }

    public void setAdvetCount(int advetCount) {
        this.advetCount = advetCount;
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public void addOne() {
        this.advetCount++;
    }
    
}
