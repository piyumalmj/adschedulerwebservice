/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;
import com.vclabs.nash.model.view.reporting.SalesView;

/**
 *
 * @author Nalaka
 * @since 28-02-2018
 */
@Component
public class SellerExcel extends Common {

    private final String WEBDIRECTORY = "SellerExcel";
    private final String FILEPATH = "SellerReport.xls";

    private CellStyle cellStyle;
    private CellStyle tableHeaderStyle;

    public void generateSellerReport(List<SalesView> sellerList, HttpServletRequest request) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("SellerReport");

        int rowCount = 1;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead);

        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.DOWN);

        for (SalesView sellerView : sellerList) {
            int cellCount = 0;

            HSSFRow row = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getWorkOrderId());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getAgencyName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getClientName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getWorkOrderType());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getInvoiceType());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getMonth());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getME());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getAmount());//df.format(sellerView.getAmount())
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getPrimeSpotCount());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getNonPrimeSpotCount());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getRate());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getWorkOrderStatus());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            if (sellerView.getInvoiceDate() != null) {
                cell.setCellValue(sellerView.getInvoiceDate().toString());
            } else {
                cell.setCellValue("");
            }
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getInvoiceNo());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(sellerView.getPayment());
            cell.setCellStyle(cellStyle);

        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead) {
        int fieldCount = 0;

        Cell cell;
        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("WO ID");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Agency Name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Client Name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Wo type");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice type");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Month");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("ME");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Amount");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Prime time spot count");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Non prime time spot count");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Rate");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("WO Status");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice no");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Payment");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        return fieldCount;
    }

    public void downloadSellerReportExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
