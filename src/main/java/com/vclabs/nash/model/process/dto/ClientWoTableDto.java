package com.vclabs.nash.model.process.dto;

/**
 * Created by Nalaka on 2019-03-21.
 */
public class ClientWoTableDto {

    private int clientId;

    private String clientName;

    private String clientType;

    public ClientWoTableDto(int clientId, String clientName,String clientType) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.clientType = clientType;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }
}
