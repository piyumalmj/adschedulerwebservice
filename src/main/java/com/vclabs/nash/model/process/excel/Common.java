/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.poi.hssf.record.ExtendedFormatRecord.CENTER;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import static org.apache.poi.ss.usermodel.CellStyle.ALIGN_CENTER;
import static org.apache.poi.ss.usermodel.CellStyle.BORDER_THIN;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hibernate.internal.util.io.StreamCopier.BUFFER_SIZE;

/**
 *
 * @author hp
 */
public class Common {

    private static final Logger LOGGER = LoggerFactory.getLogger(Common.class);

    public Common() {

    }

    public String getFilePath(HttpServletRequest request, String webDirectory, String filePath, Boolean createFile) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT/";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        File pdf_file_path = new File(webServerPath + webDirectory + "/" + filePath);

        if (pdf_file_path.isFile() && createFile) {
            pdf_file_path.delete();
        }

        return pdf_file_path.toString();
    }

    public HSSFSheet createSheet(HSSFWorkbook workBook, String sheetName) {
        HSSFSheet sheet = workBook.createSheet(sheetName);
        return sheet;
    }

    public Boolean writeFile(HSSFWorkbook workbook, String filePath) throws Exception {
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            workbook.write(fileOut);
            fileOut.close();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in Common in writeFile", e.getMessage());
            throw e;
        }
    }

    public Boolean aligncolumns(HSSFSheet shoot, int columnsCount) {
        try {
            for (int i = 0; i < columnsCount; i++) {
                shoot.autoSizeColumn(i);
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in Common in aligncolumns", e.getMessage());
            throw e;
        }
    }

    /*--------------------------------FileDownload--------------------------------------------*/
    public void downloadFile(HttpServletResponse response, String fullPath) {

        try {

            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            String mimeType = "application/octet-stream";
            /*}*/

            // set content attributes for the response
            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            // set headers for the response
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();
        } catch (IOException e) {
            LOGGER.debug("Exception in Common while downloading the file", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception in Common while downloading the file", e.getMessage());
            throw e;
        }
    }

    public CellStyle setDataRowCellStyle(HSSFWorkbook workBook) {
        CellStyle style;
        Font monthFont = workBook.createFont();
        monthFont.setFontHeightInPoints((short) 11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = workBook.createCellStyle();
        style.setAlignment(ALIGN_CENTER);
        style.setWrapText(true);
        style.setBorderRight(BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());

        return style;
    }

    public CellStyle tableHeaderStyle(HSSFWorkbook workBook) {
        CellStyle style;
        Font monthFont = workBook.createFont();
        monthFont.setFontHeightInPoints((short) 11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = workBook.createCellStyle();
        style.setAlignment(ALIGN_CENTER);
        style.setVerticalAlignment(CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setFillPattern((short) 1);
        style.setWrapText(true);
        return style;
    }

}
