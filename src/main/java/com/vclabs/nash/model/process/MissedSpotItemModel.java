/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.process;

import java.util.Date;

/**
 *
 * @author user
 */
public class MissedSpotItemModel implements Comparable<MissedSpotItemModel> {

    private int scheduleId;
    private String actualTimeBelt;
    private String schedulDate;
    private String status;
    private String schedulStartTime;
    private String schedulEndTime;
    private String cluster;
    private String scheduleTimeBelt;
    private String shiftedTimeBelt;

    private int workOrderId;
    private String workOrderName;
    private String workOrderEndDate;

    private int channelId;
    private String channelName;

    private int advertId;
    private String advertNme;
    private String advertType;
    private int duratuion;

    private String client;
    private int clientId;
    private String priority = "";

    private Date scheduleEndDate;

    public MissedSpotItemModel() {
    }

    public MissedSpotItemModel(int scheduleId, int workOrderId, String advertNme, String actualTimeBelt, int duratuion) {
        this.scheduleId = scheduleId;
        this.workOrderId = workOrderId;
        this.advertNme = advertNme;
        this.actualTimeBelt = actualTimeBelt;
        this.duratuion = duratuion;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getAdvertNme() {
        return advertNme;
    }

    public void setAdvertNme(String advertNme) {
        this.advertNme = advertNme;
    }

    public int getDuratuion() {
        return duratuion;
    }

    public void setDuratuion(int duratuion) {
        this.duratuion = duratuion;
    }

    public String getActualTimeBelt() {
        return actualTimeBelt;
    }

    public void setActualTimeBelt(String actualTimeBelt) {
        this.actualTimeBelt = actualTimeBelt;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String cannelName) {
        this.channelName = cannelName;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getSchedulDate() {
        return schedulDate;
    }

    public void setSchedulDate(String schedulDate) {
        this.schedulDate = schedulDate;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public String getAdvertType() {
        return advertType;
    }

    public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }

    public String getWorkOrderName() {
        return workOrderName;
    }

    public void setWorkOrderName(String workOrderName) {
        this.workOrderName = workOrderName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = getStatus(status);
    }

    public String getSchedulStartTime() {
        return schedulStartTime;
    }

    public void setSchedulStartTime(String schedulStartTime) {
        this.schedulStartTime = schedulStartTime;
    }

    public String getSchedulEndTime() {
        return schedulEndTime;
    }

    public void setSchedulEndTime(String schedulEndTime) {
        this.schedulEndTime = schedulEndTime;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus(String Status) {
        String sta = "";
        switch (Status) {
            case "5":
                sta = "Missed";
                break;
            case "8":
                sta = "Dummy";
                break;
            case "9":
                sta = "Suspended";
                break;
            case "11":
                sta = "Skipped";
                break;
            default:
        }

        return sta;
    }

    public String getWorkOrderEndDate() {
        return workOrderEndDate;
    }

    public void setWorkOrderEndDate(String workOrderEndDate) {
        this.workOrderEndDate = workOrderEndDate;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getScheduleTimeBelt() {
        return scheduleTimeBelt;
    }

    public void setScheduleTimeBelt(String scheduleTimeBelt) {
        this.scheduleTimeBelt = scheduleTimeBelt;
    }

    public String getShiftedTimeBelt() {
        return shiftedTimeBelt;
    }

    public void setShiftedTimeBelt(String shiftedTimeBelt) {
        this.shiftedTimeBelt = shiftedTimeBelt;
    }

    @Override
    public int compareTo(MissedSpotItemModel o) {
        return this.scheduleTimeBelt.compareTo(o.scheduleTimeBelt);
    }

    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }
}