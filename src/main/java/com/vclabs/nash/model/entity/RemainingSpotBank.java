/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "remaining_spot_bank")
public class RemainingSpotBank implements Serializable {

    private static final long serialVersionID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "remaining_id")
    private Integer remainingId;

    @Basic(optional = false)
    @Column(name = "remaining_spot")
    private Integer remainingSpot;
    
    @Basic(optional = false)
    @Column(name = "comment")
    private String comment;

    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workorderid;

    @JoinColumn(name = "Client", referencedColumnName = "Client_id")
    @ManyToOne(optional = false)
    private ClientDetails client;
    
    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne
    private Advertisement advertid;

    public RemainingSpotBank() {
    }

    public RemainingSpotBank(Integer remainingSpot, String comment, WorkOrder workorderid, ClientDetails client, Advertisement advertid) {
        this.remainingSpot = remainingSpot;
        this.comment = comment;
        this.workorderid = workorderid;
        this.client = client;
        this.advertid = advertid;
    }

    public Integer getRemainingId() {
        return remainingId;
    }

    public void setRemainingId(Integer remainingId) {
        this.remainingId = remainingId;
    }

    public Integer getRemainingSpot() {
        return remainingSpot;
    }

    public void setRemainingSpot(Integer remainingSpot) {
        this.remainingSpot = remainingSpot;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public WorkOrder getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(WorkOrder workorderid) {
        this.workorderid = workorderid;
    }

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }
    
}
