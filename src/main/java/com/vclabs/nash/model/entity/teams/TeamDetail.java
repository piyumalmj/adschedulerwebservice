package com.vclabs.nash.model.entity.teams;

import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.model.entity.SecurityUsersEntity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Sanduni on 03/01/2019
 */
@Entity
@Table(name = "team_detail")
@SqlResultSetMapping(
        name = "myTeamToDoListMapping",
        classes={
                @ConstructorResult(
                        targetClass= MyTeamToDoListDto.class,
                        columns={
                                @ColumnResult(name = "teamId"),
                                @ColumnResult(name="teamMember"),
                                @ColumnResult(name="woId"),
                                @ColumnResult(name = "woName")
                        }
                )
        }
)
public class TeamDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="team_Members",
            joinColumns = @JoinColumn( name="id"),
            inverseJoinColumns = @JoinColumn( name="username")
    )
    private Set<SecurityUsersEntity> members;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="team_Admins",
            joinColumns = @JoinColumn( name="id"),
            inverseJoinColumns = @JoinColumn( name="username")
    )
    private Set<SecurityUsersEntity> admins;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SecurityUsersEntity> getMembers() {
        return members;
    }

    public void setMembers(Set<SecurityUsersEntity> members) {
        this.members = members;
    }

    public Set<SecurityUsersEntity> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<SecurityUsersEntity> admins) {
        this.admins = admins;
    }
}
