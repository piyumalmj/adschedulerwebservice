/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author user
 */
@Entity
@Table(name = "general_audit")
public class GeneralAudit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "general_audit_id")
    private Integer general_auditId;

    @Basic(optional = false)
    @Column(name = "date_and_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAndTime;

    @Basic(optional = false)
    @Column(name = "table_name")
    private String tablename;

    @Basic(optional = false)
    @Column(name = "record_id")
    private String recordId;

    @Basic(optional = false)
    @Column(name = "field")
    private String field;

    @Basic(optional = false)
    @Column(name = "new_data")
    private String newData;

    @Basic(optional = false)
    @Column(name = "old_data")
    private String oldData;

    @Basic(optional = false)
    @Column(name = "user")
    private String user;

    public GeneralAudit() {
        this(null, null, null, null, null, null);
    }
    
    public GeneralAudit(String tablename, int recordId) {
        this(null, tablename, "" + recordId, null, null, null);
    }

    public GeneralAudit(Integer general_auditId) {
        this(general_auditId, null, null, null, null, null);
    }

    public GeneralAudit(Integer general_auditId, String tablename, String recordId, String field, String newData, String oldData) {
        this.general_auditId = general_auditId;
        this.tablename = tablename;
        this.recordId = recordId;
        this.field = field;
        this.newData = newData;
        this.oldData = oldData;
        
        this.dateAndTime = new Date();
        
        String userName = "Default_User";        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            userName = authentication.getName();
        this.user = userName;
    }

    public Integer getGeneral_auditId() {
        return general_auditId;
    }

    public void setGeneral_auditId(Integer general_auditId) {
        this.general_auditId = general_auditId;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getNewData() {
        return newData;
    }

    public void setNewData(String newData) {
        this.newData = newData;
    }

    public String getOldData() {
        return oldData;
    }

    public void setOldData(String oldData) {
        this.oldData = oldData;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
