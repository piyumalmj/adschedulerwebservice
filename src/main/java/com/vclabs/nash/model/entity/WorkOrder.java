/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import com.vclabs.nash.dashboard.dto.*;
import com.vclabs.nash.model.process.dto.WODropDownDataDto;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author user
 */
@Entity
@Table(name = "work_order")
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "pendingWOAutoStatusCountMapping",
                classes={
                        @ConstructorResult(
                                targetClass= PendingWOCountByAutoStatusDto.class,
                                columns={
                                        @ColumnResult(name="woCount"),
                                        @ColumnResult(name="revenueMonth"),
                                        @ColumnResult(name="autoStatus"),
                                        @ColumnResult(name="woId"),
                                        @ColumnResult(name="scheduleId"),
                                        @ColumnResult(name="client")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "pendingWOManualStatusCountMapping",
                classes={
                        @ConstructorResult(
                                targetClass= PendingWOCountByManualStatusDto.class,
                                columns={
                                        @ColumnResult(name="woCount"),
                                        @ColumnResult(name="revenueMonth"),
                                        @ColumnResult(name="manualStatus"),
                                        @ColumnResult(name="woId"),
                                        @ColumnResult(name="scheduleId"),
                                        @ColumnResult(name="client")
                                }
                        )
                }
        ),
@SqlResultSetMapping(
        name = "revenueMonthDtoMapping",
        classes={
                @ConstructorResult(
                        targetClass= MyRecordedRevenueDto.class,
                        columns={
                                @ColumnResult(name="revenueMonth"),
                                @ColumnResult(name="totalBudget")
                        }
                )
        }
),@SqlResultSetMapping(
        name = "schedulesReadyForInvoicingDtoMapping",
        classes={
                @ConstructorResult(
                        targetClass= SchedulesReadyForInvoicingDto.class,
                        columns={
                                @ColumnResult(name="woId"),
                                @ColumnResult(name="client"),
                                @ColumnResult(name = "endDate"),
                                @ColumnResult(name = "seller")
                        }
                )
        }
),@SqlResultSetMapping(
        name = "myCompletedSchedulesDtoMapping",
        classes={
                @ConstructorResult(
                        targetClass= MyCompletedSchedulesDto.class,
                        columns={
                                @ColumnResult(name="completedSchedules"),
                                @ColumnResult(name="client"),
                                @ColumnResult(name = "seller")
                        }
                )
        }
),@SqlResultSetMapping(
        name = "SalesRevenueDTOMapping",
        classes={
                @ConstructorResult(
                        targetClass= SalesRevenueDTO.class,
                        columns={
                                @ColumnResult(name = "Work_order_id"),
                                @ColumnResult(name="Seller"),
                                @ColumnResult(name="revenueMonth"),
                                @ColumnResult(name = "totalBudget"),
                                @ColumnResult(name = "Start_date"),
                                @ColumnResult(name = "End_date")
                        }
                )
        }
),@SqlResultSetMapping(
        name = "RevenueLineDTOMapping",
        classes={
                @ConstructorResult(
                        targetClass= RevenueLineDbDTO.class,
                        columns={
                                @ColumnResult(name = "Seller"),
                                @ColumnResult(name="Start_date"),
                                @ColumnResult(name="End_date"),
                                @ColumnResult(name = "totalBudget"),
                                @ColumnResult(name = "woType"),
                                @ColumnResult(name = "Client_name")
                        }
                )
        }
),@SqlResultSetMapping(
        name = "WODropDownDataMapping",
        classes={
                @ConstructorResult(
                        targetClass= WODropDownDataDto.class,
                        columns={
                                @ColumnResult(name="workorderid"),
                                @ColumnResult(name="ordername"),
                                @ColumnResult(name="Start_date"),
                                @ColumnResult(name="End_date")
                        }
                )
        }
)
})

public class WorkOrder implements Serializable {

    public static enum AutoStatus {

        Initial("Initial"),
        IncompleteSchedule("Incomplete Schedule"),//*
        CompleteSchedule("Complete Schedule"),//*
        CompleteAiring("Complete Airing"),//*
        CompleteInvoicing("Complete Invoicing");

        private String textValue;

        AutoStatus(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }

        public void setTextValue(String textValue) {
            this.textValue = textValue;
        }
    }

    public static enum ManualStatus {

        Initial("Initial"),
        Revised("Revised"),//*
        Hold("Hold"),//*
        PaymentDone("Payment Done");

        private String textValue;

        ManualStatus(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }

        public void setTextValue(String textValue) {
            this.textValue = textValue;
        }
    }

    public static enum SystemType {
        NASH("Nash"),
        OPS("Ops");

        private String textValue;

        SystemType(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }

        public void setTextValue(String textValue) {
            this.textValue = textValue;
        }
    }

    public static enum InventoryType {
        NEW("new"),
        OLD("old");

        private String textValue;

        InventoryType(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }

        public void setTextValue(String textValue) {
            this.textValue = textValue;
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Work_order_id")
    private Integer workorderid;
    @Basic(optional = false)
    @Column(name = "Order_name")
    private String ordername;
    @Basic(optional = false)
    @Column(name = "Client")
    private int client;
    @Basic(optional = false)
    @Column(name = "Seller")
    private String seller;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Start_date")
    @Temporal(TemporalType.DATE)
    private Date startdate;
    @Basic(optional = false)
    @Column(name = "End_date")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @Basic(optional = false)
    @Column(name = "Permission_status")
    private int permissionstatus;
    //##permissionstatus=0-->New work order
    //##permissionstatus=2-->Current work order
    //##permissionstatus=3-->Expire work order
    //##permissionstatus=4-->Hold work order
    @Basic(optional = false)
    @Column(name = "B_spot_visibility")/*1=visibale to customer 2=not visiblale to customer*/

    private int bspotvisibility;
    @Basic(optional = false)
    @Column(name = "Comment")
    private String comment;
    @Basic(optional = false)
    @Column(name = "Agency_client")
    private int agencyclient;
    @Basic(optional = false)
    @Column(name = "Bill_client")
    private int billclient;
    @Basic(optional = false)
    @Column(name = "Billing_status")
    private int billingstatus = 0;

    @Basic(optional = false)
    @Column(name = "woType")
    private String woType;
    @Basic(optional = false)
    @Column(name = "schedule_ref_no")
    private String scheduleRef;
    @Basic(optional = false)
    @Column(name = "totalBudget")
    private double totalBudget = 0;
    @Basic(optional = false)
    @Column(name = "commission")
    private int commission = 0;
    @Basic(optional = false)
    @Column(name = "revenueMonth")
    private String revenueMonth = "Not_set";

    @Basic(optional = false)
    @Column(name = "autoStatus")
    private AutoStatus autoStatus = AutoStatus.Initial;
    //##statusAuto=0    -->Initial              -->When a new Wo is created, VCL can put it as Initial
    //##statusAuto=1    -->IncompleteSchedule   -->If total spots > schedule spots  & schedule spots > 1
    //##statusAuto=2    -->CompleteSchedule     -->If total spots = schedule spots
    //##statusAuto=3    -->CompleteAiring       -->Once the schedule reached to end date
    //##statusAuto=4    -->CompleteInvoicing    -->Once the Sap file is generated

    @Basic(optional = false)
    @Column(name = "manualStatus")
    private ManualStatus manualStatus = ManualStatus.Initial;
    //##manualStatus=0    -->Initial        -->When a new Wo is created, VCL can put it as Initial
    //##manualStatus=1    -->Revised  
    //##manualStatus=2    -->Hold           -->hold all spots airing in the WO
    //##manualStatus=3    -->PaymentDone    -->Once the Payment is cleared

    @Column(name = "lastModifyDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifyDate = new Date();

    private SystemType systemTypeype = SystemType.NASH;

    private InventoryType inventoryType =InventoryType.OLD;

    public WorkOrder() {
    }

    public WorkOrder(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public WorkOrder(Integer workorderid, String ordername, int client, String seller, Date date, Date startdate, Date enddate, int permissionstatus) {
        this.workorderid = workorderid;
        this.ordername = ordername;
        this.client = client;
        this.seller = seller;
        this.date = date;
        this.startdate = startdate;
        this.enddate = enddate;
        this.permissionstatus = permissionstatus;
    }

    public Integer getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public int getPermissionstatus() {
        return permissionstatus;
    }

    public void setPermissionstatus(int permissionstatus) {
        this.permissionstatus = permissionstatus;
    }

    public int getBspotvisibility() {
        return bspotvisibility;
    }

    public void setBspotvisibility(int bspotvisibility) {
        this.bspotvisibility = bspotvisibility;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getAgencyclient() {
        return agencyclient;
    }

    public void setAgencyclient(int agencyclient) {
        this.agencyclient = agencyclient;
    }

    public int getBillclient() {
        return billclient;
    }

    public void setBillclient(int billclient) {
        this.billclient = billclient;
    }

    public int getBillingstatus() {
        return billingstatus;
    }

    public void setBillingstatus(int billingstatus) {
        this.billingstatus = billingstatus;
    }

    public String getScheduleRef() {
        return scheduleRef;
    }

    public void setScheduleRef(String scheduleRef) {
        this.scheduleRef = scheduleRef;
    }

    public String getWoType() {
        return woType;
    }

    public void setWoType(String woType) {
        this.woType = woType;
    }

    public double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public AutoStatus getAutoStatus() {
        return autoStatus;
    }

    public void setAutoStatus(AutoStatus autoStatus) {
        this.autoStatus = autoStatus;
    }

    public ManualStatus getManualStatus() {
        return manualStatus;
    }

    public void setManualStatus(ManualStatus manualStatus) {
        this.manualStatus = manualStatus;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public SystemType getSystemTypeype() {
        return systemTypeype;
    }

    public void setSystemTypeype(SystemType systemTypeype) {
        this.systemTypeype = systemTypeype;
    }

    public InventoryType getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(InventoryType inventoryType) {
        this.inventoryType = inventoryType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workorderid != null ? workorderid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkOrder)) {
            return false;
        }
        WorkOrder other = (WorkOrder) object;
        return !((this.workorderid == null && other.workorderid != null) || (this.workorderid != null && !this.workorderid.equals(other.workorderid)));
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.WorkOrder[ workorderid=" + workorderid + " ]";
    }

}
