/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "logo_container_map")
public class LogoContainerMap implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "logo_container_map_id")
    private Integer logoContainerMapId;
    @Basic(optional = false)
    @Column(name = "logo_container_id")
    private Integer logoContainerId;
    @JoinColumn(name = "logo_id", referencedColumnName = "Advert_id")
    @ManyToOne(optional = false)
    private Advertisement logo;
    

    public LogoContainerMap() {
    }

    public LogoContainerMap(Integer logoContainerMapId) {
        this.logoContainerMapId = logoContainerMapId;
    }

    public Integer getLogoContainerMapId() {
        return logoContainerMapId;
    }

    public void setLogoContainerMapId(Integer logoContainerMapId) {
        this.logoContainerMapId = logoContainerMapId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logoContainerMapId != null ? logoContainerMapId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogoContainerMap)) {
            return false;
        }
        LogoContainerMap other = (LogoContainerMap) object;
        if ((this.logoContainerMapId == null && other.logoContainerMapId != null) || (this.logoContainerMapId != null && !this.logoContainerMapId.equals(other.logoContainerMapId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.LogoContainerMap[ logoContainerMapId=" + logoContainerMapId + " ]";
    }

    public Integer getLogoContainerId() {
        return logoContainerId;
    }

    public void setLogoContainerId(Integer logoContainerId) {
        this.logoContainerId = logoContainerId;
    }

    public Advertisement getLogo() {
        return logo;
    }

    public void setLogo(Advertisement logo) {
        this.logo = logo;
    }

}
