/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "channel_detail")

public class ChannelDetails implements Serializable, Comparable<ChannelDetails> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Channel_id")
    private Integer channelid;
    @Basic(optional = false)
    @Column(name = "Channel_name")
    private String channelname;
    @Basic(optional = false)
    @Column(name = "Short_name")
    private String shortname;
    @Basic(optional = false)
    @Column(name = "Video_format")
    private String videoformat;
    @Basic(optional = false)
    @Column(name = "IsManual")
    private boolean isManual = true;
    @Basic(optional = false)
    @Column(name = "Status")
    private int status = 1;
    @Basic(optional = false)
    @Column(name = "ManualDelay")
    private int manualDelay = 1;
    @JoinColumn(name = "ServerID", referencedColumnName = "ServerId")
    @OneToOne(optional = true)
    private ServerDetails serverDetails = null;
    @Basic(optional = false)
    @Column(name = "Enabled")
    private boolean enabled = true;
    @Basic(optional = false)
    @Column(name = "remark")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "logo_position")
    private String logoPosition;
    @Basic(optional = false)
    @Column(name = "last_update")
    private Date lastUpdate = new Date();
    @Basic(optional = false)
    @Column(name = "channel_logo_path")
    private String channelLogoPath;
    private int opsChannelId;

    public ChannelDetails() {
    }

    public ChannelDetails(Integer channelid) {
        this.channelid = channelid;
    }

    public ChannelDetails(Integer channelid, String channelname, String shortname, String videoformat, boolean isManual, int status, int manualDelay) {
        this.channelid = channelid;
        this.channelname = channelname;
        this.shortname = shortname;
        this.videoformat = videoformat;
        this.isManual = isManual;
        this.status = status;
        this.manualDelay = manualDelay;
    }

    public ChannelDetails(Integer channelid, String channelname, String shortname, String videoformat) {
        this.channelid = channelid;
        this.channelname = channelname;
        this.shortname = shortname;
        this.videoformat = videoformat;
        this.isManual = false;
        this.status = 0;
        this.manualDelay = 0;
    }

    public Integer getChannelid() {
        return channelid;
    }

    public void setChannelid(Integer channelid) {
        this.channelid = channelid;
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getVideoformat() {
        return videoformat;
    }

    public void setVideoformat(String videoformat) {
        this.videoformat = videoformat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (channelid != null ? channelid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChannelDetails)) {
            return false;
        }
        ChannelDetails other = (ChannelDetails) object;
        if ((this.channelid == null && other.channelid != null) || (this.channelid != null && !this.channelid.equals(other.channelid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.ChannelDetails[ channelid=" + channelid + " ]";
    }

    @Override
    public int compareTo(ChannelDetails o) {
        return o.getChannelid() - this.getChannelid();
    }

    public boolean isIsManual() {
        return isManual;
    }

    public void setIsManual(boolean isManual) {
        this.isManual = isManual;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getManualDelay() {
        return manualDelay;
    }

    public void setManualDelay(int manualDelay) {
        this.manualDelay = manualDelay;
    }

    public ServerDetails getServerDetails() {
        return serverDetails;
    }

    public void setServerDetails(ServerDetails serverDetails) {
        this.serverDetails = serverDetails;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLogoPosition() {
        return logoPosition;
    }

    public void setLogoPosition(String logoPosition) {
        this.logoPosition = logoPosition;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getChannelLogoPath() {
        return channelLogoPath;
    }

    public void setChannelLogoPath(String channelLogoPath) {
        this.channelLogoPath = channelLogoPath;
    }

    public int getOpsChannelId() {
        return opsChannelId;
    }

    public void setOpsChannelId(int opsChannelId) {
        this.opsChannelId = opsChannelId;
    }
}
