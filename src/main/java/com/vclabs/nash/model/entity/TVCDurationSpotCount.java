/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "wo_channels_tvc_spots")
public class TVCDurationSpotCount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "wo_channels_tvc_spots_id")
    private Integer woChannelsTvcSpotsId;
    @Column(name = "bonus")
    private Boolean isBonus;
    @Column(name = "duration")
    private Integer duration;
    @Column(name = "spot_count")
    private Integer spotCount;
    @ManyToOne
    @JoinColumn(name = "work_order_channel_list_id")
    private WorkOrderChannelList woChannel;

    public TVCDurationSpotCount() {
    }

    public TVCDurationSpotCount(Integer woChannelsTvcSpotsId) {
        this.woChannelsTvcSpotsId = woChannelsTvcSpotsId;
    }

    public Integer getWoChannelsTvcSpotsId() {
        return woChannelsTvcSpotsId;
    }

    public void setWoChannelsTvcSpotsId(Integer woChannelsTvcSpotsId) {
        this.woChannelsTvcSpotsId = woChannelsTvcSpotsId;
    }

    public Boolean getIsBonus() {
        return isBonus;
    }

    public void setIsBonus(Boolean bonus) {
        this.isBonus = bonus;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getSpotCount() {
        return spotCount;
    }

    public void setSpotCount(Integer spotCount) {
        this.spotCount = spotCount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (woChannelsTvcSpotsId != null ? woChannelsTvcSpotsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TVCDurationSpotCount)) {
            return false;
        }
        TVCDurationSpotCount other = (TVCDurationSpotCount) object;
        if ((this.woChannelsTvcSpotsId == null && other.woChannelsTvcSpotsId != null) || (this.woChannelsTvcSpotsId != null && !this.woChannelsTvcSpotsId.equals(other.woChannelsTvcSpotsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "schedule.model.entity.TVCDurationSpotCpunt[ woChannelsTvcSpotsId=" + woChannelsTvcSpotsId + " ]";
    }

    public WorkOrderChannelList getWoChannel() {
        return woChannel;
    }

    public void setWoChannel(WorkOrderChannelList woChannel) {
        this.woChannel = woChannel;
    }

}
