package com.vclabs.nash.model.entity.inventory;

import com.vclabs.nash.service.utill.MathUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
@Entity
@Table(name = "inventory_row")
public class InventoryRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Time fromTime;

    private Time toTime;

    //in seconds
    private double totalTvcDuration;

    //in seconds
    private double webTvcDuration;

    @Column(precision=20, scale=2)
    private BigDecimal logoPrice;

    private int logoSpots;

    private int webLogoSpots;

    @Column(precision=20, scale=2)
    private BigDecimal crawlerPrice;

    private int clusters;

    private int crawlerSpots;

    private int webCrawlerSpots;

    private int lCrawlerSpots;

    private int webLCrawlerSpots;

    @Column(precision=20, scale=2)
    private BigDecimal lCrawlerPrice;

    @OneToMany(mappedBy="inventoryRow", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private List<InventoryPriceRange> priceRanges;

    @ManyToOne
    @JoinColumn(name="inventoryId", nullable=false)
    private Inventory inventory;

    @Transient
    private double totalTvcDurationMinutes;

    @Transient
    private double webTvcDurationPercentage;

    public InventoryRow(){
        logoPrice = new BigDecimal(00.00);
        crawlerPrice = new BigDecimal(00.00);
        lCrawlerPrice = new BigDecimal(00.00);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getLogoPrice() {
        return logoPrice;
    }

    public void setLogoPrice(BigDecimal logoPrice) {
        this.logoPrice = logoPrice;
    }

    public BigDecimal getCrawlerPrice() {
        return crawlerPrice;
    }

    public void setCrawlerPrice(BigDecimal crawlerPrice) {
        this.crawlerPrice = crawlerPrice;
    }

    public int getCrawlerSpots() {
        return crawlerSpots;
    }

    public void setCrawlerSpots(int crawlerSpots) {
        this.crawlerSpots = crawlerSpots;
    }

    public List<InventoryPriceRange> getPriceRanges() {
        if(priceRanges == null){
            priceRanges = new ArrayList<>();
        }
        return priceRanges;
    }

    public void setPriceRanges(List<InventoryPriceRange> priceRanges) {
        this.priceRanges = priceRanges;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Time getFromTime() {
        return fromTime;
    }

    public void setFromTime(Time fromTime) {
        this.fromTime = fromTime;
    }

    public Time getToTime() {
        return toTime;
    }

    public void setToTime(Time toTime) {
        this.toTime = toTime;
    }

    public double getWebTvcDuration() {
        return webTvcDuration;
    }

    public void setWebTvcDuration(double webTvcDuration) {
        this.webTvcDuration = webTvcDuration;
    }

    public double getTotalTvcDurationMinutes() {
        if(totalTvcDuration == 0){
            return 0;
        }
        return totalTvcDuration / 60.0;
    }

    public void setTotalTvcDurationMinutes(double totalTvcDurationMinutes) {
        this.totalTvcDurationMinutes = totalTvcDurationMinutes;
        this.totalTvcDuration = totalTvcDurationMinutes * 60;
    }

    public double getTotalTvcDuration() {
        return totalTvcDuration;
    }

    public void setTotalTvcDuration(double totalTvcDuration) {
        this.totalTvcDuration = totalTvcDuration;
    }

    public double getWebTvcDurationPercentage() {
        if(this.webTvcDuration == 0){
            return 0;
        }
        return MathUtil.round( (this.webTvcDuration * 100) / this.totalTvcDuration, 2);
    }

    public void setWebTvcDurationPercentage(double webTvcDurationPercentage) {
        this.webTvcDurationPercentage = webTvcDurationPercentage;
        if(this.webTvcDurationPercentage == 0){
            this.webTvcDuration = 0;
        }else if(this.totalTvcDuration > 0 && this.webTvcDurationPercentage > 0){
            this.webTvcDuration = this.totalTvcDuration * this.webTvcDurationPercentage / 100.0;
        }
    }

    public int getLogoSpots() {
        return logoSpots;
    }

    public void setLogoSpots(int logoSpots) {
        this.logoSpots = logoSpots;
    }

    public int getLCrawlerSpots() {
        return lCrawlerSpots;
    }

    public void setLCrawlerSpots(int lCrawlerSpots) {
        this.lCrawlerSpots = lCrawlerSpots;
    }

    public BigDecimal getlCrawlerPrice() {
        return lCrawlerPrice;
    }

    public void setlCrawlerPrice(BigDecimal lCrawlerPrice) {
        this.lCrawlerPrice = lCrawlerPrice;
    }

    public int getClusters() {
        return clusters;
    }

    public void setClusters(int clusters) {
        this.clusters = clusters;
    }

    public int getWebLogoSpots() {
        return webLogoSpots;
    }

    public void setWebLogoSpots(int webLogoSpots) {
        this.webLogoSpots = webLogoSpots;
    }

    public int getWebCrawlerSpots() {
        return webCrawlerSpots;
    }

    public void setWebCrawlerSpots(int webCrawlerSpots) {
        this.webCrawlerSpots = webCrawlerSpots;
    }

    public int getWebLCrawlerSpots() {
        return webLCrawlerSpots;
    }

    public void setWebLCrawlerSpots(int webLCrawlerSpots) {
        this.webLCrawlerSpots = webLCrawlerSpots;
    }


}
