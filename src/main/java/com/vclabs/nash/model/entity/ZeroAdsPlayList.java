package com.vclabs.nash.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Nalaka on 2018-09-18.
 */
@Entity
public class ZeroAdsPlayList implements Serializable {

    public static enum Status {

        ACTIVATE, DEACTIVATE
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer zeroPlayListId;

    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Advertisement advertid;

    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;

    private Status advetStatus;

    private int orderNumber;

    public ZeroAdsPlayList() {
    }

    public Integer getZeroPlayListId() {
        return zeroPlayListId;
    }

    public void setZeroPlayListId(Integer zeroPlayListId) {
        this.zeroPlayListId = zeroPlayListId;
    }

    public Advertisement getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Advertisement advertid) {
        this.advertid = advertid;
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public Status getAdvetStatus() {
        return advetStatus;
    }

    public void setAdvetStatus(Status advetStatus) {
        this.advetStatus = advetStatus;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

}
