/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import com.vclabs.nash.model.process.dto.ClientWoTableDto;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author user
 */
@Entity
@Table(name = "client_details")
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "clientWoTableMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = ClientWoTableDto.class,
                                columns = {
                                        @ColumnResult(name = "Client_id"),
                                        @ColumnResult(name = "Client_name"),
                                        @ColumnResult(name = "Client_type")
                                }
                        )
                }
        )
})
public class ClientDetails implements Serializable, Comparable<ClientDetails> {

    public static enum Status {

        ACTIVE, BLACKLIST, DELETE
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Client_id")
    private Integer clientid;
    @Basic(optional = false)
    @Column(name = "Client_name")
    private String clientname;
    @Basic(optional = false)
    @Column(name = "Client_type")
    private String clienttype;
    @Basic(optional = false)
    @Column(name = "Address")
    private String address;
    @Basic(optional = false)
    @Column(name = "Email_address")
    private String emailaddress;
    @Basic(optional = false)
    @Column(name = "vat_no")
    private String vat_no;
    @Basic(optional = false)
    @Column(name = "contact_person")
    private String contact_person;
    @Basic(optional = false)
    @Column(name = "Telephone_num")
    private String telephonenum;
    @Basic(optional = false)
    @Column(name = "cx_cade")
    private String cx_cade;
    @Basic(optional = false)
    @Column(name = "Contact_details")
    private String contactdetails;
    @Basic(optional = false)
    @Column(name = "Enabled")
    private boolean enabled = true;
    private Status status = Status.ACTIVE;
    //This use onlny all advertisement view
    @Transient
    private Boolean canDelete = false;

    public ClientDetails() {

    }

    public ClientDetails(Integer clientid) {
        this.clientid = clientid;
    }

    public ClientDetails(Integer clientid, String clientname) {
        this.clientid = clientid;
        this.clientname = clientname;
    }

    public ClientDetails(Integer clientid, String clientname, String clienttype, String address, String emailaddress, String telephonenum, String contactdetails) {
        this.clientid = clientid;
        this.clientname = clientname;
        this.clienttype = clienttype;
        this.address = address;
        this.emailaddress = emailaddress;
        this.telephonenum = telephonenum;
        this.contactdetails = contactdetails;
    }

    public ClientDetails(Integer clientid, String clientname, String clienttype, String address, String emailaddress, String vat_no, String contact_person, String telephonenum, String cx_cade) {
        this.clientid = clientid;
        this.clientname = clientname;
        this.clienttype = clienttype;
        this.address = address;
        this.emailaddress = emailaddress;
        this.vat_no = vat_no;
        this.contact_person = contact_person;
        this.telephonenum = telephonenum;
        this.cx_cade = cx_cade;
    }

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getClienttype() {
        return clienttype;
    }

    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getTelephonenum() {
        return telephonenum;
    }

    public void setTelephonenum(String telephonenum) {
        this.telephonenum = telephonenum;
    }

    public String getContactdetails() {
        return contactdetails;
    }

    public void setContactdetails(String contactdetails) {
        this.contactdetails = contactdetails;
    }

    public String getVat_no() {
        return vat_no;
    }

    public void setVat_no(String vat_no) {
        this.vat_no = vat_no;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getCx_cade() {
        return cx_cade;
    }

    public void setCx_cade(String cx_cade) {
        this.cx_cade = cx_cade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientid != null ? clientid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientDetails)) {
            return false;
        }
        ClientDetails other = (ClientDetails) object;
        if ((this.clientid == null && other.clientid != null) || (this.clientid != null && !this.clientid.equals(other.clientid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.ClientDetails[ clientid=" + clientid + " ]";
    }

    @Override
    public int compareTo(ClientDetails o) {
        return o.getClientid() - this.getClientid();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }
}
