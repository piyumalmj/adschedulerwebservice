/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author user
 */
@Entity
@Table(name = "users")
public class SecurityUsersEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "name")
    private String name = "test_f_name";
    @Basic(optional = false)
    @Column(name = "employee_id")
    private String employeeId = "test_l_name";
    @Basic(optional = false)
    @Column(name = "address")
    private String address = "test_address";
    @Basic(optional = false)
    @Column(name = "email")
    private String email = "test_email";
    @Basic(optional = false)
    @Column(name = "contact")
    private String contact = "test_contact";
    @Basic(optional = false)
    @Column(name = "enabled")
    private short enabled = 1;
    @Basic(optional = false)
    @Column(name = "createworkorder")
    private short createWorkOrder = 0;
    @Basic(optional = false)
    @Column(name = "checker")
    private int checker = 0;
    @Basic(optional = false)
    @Column(name = "usergroup")
    private String usergroup = "No group";
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "username")
    private Collection<SecurityAuthoritiesEntity> securityAuthoritiesEntityCollection = new ArrayList<SecurityAuthoritiesEntity>();

    private boolean hasTeam = false;

    public SecurityUsersEntity() {
    }

    public SecurityUsersEntity(String username) {
        this.username = username;
    }

    public SecurityUsersEntity(String username, String password, short enabled) {
        this.username = username;
        this.setPassword(password);
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA");
        } catch (NoSuchAlgorithmException ex) {
        }

        md.update(password.getBytes());
        byte byteData[] = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        this.password = hexString.toString();
    }

    public short getEnabled() {
        return enabled;
    }

    public void setEnabled(short enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public int getChecker() {
        return checker;
    }

    public void setChecker(int checker) {
        this.checker = checker;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUsergroup() {
        return usergroup;
    }

    public void setUsergroup(String usergroup) {
        this.usergroup = usergroup;
    }

    public short getCreateWorkOrder() {
        return createWorkOrder;
    }

    public void setCreateWorkOrder(short createWorkOrder) {
        this.createWorkOrder = createWorkOrder;
    }

    @XmlTransient
    public Collection<SecurityAuthoritiesEntity> getSecurityAuthoritiesEntityCollection() {
        return securityAuthoritiesEntityCollection;
    }

    public void setSecurityAuthoritiesEntityCollection(SecurityAuthoritiesEntity auth) {
        this.securityAuthoritiesEntityCollection.add(auth);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityUsersEntity)) {
            return false;
        }
        SecurityUsersEntity other = (SecurityUsersEntity) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vcl.model.entity.SecurityUsersEntity[ username=" + username + " ]";
    }

    public boolean isHasTeam() {
        return hasTeam;
    }

    public void setHasTeam(boolean hasTeam) {
        this.hasTeam = hasTeam;
    }
}
