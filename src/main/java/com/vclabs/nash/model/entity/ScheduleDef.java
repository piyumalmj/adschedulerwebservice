/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.vclabs.nash.dashboard.dto.DailyMissedSpotsDto;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import com.vclabs.nash.model.dto.MissedSpotMapDto;
import com.vclabs.nash.model.dto.ScheduleAdvertDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "schedule_def")
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "dailyMissedSpotsMapping",
                classes={
                        @ConstructorResult(
                                targetClass= DailyMissedSpotsDto.class,
                                columns={
                                        @ColumnResult(name="channelName"),
                                        @ColumnResult(name="percentage")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "hightTrafficChannelsWidgetMapping",
                classes={
                        @ConstructorResult(
                                targetClass= HightTrafficChannelsWidgetDto.class,
                                columns={
                                        @ColumnResult(name="spotCount"),
                                        @ColumnResult(name="channelId"),
                                        @ColumnResult(name="channelName")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "scheduleAdvertMapping",
                classes={
                        @ConstructorResult(
                                targetClass= ScheduleAdvertDto.class,
                                columns={
                                        @ColumnResult(name="scheduleId"),
                                        @ColumnResult(name="status"),
                                        @ColumnResult(name="advertType"),
                                        @ColumnResult(name="duration")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "missesSpotViewMapping",
                classes={
                        @ConstructorResult(
                                targetClass= MissedSpotMapDto.class,
                                columns={
                                        @ColumnResult(name="Schedule_id"),
                                        @ColumnResult(name="schActualStartTime"),
                                        @ColumnResult(name="actualStartTime"),
                                        @ColumnResult(name="actualEndTime"),
                                        @ColumnResult(name="Date"),
                                        @ColumnResult(name="Status"),
                                        @ColumnResult(name="Schedule_start_time"),
                                        @ColumnResult(name="Schedule_end_time"),
                                        @ColumnResult(name="Priority_id",type = Integer.class),
                                        @ColumnResult(name="priorityCluster",type = Integer.class),
                                        @ColumnResult(name="priorityNumber",type = Integer.class),
                                        @ColumnResult(name="Comment"),
                                        @ColumnResult(name="Work_order_id"),
                                        @ColumnResult(name="Order_name"),
                                        @ColumnResult(name="End_date"),
                                        @ColumnResult(name="Client_name"),
                                        @ColumnResult(name="Client_id"),
                                        @ColumnResult(name="Advert_name"),
                                        @ColumnResult(name="Duration"),
                                        @ColumnResult(name="Advert_id"),
                                        @ColumnResult(name="Advert_type"),
                                        @ColumnResult(name="Channel_id"),
                                        @ColumnResult(name="Channel_name")
                                }
                        )
                }
        )
})

public class ScheduleDef implements Serializable, Comparable<ScheduleDef> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Schedule_id")
    private Integer scheduleid;
    @Column(name = "Sequence_num")
    private Integer sequencenum;
    @Basic(optional = false)
    @Column(name = "Schedule_start_time")
    @Temporal(TemporalType.TIME)
    private Date schedulestarttime;
    @Basic(optional = false)
    @Column(name = "Schedule_end_time")
    @Temporal(TemporalType.TIME)
    private Date scheduleendtime;
    @Basic(optional = false)
    @Column(name = "Actual_start_time")
    @Temporal(TemporalType.TIME)
    private Date actualstarttime;
    @Basic(optional = false)
    @Column(name = "Actual_end_time")
    @Temporal(TemporalType.TIME)
    private Date actualendtime;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Status")
    private String status;
    // ##### 0-Initial_State
    // ##### 1-Scheduled_State
    // ##### 2-Played_State
    // ##### 3-Stopped_State
    // ##### 5-NotPlayed_State
    // ##### 6-Stopped_by_user
    // ##### 7-Reschedule
    // ##### 8-Invalid_State //No_MediaFile
    // ##### 9-Hold_advert
    // ##### 10-Partially airing####
    // ##### 11-Skipped###
    @Basic(optional = false)
    @Column(name = "Comment")
    private String comment;
    @Basic(optional = false)
    @Column(name = "Program")
    private String program;
     @Basic(optional = false)
    @Column(name = "revenue_line")
    private String revenueLine;
    @Basic(optional = false)
    @Column(name = "month")
    private String month;
    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Advertisement advertid;
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;
    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workorderid;
    
    @JoinColumn(name = "Priority_id", referencedColumnName = "priority_id" ,nullable = true)
    @ManyToOne()
    private PriorityDef priority = null;
    
//    @Basic(optional = false)
//    @Column(name = "initial_duration")
//    private int slotDuration;
    @Basic(optional = false)
    @Column(name = "user")
    private String user;
    @Basic(optional = false)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public ScheduleDef() { 
        this(null);
    }

    public ScheduleDef(Integer scheduleid) {
        this.scheduleid = scheduleid;
        this.createdDate = new Date();        
        String userName = "Default_User";        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            userName = authentication.getName();
        this.user = userName;
        
    }

    public ScheduleDef(Integer scheduleid, Date schedulestarttime, Date scheduleendtime, Date date, String status) {
        this.scheduleid = scheduleid;
        this.schedulestarttime = schedulestarttime;
        this.scheduleendtime = scheduleendtime;
        this.date = date;
        this.status = status;
        
        this.createdDate = new Date();        
        String userName = "Default_User";        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            userName = authentication.getName();
        this.user = userName;
    }

    public Integer getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(Integer scheduleid) {
        this.scheduleid = scheduleid;
    }

    public Date getSchedulestarttime() {
        return schedulestarttime;
    }

    public void setSchedulestarttime(Date schedulestarttime) {
        this.schedulestarttime = schedulestarttime;
    }

    public Date getScheduleendtime() {
        return scheduleendtime;
    }

    public void setScheduleendtime(Date scheduleendtime) {
        this.scheduleendtime = scheduleendtime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }
    
    public String getStatusbyName() {
        String sStatus = "Undefined";
        if(status.equals("0")) sStatus = "Not Scheduled";
        else if(status.equals("1")) sStatus = "Scheduled";
        else if(status.equals("2")) sStatus = "Played";
        else if(status.equals("3")) sStatus = "Stopped";
        else if(status.equals("5")) sStatus = "Timed Out";
        else if(status.equals("6")) sStatus = "Stopped By Admin";
        else if(status.equals("7")) sStatus = "Rescheduled";
        else if(status.equals("9")) sStatus = "Suspended";
        else if (status.equals("10")) sStatus = "Partially airing";
        else if (status.equals("11")) sStatus = "Skipped";
        
        return sStatus;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WorkOrder getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(WorkOrder workorderid) {
        this.workorderid = workorderid;
    }

    public Integer getSequencenum() {
        return sequencenum;
    }

    public void setSequencenum(Integer sequencenum) {
        this.sequencenum = sequencenum;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Advertisement getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Advertisement advertid) {
        this.advertid = advertid;
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public Date getActualstarttime() {
        return actualstarttime;
    }

    public void setActualstarttime(Date actualstarttime) {
        this.actualstarttime = actualstarttime;
    }

    public Date getActualendtime() {
        return actualendtime;
    }

    public void setActualendtime(Date actualendtime) {
        this.actualendtime = actualendtime;
    }

    public String getRevenueLine() {
        return revenueLine;
    }

    public void setRevenueLine(String revenueLine) {
        this.revenueLine = revenueLine;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scheduleid != null ? scheduleid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScheduleDef)) {
            return false;
        }
        ScheduleDef other = (ScheduleDef) object;
        if ((this.scheduleid == null && other.scheduleid != null) || (this.scheduleid != null && !this.scheduleid.equals(other.scheduleid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.ScheduleDef[ scheduleid=" + scheduleid + " ]";
    }

    @Override
    public int compareTo(ScheduleDef o) {
        long date_dif = this.date.getTime() - o.date.getTime();
        if(date_dif == 0)return (int)(this.schedulestarttime.getTime() - o.schedulestarttime.getTime());
        else return (int)date_dif;            
    }

    public PriorityDef getPriority() {
        return priority;
    }

    public void setPriority(PriorityDef priority) {
        this.priority = priority;
    }

//    public int getSlotDuration() {
//        return slotDuration;
//    }
//
//    public void setSlotDuration(int slotDuration) {
//        this.slotDuration = slotDuration;
//    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
