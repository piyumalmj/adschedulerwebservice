package com.vclabs.nash.model.entity.inventory;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Nalaka on 2018-09-26.
 */
@Entity
@Table(name = "inventory_price_range")
public class InventoryPriceRange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double lowerLimit;

    private boolean equalLowerLimit;

    private double upperLimit;

    private boolean equalUpperLimit;

    @Column(precision=20, scale=2)
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name="inventoryRowId", nullable=false)
    private InventoryRow inventoryRow;

    public InventoryPriceRange(){
        price = new BigDecimal(00.00);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(double lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public boolean isEqualLowerLimit() {
        return equalLowerLimit;
    }

    public void setEqualLowerLimit(boolean equalLowerLimit) {
        this.equalLowerLimit = equalLowerLimit;
    }

    public double getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(double upperLimit) {
        this.upperLimit = upperLimit;
    }

    public boolean isEqualUpperLimit() {
        return equalUpperLimit;
    }

    public void setEqualUpperLimit(boolean equalUpperLimit) {
        this.equalUpperLimit = equalUpperLimit;
    }

    public InventoryRow getInventoryRow() {
        return inventoryRow;
    }

    public void setInventoryRow(InventoryRow inventoryRow) {
        this.inventoryRow = inventoryRow;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
