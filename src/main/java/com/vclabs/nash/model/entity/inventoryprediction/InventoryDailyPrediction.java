package com.vclabs.nash.model.entity.inventoryprediction;

import com.vclabs.nash.model.entity.ChannelDetails;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Sanduni on 08/10/2018
 */
@Entity
@Table(name = "inventory_daily_prediction")
public class InventoryDailyPrediction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date dateAndTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date date;

    private Time fromTime;

    private Time toTime;

    private double inventory;

    @OneToOne
    private ChannelDetails channelId;

    @OneToOne
    private PredictionResultMeta predictionResultMetaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getFromTime() {
        return fromTime;
    }

    public void setFromTime(Time fromTime) {
        this.fromTime = fromTime;
    }

    public Time getToTime() {
        return toTime;
    }

    public void setToTime(Time toTime) {
        this.toTime = toTime;
    }

    public double getInventory() {
        return inventory;
    }

    public void setInventory(double inventory) {
        this.inventory = inventory;
    }

    public ChannelDetails getChannelId() {
        return channelId;
    }

    public void setChannelId(ChannelDetails channelId) {
        this.channelId = channelId;
    }

    public PredictionResultMeta getPredictionResultMetaId() {
        return predictionResultMetaId;
    }

    public void setPredictionResultMetaId(PredictionResultMeta predictionResultMetaId) {
        this.predictionResultMetaId = predictionResultMetaId;
    }
}
