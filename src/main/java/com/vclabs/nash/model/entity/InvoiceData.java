/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.vclabs.nash.model.view.CustomDateSerializer;

/**
 *
 * @author hp
 */
@Entity
@Table(name = "invoice_data")
public class InvoiceData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Invoice_data_id")
    private Integer invoiceDataId;

    @JsonSerialize(using = CustomDateSerializer.class)
    @Basic(optional = false)
    @Column(name = "invoiceDate")
    private Date invoiceDate;

    @Basic(optional = false)
    @Column(name = "invoiceNo")
    private Integer invoiceNo;

    @Basic(optional = false)
    @Column(name = "handOverDate_ME")
    private String handOverDate_ME;

    @Basic(optional = false)
    @Column(name = "handOverDate_Agency")
    private String handOverDate_Agency;

    @JsonSerialize(using = CustomDateSerializer.class)
    @Basic(optional = false)
    @Column(name = "Payment_Date")
    private Date Payment_Date;

    @Basic(optional = false)
    @Column(name = "paidRef")
    private String paidRef;

    @Basic(optional = false)
    @Column(name = "paidAmount")
    private double paidAmount;

    @Basic(optional = false)
    @Column(name = "difference")
    private String difference;

    @Basic(optional = false)
    @Column(name = "clearingDocNo")
    private String clearingDocNo;

    @JoinColumn(name = "invoice_id", referencedColumnName = "Invoice_id")
    @OneToOne(optional = false)
    private InvoiceDef invoiceId;

    public InvoiceData() {
    }

    public Integer getInvoiceDataId() {
        return invoiceDataId;
    }

    public void setInvoiceDataId(Integer invoiceDataId) {
        this.invoiceDataId = invoiceDataId;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Integer invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHandOverDate_ME() {
        return handOverDate_ME;
    }

    public void setHandOverDate_ME(String handOverDate_ME) {
        this.handOverDate_ME = handOverDate_ME;
    }

    public String getHandOverDate_Agency() {
        return handOverDate_Agency;
    }

    public void setHandOverDate_Agency(String handOverDate_Agency) {
        this.handOverDate_Agency = handOverDate_Agency;
    }

    public Date getPayment_Date() {
        return Payment_Date;
    }

    public void setPayment_Date(Date Payment_Date) {
        this.Payment_Date = Payment_Date;
    }

    public String getPaidRef() {
        return paidRef;
    }

    public void setPaidRef(String paidRef) {
        this.paidRef = paidRef;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getClearingDocNo() {
        return clearingDocNo;
    }

    public void setClearingDocNo(String clearingDocNo) {
        this.clearingDocNo = clearingDocNo;
    }

    public InvoiceDef getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(InvoiceDef invoiceId) {
        this.invoiceId = invoiceId;
    }

}
