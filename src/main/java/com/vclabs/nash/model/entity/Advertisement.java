/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.model.view.CustomDateSerializer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import com.vclabs.nash.model.process.dto.AdvertisementDropDownDataDto;

/**
 *
 * @author user
 */
@Entity
@Table(name = "advertisement_def")
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "ingestStatusWidgetMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = IngestStatusWidgetDto.class,
                                columns = {
                                        @ColumnResult(name = "cutId"),
                                        @ColumnResult(name = "client"),
                                        @ColumnResult(name = "status")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "priorityAdvertisementWidgetMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = PriorityAdvertisementWidgetDto.class,
                                columns = {
                                        @ColumnResult(name = "advertId"),
                                        @ColumnResult(name = "advertName"),
                                        @ColumnResult(name = "status"),
                                        @ColumnResult(name = "priorityNo"),
                                        @ColumnResult(name = "timeBelt"),
                                        @ColumnResult(name = "channel")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "AdvertisementDropDownDataMapping",
                classes={
                        @ConstructorResult(
                                targetClass= AdvertisementDropDownDataDto.class,
                                columns={
                                        @ColumnResult(name="advertid"),
                                        @ColumnResult(name="advertname"),
                                        @ColumnResult(name="Advert_type")
                                }
                        )
                }
        )
})
public class Advertisement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Advert_id")
    private Integer advertid;
    @Basic(optional = false)
    @Column(name = "Advert_name")
    private String advertname;
    @Basic(optional = false)
    @Column(name = "Advert_path")
    private String advertpath;
    //##-ADVERT ##-LOGO ##-LOGO_CONTAINER ##-CRAWLER ##-V_SHAPE ##-L_SHAPE ##-FILLER ##-SLIDE
    @Basic(optional = false)
    @Column(name = "Advert_type")
    private String adverttype;
    @Basic(optional = false)
    @Column(name = "Commercial_category")
    private String commercialcategory;
    @Basic(optional = false)
    @Column(name = "Duration")
    private int duration;
    @Basic(optional = false)
    @Column(name = "Language")
    private String language;
    @Basic(optional = false)
    @Column(name = "vedioType")
    private String vedioType;
    @JoinColumn(name = "Client", referencedColumnName = "Client_id")
    @ManyToOne(optional = false)
    private ClientDetails client;
    @Basic(optional = false)
    @Column(name = "LogoContainer_Id")
    private int logoContainerId;
    @Column(name = "Width")
    private int width;
    @Basic(optional = false)
    @Column(name = "Hight")
    private int hight;
    @Basic(optional = false)
    @Column(name = "X_position")
    private int xposition;
    @Basic(optional = false)
    @Column(name = "Y_position")
    private int yposition;
    @Basic(optional = false)
    @Column(name = "File_Available")
    private boolean isFileAvailable = false;
    @Basic(optional = false)
    @Column(name = "create_date")
    @JsonSerialize(using = CustomDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate = new Date();
    @Basic(optional = false)
    @Column(name = "expire_date")
    @JsonSerialize(using = CustomDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expireDate = new Date();
    @Basic(optional = false)
    @Column(name = "suspend_date")
    @JsonSerialize(using = CustomDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date suspendDate = new Date();
    @Basic(optional = false)
    @Column(name = "user")
    private String user = "test_user";
    @Basic(optional = false)
    @Column(name = "enabled")
    private int Enabled = 1;//##1=enabled##0=disable
    @Basic(optional = false)
    @Column(name = "status")
    private int status = 1;//##1=Activate##2-Hold##3-Deleted
    @JsonSerialize(using = CustomDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifydate = new Date();

    //This use onlny all advertisement view
    @Transient
    private Boolean canDelete = false;
    @Transient
    private String statustName;
    @Transient
    private Boolean canClientChange = false;

    @JoinColumn(name = "codeMapping", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private CodeMapping codeMapping;

    public Advertisement() {
        this(null, null, null, null, null, 0, null, null);
    }

    public Advertisement(Integer advertid) {
        this(advertid, null, null, null, null, 0, null, null);
    }

    public Advertisement(Integer advertid, String advertname, String advertpath, String adverttype, String commercialcategory, int duration, String language, ClientDetails client) {
        this.advertid = advertid;
        this.advertname = advertname;
        this.advertpath = advertpath;
        this.adverttype = adverttype;
        this.commercialcategory = commercialcategory;
        this.duration = duration;
        this.language = language;
        this.client = client;

        String userName = "Default_User";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            userName = authentication.getName();
        }
        this.user = userName;
    }

    public Integer getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Integer advertid) {
        this.advertid = advertid;
    }

    public String getAdvertname() {
        return advertname;
    }

    public void setAdvertname(String advertname) {
        this.advertname = advertname;
    }

    public String getAdvertpath() {
        return advertpath;
    }

    public void setAdvertpath(String advertpath) {
        this.advertpath = advertpath;
    }

    public String getAdverttype() {
        return adverttype;
    }

    public void setAdverttype(String adverttype) {
        this.adverttype = adverttype;
    }

    public String getCommercialcategory() {
        return commercialcategory;
    }

    public void setCommercialcategory(String commercialcategory) {
        this.commercialcategory = commercialcategory;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public int getLogoContainerId() {
        return logoContainerId;
    }

    public void setLogoContainerId(int logoContainerId) {
        this.logoContainerId = logoContainerId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public int getXposition() {
        return xposition;
    }

    public void setXposition(int xposition) {
        this.xposition = xposition;
    }

    public int getYposition() {
        return yposition;
    }

    public void setYposition(int yposition) {
        this.yposition = yposition;
    }

    public boolean isFileAvailable() {
        return isFileAvailable;
    }

    public void setFileAvailable(boolean IsFileAvailable) {
        this.isFileAvailable = IsFileAvailable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(Date suspendDate) {
        this.suspendDate = suspendDate;
    }

    public int getEnabled() {
        return Enabled;
    }

    public void setEnabled(int Enabled) {
        this.Enabled = Enabled;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Date getLastModifydate() {
        return lastModifydate;
    }

    public void setLastModifydate(Date lastModifydate) {
        this.lastModifydate = lastModifydate;
    }

    public String getVedioType() {
        return vedioType;
    }

    public void setVedioType(String vedioType) {
        this.vedioType = vedioType;
    }

    public String getStatustName() {
        if (this.Enabled==1 && this.status == 1) {
            statustName = "Active";
        } else if (this.Enabled==0 && this.status == 1) {
            statustName = "Expire";
        } else if (this.Enabled==0 && this.status == 2) {
            statustName = "Suspended";
        } else if (this.status == 3) {
            statustName = "Deleted";
        } else {
            statustName = "Check";
        }
        return statustName;
    }

    public void setStatustName(String statustName) {
        this.statustName = statustName;
    }

    public Boolean getCanClientChange() {
        return canClientChange;
    }

    public void setCanClientChange(Boolean canClientChange) {
        this.canClientChange = canClientChange;
    }

    public CodeMapping getCodeMapping() {
        return codeMapping;
    }

    public void setCodeMapping(CodeMapping codeMapping) {
        this.codeMapping = codeMapping;
    }
}
