package com.vclabs.nash.model.entity;

import javax.persistence.*;

/**
 * Created by dperera on 18/09/2019.
 */
@Entity
@Table(name = "advert_setting")
public class AdvertSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "min_limit")
    private Integer minLimit;

    @Basic(optional = false)
    @Column(name = "max_limit")
    private Integer maxLimit;

    @Basic(optional = false)
    @Column(name = "max_slots")
    private Integer maxSlots;

    @JoinColumn(name = "channel_id", referencedColumnName = "Channel_id")
    @OneToOne
    private ChannelDetails channelDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMinLimit() {
        return minLimit;
    }

    public void setMinLimit(Integer minLimit) {
        this.minLimit = minLimit;
    }

    public Integer getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(Integer maxLimit) {
        this.maxLimit = maxLimit;
    }

    public ChannelDetails getChannelDetails() {
        return channelDetails;
    }

    public void setChannelDetails(ChannelDetails channelDetails) {
        this.channelDetails = channelDetails;
    }

    public Integer getMaxSlots() {
        return maxSlots;
    }

    public void setMaxSlots(Integer maxSlots) {
        this.maxSlots = maxSlots;
    }
}
