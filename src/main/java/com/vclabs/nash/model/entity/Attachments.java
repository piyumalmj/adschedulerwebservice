/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */


package com.vclabs.nash.model.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.vclabs.nash.model.view.CustomDateTimeSerializer;

/**
 *
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "attachments")
public class Attachments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "attachments_id")
    private Integer attachmentsId;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "file_path")
    private String filePath;
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date createdDate;
    @Column(name = "created_user")
    private String createdUser;
    @Column(name = "enabled")
    private Boolean enabled;
    @Column(name = "reference_table")
    private String referenceTable;
    @Column(name = "reference_id")
    private Integer referenceId;
    @Column(name = "description")
    private String description;
    

    public Attachments() {
        this.createdDate = new Date();
        String userName = "Default_User";        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            userName = authentication.getName();
        this.createdUser = userName;
    }

    public Attachments(Integer attachmentsId) {
        this.attachmentsId = attachmentsId;
    }

    public Integer getAttachmentsId() {
        return attachmentsId;
    }

    public void setAttachmentsId(Integer attachmentsId) {
        this.attachmentsId = attachmentsId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attachmentsId != null ? attachmentsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attachments)) {
            return false;
        }
        Attachments other = (Attachments) object;
        if ((this.attachmentsId == null && other.attachmentsId != null) || (this.attachmentsId != null && !this.attachmentsId.equals(other.attachmentsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.Attachments[ attachmentsId=" + attachmentsId + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReferenceTable() {
        return referenceTable;
    }

    public void setReferenceTable(String referenceTable) {
        this.referenceTable = referenceTable;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

}
