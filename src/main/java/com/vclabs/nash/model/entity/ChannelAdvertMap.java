package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "channel_advert_map")
public class ChannelAdvertMap implements Serializable, Comparable<ChannelAdvertMap>{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "chan_advert_id")
    private Integer channelAdvertMapid;
    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(optional = false)
    private Advertisement advertid;
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;
    @Basic(optional = false)
    @Column(name = "percentage")
    private Integer percentage;
    @Column(name = "status")
    private Integer status = 1;//1=enble 0=disable

    @Transient
    private int slotCount = 0;

    public ChannelAdvertMap() {
    }

    public ChannelAdvertMap(Integer channelAdvertMapid, Advertisement advertid, ChannelDetails channelid) {
        this.channelAdvertMapid = channelAdvertMapid;
        this.advertid = advertid;
        this.channelid = channelid;
    }

    public Integer getChannelAdvertMapid() {
        return channelAdvertMapid;
    }

    public void setChannelAdvertMapid(Integer channelAdvertMapid) {
        this.channelAdvertMapid = channelAdvertMapid;
    }

    public Advertisement getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Advertisement advertid) {
        this.advertid = advertid;
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ChannelAdvertMap{" + "channelAdvertMapid=" + channelAdvertMapid + ", advertid=" + advertid + ", channelid=" + channelid + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.advertid);
        hash = 71 * hash + Objects.hashCode(this.channelid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelAdvertMap other = (ChannelAdvertMap) obj;
        if (!Objects.equals(this.advertid, other.advertid)) {
            return false;
        }
        if (!Objects.equals(this.channelid, other.channelid)) {
            return false;
        }
        return true;
    }

    public int getSlotCount() {
        return slotCount;
    }

    public void setSlotCount(int slotCount) {
        this.slotCount = slotCount;
    }

    @Override
    public int compareTo(ChannelAdvertMap o) {
        return o.slotCount - this.slotCount;
    }

}
