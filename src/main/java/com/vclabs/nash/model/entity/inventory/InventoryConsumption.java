package com.vclabs.nash.model.entity.inventory;

import com.vclabs.nash.model.entity.ChannelDetails;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Nalaka on 2018-09-26.
 */
@Entity
@Table(name = "inventory_consumption")
public class InventoryConsumption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //in seconds
    private double spentTime;

    //in seconds
    private double remainingTime;

    @Transient
    private double remainingScheduleTime;

    private int spentLogoCount;

    private int remainingLogoCount;

    private int spentCrawlerCount;

    private int remainingCrawlerCount;

    private int spentLCrawlerCount;

    private int remainingLCrawlerCount;

    @DateTimeFormat(pattern = "yy-MM-dd-hh-mm")
    private Date fromTime;

    @DateTimeFormat(pattern = "yy-MM-dd-hh-mm")
    private Date toTime;

    @OneToOne
    private ChannelDetails channel;

    @OneToOne
    private InventoryRow inventoryRow;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(double spentTime) {
        this.spentTime = spentTime;
    }

    public double getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(double remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public ChannelDetails getChannel() {
        return channel;
    }

    public void setChannel(ChannelDetails channel) {
        this.channel = channel;
    }

    public int getSpentLogoCount() {
        return spentLogoCount;
    }

    public void setSpentLogoCount(int spentLogoCount) {
        this.spentLogoCount = spentLogoCount;
    }

    public int getRemainingLogoCount() {
        return remainingLogoCount;
    }

    public void setRemainingLogoCount(int remainingLogoCount) {
        this.remainingLogoCount = remainingLogoCount;
    }

    public int getSpentCrawlerCount() {
        return spentCrawlerCount;
    }

    public void setSpentCrawlerCount(int spentCrawlerCount) {
        this.spentCrawlerCount = spentCrawlerCount;
    }

    public int getRemainingCrawlerCount() {
        return remainingCrawlerCount;
    }

    public void setRemainingCrawlerCount(int remainingCrawlerCount) {
        this.remainingCrawlerCount = remainingCrawlerCount;
    }

    public InventoryRow getInventoryRow() {
        return inventoryRow;
    }

    public void setInventoryRow(InventoryRow inventoryRow) {
        this.inventoryRow = inventoryRow;
    }

    public double getRemainingScheduleTime() {
        return remainingScheduleTime;
    }

    public void setRemainingScheduleTime(double remainingScheduleTime) {
        this.remainingScheduleTime = remainingScheduleTime;
    }

    public int getSpentLCrawlerCount() {
        return spentLCrawlerCount;
    }

    public void setSpentLCrawlerCount(int spentLCrawlerCount) {
        this.spentLCrawlerCount = spentLCrawlerCount;
    }

    public int getRemainingLCrawlerCount() {
        return remainingLCrawlerCount;
    }

    public void setRemainingLCrawlerCount(int remainingLCrawlerCount) {
        this.remainingLCrawlerCount = remainingLCrawlerCount;
    }
}
