/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "work_order_channel_list")
public class WorkOrderChannelList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Ordered_channel_list_id")
    private Integer orderedchannellistid;
    @Basic(optional = false)
    @Column(name = "Numbert_of_spot")
    private int numbertofspot;
    @Basic(optional = false)
    @Column(name = "Bonus_spot")
    private int bonusspot;
    @Basic(optional = false)
    @Column(name = "Tvc_package_value")
    private double tvcpackagevalue;

    @Basic(optional = false)
    @Column(name = "Logo_spots")
    private int logospots;
    @Basic(optional = false)
    @Column(name = "Logo_b_spots")
    private int logobspots;
    @Basic(optional = false)
    @Column(name = "Logo_package_value")
    private double logopackagevalue;

    @Basic(optional = false)
    @Column(name = "Crowler_spots")
    private int crowlerspots;
    @Basic(optional = false)
    @Column(name = "Crowler_b_spots")
    private int crowlerbspots;
    @Basic(optional = false)
    @Column(name = "Crowler_package_value")
    private double crowlerpackagevalue;

    @Basic(optional = false)
    @Column(name = "V_sqeez_spots")
    private int vsqeezspots;
    @Basic(optional = false)
    @Column(name = "V_sqeeze_b_spots")
    private int vsqeezebspots;
    @Basic(optional = false)
    @Column(name = "V_sqeez_package_value")
    private double vsqeezpackagevalue;

    @Basic(optional = false)
    @Column(name = "L_sqeeze_spots")
    private int lsqeezespots;
    @Basic(optional = false)
    @Column(name = "L_sqeeze_b_spots")
    private int lsqeezebspots;
    @Basic(optional = false)
    @Column(name = "L_sqeez_package_value")
    private double lsqeezpackagevalue;

    @Basic(optional = false)
    @Column(name = "Package_value")
    private double packagevalue;
    @Basic(optional = false)
    @Column(name = "Avarage_package_value")
    private double avaragepackagevalue;
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;
    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workorderid;    
    @OneToMany(mappedBy="woChannel", fetch=FetchType.EAGER)
    private List<TVCDurationSpotCount> tvcSpots = new ArrayList<>();

    public WorkOrderChannelList() {
    }

    public WorkOrderChannelList(Integer orderedchannellistid) {
        this.orderedchannellistid = orderedchannellistid;
    }

    public WorkOrderChannelList(Integer orderedchannellistid, int numbertofspot, int bonusspot, int tvcpackagevalue, int logospots, int logobspots, int logopackagevalue, int crowlerspots, int crowlerbspots, int crowlerpackagevalue, int vsqeezspots, int vsqeezebspots, int vsqeezpackagevalue, int lsqeezespots, int lsqeezebspots, int lsqeezpackagevalue, int packagevalue, int avaragepackagevalue, ChannelDetails channelid, WorkOrder workorderid) {
        this.orderedchannellistid = orderedchannellistid;
        this.numbertofspot = numbertofspot;
        this.bonusspot = bonusspot;
        this.tvcpackagevalue = tvcpackagevalue;
        this.logospots = logospots;
        this.logobspots = logobspots;
        this.logopackagevalue = logopackagevalue;
        this.crowlerspots = crowlerspots;
        this.crowlerbspots = crowlerbspots;
        this.crowlerpackagevalue = crowlerpackagevalue;
        this.vsqeezspots = vsqeezspots;
        this.vsqeezebspots = vsqeezebspots;
        this.vsqeezpackagevalue = vsqeezpackagevalue;
        this.lsqeezespots = lsqeezespots;
        this.lsqeezebspots = lsqeezebspots;
        this.lsqeezpackagevalue = lsqeezpackagevalue;
        this.packagevalue = packagevalue;
        this.avaragepackagevalue = avaragepackagevalue;
        this.channelid = channelid;
        this.workorderid = workorderid;
    }

    public Integer getOrderedchannellistid() {
        return orderedchannellistid;
    }

    public void setOrderedchannellistid(Integer orderedchannellistid) {
        this.orderedchannellistid = orderedchannellistid;
    }

    public int getNumbertofspot() {
        return numbertofspot;
    }

    public void setNumbertofspot(int numbertofspot) {
        this.numbertofspot = numbertofspot;
    }

    public int getBonusspot() {
        return bonusspot;
    }

    public void setBonusspot(int bonusspot) {
        this.bonusspot = bonusspot;
    }

    public int getLogospots() {
        return logospots;
    }

    public void setLogospots(int logospots) {
        this.logospots = logospots;
    }

    public int getLogobspots() {
        return logobspots;
    }

    public void setLogobspots(int logobspots) {
        this.logobspots = logobspots;
    }

    public int getCrowlerspots() {
        return crowlerspots;
    }

    public void setCrowlerspots(int crowlerspots) {
        this.crowlerspots = crowlerspots;
    }

    public int getCrowlerbspots() {
        return crowlerbspots;
    }

    public void setCrowlerbspots(int crowlerbspots) {
        this.crowlerbspots = crowlerbspots;
    }

    public int getVsqeezspots() {
        return vsqeezspots;
    }

    public void setVsqeezspots(int vsqeezspots) {
        this.vsqeezspots = vsqeezspots;
    }

    public int getVsqeezebspots() {
        return vsqeezebspots;
    }

    public void setVsqeezebspots(int vsqeezebspots) {
        this.vsqeezebspots = vsqeezebspots;
    }

    public int getLsqeezespots() {
        return lsqeezespots;
    }

    public void setLsqeezespots(int lsqeezespots) {
        this.lsqeezespots = lsqeezespots;
    }

    public int getLsqeezebspots() {
        return lsqeezebspots;
    }

    public void setLsqeezebspots(int lsqeezebspots) {
        this.lsqeezebspots = lsqeezebspots;
    }

    public double getPackagevalue() {
        return packagevalue;
    }

    public void setPackagevalue(double packagevalue) {
        this.packagevalue = packagevalue;
    }

    public double getAvaragepackagevalue() {
        return avaragepackagevalue;
    }

    public void setAvaragepackagevalue(double avaragepackagevalue) {
        this.avaragepackagevalue = avaragepackagevalue;
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public WorkOrder getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(WorkOrder workorderid) {
        this.workorderid = workorderid;
    }

    public double getTvcpackagevalue() {
        return tvcpackagevalue;
    }

    public void setTvcpackagevalue(double tvcpackagevalue) {
        this.tvcpackagevalue = tvcpackagevalue;
    }

    public double getLogopackagevalue() {
        return logopackagevalue;
    }

    public void setLogopackagevalue(double logopackagevalue) {
        this.logopackagevalue = logopackagevalue;
    }

    public double getCrowlerpackagevalue() {
        return crowlerpackagevalue;
    }

    public void setCrowlerpackagevalue(double crowlerpackagevalue) {
        this.crowlerpackagevalue = crowlerpackagevalue;
    }

    public double getVsqeezpackagevalue() {
        return vsqeezpackagevalue;
    }

    public void setVsqeezpackagevalue(double vsqeezpackagevalue) {
        this.vsqeezpackagevalue = vsqeezpackagevalue;
    }

    public double getLsqeezpackagevalue() {
        return lsqeezpackagevalue;
    }

    public void setLsqeezpackagevalue(double lsqeezpackagevalue) {
        this.lsqeezpackagevalue = lsqeezpackagevalue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderedchannellistid != null ? orderedchannellistid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkOrderChannelList)) {
            return false;
        }
        WorkOrderChannelList other = (WorkOrderChannelList) object;
        if ((this.orderedchannellistid == null && other.orderedchannellistid != null) || (this.orderedchannellistid != null && !this.orderedchannellistid.equals(other.orderedchannellistid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.WorkOrderChannelList[ orderedchannellistid=" + orderedchannellistid + " ]";
    }

    public List<TVCDurationSpotCount> getTvcSpots() {
        return tvcSpots;
    }

    public void setTvcSpots(List<TVCDurationSpotCount> tvcSpots) {
        this.tvcSpots = tvcSpots;
    }
    
    public void addTvcSpot(TVCDurationSpotCount tvcSpot) {
        this.tvcSpots.add(tvcSpot);
    }

}
