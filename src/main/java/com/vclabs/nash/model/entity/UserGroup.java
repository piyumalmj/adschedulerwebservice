/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "user_group")
public class UserGroup implements Serializable {

    private static final long serialVersionID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ugid")
    private Integer ugid;

    @Basic(optional = false)
    @Column(name = "group_name")
    private String userGroupName;

    @Basic(optional = false)
    @Column(name = "read_ac")
    private Integer readAcc;

    @Basic(optional = false)
    @Column(name = "write_ac")
    private Integer writeAcc;

    @JoinColumn(name = "page_id", referencedColumnName = "page_id")
    @ManyToOne(optional = false)
    private PageName pageid;

    public UserGroup() {
    }

    public UserGroup(Integer ugid, String userGroupName, Integer readAcc, Integer writeAcc, PageName pageid) {
        this.ugid = ugid;
        this.userGroupName = userGroupName;
        this.readAcc = readAcc;
        this.writeAcc = writeAcc;
        this.pageid = pageid;
    }

    public Integer getUgid() {
        return ugid;
    }

    public void setUgid(Integer ugid) {
        this.ugid = ugid;
    }

    public String getUserGroupName() {
        return userGroupName;
    }

    public void setUserGroupName(String userGroupName) {
        this.userGroupName = userGroupName;
    }

    public Integer getReadAcc() {
        return readAcc;
    }

    public void setReadAcc(Integer readAcc) {
        this.readAcc = readAcc;
    }

    public Integer getWriteAcc() {
        return writeAcc;
    }

    public void setWriteAcc(Integer writeAcc) {
        this.writeAcc = writeAcc;
    }

    public PageName getPageid() {
        return pageid;
    }

    public void setPageid(PageName pageid) {
        this.pageid = pageid;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.UserGroup[ ugid=" + ugid + " ]";
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

}
