package com.vclabs.nash.model.entity1;

import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

/**
 * Created by Sanduni on 24/01/2019
 */
@Entity
@Table(name = "channel_inst")
@SqlResultSetMapping(
        name = "currentPredictionViewMapping",
        classes={
                @ConstructorResult(
                        targetClass= ChannelPredictionDto.class,
                        columns={
                                @ColumnResult(name = "id"),
                                @ColumnResult(name="name"),
                                @ColumnResult(name="count")
                        }
                )
        }
)
public class AdvertChannel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ChannelID")
    private Integer id;

    @Column(name = "ChannelNameShort")
    private String shortName;

    @Column(name = "ChannelNameLong")
    private String longName;

    @Column(name = "ServerIPAddress")
    private String serverIpAddress;

    @Column(name = "SDIPortNumber")
    private Integer sDIPortNumber;

    @Column(name = "Active")
    private int active;

    @Column(name = "Data_file_path")
    private String dataFilePath;

    @Column(name = "Hash_file_path")
    private String hashFilePath;

    @Column(name = "Advert_file_path")
    private String advertFilePath;

    @Column(name = "Engin_file_path")
    private String engineFilePath;

    @Column(name = "DestinationPath")
    private String destinationPath;

    @Column(name = "Status")
    private int status;

    @Column(name = "Commands")
    private String commands;

    @Column(name = "MIchannelId")
    private int miChannelId;

    @Column(name = "BinaryPath")
    private String binaryPath;

    @Column(name = "FreeDiskSpace")
    private Long freeDiskSpace;

    @Column(name = "ClusterPredictionSwitch")
    private String clusterPredictionSwitch;

    @Column(name = "PredictionMaxLength")
    private int predictionMaxLength;
}
