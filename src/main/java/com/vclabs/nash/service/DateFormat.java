/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nalaka Dissanayake
 * @version 1.1.4
 */
public class DateFormat {

    SimpleDateFormat date_separate_by_slash = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat date_not_separate = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat date_separate_by_dot = new SimpleDateFormat("yyyy.MM.dd");
    SimpleDateFormat date_full_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat date_separate_by_dash = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat fullTime = new SimpleDateFormat("HH:mm:ss");
    SimpleDateFormat hourAndMinutesTime = new SimpleDateFormat("HH:mm");
    SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

    public DateFormat() {
    }

    /**
     * Year month date Separate by slash
     *
     * @since 1.1.4
     * @param Date
     * @return String DD/MM/YYY
     */
    public String getDateDDMMYYY_slash(Date date) {
        return date_separate_by_slash.format(date);
    }

    /**
     * Year month date not separated
     *
     * @since 1.1.4
     * @param Date
     * @return String YYYMMDD
     */
    public String getDateYYYMMDD(Date date) {
        return date_not_separate.format(date);
    }

    /**
     * Year month date Separate by dot
     *
     * @since 1.1.4
     * @param Date
     * @return String YYY.MM.DD
     */
    public String getDateYYYMMDD_Dot(Date date) {
        return date_not_separate.format(date);
    }

    /**
     * Year month date Separate by dash Hour minutes seconds Separate by colan
     *
     * @since 1.1.4
     * @param String
     * @return String yyyy-MM-dd HH:mm:ss
     */
    public Date getDataYYYMMDD_dash_HHmmss_colan(String dateString) {
        try {
            return date_full_format.parse(dateString);
        } catch (Exception e) {
            return new Date();
        }
    }

    /**
     * Year month date Separate by dash Hour minutes seconds Separate by colan
     *
     * @since 1.1.4
     * @param String
     * @return String yyyy-MM-dd HH:mm:ss
     */
    public String getDataYYYMMDD_dash_HHmmss_colan(Date date) {
        return date_full_format.format(date);
    }

    /**
     * Year month date Separate by dash
     *
     * @since 1.1.4
     * @param Date
     * @return String yyyy-MM-dd
     */
    public Date getDataYYYMMDD_dash(String dateString) {
        try {
            return date_separate_by_dash.parse(dateString);
        } catch (Exception e) {
            return new Date();
        }
    }

    /**
     * Year month date Separate by dash
     *
     * @since 1.1.4
     * @param String
     * @return String yyyy-MM-dd
     */
    public String getDataYYYMMDD_dash(Date date) {
        return date_separate_by_dash.format(date);
    }

    /**
     * Year month date Separate by dash
     *
     * @since 1.1.4
     * @param String
     * @return String HH:MM:SS
     */
    public String getTimeHHMMSS(Date date) {
        return fullTime.format(date);
    }

    public String getTimeHHMM(Date date) {
        return hourAndMinutesTime.format(date);
    }

    /**
     * Year month date Separate by dash
     *
     * @since 1.1.4
     * @param String
     * @return String yyyy-MM-dd 00:00:00
     */
    public String getDataYYYMMDD_dash__00_00_00(Date date) {
        return default_format.format(date);
    }

    /**
     * Year month date Separate by dash
     *
     * @since 1.1.4
     * @param Date
     * @return String yyyy-MM-dd 00:00:00
     */
    public Date getDataYYYMMDD_dash__00_00_00(String date) throws ParseException {
        return default_format.parse(date);
    }
}
