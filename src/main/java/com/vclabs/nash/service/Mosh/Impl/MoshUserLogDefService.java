package com.vclabs.nash.service.Mosh.Impl;

import com.vclabs.nash.model.dao.MoshUserLogDefDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.MoshUserLogDef;
import com.vclabs.nash.model.process.MoshDetails;
import com.vclabs.nash.service.Mosh.MoshUserLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Nalaka on 2019-04-08.
 */
@Service
public class MoshUserLogDefService implements MoshUserLogService {

    @Autowired
    private MoshUserLogDefDAO moshUserLogDefDAO;

    @Transactional
    @Override
    public MoshUserLogDef save(MoshDetails moshDetails) {
        MoshUserLogDef moshUserLogDef = new MoshUserLogDef();
        moshUserLogDef.setUser(moshDetails.getUser());
        moshUserLogDef.setLoginTime(new Date());
        moshUserLogDef.setStatus(MoshUserLogDef.Status.LOGIN);
        moshUserLogDef.setIpAddress(moshDetails.getIpAddress());
        moshUserLogDef.setChannelDetails(new ChannelDetails(moshDetails.getChannelId()));
        return moshUserLogDefDAO.save(moshUserLogDef);
    }

    @Transactional
    @Override
    public MoshUserLogDef update(MoshDetails moshDetails) {
        MoshUserLogDef moshUserLogDef = moshUserLogDefDAO.getById(moshDetails.getId());
        moshUserLogDef.setLogoutTime(new Date());
        moshUserLogDef.setStatus(MoshUserLogDef.Status.LOGOUT);
        //moshUserLogDef.setIpAddress(moshDetails.getIpAddress());
        //moshUserLogDef.setChannelDetails(new ChannelDetails(moshDetails.getChannelId()));
        return moshUserLogDefDAO.update(moshUserLogDef);
    }

    @Transactional
    @Override
    public MoshUserLogDef getById(int id) {
        return moshUserLogDefDAO.getById(id);
    }
}
