package com.vclabs.nash.service.inventoryprediction.impl;

import com.vclabs.nash.service.inventoryprediction.InventoryPredictionFileService;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-10-18.
 */
@Service
public class InventoryPredictionFileServiceImpl implements InventoryPredictionFileService {

    @Value("${file.inventory.prediction.path}")
    private String inventoryFilesFolderPath;

    @Value("${file.inventory.shFile.copy}")
    private String shCopyFilePath;

    @Autowired
    private InventoryPredictionService inventoryPredictionService;

    private static final Logger logger = LoggerFactory.getLogger(InventoryPredictionFileServiceImpl.class);

    @Override
    public void copyPredictionFiles() {
        logger.info("Copy Prediction Files");
        Process p;
        try {
            List<String> cmdList = new ArrayList<>();
            cmdList.add("sh");
            cmdList.add(shCopyFilePath);
            logger.info(shCopyFilePath);
            logger.info("Process Start");
            ProcessBuilder pb = new ProcessBuilder(cmdList);
            p = pb.start();
            p.waitFor();
            logger.info("Process end");
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line = reader.readLine()) != null) {
                logger.info("===================================================== LINE :"+line);
            }
        }catch (Exception e) {logger.info("Exception in copyPredictionFiles()->InventoryPredictionFileServiceImpl:",e);}
    }

    @Override
    public void readAllInventoryFile() {
        String[] directories = getFileNames(inventoryFilesFolderPath);
        String filePath;
        for (String directoryName : directories) {
            filePath = inventoryFilesFolderPath.concat(File.separator).concat(directoryName);
            String[] files = getFileNames(filePath);
            for (String fileName : files) {
                filePath = filePath.concat(File.separator).concat("Latest").concat(File.separator).concat(fileName);
                if (fileName.startsWith("Day-Prediction")) {
                    inventoryPredictionService.saveDailyInventoryPredictions(filePath);
                } else if (fileName.startsWith("Week-Prediction")) {
                    inventoryPredictionService.saveWeeklyInventoryPredictions(filePath);
                } else if (fileName.startsWith("Month-Prediction")) {
                    inventoryPredictionService.saveMonthlyInventoryPredictions(filePath);
                } else {
                    logger.info("Invalid file name : {}", fileName);
                }
            }
        }
    }

    public String[] getFileNames(String directoryPath) {
        File file = new File(directoryPath);
        String[] fileNames = file.list();
        logger.info("File Name Array size :{}", fileNames.length);
        return fileNames;
    }
}
