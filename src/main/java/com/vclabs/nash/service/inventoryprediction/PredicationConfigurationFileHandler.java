package com.vclabs.nash.service.inventoryprediction;

import com.vclabs.nash.service.exception.PredicationConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 18/10/2018.
 */
@Component
public class PredicationConfigurationFileHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredicationConfigurationFileHandler.class);

    @Value("${file.inventory.prediction.configuration_new}")
    private String INVENTORY_PREDICTION_CONFIGURATION_NEW_FILE_PATH;

    @Value("${file.inventory.prediction.configuration_old}")
    private String INVENTORY_PREDICTION_CONFIGURATION_OLD_FILE_PATH;

    @Value("${file.inventory.shFile.write}")
    private String shWriteFilePath;

    //this method copy the configuration file to the remote server location
    public void copyConfigurationFile() throws PredicationConfigException {
        if (validate()) {
            File newFile = new File(INVENTORY_PREDICTION_CONFIGURATION_NEW_FILE_PATH);
            File oldFile = new File(INVENTORY_PREDICTION_CONFIGURATION_OLD_FILE_PATH);
            oldFile.delete();
            try {
                Files.move(newFile.toPath(), oldFile.toPath());
                newFile.delete();
                writeConfigFile();
                //sh file should be invoked here
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    private boolean validate() throws PredicationConfigException{
        try (FileReader fileReader = new FileReader(INVENTORY_PREDICTION_CONFIGURATION_NEW_FILE_PATH)){
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            int lineNumber = 1;
            while((line = bufferedReader.readLine()) != null) {
                if(lineNumber == 1){
                    if(!validateParameterNameLine(line)){
                        throw new PredicationConfigException("Parameter name line should be advert_id,ops_id,name,times");
                    }
                }else if(!validateParameterValueLine(line)){
                    throw new PredicationConfigException("Syntax error in the line number "+ lineNumber);
                }
                lineNumber++;
            }
            return true;
        } catch (FileNotFoundException e) {
            throw new PredicationConfigException("Prediction configuration file can not be found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean validateParameterNameLine(String headerLine){
        return headerLine != null && headerLine.equals("advert_id,ops_id,name,times");
    }

    private boolean validateParameterValueLine(String line){
        if(line != null){
            String[] parameters = line.split(",");
            if (parameters.length > 4 && isDigit(parameters[0]) && isDigit(parameters[1])){
                if(line.split(",")[3].startsWith("\"") && line.endsWith("\"")){ //check the time belt string format
                    String[] timeBelts = line.split("\"")[1].split(",");
                    for(int i = 0; i < timeBelts.length; i++){
                        if(!isDigit(timeBelts[i])){ //check time belt values
                            return false;
                        }
                    }
                    return true;
                }else {
                    return false;
                }
            }
        }
        return false;
    }

    private boolean isDigit(String stringValue) {
        return stringValue.chars().allMatch( Character::isDigit);
    }

    private void writeConfigFile(){

        LOGGER.info("write config file");
        Process p;
        try {
            List<String> cmdList = new ArrayList<>();
            cmdList.add("sh");
            cmdList.add(shWriteFilePath);
            LOGGER.info(shWriteFilePath);
            LOGGER.info("Process Builder");
            ProcessBuilder pb = new ProcessBuilder(cmdList);
            LOGGER.info("Process Start");
            p = pb.start();
            LOGGER.info("Process End");
            p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line = reader.readLine()) != null) {
                LOGGER.info("===================================================== LINE :"+line);
            }
        }catch (Exception e) {LOGGER.info("Exception in writeConfigFile()->PredicationConfigurationFileHandler:",e);}

    }
}
