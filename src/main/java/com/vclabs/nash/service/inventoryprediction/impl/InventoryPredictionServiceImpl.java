package com.vclabs.nash.service.inventoryprediction.impl;

import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionDailyDAO;
import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionMonthlyDAO;
import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionWeeklyDAO;
import com.vclabs.nash.model.dao.inventoryprediction.PredictionResultMetaDAO;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta.InventoryType.DAILY;
import static com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta.InventoryType.MONTHLY;
import static com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta.InventoryType.WEEKLY;

/**
 * Created by Sanduni on 08/10/2018
 */
@Service
public class InventoryPredictionServiceImpl implements InventoryPredictionService {

    @Autowired
    private InventoryPredictionDailyDAO inventoryPredictionDailyDAO;

    @Autowired
    private InventoryPredictionWeeklyDAO inventoryPredictionWeeklyDAO;

    @Autowired
    private InventoryPredictionMonthlyDAO inventoryPredictionMonthlyDAO;

    @Autowired
    private PredictionResultMetaDAO predictionResultMetaDAO;

    @Autowired
    private ChannelDetailDAO channelDetailDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryPredictionServiceImpl.class);

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

    @Override
    @Transactional
    public void saveDailyInventoryPredictions(String filePath) {
        String line = "";
        String splitBy = ",";

        PredictionResultMeta predictionResultMeta = createPredictionResultMeta(filePath, PredictionResultMeta.InventoryType.DAILY);
        Integer channelId = Integer.parseInt(getChannelId(filePath));
        Path path = FileSystems.getDefault().getPath(filePath);
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = Files.newBufferedReader(path);
            int lineNumber = 0;
            ArrayList<InventoryDailyPrediction> inventoryData = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                if (lineNumber > 0) {
                    String[] values = line.split(splitBy);
                    String invetoryValue = values[1].trim();
                    double inventoryAsSecond = Double.parseDouble(invetoryValue) * 60;
                    InventoryDailyPrediction inventoryDailyPrediction = new InventoryDailyPrediction();
                    inventoryDailyPrediction.setDateAndTime(simpleDateFormat.parse(values[0]));
                    inventoryDailyPrediction.setInventory(inventoryAsSecond);
                    inventoryDailyPrediction.setChannelId(channelDetailDAO.getChannelByOpsChannelId(channelId));
                    inventoryDailyPrediction.setFromTime(getFromTime(values[0]));
                    inventoryDailyPrediction.setToTime(getToTime(values[0]));
                    inventoryDailyPrediction.setDate(getStartOfDay(values[0]));
                    inventoryDailyPrediction.setPredictionResultMetaId(predictionResultMeta);
                    inventoryData.add(inventoryDailyPrediction);
                }
                lineNumber++;
            }
            compareDailyInventoryPredictionsWithExsitingData(inventoryData);
        } catch (Exception e) {
            LOGGER.debug("Exception occurred in saveDailyInventoryPredictions()");
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                LOGGER.debug("saveDailyInventoryPredictions close bufferedReader");
            }
        }
    }

    @Override
    @Transactional
    public void saveWeeklyInventoryPredictions(String filePath) {
        BufferedReader bufferedReader = null;
        String line = "";
        String splitBy = ",";

        PredictionResultMeta predictionResultMeta = createPredictionResultMeta(filePath, WEEKLY);
        Integer channelId = Integer.parseInt(getChannelId(filePath));

        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            int lineNumber = 0;
            ArrayList<InventoryWeeklyPrediction> inventoryData = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                if (lineNumber > 0) {
                    // use comma as separator
                    String[] values = line.split(splitBy);
                    String invetoryValue = values[1].trim();
                    double inventoryAsSecond = Double.parseDouble(invetoryValue) * 60;

                    InventoryWeeklyPrediction inventoryWeeklyPrediction = new InventoryWeeklyPrediction();
                    inventoryWeeklyPrediction.setDateAndTime(simpleDateFormat.parse(values[0]));
                    inventoryWeeklyPrediction.setInventory(inventoryAsSecond);

                    inventoryWeeklyPrediction.setChannelId(channelDetailDAO.getChannelByOpsChannelId(channelId));
                    inventoryWeeklyPrediction.setFromTime(getFromTime(values[0]));
                    inventoryWeeklyPrediction.setToTime(getToTime(values[0]));
                    inventoryWeeklyPrediction.setDate(getStartOfDay(values[0]));
                    inventoryWeeklyPrediction.setPredictionResultMetaId(predictionResultMeta);
                    inventoryData.add(inventoryWeeklyPrediction);
                }
                lineNumber++;
            }
            compareWeeklyInventoryPredictionsWithExsitingData(inventoryData);
        } catch (Exception e) {
            LOGGER.debug("Exception occurred in saveWeeklyInventoryPredictions()");
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                LOGGER.debug("saveWeeklyInventoryPredictions close bufferedReader");
            }
        }
    }

    @Override
    @Transactional
    public void saveMonthlyInventoryPredictions(String filePath){
        BufferedReader bufferedReader = null;
        String line = "";
        String splitBy = ",";

        PredictionResultMeta predictionResultMeta = createPredictionResultMeta(filePath, PredictionResultMeta.InventoryType.MONTHLY);
        Integer channelId = Integer.parseInt(getChannelId(filePath));

        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            int lineNumber = 0;
            ArrayList<InventoryMonthlyPrediction> inventoryData = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                if (lineNumber > 0) {
                    // use comma as separator
                    String[] values = line.split(splitBy);
                    String invetoryValue = values[1].trim();
                    double inventoryAsSecond = Double.parseDouble(invetoryValue) * 60;

                    InventoryMonthlyPrediction inventoryMonthlyPrediction = new InventoryMonthlyPrediction();
                    inventoryMonthlyPrediction.setDateAndTime(simpleDateFormat.parse(values[0]));
                    inventoryMonthlyPrediction.setInventory(inventoryAsSecond);

                    inventoryMonthlyPrediction.setChannelId(channelDetailDAO.getChannelByOpsChannelId(channelId));
                    inventoryMonthlyPrediction.setFromTime(getFromTime(values[0]));
                    inventoryMonthlyPrediction.setToTime(getToTime(values[0]));
                    inventoryMonthlyPrediction.setDate(getStartOfDay(values[0]));
                    inventoryMonthlyPrediction.setPredictionResultMetaId(predictionResultMeta);
                    inventoryData.add(inventoryMonthlyPrediction);
                }
                lineNumber++;
            }
            compareMonthlyInventoryPredictionsWithExsitingData(inventoryData);
        } catch (IOException e ) {
            LOGGER.debug("IOException occurred in saveMonthlyInventoryPredictions()");
        } catch (ParseException e){
            LOGGER.debug("ParseException occurred in saveMonthlyInventoryPredictions()");
        }
        finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                LOGGER.debug("saveMonthlyInventoryPredictions close bufferedReader");
            }
        }
    }

    @Override
    @Transactional
    public InventoryDailyPrediction findDailyPredictionById(long id) {
        return inventoryPredictionDailyDAO.findById(id);
    }

    @Override
    public List<InventoryDailyPrediction> findAllDailyPredictions() {
        return inventoryPredictionDailyDAO.findAll();
    }

    @Override
    public List<InventoryDailyPrediction> findLatestDailyPredictions(Date startingDate, Date lastDate) {
        return inventoryPredictionDailyDAO.findLatestPredictions(startingDate, lastDate);
    }

    @Override
    public List<InventoryWeeklyPrediction> findLatestWeeklyPredictions(Date startingDate, Date lastDate) {
        return inventoryPredictionWeeklyDAO.findLatestPredictions(startingDate, lastDate);
    }

    @Override
    public List<InventoryMonthlyPrediction> findLatestMonthlyPredictions(Date startingDate, Date lastDate) {
        return inventoryPredictionMonthlyDAO.findLatestPredictions(startingDate, lastDate);
    }

    @Transactional
    public void compareDailyInventoryPredictionsWithExsitingData(List<InventoryDailyPrediction> inventoryDailyPredictions) {
        Date startingDate = inventoryDailyPredictions.get(0).getDateAndTime();
        Date lastDate = inventoryDailyPredictions.get(inventoryDailyPredictions.size() - 1).getDateAndTime();
        List<InventoryDailyPrediction> existingInventoryDailyPredictions = findLatestDailyPredictions(startingDate, lastDate);
        for (int i = 0; i < inventoryDailyPredictions.size(); i++) {
            for (InventoryDailyPrediction existingPrediction : existingInventoryDailyPredictions) {
                if (inventoryDailyPredictions.get(i).getDateAndTime().equals(existingPrediction.getDateAndTime())) {
                    existingPrediction.setInventory(inventoryDailyPredictions.get(i).getInventory());
                    existingPrediction.setPredictionResultMetaId(inventoryDailyPredictions.get(i).getPredictionResultMetaId());
                    inventoryPredictionDailyDAO.update(existingPrediction);
                    inventoryDailyPredictions.remove(inventoryDailyPredictions.get(i));
                    i--;
                    break;
                }
            }
        }
        for (InventoryDailyPrediction prediction : inventoryDailyPredictions) {
            inventoryPredictionDailyDAO.save(prediction);
        }
    }

    @Transactional
    public void compareWeeklyInventoryPredictionsWithExsitingData(List<InventoryWeeklyPrediction> inventoryWeeklyPredictions) {
        Date startingDate = inventoryWeeklyPredictions.get(0).getDateAndTime();
        Date lastDate = inventoryWeeklyPredictions.get(inventoryWeeklyPredictions.size() - 1).getDateAndTime();
        List<InventoryWeeklyPrediction> existingInventoryWeeklyPredictions = findLatestWeeklyPredictions(startingDate, lastDate);
        for (int i = 0; i < inventoryWeeklyPredictions.size(); i++) {
            for (InventoryWeeklyPrediction existingPrediction : existingInventoryWeeklyPredictions) {
                if (inventoryWeeklyPredictions.get(i).getDateAndTime().equals(existingPrediction.getDateAndTime())) {
                    existingPrediction.setInventory(inventoryWeeklyPredictions.get(i).getInventory());
                    existingPrediction.setPredictionResultMetaId(inventoryWeeklyPredictions.get(i).getPredictionResultMetaId());
                    inventoryPredictionWeeklyDAO.update(existingPrediction);
                    inventoryWeeklyPredictions.remove(inventoryWeeklyPredictions.get(i));
                    i--;
                    break;
                }
            }
        }
        for (InventoryWeeklyPrediction prediction : inventoryWeeklyPredictions) {
            inventoryPredictionWeeklyDAO.save(prediction);
        }
    }

    @Transactional
    public void compareMonthlyInventoryPredictionsWithExsitingData(List<InventoryMonthlyPrediction> inventoryMonthlyPredictions) {
        List<InventoryMonthlyPrediction> existingInventoryMonthlyPredictions = inventoryPredictionMonthlyDAO.findAll();
        for (int i = 0; i < inventoryMonthlyPredictions.size(); i++) {
            for (InventoryMonthlyPrediction existingPrediction : existingInventoryMonthlyPredictions) {
                if (inventoryMonthlyPredictions.get(i).getDateAndTime().equals(existingPrediction.getDateAndTime())) {
                    existingPrediction.setInventory(inventoryMonthlyPredictions.get(i).getInventory());
                    existingPrediction.setPredictionResultMetaId(inventoryMonthlyPredictions.get(i).getPredictionResultMetaId());
                    inventoryPredictionMonthlyDAO.update(existingPrediction);
                    inventoryMonthlyPredictions.remove(inventoryMonthlyPredictions.get(i));
                    i--;
                    break;
                }
            }
        }
        for (InventoryMonthlyPrediction prediction : inventoryMonthlyPredictions) {
            inventoryPredictionMonthlyDAO.save(prediction);
        }
    }

    @Override
    @Transactional
    public InventoryDailyPrediction save(InventoryDailyPrediction inventoryDailyPrediction) {
        inventoryDailyPrediction = inventoryPredictionDailyDAO.save(inventoryDailyPrediction);
        return inventoryDailyPrediction;
    }

    @Override
    @Transactional
    public InventoryWeeklyPrediction save(InventoryWeeklyPrediction inventoryWeeklyPrediction) {
        inventoryWeeklyPrediction = inventoryPredictionWeeklyDAO.save(inventoryWeeklyPrediction);
        return inventoryWeeklyPrediction;
    }

    @Override
    @Transactional
    public InventoryMonthlyPrediction save(InventoryMonthlyPrediction inventoryMonthlyPrediction) {
        inventoryMonthlyPrediction = inventoryPredictionMonthlyDAO.save(inventoryMonthlyPrediction);
        return inventoryMonthlyPrediction;
    }

    @Override
    public List<InventoryDailyPrediction> getLatestDailyInventory() {
        return inventoryPredictionDailyDAO.findUnProcessedPredictions();
    }

    @Override
    @Transactional
    public List<InventoryWeeklyPrediction> getLatestWeeklyInventory() {
        return inventoryPredictionWeeklyDAO.findLatestWeeklyPrediction();
    }

    @Override
    @Transactional
    public List<InventoryMonthlyPrediction> getLatestMonthlyInventory() {
        return inventoryPredictionMonthlyDAO.findLatestMonthlyPrediction();
    }

    @Override
    @Transactional
    public void updateDailyPredictionResultMeta() {
        List<PredictionResultMeta> dailyPredictionResultMetalis = predictionResultMetaDAO.findLatestPredictionResultMetaByInventoryType(DAILY);
        updatePredictionResultMeta(dailyPredictionResultMetalis);
    }

    @Override
    @Transactional
    public void updateWeeklyPredictionResultMeta() {
        List<PredictionResultMeta> weeklyPredictionResultMetalis = predictionResultMetaDAO.findLatestPredictionResultMetaByInventoryType(WEEKLY);
        updatePredictionResultMeta(weeklyPredictionResultMetalis);
    }

    @Override
    @Transactional
    public void updateMonthlylyPredictionResultMeta() {
        List<PredictionResultMeta> monthlyPredictionResultMetalis = predictionResultMetaDAO.findLatestPredictionResultMetaByInventoryType(MONTHLY);
        updatePredictionResultMeta(monthlyPredictionResultMetalis);
    }

    private void updatePredictionResultMeta(List<PredictionResultMeta> predictionResultMetaList) {
        for (PredictionResultMeta predictionResultMeta : predictionResultMetaList) {
            predictionResultMeta.setProcess(Boolean.TRUE);
            predictionResultMetaDAO.update(predictionResultMeta);
        }
    }

    private String getChannelId(String filePath) {
        File file = new File(filePath);
        String fileName = file.getName();
        fileName = fileName.substring(0, fileName.indexOf('.'));
        fileName = fileName.substring(fileName.lastIndexOf('-') + 1);
        return fileName;
    }

    private Date getStartOfDay(String date) {
        Date startDate = null;
        try {
            Date dateAndTime = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAndTime);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            startDate = calendar.getTime();
        } catch (Exception e) {
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return startDate;
    }

    private Time getFromTime(String date) {
        Time fromTime = null;
        try {
            Date dateAndTime = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAndTime);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            fromTime = new Time(dateAndTime.getTime());

        } catch (ParseException e) {
            LOGGER.debug("ParseException : {}", e.getMessage());
        }
        return fromTime;
    }

    private Time getToTime(String date) {
        Time toTime = null;
        try {
            Date dateAndTime = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateAndTime);
            calendar.add(Calendar.HOUR_OF_DAY, 1);
            dateAndTime = calendar.getTime();
            toTime = new Time(dateAndTime.getTime());
        } catch (ParseException e) {
            LOGGER.debug("ParseException : {}", e.getMessage());
        }
        return toTime;
    }

    private PredictionResultMeta createPredictionResultMeta(String filePath, PredictionResultMeta.InventoryType inventoryType) {
        PredictionResultMeta predictionResultMeta = new PredictionResultMeta();
        predictionResultMeta.setDate(new Date());
        predictionResultMeta.setFileName(filePath);
        predictionResultMeta.setInventoryType(inventoryType);
        predictionResultMeta.setProcess(Boolean.FALSE);
        return predictionResultMetaDAO.save(predictionResultMeta);
    }
}
