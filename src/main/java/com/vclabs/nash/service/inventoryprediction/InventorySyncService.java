package com.vclabs.nash.service.inventoryprediction;

/**
 * Created by Nalaka on 2018-10-15.
 */
public interface InventorySyncService {

    void syncPredictionInventoryWithSystemInventory();
}
