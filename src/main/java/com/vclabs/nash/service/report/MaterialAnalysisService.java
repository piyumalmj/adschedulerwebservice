/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.process.excel.MaterialAnalysisExcel;
import com.vclabs.nash.model.view.reporting.MaterialAnalysis;

/**
 *
 * @author Nalaka
 * @since 02-03-2018
 */
@Service
@Transactional("main")
public class MaterialAnalysisService {
    
    @Autowired
    private AdvertisementDAO advertisementDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private MaterialAnalysisExcel materialAnalysisExcel;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MaterialAnalysisService.class);
    
    public List<MaterialAnalysis> getMaterialAnalysisReport(List<Integer> cutIds, List<Integer> clientList) {
        
        List<MaterialAnalysis> materialAnalysisList = new ArrayList();
        List<Advertisement> advertisementList = advertisementDao.getAdvertisementByMultyIDSAndClient(cutIds, clientList);
        
        for (Advertisement advertisement : advertisementList) {
            MaterialAnalysis materialAnalysis = new MaterialAnalysis();
            
            materialAnalysis.setCutId(advertisement.getAdvertid());
            materialAnalysis.setAdvertisementName(advertisement.getAdvertname());
            
            if (advertisement.getClient().getClienttype().equals("Agency")) {
                materialAnalysis.setAgency(advertisement.getClient().getClientname());
                materialAnalysis.setClient("----------");
            } else {
                materialAnalysis.setAgency("----------");
                materialAnalysis.setClient(advertisement.getClient().getClientname());
            }
            
            materialAnalysis.setCaptureDate(advertisement.getCreateDate());
            materialAnalysis.setExpiredDate(advertisement.getExpireDate());
            ScheduleDef scheduleDef = schedulerDao.getLastAiredSpot(advertisement.getAdvertid());
            if (scheduleDef != null) {
                if (!scheduleDef.getActualstarttime().toString().equals("00:00:00")) {
                    materialAnalysis.setLastAiredDate(scheduleDef.getDate());
                } else {
                    materialAnalysis.setLastAiredDate(null);
                }
            } else {
                materialAnalysis.setLastAiredDate(null);
            }
            
            switch (advertisement.getStatus()) {
                case 1:
                    materialAnalysis.setStatus("Active");
                    break;
                case 2:
                    materialAnalysis.setStatus("Hold");
                    break;
                case 3:
                    materialAnalysis.setStatus("Deleted");
                    break;
            }
            
            materialAnalysisList.add(materialAnalysis);
        }
        
        return materialAnalysisList;
    }
    
    public List<Integer> getAdvertList() {
        return advertisementDao.getAdvertisementIDList();
    }
    
    public Boolean generateMaterialAnalysisReportExcel(HttpServletRequest request, List<Integer> cutList, List<Integer> clientList) throws Exception {
        try {
            materialAnalysisExcel.generateMaterialAnalysisReport(getMaterialAnalysisReport(cutList, clientList), request);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in MaterialAnalysisService in generateMaterialAnalysisReportExcel() : {} ", e.getMessage());
            throw e;
        }
    }
    
    public void downloadMaterialAnalysisReportExcel(HttpServletResponse response, HttpServletRequest request) {
        materialAnalysisExcel.downloadMaterianAnalysisReportExcell(response, request);
    }
}
