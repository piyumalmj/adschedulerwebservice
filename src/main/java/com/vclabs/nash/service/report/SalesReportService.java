/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.InvoiceDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.TimeSlotScheduleMapDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.InvoiceDef;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.excel.SellerExcel;
import com.vclabs.nash.model.view.reporting.SalesView;

/**
 *
 * @author Nalaka
 * @since 27-02-2018
 */
@Service
@Transactional("main")
public class SalesReportService {

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private TimeSlotScheduleMapDAO timeSlotScheduleMapDao;
    @Autowired
    private InvoiceDAO invoiceDao;
    @Autowired
    private SellerExcel sellerExcel;

    private static final Logger logger = LoggerFactory.getLogger(SalesReportService.class);

    public List<SalesView> getSellerReport(List<String> woType, List<String> meList, List<String> monthList, List<Integer> yearList, List<Integer> clientIntList, List<Integer> agencyIntList) {

        List<WorkOrder> userWorkOrderList = workOrderDao.getSelectedUserWorkOrederList(woType, meList, monthList, clientIntList, agencyIntList);
        Boolean isAll = Boolean.TRUE;
        for (int year : yearList) {
            if (year == -111) {
                isAll = Boolean.FALSE;
            }
        }

        if (isAll && !yearList.isEmpty()) {
            filterRevenueYear(yearList, userWorkOrderList);
        }

        List<SalesView> sellerReportList = new ArrayList<>();

        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.DOWN);

        try {
            for (WorkOrder workOrder : userWorkOrderList) {
                SalesView salesView = new SalesView();

                salesView.setWorkOrderId(workOrder.getWorkorderid());
                salesView.setAgencyName(clientDetailDao.getSelectedClient(workOrder.getAgencyclient()).getClientname());
                salesView.setClientName(clientDetailDao.getSelectedClient(workOrder.getClient()).getClientname());
                salesView.setWorkOrderType(workOrder.getWoType());

                salesView.setInvoiceType(getInvoiceType(workOrder.getAgencyclient(), workOrder.getCommission(), workOrder.getBillclient()) + "");

                salesView.setMonth(workOrder.getRevenueMonth());
                salesView.setME(workOrder.getSeller());
                salesView.setAmount(df.format(workOrder.getTotalBudget()));
                List<Integer> spotCountList = getPrimeTimeNonPrimeTimeSpotCount(workOrder.getWorkorderid());
                salesView.setPrimeSpotCount(spotCountList.get(0));
                salesView.setNonPrimeSpotCount(spotCountList.get(1));
                salesView.setWorkOrderStatus(workOrder.getManualStatus() + " / " + workOrder.getAutoStatus());
                salesView.setRate(df.format(workOrder.getTotalBudget() / (salesView.getPrimeSpotCount() + salesView.getNonPrimeSpotCount())));

                InvoiceDef invoiceDef = invoiceDao.getSelectedInvoiceByWorkOrder(workOrder.getWorkorderid());

                if (invoiceDef != null) {
                    salesView.setInvoiceDate(invoiceDef.getDate());
                    salesView.setInvoiceNo(invoiceDef.getInvoiceid());
                    salesView.setPayment(invoiceDef.getTotalamount());
                }

                sellerReportList.add(salesView);
            }
        } catch (Exception e) {
            logger.debug("Exception in SalesReportService in getSellerReport() : {}", e.getMessage());
        }
        return sellerReportList;
    }

    public void filterRevenueYear(List<Integer> yearList, List<WorkOrder> workOrderList) {

        for (int i = 0; i < workOrderList.size(); i++) {
            int revenueMonth = getMonthName(workOrderList.get(i).getRevenueMonth());
            int woYear = getRevenueYear(revenueMonth, workOrderList.get(i));
            Boolean isRemove = Boolean.TRUE;
            for (int year : yearList) {
                if (woYear == year) {
                    isRemove = Boolean.FALSE;
                }
            }
            if (isRemove) {
                workOrderList.remove(workOrderList.get(i));
                i--;
            }
        }

    }

    public int getRevenueYear(int month, WorkOrder workOrder) {
        Map<Integer, Integer> yearMonthMap = new TreeMap<Integer, Integer>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(workOrder.getStartdate());

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(workOrder.getEnddate());

        while (calendar.before(endCalendar)) {
            int woYear = calendar.get(Calendar.YEAR);
            int woMonth = calendar.get(Calendar.MONTH);
            calendar.add(Calendar.MONTH, 1);

            yearMonthMap.put(woMonth, woYear);
        }
        int year = 0;
        try {
            year = yearMonthMap.get(month);
        } catch (NullPointerException e) {
            logger.debug("Exception in SalesReportService in getRevenueYear() : {}", e.getMessage());
        }
        return year;
    }

    public int getMonthName(String month) {
        int monthNumber = -1;
        switch (month) {
            case "January":
                monthNumber = 0;
                break;

            case "February":
                monthNumber = 1;
                break;

            case "March":
                monthNumber = 2;
                break;

            case "April":
                monthNumber = 3;
                break;

            case "May":
                monthNumber = 4;
                break;

            case "June":
                monthNumber = 5;
                break;
            case "July":
                monthNumber = 6;
                break;

            case "August":
                monthNumber = 7;
                break;

            case "September":
                monthNumber = 8;
                break;

            case "October":
                monthNumber = 9;
                break;

            case "November":
                monthNumber = 10;
                break;

            case "December":
                monthNumber = 11;
                break;
            default:
                monthNumber = -1;
                break;
        }

        return monthNumber;
    }

    public int getInvoiceType(int agencyID, int commistion, int billingclient) {

        if (agencyID == 0 && commistion == 0) {
            return 1;
        } else if (agencyID == billingclient && commistion == 15) {
            return 2;
        } else if (agencyID != billingclient && billingclient != 0 && commistion == 15) {
            return 3;
        } else if (agencyID != 0 && commistion == 0) {
            return 4;
        } else {
            return 0;
        }
    }

    public List<Integer> getPrimeTimeNonPrimeTimeSpotCount(int workOrderId) {

        int primeTimeSpot = 0;
        int nonPrimeTimeSpot = 0;
        List<Integer> spotArry = new ArrayList<>();

        primeTimeSpot = schedulerDao.getPrimeTimeSpotCount(workOrderId);
        nonPrimeTimeSpot = schedulerDao.getNonPrimeTimeSpotCount(workOrderId);
//
//        List<ScheduleDef> scheduleList = schedulerDao.getScheduleListOrderByASC(workOrderId);
//        List<TimeSlotScheduleMap> timeSlotList = timeSlotScheduleMapDao.getSelectedWorkOrderTimeSlot(workOrderId);
//        Map<Integer, TimeSlotScheduleMap> timeSlotMap = new TreeMap<>();
//
//        for (TimeSlotScheduleMap timeSlotScheduleMap : timeSlotList) {
//            timeSlotMap.put(timeSlotScheduleMap.getScheduleid().getScheduleid(), timeSlotScheduleMap);
//        }
//
//        for (ScheduleDef scheduleDef : scheduleList) {
//            if (scheduleDef.getActualstarttime().toString().equals("00:00:00")) {
//                TimeSlotScheduleMap timeBelt = timeSlotMap.get(scheduleDef.getScheduleid());
//                if (timeBelt != null) {
//                    scheduleDef.setActualstarttime(timeBelt.getTimeBelt().getStartTime());
//                    scheduleDef.setActualendtime(timeBelt.getTimeBelt().getEndTime());
//                }
//            }
//
//            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
//            calendar.setTime(scheduleDef.getActualstarttime());   // assigns calendar to given date
//            int hour = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
//
//            if (hour >= 9 && hour < 18) {
//                nonPrimeTimeSpot++;
//            } else if (hour >= 18 && hour < 23) {
//                primeTimeSpot++;
//            }
//        }

        spotArry.add(primeTimeSpot);
        spotArry.add(nonPrimeTimeSpot);
        return spotArry;
    }

    public Boolean generateSellerReportExcel(HttpServletRequest request, List<String> woType, List<String> meList, List<String> monthList, List<Integer> yearList, List<Integer> clientIntList, List<Integer> agencyIntList) throws Exception {
        try {
            sellerExcel.generateSellerReport(getSellerReport(woType, meList, monthList, yearList, clientIntList, agencyIntList), request);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in SalesReportService in generateSellerReportExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadSellerReportExcel(HttpServletResponse response, HttpServletRequest request) {
        sellerExcel.downloadSellerReportExcell(response, request);
    }
}
