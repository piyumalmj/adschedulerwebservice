/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.InvoiceDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.InvoiceDef;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.excel.PaymentDueExcel;
import com.vclabs.nash.model.view.reporting.PaymentDue;

/**
 *
 * @author Nalaka
 * @since 27/02/2018
 */
@Service
@Transactional("main")
public class PaymentDueService {

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private InvoiceDAO invoiceDao;
    @Autowired
    private PaymentDueExcel paymentDueExcel;

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentDueService.class);

    public List<PaymentDue> getPaymentDueReport(List<Integer> clientList, List<Integer> agencyList, List<Integer> yearList, List<String> monthList) {

        List<PaymentDue> paymentDueList = new ArrayList();
        List<WorkOrder> woList = workOrderDao.getSelectedUserWorkOrederList(new ArrayList(), new ArrayList(), monthList, clientList, agencyList);//getAllWorkOrderList();

        Boolean isAll = Boolean.TRUE;
        for (int year : yearList) {
            if (year == -111) {
                isAll = Boolean.FALSE;
            }
        }

        if (isAll && !yearList.isEmpty()) {
            filterRevenueYear(yearList, woList);
        }

        for (WorkOrder workOrder : woList) {
            PaymentDue paymentDue = new PaymentDue();
            paymentDue.setWorkOrderId(workOrder.getWorkorderid());
            paymentDue.setAgencyName(clientDetailDao.getSelectedClient(workOrder.getAgencyclient()).getClientname());
            paymentDue.setClientName(clientDetailDao.getSelectedClient(workOrder.getClient()).getClientname());
            paymentDue.setWorkOrderType(workOrder.getWoType());

            paymentDue.setScheduleref(workOrder.getScheduleRef());
            paymentDue.setMarketingExecutive(workOrder.getSeller());
            paymentDue.setPackegeAmount(workOrder.getTotalBudget());
            paymentDue.setWorkOrderStatus(workOrder.getManualStatus() + " / " + workOrder.getAutoStatus());

            InvoiceDef invoiceDef = invoiceDao.getSelectedInvoiceByWorkOrder(workOrder.getWorkorderid());

            if (invoiceDef != null) {
                paymentDue.setInvoiceDate(invoiceDef.getDate());
                paymentDue.setInvoiceNo(invoiceDef.getInvoiceid());
                paymentDue.setInvoiceAmount(invoiceDef.getTotalamount());
            }
            paymentDueList.add(paymentDue);
        }

        return paymentDueList;
    }

    public void filterRevenueYear(List<Integer> yearList, List<WorkOrder> workOrderList) {

        for (int i = 0; i < workOrderList.size(); i++) {
            int revenueMonth = getMonthName(workOrderList.get(i).getRevenueMonth());
            int woYear = getRevenueYear(revenueMonth, workOrderList.get(i));
            Boolean isRemove = Boolean.TRUE;
            for (int year : yearList) {
                if (woYear == year) {
                    isRemove = Boolean.FALSE;
                }
            }
            if (isRemove) {
                workOrderList.remove(workOrderList.get(i));
                i--;
            }
        }

    }

    public int getRevenueYear(int month, WorkOrder workOrder) {
        Map<Integer, Integer> yearMonthMap = new TreeMap<Integer, Integer>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(workOrder.getStartdate());

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(workOrder.getEnddate());

        while (calendar.before(endCalendar)) {
            int woYear = calendar.get(Calendar.YEAR);
            int woMonth = calendar.get(Calendar.MONTH);
            calendar.add(Calendar.MONTH, 1);

            yearMonthMap.put(woMonth, woYear);
        }
        int year = 0;
        try {
            year = yearMonthMap.get(month);
        } catch (NullPointerException e) {
            LOGGER.debug("Exception in PaymentDueService while trying to get revenue year: ", e.getMessage());
        }
        return year;
    }

    public int getMonthName(String month) {
        int monthNumber = -1;
        switch (month) {
            case "January":
                monthNumber = 0;
                break;

            case "February":
                monthNumber = 1;
                break;

            case "March":
                monthNumber = 2;
                break;

            case "April":
                monthNumber = 3;
                break;

            case "May":
                monthNumber = 4;
                break;

            case "June":
                monthNumber = 5;
                break;
            case "July":
                monthNumber = 6;
                break;

            case "August":
                monthNumber = 7;
                break;

            case "September":
                monthNumber = 8;
                break;

            case "October":
                monthNumber = 9;
                break;

            case "November":
                monthNumber = 10;
                break;

            case "December":
                monthNumber = 11;
                break;
            default:
                monthNumber = -1;
                break;
        }

        return monthNumber;
    }

    public Boolean generatePaymentDueReportExcel(HttpServletRequest request, List<Integer> clientList, List<Integer> agencyList, List<Integer> yearList, List<String> monthList) throws Exception {
        try {
            paymentDueExcel.generatePaymentDueReport(getPaymentDueReport(clientList, agencyList, yearList, monthList), request);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PaymentDueService in generatePaymentDueReportExcel() : ", e.getMessage());
            throw e;
        }
    }

    public void downloadPaymentDueReportExcel(HttpServletResponse response, HttpServletRequest request) {
        paymentDueExcel.downloadPaymentDueReportExcell(response, request);
    }
}
