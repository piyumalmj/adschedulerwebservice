/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.PlayHistoryDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.PlayListHistory;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import com.vclabs.nash.model.process.MissedSpotCountInfo;
import com.vclabs.nash.model.process.MissedSpotItemModel;
import com.vclabs.nash.model.process.excel.MissedSpotCountExcel;

/**
 *
 * @author hp
 */
@Service
@Transactional("main")
public class MissedSpotCountService {

    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private PlayHistoryDAO playHistoryDao;
    @Autowired
    MissedSpotCountExcel missedSpotCountExcel;

    private static final Logger logger = LoggerFactory.getLogger(MissedSpotCountService.class);

    public List<MissedSpotItemModel>  getMissedSpotCount(String filterData) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        MissedSpotCountInfo msData = mapper.readValue(filterData, MissedSpotCountInfo.class);

        SimpleDateFormat hourAndMinutesTime = new SimpleDateFormat("HH:mm");

        List<ScheduleDef> missedSpotList = schedulerDao.getMissedSpotSchedule(msData);
        List<TimeSlotScheduleMap> scheduleMapList = schedulerDao.getTimeSlotForMissedSpot(msData);

        HashMap<Integer, TimeSlotScheduleMap> map_timebeltSchedules = new HashMap<>();
        for (TimeSlotScheduleMap item : scheduleMapList) {
            map_timebeltSchedules.put(item.getScheduleid().getScheduleid(), item);
        }

        List<MissedSpotItemModel> itemList = new ArrayList<>();

        if (missedSpotList != null) {
            for (ScheduleDef scheduleDef : missedSpotList) {

                int hour = scheduleDef.getActualstarttime().getHours();
                if (msData.getAdvertTimeBelt().equals("all") || msData.getAdvertTimeBelt().equals("" + hour)) {
                    MissedSpotItemModel spotModel = new MissedSpotItemModel();

                    spotModel.setActualTimeBelt(scheduleDef.getActualstarttime().toString() + "-" + scheduleDef.getActualendtime().toString());
                    spotModel.setSchedulDate(scheduleDef.getDate().toString());
                    spotModel.setSchedulEndTime(scheduleDef.getScheduleendtime().toString());
                    spotModel.setSchedulStartTime(scheduleDef.getSchedulestarttime().toString());
                    spotModel.setScheduleId(scheduleDef.getScheduleid());
                    spotModel.setStatus(scheduleDef.getStatus());

                    spotModel.setAdvertId(scheduleDef.getAdvertid().getAdvertid());
                    spotModel.setAdvertNme(scheduleDef.getAdvertid().getAdvertname());
                    spotModel.setAdvertType(scheduleDef.getAdvertid().getAdverttype());
                    spotModel.setDuratuion(scheduleDef.getAdvertid().getDuration());

                    spotModel.setChannelId(scheduleDef.getChannelid().getChannelid());
                    spotModel.setChannelName(scheduleDef.getChannelid().getChannelname());

                    spotModel.setClient(scheduleDef.getAdvertid().getClient().getClientname());
                    spotModel.setClientId(scheduleDef.getAdvertid().getClient().getClientid());

                    spotModel.setWorkOrderId(scheduleDef.getWorkorderid().getWorkorderid());
                    spotModel.setWorkOrderName(scheduleDef.getWorkorderid().getOrdername());

                    TimeSlotScheduleMap tmpScheduleMap = map_timebeltSchedules.get(scheduleDef.getScheduleid());
                    TimeBelts initialTimeBelt = (tmpScheduleMap != null) ? tmpScheduleMap.getTimeBelt() : null;
                    if (initialTimeBelt != null) {
                        spotModel.setScheduleTimeBelt(hourAndMinutesTime.format(initialTimeBelt.getStartTime()) + "-" + hourAndMinutesTime.format(initialTimeBelt.getEndTime()));
                    }

                    PlayListHistory playListHistory = playHistoryDao.getSelectedData(scheduleDef.getScheduleid(), scheduleDef.getDate().toString());
                    if (playListHistory != null) {
                        spotModel.setCluster("" + (playListHistory.getPlaycluster() + 1));
                        if (initialTimeBelt != null) {
                            if (playListHistory.getSchedulehour() != initialTimeBelt.getClusterCount()) {
                                spotModel.setShiftedTimeBelt(hourAndMinutesTime.format(initialTimeBelt.getStartTime()) + "-" + hourAndMinutesTime.format(initialTimeBelt.getEndTime()));
                            }
                        }
                    } else {
                        spotModel.setCluster("");
                    }

                    itemList.add(spotModel);
                }
            }
        }
        refineTimeBeltsForMissedSpotItemModels(itemList);
        Collections.sort(itemList);
        //Sort schedule list according to workOrder id;
        Collections.sort(itemList, new Comparator<MissedSpotItemModel>() {
            @Override
            public int compare(MissedSpotItemModel o1, MissedSpotItemModel o2) {
                return o1.getSchedulDate().compareTo(o2.getSchedulDate());
            }
        });
        return itemList;
    }

    public void refineTimeBeltsForMissedSpotItemModels(List<MissedSpotItemModel> items){
        SimpleDateFormat hmf = new SimpleDateFormat("HH:mm");
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        for(MissedSpotItemModel msItemModel : items){
            if(msItemModel.getScheduleTimeBelt() == null){
                try {
                    Date date = sdf.parse(msItemModel.getSchedulStartTime());
                    Date startTimeBelt = new Date(date.getYear(), date.getMonth(), date.getDate(), date.getHours(), 0);
                    Date endTimeBelt = null;
                    if(date.getHours() < 23){
                        endTimeBelt = new Date(date.getYear(), date.getMonth(), date.getDate(), date.getHours() + 1, 0);
                    }else if(date.getHours() == 23){
                        endTimeBelt = new Date(date.getYear(), date.getMonth(), date.getDate(), date.getHours() , 59);
                    }
                    String scheduleTimeBelt = hmf.format(startTimeBelt) + "-" + hmf.format(endTimeBelt);
                    msItemModel.setScheduleTimeBelt(scheduleTimeBelt);
                } catch (ParseException e) {
                    logger.debug("Exception has occurred while refining missed spot time belts: {}", e.getMessage());
                }
          }
        }
    }

    public Boolean generateMissedSpotCountReportExcel(HttpServletRequest request, String filterData) throws Exception {
        try {
            missedSpotCountExcel.generateMissedSpotCountReport(getMissedSpotCount(filterData), request);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in MissedSpotCountService in generateMissedSpotCountReportExcel: {}", e.getMessage());
            throw e;
        }
    }

    public void downloadMissedSpotCountExcel(HttpServletResponse response, HttpServletRequest request) {
        missedSpotCountExcel.downloadMissedSpotCountReportExcell(response, request);
    }

}
