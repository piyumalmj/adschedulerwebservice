package com.vclabs.nash.service.inventory.vo;

import com.vclabs.nash.model.entity.inventory.InventoryRow;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Nalaka on 2018-09-26.
 */
public class InventoryConflictRow {
    public static enum Type implements Serializable {
        TVC("TVC"),
        LOGO("Logo"),
        Crawler("Crawler"),
        LCrawler("LCrawler");
        String type;
        private Type(String type){
            this.type = type;
        }
        public String getType(){
            return type;
        }
    }

    private Date date;

    private InventoryRow inventoryRow;

    private Type type;

    private Number oldAmount;

    private Number consumedAmount;

    private Number requestedAmount;

    private Number newAmount;

    public InventoryRow getInventoryRow() {
        return inventoryRow;
    }

    public void setInventoryRow(InventoryRow inventoryRow) {
        this.inventoryRow = inventoryRow;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Number getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(Number oldAmount) {
        this.oldAmount = oldAmount;
    }

    public Number getConsumedAmount() {
        return consumedAmount;
    }

    public void setConsumedAmount(Number consumedAmount) {
        this.consumedAmount = consumedAmount;
    }

    public Number getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(Number requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public Number getNewAmount() {
        return newAmount;
    }

    public void setNewAmount(Number newAmount) {
        this.newAmount = newAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
