package com.vclabs.nash.service.inventory;

import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.service.inventory.vo.InventoryConflict;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
public interface InventoryService {

    Inventory save(Inventory inventory);

    InventoryRow save(InventoryRow inventoryRow);

    Inventory saveOrUpdate(Inventory inventory, InventoryConflict... conflict);

    Inventory generateInitialInventory(Date date);

    Inventory generateInitialInventory();

    Inventory findByChannel(int channelId);

    Inventory findByChannelAndDate(int channelId, Date date);

    boolean isExistInventory(Long channelId, Date date);

    InventoryRow findByChannelAndDateAndTime(Integer channelId, Date date, Time fromTime, Time toTime);

    List<Date> findInventoryExistDates(Integer channelId, Date fromDate, Date toDate);
}
