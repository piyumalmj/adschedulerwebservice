package com.vclabs.nash.service;

import com.vclabs.nash.controller.ScheduleController;
import com.vclabs.nash.model.view.WorkOrderScheduleView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 11/04/2019.
 */
@Service
public class ScheduleAsyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleController.class);

    @Autowired
    private SpotSpreadService spotSpreadService;

    @Async("specificTaskExecutor")
    public void recreateDailyScheduleForPatternFillAndReplaceAdvert(WorkOrderScheduleView wsv){
        try {
            LOGGER.debug("Started recreate daily schedule using pattern fill for the workorder : {}", wsv.getWorkOrderId());
            List<List<Integer>> channelGroups = new ArrayList<>();
            for(int i = 0 ; i < wsv.getLstChannelIds().size(); i++){
                if(i % 2 == 0){
                    channelGroups.add(new ArrayList<>());
                }
                List<Integer> lastGroup = channelGroups.get(channelGroups.size() - 1);
                lastGroup.add( wsv.getLstChannelIds().get(i));
            }

            for(List<Integer> channelGroup : channelGroups){
                spotSpreadService.setDailyScheduleAsync(channelGroup, wsv.getDtStartDate(), wsv.getDtEndDate(), wsv.getDtStartTime());
            }
            spotSpreadService.setPlayPermissionForWorkOrder(-102, new Date(),wsv.getRescheduleStatus());
            LOGGER.debug("Completed recreate daily schedule using pattern fill for the workorder : {}", wsv.getWorkOrderId());
        }catch (Exception e){
            LOGGER.debug("Exception has occurred while recreating daily schedule using pattern fill ", e.getMessage());
        }
    }
}
