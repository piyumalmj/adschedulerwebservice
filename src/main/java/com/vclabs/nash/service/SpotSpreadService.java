/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service;

import java.lang.reflect.Field;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vclabs.nash.model.dao.*;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;

/**
 *
 * @author hp
 */
@Service
@Transactional("main")
public class SpotSpreadService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SpotSpreadService.class);

    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private PlayListScheduleDAO playListScheduleDao;
    @Autowired
    private PlayListDAO playListDao;
    @Autowired
    private PriorityDefDAO priorityDefDao;
    @Autowired
    private ChannelDetailDAO channelDetailsDao;
    @Autowired
    private AdvertisementDAO advertismentDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private NashInventoryConsumptionService nashInventoryConsumptionService;
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private TimeSlotScheduleMapDAO timeSlotScheduleMapDao;
    @Autowired
    private LogoAuditService logoAuditService;
    @Autowired
    private AdvertSettingService advertSettingService;

    private SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");


    @Async
    public Boolean recreateDailySchedule(List<ScheduleDef> newScheduleList, List<ScheduleDef> oldScheduleList, String user, List<ScheduleDef> removeSchedule,Boolean rescheduleStatus) {
        getAuditList(newScheduleList, oldScheduleList, user);

        Map<Integer, List<ScheduleDef>> scheduleMap = new HashMap<>();
        for(ScheduleDef sd : newScheduleList){
            Integer channelId = sd.getChannelid().getChannelid();
            List<ScheduleDef> scheduleDefList = scheduleMap.get(channelId);
            if(scheduleDefList == null){
                scheduleDefList = new ArrayList<>();
                scheduleMap.put(channelId, scheduleDefList);
            }
            scheduleDefList.add(sd);
        }

        for(Map.Entry<Integer, List<ScheduleDef>> e : scheduleMap.entrySet()){
            List<ScheduleDef> scheduleList = e.getValue();
            Collections.sort(scheduleList);
            Date scheduleStartDate = scheduleList.get(0).getDate();
            Date scheduleEndDate = scheduleList.get(scheduleList.size() - 1).getDate();
            Set<Date> timeBelts = new HashSet<>();
            for(ScheduleDef sd : e.getValue()){
                timeBelts.add(sd.getSchedulestarttime());
            }
            for(Date timeBelt : timeBelts){
                setDailySchedule(e.getKey(), scheduleStartDate, scheduleEndDate, timeBelt);
            }
        }

        //setDailySchedule(iChannelID, dtScheduleStartDate, dtScheduleEndDate, dtSchedulStartTime);
        setPlayPermissionForWorkOrder(-102, new Date(),rescheduleStatus);
        /////===================New inventory system=========================/////
        //deallocateInventory(removeSchedule);
        return true;
    }

    @Async
    public Boolean recreateDailyScheduleForPatternFillAndReplaceAdvert(List<Integer> channelIds, Date dtScheduleStartDate, Date dtScheduleEndDate, Date dtSchedulStartTime,Boolean rescheduleStatus) {
        for (int id : channelIds) {
            setDailySchedule(id, dtScheduleStartDate, dtScheduleEndDate, dtSchedulStartTime);
        }
        setPlayPermissionForWorkOrder(-102, new Date(),rescheduleStatus);
        return true;
    }


    @Transactional
    public void setDailyScheduleAsync(List<Integer> channelIds, Date dtScheduleStartDate, Date dtScheduleEndDate, Date dtSchedulStartTime){
        for(int iChannelID : channelIds){
            LOGGER.debug("Before setDailySchedule | channelId : {} |  date range : {} to {} | start time : {}",
                    iChannelID, dtScheduleStartDate, dtScheduleEndDate, dtSchedulStartTime);
            setDailySchedule(iChannelID, dtScheduleStartDate, dtScheduleEndDate, dtSchedulStartTime);
            LOGGER.debug("After setDailySchedule | channelId : {} |  date range : {} to {} | start time : {}",
                    iChannelID, dtScheduleStartDate, dtScheduleEndDate, dtSchedulStartTime);        }
    }

    public Boolean setDailySchedule(int iChannelID, Date dtScheduleStartDate, Date dtScheduleEndDate, Date dtSchedulStartTime) {

        Date dtStartDate = new Date();
        Date dtStartTime = new Date();

        Calendar cal_date = Calendar.getInstance();
        cal_date.setTime(dtStartDate);
        cal_date.set(Calendar.HOUR_OF_DAY, 0);
        cal_date.set(Calendar.MINUTE, 0);
        cal_date.set(Calendar.SECOND, 0);
        cal_date.set(Calendar.MILLISECOND, 0);
        dtStartDate = cal_date.getTime();

        Calendar cal_time = Calendar.getInstance();
        cal_time.setTime(dtStartTime);
        cal_time.set(Calendar.YEAR, 1970);
        cal_time.set(Calendar.MONTH, 0);
        cal_time.set(Calendar.DAY_OF_MONTH, 1);
        cal_time.set(Calendar.MINUTE, 0);
        cal_time.set(Calendar.SECOND, 0);
        cal_time.set(Calendar.MILLISECOND, 0);

        if (dtStartDate.before(dtScheduleStartDate)) {
            dtStartDate.setTime(dtScheduleStartDate.getTime());
            cal_time.set(Calendar.HOUR_OF_DAY, 0);
            dtStartTime = cal_time.getTime();
        } else {
            cal_time.add(Calendar.HOUR_OF_DAY, 1);
            dtStartTime = cal_time.getTime();
        }

        Date dtEndDate = new Date();
        dtEndDate.setTime(dtScheduleEndDate.getTime());

        try {
            List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, "All");
            List<TimeBelts> timeBelts = timeBeltDao.getTimeBelts(iChannelID);

            for (List<ScheduleDef> lstDaySchedule : lstDailySchedules) {
                for (TimeBelts tBelt : timeBelts) {
                    tBelt.setUtilizedScheduleTime(0);
                }

                Collections.sort(lstDaySchedule);
                Collections.sort(lstDaySchedule,
                        (a, b) -> (int) ((a.getScheduleendtime().getTime() - a.getScheduleendtime().getTime())
                                - (b.getScheduleendtime().getTime() - b.getScheduleendtime().getTime())));

                List<ScheduleDef> crowlerAndSqusList = new ArrayList<>();
                for (ScheduleDef schedule : lstDaySchedule) {
                    if (schedule.getAdvertid().getAdverttype().equals("LOGO")) {
                        TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
                        mapModel.setChannelid(schedule.getChannelid());
                        mapModel.setScheduleid(schedule);
                        mapModel.setDate(schedule.getDate());
                        TimeBelts timeBelt = timeBeltDao.getTimeBelt(iChannelID, schedule.getSchedulestarttime().getHours(), 0);
                        mapModel.setTimeBelt(timeBelt);
                        mapModel.setWorkOrderId(schedule.getWorkorderid().getWorkorderid());
                        schedulerDao.saveTimeSlotMapV2(mapModel);

                        /////===================New inventory system=========================/////
                        if (schedule.getWorkorderid() != null && schedule.getWorkorderid().getInventoryType() == WorkOrder.InventoryType.NEW && schedule.getWorkorderid().getSystemTypeype() == WorkOrder.SystemType.NASH) {
                            locateInventory(timeBelt, schedule, iChannelID);
                        }
                        continue;
                    }

                    if (schedule.getAdvertid().getAdverttype().equals("ADVERT")) {
                        Collections.sort(timeBelts);
                        for (TimeBelts tBelt : timeBelts) {
                            if (((schedule.getSchedulestarttime().equals(tBelt.getStartTime())
                                    || schedule.getSchedulestarttime().before(tBelt.getStartTime()))
                                    && schedule.getScheduleendtime().after(tBelt.getStartTime()))
                                    || (schedule.getSchedulestarttime().after(tBelt.getStartTime())
                                    && schedule.getSchedulestarttime().before(tBelt.getEndTime()))) {

                                TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
                                mapModel.setChannelid(schedule.getChannelid());
                                mapModel.setScheduleid(schedule);
                                mapModel.setDate(schedule.getDate());
                                mapModel.setTimeBelt(tBelt);
                                mapModel.setWorkOrderId(schedule.getWorkorderid().getWorkorderid());

                                schedulerDao.saveTimeSlotMapV2(mapModel);

                                tBelt.addUtilizedScheduleTime(schedule.getAdvertid().getDuration());

                                /////===================New inventory system=========================/////
                                if (schedule.getWorkorderid() != null && schedule.getWorkorderid().getInventoryType() == WorkOrder.InventoryType.NEW && schedule.getWorkorderid().getSystemTypeype() == WorkOrder.SystemType.NASH) {
                                    locateInventory(tBelt, schedule, iChannelID);
                                }
                                break;
                            }
                        }
                    }

                    if (schedule.getAdvertid().getAdverttype().equals("CRAWLER") || schedule.getAdvertid().getAdverttype().equals("L_SHAPE") || schedule.getAdvertid().getAdverttype().equals("V_SHAPE")) {
                        crowlerAndSqusList.add(schedule);
                    }
                }

                if (!crowlerAndSqusList.isEmpty()) {
                    for (ScheduleDef schedule : crowlerAndSqusList) {
                        Collections.sort(timeBelts, new Comparator<TimeBelts>() {
                            @Override
                            public int compare(TimeBelts o1, TimeBelts o2) {
                                return (int) (o1.getCrowlerUtilizedScheduleTime() - o2.getCrowlerUtilizedScheduleTime());
                            }
                        });
                        for (TimeBelts tBelt : timeBelts) {
                                    if (((schedule.getSchedulestarttime().equals(tBelt.getStartTime())
                                    || schedule.getSchedulestarttime().before(tBelt.getStartTime()))
                                    && schedule.getScheduleendtime().after(tBelt.getStartTime()))
                                    || (schedule.getSchedulestarttime().after(tBelt.getStartTime())
                                    && schedule.getSchedulestarttime().before(tBelt.getEndTime()))) {

                                TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
                                mapModel.setChannelid(schedule.getChannelid());
                                mapModel.setScheduleid(schedule);
                                mapModel.setDate(schedule.getDate());
                                mapModel.setTimeBelt(tBelt);
                                mapModel.setWorkOrderId(schedule.getWorkorderid().getWorkorderid());

                                schedulerDao.saveTimeSlotMapV2(mapModel);
                                tBelt.addCrowlerUtilizedScheduleTime(1);
                                /////===================New inventory system=========================/////
                                if (schedule.getWorkorderid() != null && schedule.getWorkorderid().getInventoryType() == WorkOrder.InventoryType.NEW && schedule.getWorkorderid().getSystemTypeype() == WorkOrder.SystemType.NASH) {
                                    locateInventory(tBelt, schedule, iChannelID);
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SpotSpreadService in setDailySchedule() : {}", e.getMessage());
            throw e;
        }

        return true;
    }

    public Boolean setPlayPermissionForWorkOrder(int workOrderId, Date today,Boolean rescheduleStatus) {

        try {
            List<TimeSlotScheduleMap> slectedScheduleList;
            List<PlayList> playList;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("1970-01-01 00:00:00.000");

            if (workOrderId != -102) {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(workOrderId, today);
                playList = playListScheduleDao.getPlayList(workOrderId);
            } else {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(today);
                playList = playListScheduleDao.getPlayList(dateFormat.parse(dateFormat.format(today)));
            }

            for (int i = 0; i < slectedScheduleList.size(); i++) {
                for (int j = 0; j < playList.size(); j++) {
                    int playListScheduleID = playList.get(j).getSchedule().getScheduleid();
                    int scheduleID = slectedScheduleList.get(i).getScheduleid().getScheduleid();

                    int scheduleHour = slectedScheduleList.get(i).getTimeBelt().getHour();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    int hours = cal.get(Calendar.HOUR_OF_DAY);

                    if (playListScheduleID == scheduleID) {
                        if (rescheduleStatus) {
                            if ((playList.get(j).getStatus().equals("0") || playList.get(j).getStatus().equals("9") || playList.get(j).getStatus().equals("7") || playList.get(j).getStatus().equals("11") || playList.get(j).getStatus().equals("5")) && scheduleHour > hours) {

                                playList.get(j).setWorkOrder(slectedScheduleList.get(i).getScheduleid().getWorkorderid());
                                playList.get(j).setAdvert(slectedScheduleList.get(i).getScheduleid().getAdvertid());
                                playList.get(j).setDate(slectedScheduleList.get(i).getDate());
                                playList.get(j).setChannel(slectedScheduleList.get(i).getChannelid());
                                playList.get(j).setScheduleStartTime(slectedScheduleList.get(i).getScheduleid().getSchedulestarttime());
                                playList.get(j).setScheduleEndTime(slectedScheduleList.get(i).getScheduleid().getScheduleendtime());
                                playList.get(j).setSchedule(slectedScheduleList.get(i).getScheduleid());
                                playList.get(j).setScheduleHour(slectedScheduleList.get(i).getTimeBelt().getHour());
                                playList.get(j).setStatus(slectedScheduleList.get(i).getScheduleid().getStatus());

                            }
                        } else {
                            if ((playList.get(j).getStatus().equals("0") || playList.get(j).getStatus().equals("9") || playList.get(j).getStatus().equals("7") || playList.get(j).getStatus().equals("11")) && scheduleHour > hours) {

                                playList.get(j).setWorkOrder(slectedScheduleList.get(i).getScheduleid().getWorkorderid());
                                playList.get(j).setAdvert(slectedScheduleList.get(i).getScheduleid().getAdvertid());
                                playList.get(j).setDate(slectedScheduleList.get(i).getDate());
                                playList.get(j).setChannel(slectedScheduleList.get(i).getChannelid());
                                playList.get(j).setScheduleStartTime(slectedScheduleList.get(i).getScheduleid().getSchedulestarttime());
                                playList.get(j).setScheduleEndTime(slectedScheduleList.get(i).getScheduleid().getScheduleendtime());
                                playList.get(j).setSchedule(slectedScheduleList.get(i).getScheduleid());
                                playList.get(j).setScheduleHour(slectedScheduleList.get(i).getTimeBelt().getHour());
                                playList.get(j).setStatus(slectedScheduleList.get(i).getScheduleid().getStatus());

                            }
                        }

                        if (playListScheduleDao.saveAndUpdatePlayList(playList.get(j))) {
                            if (slectedScheduleList.remove(slectedScheduleList.get(i))) {
                                i--;
                                break;
                            }
                        }
                    }
                }
            }

            for (TimeSlotScheduleMap item : slectedScheduleList) {

                ScheduleDef dataModel = item.getScheduleid();

                PlayList model = new PlayList();
                model.setWorkOrder(dataModel.getWorkorderid());
                model.setAdvert(dataModel.getAdvertid());
                model.setDate(dataModel.getDate());
                model.setChannel(dataModel.getChannelid());
                model.setScheduleStartTime(dataModel.getSchedulestarttime());
                model.setScheduleEndTime(dataModel.getScheduleendtime());
                model.setSchedule(dataModel);

                model.addConflicting_schedules(1);

                model.setStatus(dataModel.getStatus());

                model.setComment("Status :" + model.getStatus() + " Time :" + new Date().toString());
                model.setPlayCluster(-1);
                model.setPlayOrder(-1);
                model.setActualEndTime(item.getTimeBelt().getEndTime());
                model.setActualStartTime(item.getTimeBelt().getStartTime());
                model.setScheduleHour(item.getTimeBelt().getHour());
                model.setTimeBeltEndTime(item.getTimeBelt().getEndTime());
                model.setTimeBeltStartTime(item.getTimeBelt().getStartTime());
                model.setLable(PlayList.Lable.COMMERICIAL);

                playListScheduleDao.saveAndUpdatePlayList(model);
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SpotSpreadService in setPlayPermissionForWorkOrder() : {}", e.getMessage());
            return false;
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Async("specificTaskExecutor")
    public Boolean generatePlayListForChannel(ChannelDetails channel, int hour, int minuts) {
        LOGGER.debug("Started schedule generating | hour : {}  Channel : {}", hour, channel.getChannelname());
        generatePlayList_BackEnd(channel.getChannelid(), hour, minuts);
        LOGGER.debug("Completed schedule generating | hour : {}  Channel : {}", hour, channel.getChannelname());
        return true;
    }

    /*private boolean generatePlayList_BackEnd(int _channelId, int _hour, int period) {

        //get start end times
        TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, period);
        if (time_belt == null) {
            LOGGER.debug("[GenerateAdvertPlayList_BackEnd] TimeBelt Null => channel : {} ", _channelId);
            return false;
        }

        ArrayList<PlayList> play_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        for (PlayList playList : play_logos) {
            setLogoAudit(playList);
        }

        //update not played cuts in previous schedule
        playListDao.updateSchedulesValidity(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);

        ScheduleGenerate generator = new ScheduleGenerate(_channelId, _hour, time_belt.getStartTime(), time_belt.getEndTime(), time_belt.getClusterCount(), advertismentDao.getFillerAdvertisements());

        Date today = new Date();
        java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            today = (Date) dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException ex) {
            Logger.getLogger(PlayListService.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<ScheduleDef> lstScheduleList = schedulerDao.getSchedulePrioritybyTimeBelt(time_belt.getTimeBeltId(), today);
        List<PriorityDef> lstPriorities = priorityDefDao.getPriorityListbyTimeBelt(time_belt.getTimeBeltId());
        generator.setPrioritySchedulesList(lstScheduleList, lstPriorities);
        boolean bFlag = false;

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_adverts = (ArrayList<PlayList>) playListDao.getPlayListAdvert(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        LOGGER.debug("[GenerateAdvertPlayList_BackEnd] => hour : {}  schedules size : {} ", _hour, play_list_adverts.size());

        //generate playlist adverts
        bFlag = generator.generatePlayListAdvert(play_list_adverts);
        if (!bFlag) {
            return false;
        }

        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<ArrayList<PlayList>> lst_scheduled_adverts = generator.getLst_Schedule();
        for (int i = 0; i < lst_scheduled_adverts.size(); i++) {
            ArrayList<PlayList> lst_cluster = lst_scheduled_adverts.get(i);

            for (int j = 0; j < lst_cluster.size(); j++) {
                PlayList playList = lst_cluster.get(j);
                playList.setPlayCluster(i);
                playList.setPlayOrder(j);
                playList.setStatus("1");
                playList.setComment("schedule_generated");
                LOGGER.debug("Cluster : {} Order : {} Advert : {}", i, j, playList.getAdvert().getAdvertname());
                playListDao.updatePlayList(playList);
            }
        }

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_crawlers = (ArrayList<PlayList>) playListDao.getPlayListCrawler(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        LOGGER.debug("[GenerateCrawlerPlayList_BackEnd] => hour {} schedules size : {}", _hour, play_list_crawlers.size());
        //generate playlist crawler
        ChannelDetails pChannel = channelDetailsDao.getChannel(_channelId);

        if (!pChannel.isIsManual()) {

            bFlag = generator.generatePlayListCrawler(play_list_crawlers);
            if (!bFlag) {
                return false;
            }
        } else {
            play_list_crawlers = generator.shufflePlayListCrawler(play_list_crawlers);
        }

        int iOrder = 0;
        for (PlayList playList : play_list_crawlers) {
            playList.setStatus("1");
            playList.setPlayOrder(iOrder++);
            playList.setComment("schedule_generated");
            LOGGER.debug("Cluster : {}  Advert : {}", playList.getTimeBeltStartTime(), playList.getAdvert().getAdvertname());
            playListDao.updatePlayList(playList);
        }
        //get valid cuts for schedule
        ArrayList<PlayList> play_list_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        LOGGER.debug("[GenerateLogoPlayList_BackEnd] => hour {} schedules size : {}", _hour, play_list_logos.size());
        for (PlayList playList : play_list_logos) {
            playList.setStatus("1");
            playList.setComment("schedule_generated");
            logoAuditService.setSelectedPlaylistItemStatus(playList);
            LOGGER.debug("Cluster : {} Advert : {}", playList.getTimeBeltStartTime(), playList.getAdvert().getAdvertname());
            playListDao.updatePlayList(playList);
        }

        time_belt.setIsScheduleGenerated(true);
        time_belt.setScheduleGeneratedTime(new Date());
        timeBeltDao.updateTimeBelt(time_belt);
        return true;
    }*/

    private boolean generatePlayList_BackEnd(int _channelId, int _hour, int period) {

        //get start end times
        TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, period);
        if (time_belt == null) {
            LOGGER.debug("[GenerateAdvertPlayList_BackEnd] TimeBelt Null => channel : {} ", _channelId);
            return false;
        }

        ArrayList<PlayList> play_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        for (PlayList playList : play_logos) {
            setLogoAudit(playList);
        }

        //update not played cuts in previous schedule
        playListDao.updateSchedulesValidity(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        ScheduleGenerate generator = new ScheduleGenerate(_channelId, _hour, time_belt.getStartTime(), time_belt.getEndTime(), time_belt.getClusterCount(), advertismentDao.getFillerAdvertisements());

        Date today = new Date();
        java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            today = (Date) dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException ex) {
            Logger.getLogger(PlayListService.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<ScheduleDef> lstScheduleList = schedulerDao.getSchedulePrioritybyTimeBelt(time_belt.getTimeBeltId(), today);
        List<PriorityDef> lstPriorities = priorityDefDao.getPriorityListbyTimeBelt(time_belt.getTimeBeltId());
        generator.setPrioritySchedulesList(lstScheduleList, lstPriorities);

        ArrayList<PlayList> play_list_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        LOGGER.debug("[GenerateLogoPlayList_BackEnd] => channel: {} | hour: {} | schedules size: {}",_channelId,  _hour, play_list_logos.size());
        for (PlayList playList : play_list_logos) {
            playList.setStatus("1");
            playList.setComment("schedule_generated");
            logoAuditService.setSelectedPlaylistItemStatus(playList);
            LOGGER.debug("Cluster : {} Advert : {}", playList.getTimeBeltStartTime(), playList.getAdvert().getAdvertname());
            playListDao.updatePlayList(playList);
        }

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_crawlers = (ArrayList<PlayList>) playListDao.getPlayListCrawler(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        LOGGER.debug("[GenerateCrawlerPlayList_BackEnd] => channel: {} | hour: {} | schedules size: {}",_channelId,  _hour, play_list_crawlers.size());

        //generate playlist crawler
        ChannelDetails pChannel = channelDetailsDao.getChannel(_channelId);
        boolean bFlag = false;
        if (!pChannel.isIsManual()) {

            bFlag = generator.generatePlayListCrawler(play_list_crawlers);
            if (!bFlag) {
                return false;
            }
        } else {
            play_list_crawlers = generator.shufflePlayListCrawler(play_list_crawlers);
        }

        int iOrder = 0;
        for (PlayList playList : play_list_crawlers) {
            playList.setStatus("1");
            playList.setPlayOrder(iOrder++);
            playList.setComment("schedule_generated");
            LOGGER.debug("Cluster : {}  Advert : {}", playList.getTimeBeltStartTime(), playList.getAdvert().getAdvertname());
            playListDao.updatePlayList(playList);
        }

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_adverts = (ArrayList<PlayList>) playListDao.getPlayListAdvert(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        play_list_adverts = applyMaxAllowedSlotRule(play_list_adverts);
        LOGGER.debug("[GenerateAdvertPlayList_BackEnd] => channel: {} | hour: {} | schedules size: {}",_channelId,  _hour, play_list_adverts.size());
        //generate playlist adverts
        bFlag = generator.generatePlayListAdvert(play_list_adverts);
        if (!bFlag) {
            return false;
        }

        ArrayList<ArrayList<PlayList>> lst_scheduled_adverts = generator.getLst_Schedule();
        for (int i = 0; i < lst_scheduled_adverts.size(); i++) {
            ArrayList<PlayList> lst_cluster = lst_scheduled_adverts.get(i);

            for (int j = 0; j < lst_cluster.size(); j++) {
                PlayList playList = lst_cluster.get(j);
                playList.setPlayCluster(i);
                playList.setPlayOrder(j);
                playList.setStatus("1");
                playList.setComment("schedule_generated");
                LOGGER.debug("Cluster : {} Order : {} Advert : {}", i, j, playList.getAdvert().getAdvertname());
                playListDao.updatePlayList(playList);
            }
        }
        time_belt.setIsScheduleGenerated(true);
        time_belt.setScheduleGeneratedTime(new Date());
        timeBeltDao.updateTimeBelt(time_belt);
        return true;
    }

    @Transactional
    public ArrayList<PlayList> applyMaxAllowedSlotRule(ArrayList<PlayList> playListAdverts){
        Map<Integer, ArrayList<PlayList>> map = new HashMap<>();
        for(PlayList pl : playListAdverts){
            int advertId = pl.getAdvert().getAdvertid();
            ArrayList<PlayList> list = map.get(advertId);
            if(list == null){
                list = new ArrayList<>();
                map.put(advertId, list);
            }
            list.add(pl);
        }
        ArrayList<PlayList> filteredList = new ArrayList<>();
        for(ArrayList<PlayList> playLists :  map.values()){
            if(!playLists.isEmpty()){
                PlayList fPlayList = playLists.get(0);
                int maxSlots = advertSettingService.finMaxAllowedSlots(fPlayList.getChannel().getChannelid(),
                        fPlayList.getAdvert().getDuration());
                if(maxSlots == -1){ // if the rule max slots per hour is not set
                    return playListAdverts;
                }

                //remove based on half hour time belt
                Map<String,  ArrayList<PlayList>> timeSliceMap  = new HashMap<>();
                for(PlayList playList : playLists){
                    String key = playList.getScheduleStartTime().getTime() + "_"+  playList.getScheduleEndTime().getTime();
                    ArrayList<PlayList> list = timeSliceMap.get(key);
                    if(list == null){
                        list = new ArrayList<>();
                        timeSliceMap.put(key, list);
                    }
                    list.add(playList);
                }

                int i = 0;
                int totalAdverts = playLists.size();
                ArrayList<PlayList> sameAdvertList = new ArrayList<>();
                for(ArrayList<PlayList> pl :  timeSliceMap.values()){

                    i++;
                    int slotCount = (int) Math.ceil((double)(pl.size() * maxSlots) / totalAdverts);
                    if(i == timeSliceMap.size() || ((sameAdvertList.size() + slotCount) > maxSlots )){
                        slotCount = maxSlots - sameAdvertList.size();
                    }

                    Iterator<PlayList> iterator = pl.iterator();
                    if(slotCount < pl.size()){
                        while (iterator.hasNext() && pl.size() > slotCount){
                            PlayList playList = iterator.next();
                            if(playList.getSchedule().getPriority() == null){
                                playList.setStatus("11"); // set the status to "Skipped"
                                playList.setPlayCluster(-1);
                                playListDao.saveOrUpdate(playList);
                                iterator.remove();
                            }
                        }
                    }
                    sameAdvertList.addAll(pl);
                }
                filteredList.addAll(sameAdvertList);
            }
        }
        return filteredList;
    }

    public void setLogoAudit(PlayList playList) {
        LogoAudit logoAudit = new LogoAudit();
        logoAudit.setPlaylist(playList);
        logoAudit.setStartTime(playList.getActualStartTime());
        logoAudit.setEndTime(playList.getActualEndTime());
        logoAudit.setStatus(playList.getStatus());
        logoAudit.setHour(playList.getScheduleHour());
        logoAuditService.save(logoAudit);
    }

    public Boolean getAuditList(List<ScheduleDef> newScheduleList, List<ScheduleDef> oldScheduleList, String user) {
        for (ScheduleDef newSchedule : newScheduleList) {
            for (ScheduleDef oldSchedule : oldScheduleList) {
                int one = newSchedule.getScheduleid();
                int two = oldSchedule.getScheduleid();
                if (one == two) {
                    setAudit(oldSchedule, newSchedule, user);
                    break;
                }
            }
        }
        return true;
    }

    public Boolean setAudit(ScheduleDef oldSchedule, ScheduleDef model, String user) {
        try {
            ScheduleDef dataModel = oldSchedule;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            for (Field field : (ScheduleDef.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {
                    System.out.println(field.getName() + " - " + field.getAnnotation(Column.class
                    ).name()
                            + " - " + field.get(dataModel) + " - " + field.get(model));

                    GeneralAudit auditModel = new GeneralAudit("schedule_def", model.getScheduleid());

                    auditModel.setField(field.getAnnotation(Column.class
                    ).name());
                    if(auditModel.getField().equals("Status")) {
                        auditModel.setOldData(field.get(dataModel).toString() + " by nash");
                    }else{
                        auditModel.setOldData(field.get(dataModel).toString());
                    }
                    auditModel.setNewData(field.get(model).toString());
                    auditModel.setUser(user);

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }

            return true;
        } catch (IllegalAccessException e) {
            System.err.println("[error] => " + e.getClass() + " : " + e.getStackTrace()[0].getMethodName() + " - " + e.getMessage());
        } catch (Exception e) {
            System.err.println("[error] => " + e.getClass() + " : " + e.getStackTrace()[0].getMethodName() + " - " + e.getMessage());
            throw e;
        }

        return false;
    }

    private void locateInventory(TimeBelts tBelt, ScheduleDef schedule,int iChannelID){
        Time fromTime = new Time(tBelt.getStartTime().getHours(), tBelt.getStartTime().getMinutes(), 0);
        Time toTime = new Time(tBelt.getEndTime().getHours(), tBelt.getStartTime().getMinutes(), 0);
        InventoryRow inventoryRow = inventoryService.findByChannelAndDateAndTime(iChannelID, schedule.getDate(), fromTime, toTime);
        NashInventoryConsumption nashInventoryConsumption = nashInventoryConsumptionService.findByInventoryRowId(inventoryRow.getId());
        if (nashInventoryConsumption == null) {
            nashInventoryConsumption = createNashInventoryConsumption(inventoryRow, tBelt, schedule);
            nashInventoryConsumptionService.save(nashInventoryConsumption);
        } else {
            if (schedule.getAdvertid().getAdverttype().equals("ADVERT")) {
                nashInventoryConsumption.setRemainingTime((inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration()) - (int) tBelt.getUtilizedScheduleTime());
                nashInventoryConsumption.setSpentTime(tBelt.getUtilizedScheduleTime());
            } else if (schedule.getAdvertid().getAdverttype().equals("CRAWLER")) {
                nashInventoryConsumption.setRemainingCrawlerCount((inventoryRow.getCrawlerSpots() - inventoryRow.getWebCrawlerSpots()) - (nashInventoryConsumption.getSpentCrawlerCount() + 1));
                nashInventoryConsumption.setSpentCrawlerCount(nashInventoryConsumption.getSpentCrawlerCount() + 1);
            } else if (schedule.getAdvertid().getAdverttype().equals("L_SHAPE") || schedule.getAdvertid().getAdverttype().equals("V_SHAPE")) {
                nashInventoryConsumption.setRemainingLCrawlerCount((inventoryRow.getLCrawlerSpots() - inventoryRow.getWebLCrawlerSpots()) - (nashInventoryConsumption.getSpentLCrawlerCount() + 1));
                nashInventoryConsumption.setSpentLCrawlerCount(nashInventoryConsumption.getSpentLCrawlerCount() + 1);
            }else if(schedule.getAdvertid().getAdverttype().equals("LOGO")){
                nashInventoryConsumption.setRemainingLogoCount((inventoryRow.getLogoSpots() - inventoryRow.getWebLogoSpots()) - (1 + nashInventoryConsumption.getSpentLogoCount()));
                nashInventoryConsumption.setSpentLogoCount((1 + nashInventoryConsumption.getSpentLogoCount()));
                nashInventoryConsumptionService.save(nashInventoryConsumption);
            }
            nashInventoryConsumptionService.save(nashInventoryConsumption);
        }
    }

    private NashInventoryConsumption createNashInventoryConsumption(InventoryRow inventoryRow, TimeBelts timeBelts, ScheduleDef scheduleDef) {
        NashInventoryConsumption nashInventoryConsumption = new NashInventoryConsumption();

        if (scheduleDef.getAdvertid().getAdverttype().equals("ADVERT")) {

            nashInventoryConsumption.setSpentCrawlerCount(0);
            nashInventoryConsumption.setRemainingCrawlerCount(inventoryRow.getCrawlerSpots() - inventoryRow.getWebCrawlerSpots());

            nashInventoryConsumption.setSpentLCrawlerCount(0);
            nashInventoryConsumption.setRemainingLCrawlerCount(inventoryRow.getLCrawlerSpots() - inventoryRow.getWebLCrawlerSpots());

            nashInventoryConsumption.setSpentLogoCount(0);
            nashInventoryConsumption.setRemainingLogoCount(inventoryRow.getLogoSpots() - inventoryRow.getWebLogoSpots());

            nashInventoryConsumption.setSpentTime((int) timeBelts.getUtilizedScheduleTime());
            nashInventoryConsumption.setRemainingTime((inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration()) - (int) timeBelts.getUtilizedScheduleTime());

        } else if (scheduleDef.getAdvertid().getAdverttype().equals("LOGO")) {

            nashInventoryConsumption.setSpentCrawlerCount(0);
            nashInventoryConsumption.setRemainingCrawlerCount(inventoryRow.getCrawlerSpots() - inventoryRow.getWebCrawlerSpots());

            nashInventoryConsumption.setSpentLCrawlerCount(0);
            nashInventoryConsumption.setRemainingLCrawlerCount(inventoryRow.getLCrawlerSpots() - inventoryRow.getWebLCrawlerSpots());

            nashInventoryConsumption.setSpentLogoCount(1);
            nashInventoryConsumption.setRemainingLogoCount(inventoryRow.getLogoSpots() - inventoryRow.getWebLogoSpots() - 1);

            nashInventoryConsumption.setSpentTime(0);
            nashInventoryConsumption.setRemainingTime(inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration());

        } else if (scheduleDef.getAdvertid().getAdverttype().equals("CRAWLER")) {
            nashInventoryConsumption.setSpentCrawlerCount((int) timeBelts.getUtilizedScheduleTime());
            nashInventoryConsumption.setRemainingCrawlerCount((inventoryRow.getCrawlerSpots() - inventoryRow.getWebCrawlerSpots()) - (int) timeBelts.getUtilizedScheduleTime());

            nashInventoryConsumption.setSpentLCrawlerCount(0);
            nashInventoryConsumption.setRemainingLCrawlerCount(inventoryRow.getLCrawlerSpots() - inventoryRow.getWebLCrawlerSpots());

            nashInventoryConsumption.setSpentLogoCount(0);
            nashInventoryConsumption.setRemainingLogoCount(inventoryRow.getLogoSpots() - inventoryRow.getWebLogoSpots());

            nashInventoryConsumption.setSpentTime(0);
            nashInventoryConsumption.setRemainingTime(inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration());

        } else if (scheduleDef.getAdvertid().getAdverttype().equals("L_SHAPE") || scheduleDef.getAdvertid().getAdverttype().equals("V_SHAPE")) {
            nashInventoryConsumption.setSpentCrawlerCount(0);
            nashInventoryConsumption.setRemainingCrawlerCount((inventoryRow.getCrawlerSpots() - inventoryRow.getWebCrawlerSpots()));

            nashInventoryConsumption.setSpentLCrawlerCount((int) timeBelts.getUtilizedScheduleTime());
            nashInventoryConsumption.setRemainingLCrawlerCount((inventoryRow.getLCrawlerSpots() - inventoryRow.getWebLCrawlerSpots()) - (int) timeBelts.getUtilizedScheduleTime());

            nashInventoryConsumption.setSpentLogoCount(0);
            nashInventoryConsumption.setRemainingLogoCount(inventoryRow.getLogoSpots() - inventoryRow.getWebLogoSpots());

            nashInventoryConsumption.setSpentTime(0);
            nashInventoryConsumption.setRemainingTime(inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration());
        }
        nashInventoryConsumption.setFromTime(inventoryRow.getFromTime());
        nashInventoryConsumption.setToTime(inventoryRow.getToTime());
        nashInventoryConsumption.setChannel(scheduleDef.getChannelid());
        nashInventoryConsumption.setInventoryRow(inventoryRow);

        return nashInventoryConsumption;
    }

    private void deallocateInventory(List<ScheduleDef> removeSchedule) {
        for (ScheduleDef scheduleDef : removeSchedule) {
            if(scheduleDef.getWorkorderid().getInventoryType()== WorkOrder.InventoryType.NEW) {
                TimeSlotScheduleMap timeSlotScheduleMap = timeSlotScheduleMapDao.getSelectedTimeSlot(scheduleDef.getScheduleid());
                Time fromTime = new Time(timeSlotScheduleMap.getTimeBelt().getStartTime().getHours(), timeSlotScheduleMap.getTimeBelt().getStartTime().getMinutes(), 0);
                Time toTime = new Time(timeSlotScheduleMap.getTimeBelt().getEndTime().getHours(), timeSlotScheduleMap.getTimeBelt().getStartTime().getMinutes(), 0);

                InventoryRow inventoryRow = inventoryService.findByChannelAndDateAndTime(timeSlotScheduleMap.getChannelid().getChannelid(), timeSlotScheduleMap.getDate(), fromTime, toTime);
                NashInventoryConsumption nashInventoryConsumption = nashInventoryConsumptionService.findByInventoryRowId(inventoryRow.getId());
                if (nashInventoryConsumption != null) {
                    if (scheduleDef.getAdvertid().getAdverttype().equals("ADVERT")) {
                        nashInventoryConsumption.setRemainingTime(nashInventoryConsumption.getRemainingTime() + scheduleDef.getAdvertid().getDuration());
                        nashInventoryConsumption.setSpentTime(nashInventoryConsumption.getSpentTime() - scheduleDef.getAdvertid().getDuration());
                    } else if (scheduleDef.getAdvertid().getAdverttype().equals("LOGO")) {
                        nashInventoryConsumption.setRemainingLogoCount(nashInventoryConsumption.getRemainingLogoCount() + 1);
                        nashInventoryConsumption.setSpentLogoCount(nashInventoryConsumption.getSpentLogoCount() - 1);
                    } else if (scheduleDef.getAdvertid().getAdverttype().equals("CRAWLER")) {
                        nashInventoryConsumption.setRemainingCrawlerCount(nashInventoryConsumption.getRemainingCrawlerCount() + 1);
                        nashInventoryConsumption.setSpentCrawlerCount(nashInventoryConsumption.getSpentCrawlerCount() - 1);
                    } else if (scheduleDef.getAdvertid().getAdverttype().equals("L_SHAPE") || scheduleDef.getAdvertid().getAdverttype().equals("V_SHAPE")) {
                        nashInventoryConsumption.setRemainingLCrawlerCount(nashInventoryConsumption.getRemainingLCrawlerCount() + 1);
                        nashInventoryConsumption.setSpentLCrawlerCount(nashInventoryConsumption.getSpentLCrawlerCount() - 1);
                    }
                    nashInventoryConsumptionService.save(nashInventoryConsumption);
                }
            }
        }
    }
}