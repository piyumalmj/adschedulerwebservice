package com.vclabs.nash.service.utill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 01/05/2019.
 */
@Component
public class FFMpegUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(FFMpegUtility.class);

    public boolean covertFromMpegToMp4(String srcPath,String destPath) {
        try {
            long time1 = System.currentTimeMillis();
            List<String> cmdList = new ArrayList<>();
            cmdList.add("ffmpeg");
            cmdList.add("-i");
            cmdList.add(srcPath);
            cmdList.add("-c:v");
            cmdList.add("libx264");
            cmdList.add("-crf");
            cmdList.add("28");
            cmdList.add("-preset");
            cmdList.add("ultrafast");
            cmdList.add("-strict");
            cmdList.add("-2");
            cmdList.add(destPath);

            ProcessBuilder pb = new ProcessBuilder(cmdList);
            Process p = pb.start();
            //p.waitFor();
            InputStream stream = p.getInputStream();
            if (stream.available() == 0) {
                stream = p.getErrorStream();
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
            }
            long time2 = System.currentTimeMillis();
            LOGGER.debug("FFMPEG Video Converter :: Output file : {} | Time to convert : {}", destPath, (time2 - time1));
            return true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return false;
        }
    }
}
