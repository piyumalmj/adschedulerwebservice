package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.OPSResponseDataDAO;
import com.vclabs.nash.model.entity.OPSResponseData;
import com.vclabs.nash.model.process.OPSMainSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-25.
 */
@Service
@Transactional("main")
public class OPSResponseDataService extends DateFormat {

    @Autowired
    private OPSResponseDataDAO oPSResponseDataDao;

    public OPSResponseData saveHotDealResponse(String data) {

        OPSResponseData oPSResponseData = new OPSResponseData();
        oPSResponseData.setResponseData(data);
        oPSResponseData.setCreateDate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
        oPSResponseData.setStatus(OPSResponseData.Status.FAIL);
        oPSResponseData.setType(OPSResponseData.Type.HOTDEAL);

        return oPSResponseDataDao.save(oPSResponseData);
    }

    public OPSResponseData saveCustonScheduleResponse(String data) {

        OPSResponseData oPSResponseData = new OPSResponseData();
        oPSResponseData.setResponseData(data);
        oPSResponseData.setCreateDate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
        oPSResponseData.setStatus(OPSResponseData.Status.FAIL);
        oPSResponseData.setType(OPSResponseData.Type.CUSTOMSCHEDULE);

        return oPSResponseDataDao.save(oPSResponseData);
    }

    public OPSResponseData saveBidItemResponse(String data) {

        OPSResponseData oPSResponseData = new OPSResponseData();
        oPSResponseData.setResponseData(data);
        oPSResponseData.setCreateDate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
        oPSResponseData.setStatus(OPSResponseData.Status.FAIL);
        oPSResponseData.setType(OPSResponseData.Type.BIDITEM);

        return oPSResponseDataDao.save(oPSResponseData);
    }

    public OPSResponseData savePromotionResponse(String data) {

        OPSResponseData oPSResponseData = new OPSResponseData();
        oPSResponseData.setResponseData(data);
        oPSResponseData.setCreateDate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
        oPSResponseData.setStatus(OPSResponseData.Status.FAIL);
        oPSResponseData.setType(OPSResponseData.Type.PROMOTION);

        return oPSResponseDataDao.save(oPSResponseData);
    }

    public OPSResponseData updateResponseData(OPSResponseData oPSResponseData) {
        return oPSResponseDataDao.save(oPSResponseData);
    }

    public OPSResponseData findById(long id) {
        return oPSResponseDataDao.findById(id);
    }

    public List<OPSMainSchedule> getResponseData(long id) {
        ObjectMapper m = new ObjectMapper();
        OPSResponseData failData = oPSResponseDataDao.findById(id);
        List<OPSMainSchedule> responseData = null;
        try {
            responseData = m.readValue(failData.getResponseData(), new TypeReference<List<OPSMainSchedule>>() {
            });
        } catch (IOException e) {
            System.out.println("" + e);
        }

        return responseData;
    }
}
