package com.vclabs.nash.service.teams.impl;

import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.model.dao.Teams.TeamDetailDAO;
import com.vclabs.nash.model.dao.UserDetailsDAO;
import com.vclabs.nash.model.entity.SecurityUsersEntity;
import com.vclabs.nash.model.entity.teams.TeamDetail;
import com.vclabs.nash.service.teams.TeamDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Sanduni on 03/01/2019
 */
@Service
public class TeamDetailServiceImpl implements TeamDetailService{

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamDetailServiceImpl.class);

    @Autowired
    private TeamDetailDAO repository;
    @Autowired
    private UserDetailsDAO userdetails;

    @Override
    @Transactional
    public TeamDetail save(TeamDetail teamDetail) {
        try {
            teamDetail = repository.save(teamDetail);
            List<String> userNameList = new ArrayList<>();
            Set<SecurityUsersEntity> members = teamDetail.getMembers();
            if (!members.isEmpty()) {
                for (SecurityUsersEntity securityUsersEntity : members) {
                    userNameList.add(securityUsersEntity.getUsername());
                }
                userdetails.updateHasTeamTrue(userNameList);
            }
            List<String> adminNameList = new ArrayList<>();
            Set<SecurityUsersEntity> admin = teamDetail.getAdmins();
            if (!admin.isEmpty()) {
                for (SecurityUsersEntity securityUsersEntity : admin) {
                    adminNameList.add(securityUsersEntity.getUsername());
                }
                userdetails.updateHasTeamTrue(adminNameList);
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
        }
        return teamDetail;
    }

    @Override
    @Transactional
    public TeamDetail update(TeamDetail teamDetail) {
        try {
            TeamDetail oldTeamDetail = repository.findById(teamDetail.getId());

            List<String> userNameList = new ArrayList<>();
            if (!teamDetail.getMembers().isEmpty()) {
                for (SecurityUsersEntity member : teamDetail.getMembers()) {
                    oldTeamDetail.getMembers().add(member);
                    userNameList.add(member.getUsername());
                }
                userdetails.updateHasTeamTrue(userNameList);
            }

            List<String> adminNameList = new ArrayList<>();
            if (!teamDetail.getAdmins().isEmpty()) {
                for (SecurityUsersEntity admin : teamDetail.getAdmins()) {
                    oldTeamDetail.getAdmins().add(admin);
                    adminNameList.add(admin.getUsername());
                }
                userdetails.updateHasTeamTrue(adminNameList);
            }
            repository.save(oldTeamDetail);
            return teamDetail;
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public TeamDetail findById(Integer id) {
        TeamDetail teamDetail = repository.findById(id);
        return teamDetail;
    }

    @Override
    public List<TeamDetail> findAllTeams() {
        List<TeamDetail> teams = repository.findAll();
        for(TeamDetail team : teams){
            team.setAdmins(null);
            team.setMembers(null);
        }
        return teams;
    }

    @Override
    @Transactional
    public Boolean checkTeamName(String teamName) {
        return repository.findByTeamName(teamName).size() == 0 ? true : false;
    }

    @Override
    public List<String> findAllTeamMembers(Integer teamId) {
        return repository.findAllMembersOfTeam(teamId);
    }

    @Override
    public List<String> findAllTeamAdmins(Integer teamId) {
        return repository.findAllAdminsOfTeam(teamId);
    }

    @Override
    @Transactional
    public Boolean deleteTeamMember(Integer teamId, String username) {
        List<String> userNameList = new ArrayList<>();
        userNameList.add(username);
        userdetails.updateHasTeamFalse(userNameList);
        return repository.deleteMember(teamId, username);
    }

    @Override
    @Transactional
    public Boolean deleteTeamAdmin(Integer teamId, String username) {
        List<String> adminNameList = new ArrayList<>();
        adminNameList.add(username);
        userdetails.updateHasTeamFalse(adminNameList);
        return repository.deleteAdmin(teamId, username);
    }

    @Override
    public List<MyTeamToDoListDto> getTeamMembersWODetail() {
        return repository.getTeamMembersWODetails();
    }

    @Override
    public List<TeamDetail> findAllTeamDetailsForTeamSalesRevenue() {
        return repository.findAll();
    }

}
