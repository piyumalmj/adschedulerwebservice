package com.vclabs.nash.service.cronjobs;

import com.vclabs.nash.service.*;
import com.vclabs.nash.service.async.WorkOrderStatusCheckAsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by dperera on 27/03/2019.
 */
@Component
public class CronJobExecutor {

    @Lazy
    @Autowired
    private WorkOrderStatusCheckService workOrderStatusCheckService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    @Lazy
    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    @Lazy
    @Autowired
    private AdvertisementService advertisementService;

    @Lazy
    @Autowired
    private PlayListService playListService;

    @Lazy
    @Autowired
    private WorkOrderStatusCheckAsyncService workOrderStatusCheckAsyncService;

    @Scheduled(cron = "0 15 0 1/1 * ?")
    public void checkWorkOrder() {
        //workOrderStatusCheckService.checkWorkOrderAsync();
        workOrderStatusCheckAsyncService.processWorkOrders();
    }

    @Scheduled(cron = "0 45 23 1/1 * ?")
    public void clearAndCreatePlayList(){
        schedulerService.clearAndCreatePlayListAsync();
    }

    @Scheduled(cron = "0 45 23 * * *")
    public void clearAllChannelAsync(){
        channelAdvertMapService.clearAllChannelAsync();
    }

    @Scheduled(cron = "0 40 23 1/1 * ?")
    public void advertisementExpirationAndSuspendAsync(){
        advertisementService.advertisementExpirationAndSuspendAsync();
    }

    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    public void autoScheduleGenerateCallerAsync(){
        playListService.autoScheduleGenerateCallerAsync();
    }
}
