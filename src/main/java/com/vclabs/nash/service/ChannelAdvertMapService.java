package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.ChannelAdvertMapDAO;
import com.vclabs.nash.model.dao.PlayListDAO;
import com.vclabs.nash.model.dao.ZeroAdsPlayListDAO;
import com.vclabs.nash.model.dao.ZeroAdsSettingDAO;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.process.ChannelAdvertInfo;
import com.vclabs.nash.model.view.ChannelAdvertMapView;
import com.vclabs.nash.model.view.ManualChannelAdvertMapView;
import com.vclabs.nash.model.view.PlayListBackendView;

import com.vclabs.nash.model.view.ZeroAndFillerPlayListBackendView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional("main")
public class ChannelAdvertMapService extends DateFormat{

    static HashMap<Integer, List<Integer>> channelAdvertMap = new HashMap<>();
    @Autowired
    private ChannelAdvertMapDAO channelAdvertMapDAO;
    @Autowired
    private AdvertisementDAO advertisementDAO;
    @Autowired
    private PlayListDAO playListDao;
    @Autowired
    private ZeroAdsPlayListDAO zeroAdsPlayListDao;
    @Autowired
    private ZeroAdsSettingService zeroAdsSettingService;
    @Autowired
    private ZeroAdsSettingDAO zeroAdsSettingDao;

    @Async("dailyTaskExecutor")
    public void clearAllChannelAsync(){
        logger.debug("Started clearAllChannelAsync cron job | (cron = 0 45 23 1/1 * ?) | ( current time : {} )", new Date().toString());
        clearAll();
        logger.debug("Completed clearAllChannelAsync cron job | (cron = 0 45 23 1/1 * ?) | ( current time : {} )", new Date().toString());
    }

    //When generate new play list, Channel map also cleared
    //@Scheduled(cron = "0 55 23 * * *") MTNL
    public void clearAll() {
        channelAdvertMap.clear();
    }

    private static final Logger logger = LoggerFactory.getLogger(ChannelAdvertMapService.class);

    public ChannelAdvertMapView getAllChannelAdvertMap() {

        ChannelAdvertMapView viewModel = new ChannelAdvertMapView();

        List<ChannelAdvertMap> allChanAdvetMap = channelAdvertMapDAO.getAllChannelAdvertMap();

        for (ChannelAdvertMap model : allChanAdvetMap) {
            viewModel.getChannelIdMap().put(model.getChannelid().getChannelid(), model.getChannelid().getChannelname());
            viewModel.getAdvertIdMap().put(model.getAdvertid().getAdvertid(), model.getAdvertid().getAdvertname());

            String key = model.getChannelid().getChannelid() + "_" + model.getAdvertid().getAdvertid();
            viewModel.getChannelAdvertMap().put(key, model.getPercentage());
        }

        return viewModel;
    }

    public Boolean saveChannelAdvertMap(String mapData) throws Exception {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<ChannelAdvertInfo> channelAdvertModelList;
            channelAdvertModelList = mapper.readValue(mapData, new TypeReference<List<ChannelAdvertInfo>>() {
            });

            List<ChannelAdvertMap> allChanAdvetMap = channelAdvertMapDAO.getAllChannelAdvertMap();
            Boolean updated = false;
            for (int i = 0; i < allChanAdvetMap.size(); i++) {
                updated = false;
                for (int j = 0; j < channelAdvertModelList.size(); j++) {

                    if (allChanAdvetMap.get(i).getChannelid().getChannelid() == channelAdvertModelList.get(j).getChannelId()
                            && allChanAdvetMap.get(i).getAdvertid().getAdvertid() == channelAdvertModelList.get(j).getAdvertId()) {

                        if (channelAdvertModelList.get(j).getPercentage() == 0) {
                            allChanAdvetMap.get(i).setStatus(0);
                        }
                        allChanAdvetMap.get(i).setPercentage(channelAdvertModelList.get(j).getPercentage());
                        channelAdvertMapDAO.saveOrUpdateChannelAdvertMap(allChanAdvetMap.get(i));

                        updated = true;
                        channelAdvertModelList.remove(channelAdvertModelList.get(j));
                        j--;
                    }
                }
                if (updated) {
                    List<Integer> advertList = channelAdvertMap.get(allChanAdvetMap.get(i).getChannelid().getChannelid());
                    if (advertList != null) {
                        channelAdvertMap.remove(allChanAdvetMap.get(i).getChannelid().getChannelid());
                    }
                    allChanAdvetMap.remove(allChanAdvetMap.get(i));
                    i--;

                }
            }

            for (ChannelAdvertMap updateModel : allChanAdvetMap) {
                updateModel.setStatus(0);
                channelAdvertMapDAO.saveOrUpdateChannelAdvertMap(updateModel);
            }

            for (ChannelAdvertInfo newDataModel : channelAdvertModelList) {
                Advertisement advert = new Advertisement();
                advert.setAdvertid(newDataModel.getAdvertId());

                ChannelDetails channel = new ChannelDetails();
                channel.setChannelid(newDataModel.getChannelId());

                ChannelAdvertMap newModel = new ChannelAdvertMap();

                newModel.setAdvertid(advert);
                newModel.setChannelid(channel);
                newModel.setPercentage(newDataModel.getPercentage());
                newModel.setStatus(1);

                channelAdvertMapDAO.saveOrUpdateChannelAdvertMap(newModel);
            }
        } catch (Exception e) {
            logger.debug("Exception in ChannelAdvertMapService in saveChannelAdvertMap() : {}", e.getMessage());
            throw e;
        }
        return true;
    }

    public Boolean saveAdvertMap(String mapData) throws Exception {
        try {

            ObjectMapper mapper = new ObjectMapper();
            List<ChannelAdvertInfo> channelAdvertModelList;
            channelAdvertModelList = mapper.readValue(mapData, new TypeReference<List<ChannelAdvertInfo>>() {
            });

            for (ChannelAdvertInfo newDataModel : channelAdvertModelList) {
                Advertisement advert = new Advertisement();
                advert.setAdvertid(newDataModel.getAdvertId());

                ChannelDetails channel = new ChannelDetails();
                channel.setChannelid(newDataModel.getChannelId());

                ChannelAdvertMap newModel = new ChannelAdvertMap();

                newModel.setAdvertid(advert);
                newModel.setChannelid(channel);
                newModel.setPercentage(newDataModel.getPercentage());
                newModel.setStatus(1);

                channelAdvertMapDAO.saveOrUpdateChannelAdvertMap(newModel);
            }
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ChannelAdvertMapService in saveAdvertMap() : {}", e.getMessage());
            throw e;
        }
    }

    public ZeroAndFillerPlayListBackendView getPlayList(int channelId) {
        List<Integer> advertList = channelAdvertMap.get(channelId);
        if (advertList == null || advertList.isEmpty()) {
            channelAdvertMap.put(channelId, loadPlayList(channelId));
            advertList = channelAdvertMap.get(channelId);
        }
        int advertid = advertList.remove(0);
        return getPlayListModel(channelId, advertid);
    }

    public List<Integer> loadPlayList(int channelId) {

        ZeroAdsSetting zeroAdsSetting = zeroAdsSettingDao.findByChannelId(channelId);

        final int TEMP_LIST_SIZE = 100;
        LinkedList<Integer> advertID = new LinkedList<>();
        List<Integer> advertIdList = new ArrayList<>();

        if (zeroAdsSetting == null || zeroAdsSetting.getpListType() == ZeroAdsSetting.PlayListType.AUTOPLAYLIST) {

            List<ChannelAdvertMap> chanAdvetMapList = channelAdvertMapDAO.getChannelAdvertMap(channelId);

            if (chanAdvetMapList.isEmpty()) {
                return advertIdList;
            }
            for (ChannelAdvertMap item : chanAdvetMapList) {
                int advertCount = (TEMP_LIST_SIZE * item.getPercentage()) / 100;
                item.setSlotCount(advertCount);
            }

            Collections.sort(chanAdvetMapList);
            int lastAdvertID = 0;
            int lastClientID = 0;
            ChannelAdvertMap nextItem = chanAdvetMapList.get(0);


            while (nextItem != null) {

                //advertIdList.add(nextItem.getAdvertid().getAdvertid());
                advertID.add(nextItem.getAdvertid().getAdvertid());
                nextItem.setSlotCount(nextItem.getSlotCount() - 1);
                Collections.sort(chanAdvetMapList);
                lastAdvertID = nextItem.getAdvertid().getAdvertid();
                lastClientID = nextItem.getAdvertid().getClient().getClientid();

                nextItem = null;

                for (ChannelAdvertMap cam : chanAdvetMapList) {

                    if (cam.getAdvertid().getAdvertid() == lastAdvertID) {
                        continue;
                    }

                    if (cam.getAdvertid().getClient().getClientid() != lastClientID && cam.getSlotCount() > 0) {
                        nextItem = cam;
                        break;
                    }
                }

            }
        /*-----------------------Add remaining advert into linklist-----------------*/
            int listSize = advertID.size() == 1 ? advertID.size() : advertID.size() - 1;
            Random rnd = new Random();

            for (ChannelAdvertMap advertDetais : chanAdvetMapList) {

                int tryCount = 0;
                while (advertDetais.getSlotCount() > 0) {
                    tryCount++;
                    if (tryCount == 10) {
                        break;
                    }
                    int listIndex = rnd.nextInt(listSize);
                    if (listIndex != 0 && listIndex != (listSize)) {

                        int previousAdvertID = advertID.get(listIndex - 1);
                        int previousClientID = 0;
                        for (ChannelAdvertMap advert1 : chanAdvetMapList) {
                            if (advert1.getAdvertid().getAdvertid() == previousAdvertID) {
                                previousClientID = advert1.getAdvertid().getClient().getClientid();
                                break;
                            }
                        }
                        int nextAdvertID = advertID.get(listIndex + 1);
                        int nextClientID = 0;
                        for (ChannelAdvertMap advert1 : chanAdvetMapList) {
                            if (advert1.getAdvertid().getAdvertid() == nextAdvertID) {
                                nextClientID = advert1.getAdvertid().getClient().getClientid();
                                break;
                            }
                        }
                        int currentAdvertID = advertDetais.getAdvertid().getAdvertid();
                        int currentClientID = advertDetais.getAdvertid().getClient().getClientid();

                        if (previousAdvertID != currentAdvertID && nextAdvertID != currentAdvertID && previousClientID != currentClientID && nextClientID != currentClientID) {
                            advertID.add(listIndex, currentAdvertID);
                            advertDetais.setSlotCount(advertDetais.getSlotCount() - 1);
                        }
                    }
                }
            }
            advertIdList.clear();
            for (int i = 0; i < advertID.size(); i++) {
                int clientID = 0;
                for (ChannelAdvertMap advert1 : chanAdvetMapList) {
                    if (advert1.getAdvertid().getAdvertid() == advertID.get(i)) {
                        clientID = advert1.getAdvertid().getClient().getClientid();
                        break;
                    }
                }
                advertIdList.add(advertID.get(i));
                logger.info("Auto List--> Channel ID : " + channelId + " Advert ID : " +advertID.get(i) + " Client ID " + clientID);
            }

        } else {
            List<ManualChannelAdvertMapView> viewPlayList = getZeroAdsManualPlayList(channelId);
            for (ManualChannelAdvertMapView manualChannelAdvertMapView : viewPlayList) {
                advertIdList.add(manualChannelAdvertMapView.getAdvertId());
                logger.info("Manual List--> Channel ID : " + channelId + " Advert ID : " +manualChannelAdvertMapView.getAdvertId());
            }
        }
        System.out.println("Advert list size :"+advertIdList.size());
        return advertIdList;
    }

    public Boolean clearPalyLis(int channelId) {
        try {
            channelAdvertMap.remove(channelId);
            return true;
        } catch (Exception ex) {
            logger.debug("Exception in ChannelAdvertMapService in clearPalyLis() : {}", ex.getMessage());
            return false;
        }
    }

    public ZeroAndFillerPlayListBackendView getPlayListModel(int channelId, int advertId) {
        ZeroAndFillerPlayListBackendView tmp = new ZeroAndFillerPlayListBackendView();
        int currentHour = LocalDateTime.now().getHour();
        int nextHour = currentHour++;

        String defaultDate = "1970-01-01 00:00:00";
        PlayList playList = new PlayList();
        playList.setScheduleEndTime(getDataYYYMMDD_dash_HHmmss_colan("1970-01-01 " + nextHour + ":00:00.000"));
        playList.setScheduleStartTime(getDataYYYMMDD_dash_HHmmss_colan("1970-01-01 " + currentHour + ":00:00.000"));
        playList.setTimeBeltEndTime(getDataYYYMMDD_dash_HHmmss_colan(defaultDate));
        playList.setTimeBeltStartTime(getDataYYYMMDD_dash_HHmmss_colan(defaultDate));
        playList.setActualEndTime(getDataYYYMMDD_dash_HHmmss_colan(defaultDate));
        playList.setActualStartTime(getDataYYYMMDD_dash_HHmmss_colan(defaultDate));
        playList.setScheduleHour(10);
        playList.setChannel(new ChannelDetails(channelId));
        playList.setAdvert(advertisementDAO.getSelectedAdvertisement(advertId));
        playList.setSchedule(new ScheduleDef(1));
        playList.setWorkOrder(new WorkOrder(0));
        playList.setDate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash__00_00_00(new Date())));
        playList.setStatus("0");
        playList.setPlayCluster(-1);
        playList.setPlayOrder(-1);
        playList.setRetryCount(0);
        playList.setComment("Zero_Advert_generated");
        playList.setLable(PlayList.Lable.ZERO);

        //Write advert to play list
        playListDao.updatePlayList(playList);

        tmp.setPlaylistid(playList.getPlaylistId());
        tmp.setScheduleHour(playList.getScheduleHour());
        tmp.setScheduleStartTime(playList.getScheduleStartTime());
        tmp.setScheduleEndTime(playList.getScheduleEndTime());
        tmp.setTimeBeltStartTime(playList.getTimeBeltStartTime());
        tmp.setTimeBeltEndTime(playList.getTimeBeltEndTime());
        tmp.setActualStartTime(playList.getActualStartTime());
        tmp.setActualEndTime(playList.getActualEndTime());
        tmp.setStatus(playList.getStatus());
        tmp.setRetryCount(playList.getRetryCount());

        tmp.setPlayCluster(playList.getPlayCluster());
        tmp.setPlayOrder(playList.getPlayOrder());

        Advertisement advert = advertisementDAO.getSelectedAdvertisement(advertId);
        tmp.setAdvertId(advert.getAdvertid());
        tmp.setAdvertName(advert.getAdvertname());
        tmp.setAdvertType("FILLER");
        tmp.setAdvertPath(advert.getAdvertpath());
        tmp.setAdvertCommercialCategory(advert.getCommercialcategory());
        tmp.setAdvertDuration(advert.getDuration());
        tmp.setLogoContainerId(advert.getLogoContainerId());
        logger.info("Channel ID : " + channelId + " Advert ID : " + tmp.getAdvertId() + " Advert Name : " + tmp.getAdvertName());
        return tmp;
    }

    public List<ManualChannelAdvertMapView> getZeroAdsManualPlayList(int channelId) {

        List<ZeroAdsPlayList> playList = zeroAdsPlayListDao.getPlayListByChannel(channelId);
        List<ManualChannelAdvertMapView> viewPlayList = new ArrayList<>();
        if (!playList.isEmpty()) {
            for (ZeroAdsPlayList zeroAdsPlayList : playList) {
                ManualChannelAdvertMapView viewModel = new ManualChannelAdvertMapView();
                viewModel.setZeroAdsId(zeroAdsPlayList.getZeroPlayListId());
                viewModel.setAdvertId(zeroAdsPlayList.getAdvertid().getAdvertid());
                viewModel.setAdvertName(zeroAdsPlayList.getAdvertid().getAdvertname());
                viewModel.setAdvertStatus(zeroAdsPlayList.getAdvertid().getStatus());
                viewModel.setAdvertEnable(zeroAdsPlayList.getAdvertid().getEnabled());
                viewModel.setChannelId(zeroAdsPlayList.getChannelid().getChannelid());
                viewModel.setChannelName(zeroAdsPlayList.getChannelid().getChannelname());
                viewModel.setOrderNum(zeroAdsPlayList.getOrderNumber());
                viewPlayList.add(viewModel);
            }
        }

        return viewPlayList;

    }

    public Boolean saveOrUpdateZeroAdsRecorde(int channelId, String playListData) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            List<ManualChannelAdvertMapView> viewPlayList;
            viewPlayList = mapper.readValue(playListData, new TypeReference<List<ManualChannelAdvertMapView>>() {
            });

            List<ZeroAdsPlayList> playList = zeroAdsPlayListDao.getPlayListByChannel(channelId);
            HashMap<Integer, ZeroAdsPlayList> playListMap = new HashMap<>();

            for (ZeroAdsPlayList zeroAdsPlayList : playList) {
                playListMap.put(zeroAdsPlayList.getZeroPlayListId(), zeroAdsPlayList);
            }

            for (ManualChannelAdvertMapView manualChannelAdvertMapView : viewPlayList) {

                if (manualChannelAdvertMapView.getZeroAdsId() != 0) {
                    ZeroAdsPlayList zeroAdsPlayList = playListMap.get(manualChannelAdvertMapView.getZeroAdsId());
                    zeroAdsPlayList.setOrderNumber(manualChannelAdvertMapView.getOrderNum());
                    zeroAdsPlayListDao.updateZeroAdsPlayList(zeroAdsPlayList);

                    ZeroAdsSetting zeroAdsSetting = zeroAdsSettingService.getSelectedChannelSetting(zeroAdsPlayList.getChannelid().getChannelid());
                    if (zeroAdsSetting == null) {
                        zeroAdsSettingService.setSettinglAsManualPlayList(zeroAdsPlayList.getChannelid().getChannelid());
                    }

                    playListMap.remove(manualChannelAdvertMapView.getZeroAdsId());
                } else {
                    ZeroAdsPlayList zeroAdsPlayList = new ZeroAdsPlayList();
                    zeroAdsPlayList.setOrderNumber(manualChannelAdvertMapView.getOrderNum());
                    zeroAdsPlayList.setAdvetStatus(ZeroAdsPlayList.Status.ACTIVATE);
                    ChannelDetails channelDetails = new ChannelDetails();
                    channelDetails.setChannelid(manualChannelAdvertMapView.getChannelId());
                    zeroAdsPlayList.setChannelid(channelDetails);
                    Advertisement advertisement = new Advertisement();
                    advertisement.setAdvertid(manualChannelAdvertMapView.getAdvertId());
                    zeroAdsPlayList.setAdvertid(advertisement);
                    zeroAdsPlayListDao.saveZeroAdsPlayList(zeroAdsPlayList);

                    ZeroAdsSetting zeroAdsSetting = zeroAdsSettingService.getSelectedChannelSetting(zeroAdsPlayList.getChannelid().getChannelid());
                    if (zeroAdsSetting == null) {
                        zeroAdsSettingService.setSettinglAsManualPlayList(zeroAdsPlayList.getChannelid().getChannelid());
                    }
                }
            }

            for (Map.Entry<Integer, ZeroAdsPlayList> entry : playListMap.entrySet()) {
                ZeroAdsPlayList zeroAdsPlayList = entry.getValue();
                zeroAdsPlayList.setAdvetStatus(ZeroAdsPlayList.Status.DEACTIVATE);
                zeroAdsPlayListDao.updateZeroAdsPlayList(zeroAdsPlayList);
            }
            return true;
        } catch (Exception ex) {
            logger.debug("Exception in ChannelAdvertMapService in saveOrUpdateZeroAdsRecorde() : {}", ex.getMessage());
            return false;
        }

    }

    public void removeSuspendOrExpireAdverFromZeroAds(int advertId) {
        List<ChannelAdvertMap> removeAutoChannelAdertList = channelAdvertMapDAO.getChannelAdvertMapByAdvertId(advertId);
        List<ZeroAdsPlayList> removeManualChannelAdertList = zeroAdsPlayListDao.getPlayListByAdvert(advertId);
        for (ChannelAdvertMap channelAdvertMap : removeAutoChannelAdertList) {
            channelAdvertMap.setStatus(0);
            channelAdvertMapDAO.saveOrUpdateChannelAdvertMap(channelAdvertMap);
            clearPalyLis(channelAdvertMap.getChannelid().getChannelid());
            logger.info("removeSuspendOrExpireAdverFromZeroAds auto playlis remov Channel: {} , advert ID : {}",channelAdvertMap.getChannelid().getChannelid(),advertId);
        }

        for (ZeroAdsPlayList zeroAdsPlayList : removeManualChannelAdertList) {
            zeroAdsPlayList.setAdvetStatus(ZeroAdsPlayList.Status.DEACTIVATE);
            zeroAdsPlayListDao.updateZeroAdsPlayList(zeroAdsPlayList);
            clearPalyLis(zeroAdsPlayList.getChannelid().getChannelid());
            logger.info("removeSuspendOrExpireAdverFromZeroAds manual playlis remov Channel: {} , advert ID : {}",zeroAdsPlayList.getChannelid().getChannelid(),advertId);
        }
    }
}
