/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hibernate.internal.util.io.StreamCopier.BUFFER_SIZE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.OrderAdvertListDAO;
import com.vclabs.nash.model.dao.PlayHistoryDAO;
import com.vclabs.nash.model.dao.PlayListScheduleDAO;

import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.dao.TimeSlotScheduleMapDAO;
import com.vclabs.nash.model.dao.UserDetailsDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.entity.OrderAdvertList;
import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.model.entity.PlayListHistory;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.ExcelGenerater;
import com.vclabs.nash.model.process.OneDayScheduleInfo;
import com.vclabs.nash.model.process.TRDetails;
import com.vclabs.nash.model.process.excel.ComAvailabilityChannelWiseExcel;
import com.vclabs.nash.model.process.excel.ComAvailabilityHourlyExcel;
import com.vclabs.nash.model.process.excel.DummyCutExcel;
import com.vclabs.nash.model.process.excel.ScheduleAnalysisExcel;
import com.vclabs.nash.model.process.excel.SchedulePrintExcel;
import com.vclabs.nash.model.view.AdvertisementDefView;
import com.vclabs.nash.model.view.ReportView;
import com.vclabs.nash.model.view.WorkOrderChannelListView;
import com.vclabs.nash.model.view.WorkOrderChannelScheduleView;
import com.vclabs.nash.model.view.WorkOrderScheduleView;
import com.vclabs.nash.model.view.WorkOrderUpdateView;
import com.vclabs.nash.model.view.reporting.ComAvailabilityChannel;
import com.vclabs.nash.model.view.reporting.ComAvailabilityHourDetails;
import com.vclabs.nash.model.view.reporting.DummyCutView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class ReportingService extends com.vclabs.nash.service.DateFormat {

    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private PlayListScheduleDAO playListScheduleDao;
    @Autowired
    private PlayHistoryDAO playHistoryDao;
    @Autowired
    WorkOrderDAO workOrderDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private UserDetailsDAO userDetailsDao;
    @Autowired
    private AdvertisementDAO advertisementDao;
    @Autowired
    private OrderAdvertListDAO orderAdvertListDAO;
    @Autowired
    private TimeBeltDAO timeBeltDAO;
    @Autowired
    private TimeSlotScheduleMapDAO timeSlotScheduleMapDao;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;
    @Autowired
    private WorkOrderService workOrderService;

    @Autowired
    private DummyCutExcel dummyCutExcel;
    @Autowired
    private ComAvailabilityHourlyExcel comAvailabilityHourlyExcel;
    @Autowired
    private ComAvailabilityChannelWiseExcel comAvailabilityChannelWiseExcel;
    @Autowired
    private SchedulePrintExcel schedulePrintExcel;
    @Autowired
    private ScheduleAnalysisExcel scheduleAnalysisExcel;

    private static final Logger logger = LoggerFactory.getLogger(ReportingService.class);

    public List<ReportView> transeMissionServicereport(String channelId, String workOrderId, String from, String to) {
        List<ScheduleDef> ScheduleDefList = new ArrayList<ScheduleDef>();
        Date fromDate = null;
        Date toDate = null;
        int i_workOrderId = 0;
        int i_channelId = 0;
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            fromDate = format.parse(from);
            toDate = format.parse(to);
        } catch (Exception ex) {
            logger.debug("Exception in ReportingService in transeMissionServicereport() : {}", ex.getMessage());
        }
        try {
            i_workOrderId = Integer.parseInt(workOrderId);
        } catch (Exception ex) {
            logger.debug("Exception in ReportingService in transeMissionServicereport() : {}", ex.getMessage());
        }
        try {
            i_channelId = Integer.parseInt(channelId);
        } catch (Exception ex) {
            logger.debug("Exception in ReportingService in transeMissionServicereport() : {}", ex.getMessage());
        }
        if ((channelId.equals("-111")) && (workOrderId.equals("-111"))) {
            ScheduleDefList = schedulerDao.allChannelsAllWorkOrders(fromDate, toDate);
        } else if ((channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
            //WorkOrder workOrder = new WorkOrder();
            //workOrder.setWorkorderid(i_workOrderId);
            ScheduleDefList = schedulerDao.allChannelsNotAllWorkOrders(i_workOrderId, fromDate, toDate);
        } else if ((!channelId.equals("-111")) && (workOrderId.equals("-111"))) {
            ScheduleDefList = schedulerDao.notAllChannelsAllWorkOrders(i_channelId, fromDate, toDate);
        } else if ((!channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
            ScheduleDefList = schedulerDao.notAllChannelsNotAllWorkOrders(i_channelId, i_workOrderId, fromDate, toDate);
        }
        return getViewModel(ScheduleDefList);
    }

    public List<ReportView> transeMissionServicereport_V2(String channelId, String workOrderId, String from, String to) throws ParseException {

        List<ReportView> viewModel = new ArrayList<>();
        List<ScheduleDef> ScheduleDefList = new ArrayList<ScheduleDef>();

        try {

            SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");

            int i_channelId = Integer.parseInt(channelId);
            int i_workOrderId = Integer.parseInt(workOrderId);
            Date fromDate = null;
            Date toDate = null;
            fromDate = dtFormat.parse(from);
            toDate = dtFormat.parse(to);

            //User select today data
            //if (dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && dtFormat.format(new Date()).equals(dtFormat.format(toDate)) && i_workOrderId == -111 && i_channelId != -111) {

//                OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);
//                viewModel = getViewModelForPlayList(playListScheduleDao.getOneDaySchedules(scheduleInfo));

                //User get today and passed day data
            //} else {

                if ((channelId.equals("-111")) && (workOrderId.equals("-111"))) {
//                    ScheduleDefList = schedulerDao.allChannelsAllWorkOrders(fromDate, toDate);//For test
                    ScheduleDefList = schedulerService.allChannelsAllWorkOrders_v2(fromDate, toDate);
                } else if ((channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
                    //WorkOrder workOrder = new WorkOrder();
                    //workOrder.setWorkorderid(i_workOrderId);
                   // ScheduleDefList = schedulerDao.allChannelsNotAllWorkOrders(i_workOrderId, fromDate, toDate);
                    ScheduleDefList = schedulerService.allChannelsNotAllWorkOrders_v2(i_workOrderId, fromDate, toDate);
                } else if ((!channelId.equals("-111")) && (workOrderId.equals("-111"))) {
                    //ScheduleDefList = schedulerDao.notAllChannelsAllWorkOrders(i_channelId, fromDate, toDate);
                    ScheduleDefList = schedulerService.notAllChannelsAllWorkOrders_v2(i_channelId, fromDate, toDate);
                } else if ((!channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
//                    ScheduleDefList = schedulerDao.notAllChannelsNotAllWorkOrders(i_channelId, i_workOrderId, fromDate, toDate);
                    ScheduleDefList = schedulerService.notAllChannelsNotAllWorkOrders_v2(i_channelId, i_workOrderId, fromDate, toDate);
                }
                viewModel = getViewModel(ScheduleDefList);
                /*if (!dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && dtFormat.format(new Date()).equals(dtFormat.format(toDate))) {

                 OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);

                 while (!dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                 scheduleInfo.setDate(fromDate);
                 List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                 Collections.sort(viewModeltem);
                 viewModel.addAll(viewModeltem);
                 fromDate = addDays(fromDate, 1);
                 }
                 scheduleInfo.setDate(toDate);
                 viewModel.addAll(getViewModelForPlayList(playListScheduleDao.getOneDaySchedules(scheduleInfo)));

                 } else if (!dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && !dtFormat.format(new Date()).equals(dtFormat.format(toDate))) {

                 OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);
                 if (dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                 List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                 Collections.sort(viewModeltem);
                 viewModel.addAll(viewModeltem);
                 } else {
                 while (!dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                 scheduleInfo.setDate(fromDate);
                 List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                 Collections.sort(viewModeltem);
                 viewModel.addAll(viewModeltem);
                 fromDate = addDays(fromDate, 1);
                 }
                 }*/
            //}
            for (int i = 0; i < viewModel.size(); i++) {
                if (viewModel.get(i).getWorkOrderId() == 1 && viewModel.get(i).getClient().equals("1") && viewModel.get(i).getAdvertName().equals("None")) {
                    viewModel.remove(viewModel.get(i));
                    i--;
                }
            }
            return viewModel;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionServicereport_V2() : {}", e.getMessage());
            return viewModel;
        }
    }

    public Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public List<ReportView> getViewModel(List<ScheduleDef> dataArray) {
        List<ReportView> viewModel = new ArrayList<>();
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            for (ScheduleDef dataModel : dataArray) {
                ReportView model = new ReportView();
                model.setClient("" + dataModel.getWorkorderid().getClient());
                model.setChannelName(dataModel.getChannelid().getShortname());
                model.setWorkOrderId(dataModel.getWorkorderid().getWorkorderid());
                model.setAdvertName(dataModel.getAdvertid().getAdvertname());
                model.setDate(dtFormat.format(dataModel.getDate()));
                model.setScheduleStartTime(dataModel.getSchedulestarttime().toString());
                model.setScheduleEndTime(dataModel.getScheduleendtime().toString());
                model.setActualTime(dataModel.getActualstarttime().toString());
                model.setStatus(dataModel.getStatusbyName());

                viewModel.add(model);
            }
            return viewModel;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in getViewModel() : {}", e.getMessage());
            return viewModel;
        }
    }

    public List<ReportView> getViewModelForPlayList(List<PlayList> dataArray) {
        List<ReportView> viewModel = new ArrayList<>();
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Collections.sort(dataArray, new Comparator<PlayList>() {
            @Override
            public int compare(PlayList o1, PlayList o2) {
                return o1.getScheduleHour() - o2.getScheduleHour();
            }
        });
        try {
            for (PlayList dataModel : dataArray) {
                ReportView model = new ReportView();
                model.setClient("" + dataModel.getWorkOrder().getClient());
                model.setChannelName(dataModel.getChannel().getShortname());
                model.setWorkOrderId(dataModel.getWorkOrder().getWorkorderid());
                model.setAdvertName(dataModel.getAdvert().getAdvertname());
                model.setDate(dtFormat.format(dataModel.getDate()));
                model.setScheduleStartTime(dataModel.getSchedule().getSchedulestarttime().toString());
                model.setScheduleEndTime(dataModel.getSchedule().getScheduleendtime().toString());
                model.setActualTime(timeFormat.format(dataModel.getActualStartTime()));
                model.setStatus(dataModel.getStatusbyName());

                viewModel.add(model);
            }
            return viewModel;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in getViewModelForPlayList() : {}", e.getMessage());
            return viewModel;
        }
    }

    public List<ReportView> getViewModelForPlayListHistory(List<PlayListHistory> dataArray) {
        List<ReportView> viewModel = new ArrayList<>();
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            for (PlayListHistory dataModel : dataArray) {
                ReportView model = new ReportView();
                model.setClient("" + dataModel.getScheduleid().getWorkorderid().getClient());
                model.setChannelName(dataModel.getScheduleid().getChannelid().getShortname());
                model.setWorkOrderId(dataModel.getScheduleid().getWorkorderid().getWorkorderid());
                model.setAdvertName(dataModel.getScheduleid().getAdvertid().getAdvertname());
                model.setDate(dtFormat.format(dataModel.getDate()));
                model.setScheduleStartTime(dataModel.getScheduleid().getSchedulestarttime().toString());
                model.setScheduleEndTime(dataModel.getScheduleid().getScheduleendtime().toString());
                model.setActualTime(dataModel.getScheduleid().getActualstarttime().toString());
                model.setStatus(dataModel.getStatusbyName());

                viewModel.add(model);
            }
            return viewModel;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in getViewModelForPlayListHistory() : {}", e.getMessage());
            return viewModel;
        }
    }

    public Boolean transeMissionReportPrintService(HttpServletRequest request, String channelId, String workOrderId, String from, String to) throws Exception {
        List<ScheduleDef> ScheduleDefList = new ArrayList<ScheduleDef>();
        Date fromDate = null;
        Date toDate = null;
        int i_workOrderId = 0;
        int i_channelId = 0;
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            fromDate = format.parse(from);
            toDate = format.parse(to);
            i_workOrderId = Integer.parseInt(workOrderId);
            i_channelId = Integer.parseInt(channelId);
        } catch (ParseException e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintService() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintService() : {}", e.getMessage());
            throw e;
        }

        try {
            TRDetails model = new TRDetails();
            if ((channelId.equals("-111")) && (workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.allChannelsAllWorkOrders_v2(fromDate, toDate);
            } else if ((channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
                //WorkOrder workOrder = new WorkOrder();
                //workOrder.setWorkorderid(i_workOrderId);
                WorkOrder woModel = workOrderDao.getSelectedWorkOrder(i_workOrderId);
                ClientDetails endClient = clientDetailDao.getSelectedClient(woModel.getClient());

                model.setMarketingEx(userDetailsDao.getUserDetails(woModel.getSeller()).getName());
                model.setRegNO("" + woModel.getWorkorderid());
                model.setProduct(woModel.getOrdername());
                model.setScheduleRef(woModel.getScheduleRef());
                if (woModel.getAgencyclient() != 0) {
                    ClientDetails asgencyClient = clientDetailDao.getSelectedClient(woModel.getAgencyclient());
                    model.setAgency(asgencyClient.getClientname());
                }
                model.setAdvertiser(endClient.getClientname());
                ScheduleDefList = schedulerService.allChannelsNotAllWorkOrders_v2(i_workOrderId, fromDate, toDate);
            } else if ((!channelId.equals("-111")) && (workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.notAllChannelsAllWorkOrders_v2(i_channelId, fromDate, toDate);
            } else if ((!channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.notAllChannelsNotAllWorkOrders_v2(i_channelId, i_workOrderId, fromDate, toDate);
            }

            return new PDFGenerator().transmissionReportPDFGenerator_V2(ScheduleDefList, model, request);

        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintService() : {}", e.getMessage());
            throw e;
        }

    }

    public Boolean transeMissionReportPrintServiceExcel(HttpServletRequest request, String channelId, String workOrderId, String from, String to) throws Exception {
        List<ScheduleDef> ScheduleDefList = new ArrayList<ScheduleDef>();
        Date fromDate = null;
        Date toDate = null;
        int i_workOrderId = 0;
        int i_channelId = 0;
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            fromDate = format.parse(from);
            toDate = format.parse(to);
            i_workOrderId = Integer.parseInt(workOrderId);
            i_channelId = Integer.parseInt(channelId);
        } catch (ParseException e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintServiceExcel() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintServiceExcel() : {}", e.getMessage());
            throw e;
        }

        try {
            TRDetails model = new TRDetails();
            if ((channelId.equals("-111")) && (workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.allChannelsAllWorkOrders_v2(fromDate, toDate);
            } else if ((channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
                //WorkOrder workOrder = new WorkOrder();
                //workOrder.setWorkorderid(i_workOrderId);
                WorkOrder woModel = workOrderDao.getSelectedWorkOrder(i_workOrderId);
                ClientDetails endClient = clientDetailDao.getSelectedClient(woModel.getClient());

                model.setMarketingEx(userDetailsDao.getUserDetails(woModel.getSeller()).getName());
                model.setRegNO("" + woModel.getWorkorderid());
                if (woModel.getAgencyclient() != 0) {
                    ClientDetails asgencyClient = clientDetailDao.getSelectedClient(woModel.getAgencyclient());
                    model.setAgency(asgencyClient.getClientname());
                }
                model.setAdvertiser(endClient.getClientname());
                model.setProduct(woModel.getOrdername());
                model.setScheduleRef(woModel.getScheduleRef());
                ScheduleDefList = schedulerService.allChannelsNotAllWorkOrders_v2(i_workOrderId, fromDate, toDate);
            } else if ((!channelId.equals("-111")) && (workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.notAllChannelsAllWorkOrders_v2(i_channelId, fromDate, toDate);
            } else if ((!channelId.equals("-111")) && (!workOrderId.equals("-111"))) {
                ScheduleDefList = schedulerService.notAllChannelsNotAllWorkOrders_v2(i_channelId, i_workOrderId, fromDate, toDate);
            }

            return new ExcelGenerater().generateTR(ScheduleDefList, model, request);

        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintServiceExcel() : {}", e.getMessage());
            throw e;
        }

    }

    public Boolean transeMissionReportPrintService_V2(HttpServletRequest request, String channelId, String workOrderId, String from, String to) {
        List<ReportView> viewModel = new ArrayList<>();
        List<ScheduleDef> ScheduleDefList = new ArrayList<ScheduleDef>();

        try {

            SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");

            int i_channelId = Integer.parseInt(channelId);
            int i_workOrderId = Integer.parseInt(workOrderId);
            Date fromDate = null;
            Date toDate = null;
            fromDate = dtFormat.parse(from);
            toDate = dtFormat.parse(to);

            //User select today data
            if (dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && dtFormat.format(new Date()).equals(dtFormat.format(toDate))) {

                OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);
                viewModel = getViewModelForPlayList(playListScheduleDao.getOneDaySchedules(scheduleInfo));

                //User get today and passed day data
            } else if (!dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && dtFormat.format(new Date()).equals(dtFormat.format(toDate))) {

                OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);

                while (!dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                    scheduleInfo.setDate(fromDate);
                    List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                    Collections.sort(viewModeltem);
                    viewModel.addAll(viewModeltem);
                    fromDate = addDays(fromDate, 1);
                }
                scheduleInfo.setDate(toDate);
                viewModel.addAll(getViewModelForPlayList(playListScheduleDao.getOneDaySchedules(scheduleInfo)));

            } else if (!dtFormat.format(new Date()).equals(dtFormat.format(fromDate)) && !dtFormat.format(new Date()).equals(dtFormat.format(toDate))) {

                OneDayScheduleInfo scheduleInfo = new OneDayScheduleInfo(i_workOrderId, i_channelId, -111, "-111", toDate);
                if (dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                    List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                    Collections.sort(viewModeltem);
                    viewModel.addAll(viewModeltem);
                } else {
                    while (!dtFormat.format(toDate).equals(dtFormat.format(fromDate))) {
                        scheduleInfo.setDate(fromDate);
                        List<ReportView> viewModeltem = getViewModelForPlayListHistory(playHistoryDao.getOneSchedule(scheduleInfo));
                        Collections.sort(viewModeltem);
                        viewModel.addAll(viewModeltem);
                        fromDate = addDays(fromDate, 1);
                    }
                }
            }
            for (int i = 0; i < viewModel.size(); i++) {
                if (viewModel.get(i).getWorkOrderId() == 1 && viewModel.get(i).getClient().equals("1") && viewModel.get(i).getAdvertName().equals("None")) {
                    viewModel.remove(viewModel.get(i));
                    i--;
                }
            }

            return new PDFGenerator().transmissionReportPDFGenerator(ScheduleDefList, request);
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in transeMissionReportPrintService_V2() : {}", e.getMessage());
            return false;
        }
    }

    public void downloadPDF(HttpServletRequest request, HttpServletResponse response) {
        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String fullPath = webServerPath + "/PDF/TransmissionReport.pdf";

        try {
            //construct the complete absolute path of the file
            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            // get MIME type of the file
	    /*String mimeType = context.getMimeType(fullPath);//http://www.aibn.com/help/Learn/mimetypes.html,http://www.freeformatter.com/mime-types-list.html
             if (mimeType == null)
             {*/
            // set to binary type if MIME mapping not found
            String mimeType = "application/octet-stream";
            /*}*/

            // set content attributes for the response
            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            // set headers for the response
            String headerKey = "Content-Disposition";//https://support.microsoft.com/en-us/kb/260519
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());//http://www.freeformatter.com/mime-types-list.html
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();
        } catch (IOException e) {
            logger.debug("Exception in ReportingService in downloadPDF() : {}", e.getMessage());

        } catch (Exception e) {
            logger.debug("Exception in ReportingService in downloadPDF() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadExcel(HttpServletRequest request, HttpServletResponse response) {
        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String fullPath = webServerPath + "/PDF/TransmissionReport.xls";

        try {
            //construct the complete absolute path of the file
            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            // get MIME type of the file
	    /*String mimeType = context.getMimeType(fullPath);//http://www.aibn.com/help/Learn/mimetypes.html,http://www.freeformatter.com/mime-types-list.html
             if (mimeType == null)
             {*/
            // set to binary type if MIME mapping not found
            String mimeType = "application/octet-stream";
            /*}*/

            // set content attributes for the response
            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            // set headers for the response
            String headerKey = "Content-Disposition";//https://support.microsoft.com/en-us/kb/260519
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());//http://www.freeformatter.com/mime-types-list.html
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();
        } catch (IOException e) {
            logger.debug("Exception in ReportingService in downloadExcel() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in downloadExcel() : {}", e.getMessage());
            throw e;
        }
    }

    /*--------------------------------DummyCut Report-----------------------------------------*/
    public List<DummyCutView> getDummyCutReport() {

        List<DummyCutView> dummyCutViewList = new ArrayList<>();
        List<Advertisement> dummyCutList = advertisementDao.getDummyCuts();

        for (Advertisement dummyCut : dummyCutList) {
            List<OrderAdvertList> workOrderList = orderAdvertListDAO.getWorkOrderIdList(dummyCut.getAdvertid());
            if (!workOrderList.isEmpty()) {
                for (OrderAdvertList advertAndWorkOrder : workOrderList) {
                    dummyCutViewList.add(setAdvertDetailForDummyCut(advertAndWorkOrder));
                    break;
                }
            } else {
                dummyCutViewList.add(setAdvertDetailForDummyCut(dummyCut));
            }
        }
        Collections.sort(dummyCutViewList);
        return dummyCutViewList;
    }

    public DummyCutView setAdvertDetailForDummyCut(Advertisement advertisement) {

        DummyCutView dummyCutView = new DummyCutView();
        dummyCutView.setCreateDate(advertisement.getCreateDate());
        dummyCutView.setCutID(advertisement.getAdvertid());
        dummyCutView.setDuration(advertisement.getDuration());
        dummyCutView.setLanguage(advertisement.getLanguage().equals("-1") ? "" : advertisement.getLanguage());

        return dummyCutView;
    }

    public DummyCutView setAdvertDetailForDummyCut(OrderAdvertList advertWorkOrder) {

        DummyCutView dummyCutView = new DummyCutView();
        dummyCutView.setAiringEndtDate(advertWorkOrder.getWorkorderid().getEnddate());
        dummyCutView.setAiringStartDate(advertWorkOrder.getWorkorderid().getStartdate());
        dummyCutView.setMe(advertWorkOrder.getWorkorderid().getSeller());
        dummyCutView.setProduct(advertWorkOrder.getWorkorderid().getOrdername());
        dummyCutView.setWoId(advertWorkOrder.getWorkorderid().getWorkorderid());

        dummyCutView.setClientName(advertWorkOrder.getAdvertid().getClient().getClientname());
        dummyCutView.setCreateDate(advertWorkOrder.getAdvertid().getCreateDate());
        dummyCutView.setCutID(advertWorkOrder.getAdvertid().getAdvertid());
        dummyCutView.setDuration(advertWorkOrder.getAdvertid().getDuration());
        dummyCutView.setLanguage(advertWorkOrder.getAdvertid().getLanguage().equals("-1") ? "" : advertWorkOrder.getAdvertid().getLanguage());

        dummyCutView.setNumberOfSpot(schedulerDao.getSelectedAdvertScheduleCount(advertWorkOrder.getWorkorderid().getWorkorderid(), advertWorkOrder.getAdvertid().getAdvertid()));
        dummyCutView.setDateRang();

        return dummyCutView;
    }

    public Boolean generateDummyCutExcel(HttpServletRequest request) throws Exception {
        try {
            dummyCutExcel.generateDummyCutExcel(request, getDummyCutReport());
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in generateDummyCutExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadDummyCutExcell(HttpServletResponse response, HttpServletRequest request) {
        dummyCutExcel.downloadDummyCutExcell(response, request);
    }

    /*--------------------------------Commercial availability Hourly Report-----------------------------------------*/
    public Map<Integer, ComAvailabilityChannel> getComAvailabilityHourly(String from, String to) {
        //from = "2017-12-08";
        //to = "2017-12-08";
        int channelID = -99;
        List<ScheduleDef> spotList = schedulerDao.getCommercialAvailability(channelID, getDataYYYMMDD_dash(from), getDataYYYMMDD_dash(to));
        Map<Integer, ComAvailabilityChannel> channelMap = new TreeMap<Integer, ComAvailabilityChannel>();
        HashMap<Integer, String> timeMap = new HashMap<Integer, String>();

        int channelId = -99;
        String StartTime = "";
        int hourA = -1;
        int hourB = -1;
        for (ScheduleDef scheduleDef : spotList) {

            if (scheduleDef.getActualstarttime().toString().equals("00:00:00")) {
                TimeSlotScheduleMap model = timeSlotScheduleMapDao.getSelectedTimeSlot(scheduleDef.getScheduleid());
                if (model != null) {
                    scheduleDef.setActualstarttime(model.getTimeBelt().getStartTime());
                }
            }

            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(scheduleDef.getActualstarttime());   // assigns calendar to given date
            hourB = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

            String timeData = timeMap.get(hourB);
            if (timeData == null) {
                timeMap.put(hourB, hourB + "");
            }

            if (channelId != scheduleDef.getChannelid().getChannelid()) {
                channelId = scheduleDef.getChannelid().getChannelid();
                ComAvailabilityChannel comAvailabilityChannel = new ComAvailabilityChannel();
                comAvailabilityChannel.setChannelId(channelId);
                comAvailabilityChannel.setChannelName(scheduleDef.getChannelid().getChannelname());

                hourA = hourB;
                comAvailabilityChannel.getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), channelId, true));

                channelMap.put(channelId, comAvailabilityChannel);
            } else {
                if (hourA == hourB) {
                    ComAvailabilityChannel comAvailabilityChannel = channelMap.get(channelId);

                    ComAvailabilityHourDetails comAvailabilityHourDetails = comAvailabilityChannel.getHourDetailsMap().get(hourA);
                    comAvailabilityHourDetails.addAdvertCount();
                    comAvailabilityHourDetails.addAdvertDuration(scheduleDef.getAdvertid().getDuration());
                } else {
                    hourA = hourB;
                    channelMap.get(channelId).getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), channelId, true));
                }
            }
        }
        fillEmptyTimeSlot(timeMap, channelMap);
        return channelMap;
    }

    public void fillEmptyTimeSlot(HashMap<Integer, String> timeMap, Map<Integer, ComAvailabilityChannel> channelMap) {
        for (Entry<Integer, ComAvailabilityChannel> ComAvailabilityChannel : channelMap.entrySet()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = ComAvailabilityChannel.getValue().getHourDetailsMap();
            for (Entry<Integer, String> entry : timeMap.entrySet()) {
                ComAvailabilityHourDetails comAvailabilityHourDetails = hourDetailsMap.get(entry.getKey());
                if (comAvailabilityHourDetails == null) {
                    hourDetailsMap.put(entry.getKey(), getComAvailabilityHourDetails(entry.getKey(), 0, ComAvailabilityChannel.getValue().getChannelId(), false));
                } else {
                    comAvailabilityHourDetails.calulateTimeUtilization();
                    comAvailabilityHourDetails.calulateActualUtilization();
                }
            }
        }
    }

    public ComAvailabilityHourDetails getComAvailabilityHourDetails(int hour, int duration, int channelId, Boolean addRealData) {

        ComAvailabilityHourDetails comAvailabilityHourDetails = new ComAvailabilityHourDetails();
        comAvailabilityHourDetails.setHour(hour);
        comAvailabilityHourDetails.setTime(hour + ":00");
        if (addRealData) {
            comAvailabilityHourDetails.addAdvertCount();

            TimeBelts timeBelts = timeBeltDAO.getTimeBelt(channelId, hour, 0);
            comAvailabilityHourDetails.setTotalScheduleTime(timeBelts.getTotalScheduleTime());
        }
        comAvailabilityHourDetails.addAdvertDuration(duration);

        return comAvailabilityHourDetails;
    }

    public Boolean generateComAvailabilityHourlyExcel(HttpServletRequest request, String date, int value) throws Exception {
        try {
            String from = date;
            String to = date;
            comAvailabilityHourlyExcel.generateComAvailabilityHourlyExcel(value, request, getComAvailabilityHourly(from, to));
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in generateComAvailabilityHourlyExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadComAvailabilityHourlyExcell(HttpServletResponse response, HttpServletRequest request) {
        comAvailabilityHourlyExcel.downloadComAvailabilityHourlyExcell(response, request);
    }
    /*--------------------------------Commercial availability ChannelWise Report-----------------------------------------*/

    public Map<String, ComAvailabilityChannel> getComAvailabilityChannelWise(String startDate, String endData, int channelId) {
        List<ScheduleDef> spotList = schedulerDao.getCommercialAvailability(channelId, getDataYYYMMDD_dash(startDate), getDataYYYMMDD_dash(endData));
        Map<String, ComAvailabilityChannel> channelMap = new TreeMap<>();

        HashMap<Integer, String> timeMap = new HashMap<Integer, String>();

        String StartTime = "";
        String StartDate = "";
        int hourA = -1;
        int hourB = -1;
        for (ScheduleDef scheduleDef : spotList) {

            if (scheduleDef.getActualstarttime().toString().equals("00:00:00")) {
                TimeSlotScheduleMap model = timeSlotScheduleMapDao.getSelectedTimeSlot(scheduleDef.getScheduleid());
                if (model != null) {
                    scheduleDef.setActualstarttime(model.getTimeBelt().getStartTime());
                }
            }
            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(scheduleDef.getActualstarttime());   // assigns calendar to given date
            hourB = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

            String timeData = timeMap.get(hourB);
            if (timeData == null) {
                timeMap.put(hourB, hourB + "");
            }

            if (!StartDate.equals(getDataYYYMMDD_dash(scheduleDef.getDate()))) {
                StartDate = getDataYYYMMDD_dash(scheduleDef.getDate());
                ComAvailabilityChannel comAvailabilityChannel = new ComAvailabilityChannel();
                comAvailabilityChannel.setChannelId(channelId);
                comAvailabilityChannel.setChannelName(scheduleDef.getChannelid().getChannelname());
                comAvailabilityChannel.setDate(scheduleDef.getDate());

                hourA = hourB;
                comAvailabilityChannel.getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), channelId, true));

                channelMap.put(StartDate, comAvailabilityChannel);
            } else {
                if (hourA == hourB) {
                    ComAvailabilityChannel comAvailabilityChannel = channelMap.get(StartDate);

                    ComAvailabilityHourDetails comAvailabilityHourDetails = comAvailabilityChannel.getHourDetailsMap().get(hourA);
                    comAvailabilityHourDetails.addAdvertCount();
                    comAvailabilityHourDetails.addAdvertDuration(scheduleDef.getAdvertid().getDuration());
                } else {
                    hourA = hourB;
                    channelMap.get(StartDate).getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), channelId, true));
                }
            }
        }
        fillEmptyTimeSlotForChannelWise(timeMap, channelMap);

        return channelMap;
    }

    public void fillEmptyTimeSlotForChannelWise(HashMap<Integer, String> timeMap, Map<String, ComAvailabilityChannel> channelMap) {
        for (Entry<String, ComAvailabilityChannel> ComAvailabilityChannel : channelMap.entrySet()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = ComAvailabilityChannel.getValue().getHourDetailsMap();
            for (Entry<Integer, String> entry : timeMap.entrySet()) {
                ComAvailabilityHourDetails comAvailabilityHourDetails = hourDetailsMap.get(entry.getKey());
                if (comAvailabilityHourDetails == null) {
                    hourDetailsMap.put(entry.getKey(), getComAvailabilityHourDetails(entry.getKey(), 0, ComAvailabilityChannel.getValue().getChannelId(), false));
                } else {
                    comAvailabilityHourDetails.calulateTimeUtilization();
                    comAvailabilityHourDetails.calulateActualUtilization();
                }
            }
        }
    }

    public Boolean generateComAvailabilityChannelWiseExcel(HttpServletRequest request, String startDate, String endDate, int value, int channelId) throws Exception {
        try {
            comAvailabilityChannelWiseExcel.generateComAvailabilityChannelWiseExcel(value, request, getComAvailabilityChannelWise(startDate, endDate, channelId));
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in generateComAvailabilityChannelWiseExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadComAvailabilityChannelWiseExcell(HttpServletResponse response, HttpServletRequest request) {
        comAvailabilityChannelWiseExcel.downloadComAvailabilityChannelWiseExcell(response, request);
    }

    /*--------------------------------Schedule Print Report-----------------------------------------*/
    public Boolean generateScheduleExcel(HttpServletRequest request, int workOrderId) throws Exception {
        try {

            /*------------------------------------------Schedule Spot List--------------------*/
            WorkOrderScheduleView workOrderScheduleView = schedulerService.getWOScheduleViewData(workOrderId, -999, Boolean.FALSE);

            /*------------------------------------------WorkOrder's Advertisement List--------------------*/
            List<AdvertisementDefView> advertisementDefViewList = schedulerService.getSelectedAdvert(workOrderId, "All");

            /*------------------------------------------WorkOrder's Channel List--------------------*/
            List<WorkOrderChannelScheduleView> channelSpotList = new ArrayList<>();
            List<WorkOrderChannelListView> WorkOrderChannelListView = workOrderService.getSelectedOrderChannelList(workOrderId);

            for (WorkOrderChannelListView workOrderChannelView : WorkOrderChannelListView) {

                WorkOrderChannelScheduleView workOrderChannelScheduleView = schedulerService.getScheduleAvailableSpotCount(workOrderId, workOrderChannelView.getChannelid());
                workOrderChannelScheduleView.setChannelId(workOrderChannelView.getChannelid());
                workOrderChannelScheduleView.setChannelName(workOrderChannelView.getChannelname());

                channelSpotList.add(workOrderChannelScheduleView);

            }

            /*------------------------------------------WorkOrder Details--------------------*/
            List<WorkOrderUpdateView> workOrderUpdateView = workOrderService.getSelectedWorkOrder(workOrderId);

            schedulePrintExcel.generateScheduleExcel(request, workOrderScheduleView, advertisementDefViewList, channelSpotList, workOrderUpdateView.get(0));
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in generateScheduleExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadSchedulePrintExcell(HttpServletResponse response, HttpServletRequest request) {
        schedulePrintExcel.downloadSchedulePrintExcel(response, request);
    }

    /*--------------------------------Schedule Analysis Report-----------------------------------------*/
    public Map<String, Map<String, ComAvailabilityChannel>> getScheduleAnalysis(int workOrderId, List<Integer> workOrderIDs, List<Integer> agnyIDS, int clientId) {

        List<ScheduleDef> spotList = schedulerDao.getScheduleAnalysisData(workOrderIDs, agnyIDS, clientId);
        Map<String, Map<String, ComAvailabilityChannel>> channelMap = new TreeMap<>();
        Map<String, ComAvailabilityChannel> spotMap = new TreeMap<>();

        HashMap<Integer, String> timeMap = new HashMap<Integer, String>();

        String StartTime = "";
        String StartDate = "";
        int hourA = -1;
        int hourB = -1;
        int channelId = -1;
        String channelName = "";
        for (ScheduleDef scheduleDef : spotList) {

            if (channelId == scheduleDef.getChannelid().getChannelid() || channelId == -1) {
                if (channelId == -1) {
                    channelId = scheduleDef.getChannelid().getChannelid();
                    channelName = scheduleDef.getChannelid().getChannelname();

                }

                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                calendar.setTime(scheduleDef.getActualstarttime());   // assigns calendar to given date
                hourB = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

                String timeData = timeMap.get(hourB);
                if (timeData == null) {
                    timeMap.put(hourB, hourB + "");
                }

                if (!StartDate.equals(getDataYYYMMDD_dash(scheduleDef.getDate()))) {
                    StartDate = getDataYYYMMDD_dash(scheduleDef.getDate());
                    ComAvailabilityChannel comAvailabilityChannel = new ComAvailabilityChannel();
                    comAvailabilityChannel.setChannelId(scheduleDef.getChannelid().getChannelid());
                    comAvailabilityChannel.setChannelName(scheduleDef.getChannelid().getChannelname());
                    comAvailabilityChannel.setDate(scheduleDef.getDate());

                    hourA = hourB;
                    comAvailabilityChannel.getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), scheduleDef.getChannelid().getChannelid(), true));

                    spotMap.put(StartDate, comAvailabilityChannel);
                } else {
                    if (hourA == hourB) {
                        ComAvailabilityChannel comAvailabilityChannel = spotMap.get(StartDate);

                        ComAvailabilityHourDetails comAvailabilityHourDetails = comAvailabilityChannel.getHourDetailsMap().get(hourA);
                        comAvailabilityHourDetails.addAdvertCount();
                        comAvailabilityHourDetails.addAdvertDuration(scheduleDef.getAdvertid().getDuration());
                    } else {
                        hourA = hourB;
                        spotMap.get(StartDate).getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), scheduleDef.getChannelid().getChannelid(), true));
                    }
                }

                // fillEmptyTimeSlotForChannelWise(timeMap, spotMap);
            } else {
                fillEmptyTimeSlotForChannelWise(timeMap, spotMap);
                channelMap.put(channelName, spotMap);
                channelId = scheduleDef.getChannelid().getChannelid();
                channelName = scheduleDef.getChannelid().getChannelname();

                spotMap = new TreeMap<>();

                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                calendar.setTime(scheduleDef.getActualstarttime());   // assigns calendar to given date
                hourB = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

                String timeData = timeMap.get(hourB);
                if (timeData == null) {
                    timeMap.put(hourB, hourB + "");
                }

                StartDate = getDataYYYMMDD_dash(scheduleDef.getDate());
                ComAvailabilityChannel comAvailabilityChannel = new ComAvailabilityChannel();
                comAvailabilityChannel.setChannelId(scheduleDef.getChannelid().getChannelid());
                comAvailabilityChannel.setChannelName(scheduleDef.getChannelid().getChannelname());
                comAvailabilityChannel.setDate(scheduleDef.getDate());

                hourA = hourB;
                comAvailabilityChannel.getHourDetailsMap().put(hourA, getComAvailabilityHourDetails(hourA, scheduleDef.getAdvertid().getDuration(), scheduleDef.getChannelid().getChannelid(), true));

                spotMap.put(StartDate, comAvailabilityChannel);

            }
        }
        fillEmptyTimeSlotForChannelWise(timeMap, spotMap);
        channelMap.put(channelName, spotMap);

        return channelMap;
    }

    public Boolean generateScheduleAnalusisExcel(HttpServletRequest request, int workOrderID, List<Integer> workOrderIDs, List<Integer> agencyIDS, int clientId, int value) throws Exception {
        try {
            scheduleAnalysisExcel.generateScheduleAnalysisExcel(value, request, this.getScheduleAnalysis(workOrderID, workOrderIDs, agencyIDS, clientId));
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ReportingService in generateScheduleAnalusisExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadScheduleAnalusisExcel(HttpServletResponse response, HttpServletRequest request) {
        scheduleAnalysisExcel.downloadScheduleAnalysisExcell(response, request);
    }

    /*--------------------------------Client Revenue Report-----------------------------------------*/
}
