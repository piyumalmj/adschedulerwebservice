/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.PriorityDefDAO;
import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.entity.PriorityDef;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.process.ClusterInfo;

/**
 *
 * @author Sanira Nanayakkara
 */
@Service
@Transactional("main")
public class TimeBeltService {

    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private PriorityDefDAO priorityDao;
    @Autowired
    private WorkOrderChannelListDAO workOrderChannelDao;

    public List<TimeBelts> getTimeBeltListbyChannel(int channelID) {
        return timeBeltDao.getTimeBelts(channelID);
    }

    public Boolean updateClusterCount(List<ClusterInfo> dataList) {

        for (ClusterInfo item : dataList) {
            TimeBelts timeBelt = timeBeltDao.getTimeBeltbyID(item.getTimebeltId());

            if (timeBelt != null && (timeBelt.getClusterCount() != item.getClusterCount() || timeBelt.getTotalScheduleTime() != item.getMaxScheduleTime())) {
                timeBelt.setClusterCount(item.getClusterCount());
                timeBelt.setTotalScheduleTime(item.getMaxScheduleTime());

                if (!timeBeltDao.updateTimeBelt(timeBelt)) {
                    return false;
                }
                List<PriorityDef> lstPriority = priorityDao.getPriorityListbyTimeBelt(item.getTimebeltId());
                for (int i = 0; i < timeBelt.getClusterCount(); i++) {
                    boolean bFound = false;

                    for (int j = lstPriority.size() - 1; j >= 0; j--) {
                        PriorityDef priorityItem = lstPriority.get(j);
                        if (priorityItem.getCluster() == i) {
                            lstPriority.remove(priorityItem);
                            bFound = true;
                        }
                    }
                    if (!bFound) {
                        PriorityDef pPriorityDef = new PriorityDef();
                        pPriorityDef.setCluster(i);
                        pPriorityDef.setPriority(1);
                        pPriorityDef.setTimebeltId(timeBelt);
                        priorityDao.savePriorityDef(pPriorityDef);
                    }
                }
                //remove extra priorities - when cluster count decreased
                for (PriorityDef priority_item : lstPriority) {
                    priorityDao.removePriorityDef(priority_item);
                }
            }
        }
        return true;
    }

    public List<TimeBelts> timeBeltListbyWorkOrder(int workOrderID, Boolean sortList) { //tested

        List<TimeBelts> lstAllChannels = new ArrayList<>();
        List<WorkOrderChannelList> lstWoChannel = workOrderChannelDao.getSelectedWorkOrderSpotCount(workOrderID);
        for (WorkOrderChannelList wochList : lstWoChannel) {
            List<TimeBelts> lstTimeBelts = timeBeltDao.getTimeBelts(wochList.getChannelid().getChannelid());
            lstAllChannels.addAll(lstTimeBelts);
        }
        if (sortList) {
            Collections.sort(lstAllChannels);
        }
        return lstAllChannels;
    }
}
