/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vclabs.nash.dashboard.service.PriorityAdvertisementWidgetService;
import org.slf4j.LoggerFactory;
import com.vclabs.nash.model.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.PriorityDefDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.PriorityDef;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;

/**
 *
 * @author Sanira Nanayakkara
 */
@Service
@Transactional(value = "main", propagation = Propagation.REQUIRED)
public class PriorityService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PriorityService.class);

    @Autowired
    private PriorityDefDAO priorityDao;
    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private SchedulerDAO scheduleDao;
    @Autowired
    private PriorityAdvertisementWidgetService priorityAdvertisementWidgetService;

    public List<PriorityUpdateView> getPriorityListbyChannel(int id) {
        List<PriorityDef> priorityList = priorityDao.getPrioritybyChannel(id);
        List<PriorityUpdateView> returnList = new ArrayList<PriorityUpdateView>();
        HashMap<PriorityUpdateView, Integer> mapPriorityCount = new HashMap<>();

        for (PriorityDef item : priorityList) {
            PriorityUpdateView pUpdateView = new PriorityUpdateView();
            pUpdateView.setCluster(item.getCluster());
            pUpdateView.setStartTime(item.getTimebeltId().getStartTime());
            pUpdateView.setEndTime(item.getTimebeltId().getEndTime());
            pUpdateView.setTimebeltId(item.getTimebeltId().getTimeBeltId());
            pUpdateView.setPriorityCount(0);

            if (!mapPriorityCount.containsKey(pUpdateView)) {
                mapPriorityCount.put(pUpdateView, 1);
            } else {
                mapPriorityCount.put(pUpdateView, mapPriorityCount.get(pUpdateView) + 1);
            }
        }

        List<TimeBelts> timebeltList = timeBeltDao.getTimeBelts(id);
        for (TimeBelts item : timebeltList) {
            for (int i = 0; i < item.getClusterCount(); i++) {
                PriorityUpdateView pUpdateView = new PriorityUpdateView();
                pUpdateView.setCluster(i);
                pUpdateView.setStartTime(item.getStartTime());
                pUpdateView.setEndTime(item.getEndTime());
                pUpdateView.setTimebeltId(item.getTimeBeltId());
                pUpdateView.setPriorityCount(0);

                if (!mapPriorityCount.containsKey(pUpdateView)) {
                    mapPriorityCount.put(pUpdateView, 0);
                }
            }
        }

        for (Map.Entry<PriorityUpdateView, Integer> mapEntry : mapPriorityCount.entrySet()) {
            PriorityUpdateView item = mapEntry.getKey();
            item.setPriorityCount(mapEntry.getValue());

            long iItemID = item.getTimebeltId();
            iItemID = iItemID << 32 | item.getCluster();
            item.setPriorityId(iItemID);

            returnList.add(item);
        }
        Collections.sort(returnList);
        return returnList;
    }

    public boolean savePriorityDef(PriorityDef model) {
        return priorityDao.savePriorityDef(model);
    }

    public boolean updatePriorityDef(PriorityDef model) {
        return priorityDao.updatePriorityDef(model);
    }

    public boolean savePriority(PriorityDef model) {
        return priorityDao.savePriorityDef(model);
    }

    public boolean updatePriority(PriorityDef model) {
        return priorityDao.savePriorityDef(model);
    }

    public boolean updatePriorityCount(List<Pair<Long, Integer>> dataList) {
        for (Pair<Long, Integer> item : dataList) {
            long lPriorityID = item.getFirst();
            int iTimbeltID = (int) (lPriorityID >> 32);
            int iCluster = (int) lPriorityID;

            List<PriorityDef> lstPrioritys = priorityDao.getPriorityListbyCluster(iTimbeltID, iCluster);

            for (int i = 1; i <= item.getSecond(); i++) {
                boolean bFound = false;

                for (int j = lstPrioritys.size() - 1; j >= 0; j--) {
                    PriorityDef priority_item = lstPrioritys.get(j);
                    if (priority_item.getPriority() == i) {
                        lstPrioritys.remove(priority_item);
                        bFound = true;
                        break;
                    }
                }
                if (!bFound) {
                    PriorityDef pPriorityDef = new PriorityDef();
                    pPriorityDef.setCluster(iCluster);
                    pPriorityDef.setPriority(i);
                    pPriorityDef.setTimebeltId(timeBeltDao.getTimeBeltbyID(iTimbeltID));
                    priorityDao.savePriorityDef(pPriorityDef);
                }
            }
            //remove extra priorities - when cluster count decreased
            for (PriorityDef priority_item : lstPrioritys) {
                priorityDao.removePriorityDef(priority_item);
            }

        }
        return true;
    }

//    public Boolean InsertPriority(PriorityInsertView pPriorityInsert) {
//        if (pPriorityInsert.getiTimebeltID() == -111 || pPriorityInsert.getiCluster() == -111) {
//            return false;
//        }
//
//        TimeBelts tBelt = timeBeltDao.getTimeBeltbyID(pPriorityInsert.getiTimebeltID());
//        Date dtStart = pPriorityInsert.getDtStratDate();
//        try {
//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//            DateFormat timeFormat = new SimpleDateFormat("1970-01-01 HH:mm:ss");
//
//            Date today = (Date) dateFormat.parse(dateFormat.format(new Date()));
//            Date timeBeltStart = (Date) timeFormat.parse(timeFormat.format(new Date()));
//
//            if (today.after(pPriorityInsert.getDtStratDate())) {
//                dtStart = today;
//            }
//
//            if ((dtStart.equals(today)) && timeBeltStart.after(tBelt.getStartTime())) {
//                Calendar gcal = Calendar.getInstance();
//                ;
//                gcal.setTime(dtStart);
//                gcal.add(Calendar.DATE, 1);
//            }
//            pPriorityInsert.setDtStratDate(dtStart);
//
//        } catch (ParseException ex) {
//            Logger.getLogger(PriorityService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        HashSet<Integer> setDays = new HashSet<>();
//        String stDays = pPriorityInsert.getStDays();
//        for (int i = 0;
//             i < stDays.length();
//             i++) {
//            setDays.add(Character.getNumericValue(stDays.charAt(i)));
//        }
//
//        PriorityDef pPriorityDf = priorityDao.getPriorityDefbyPriority(pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiCluster(), pPriorityInsert.getiPriority());
//
//        List<ScheduleDef> lstScheduleMap = scheduleDao.getSchedulePriorityMapList(pPriorityDf, pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate());
//
//        List<TimeSlotScheduleMap> lstSchedules = scheduleDao.getSchedulesbyTimebeltAdvert(pPriorityInsert.getiWorkOrderID(), pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiAdvertisementID(), pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate());
//
//        HashMap<Date, ScheduleDef> mapDatePriority = new HashMap<Date, ScheduleDef>();
//        for (ScheduleDef item : lstScheduleMap) {
//            mapDatePriority.put(item.getDate(), item);
//        }
//
//        HashMap<Date, ScheduleDef> mapDateSchedules = new HashMap<Date, ScheduleDef>();
//        for (TimeSlotScheduleMap item : lstSchedules) {
//            mapDateSchedules.put(item.getDate(), item.getScheduleid());
//        }
//
//        for (Map.Entry<Date, ScheduleDef> entry : mapDateSchedules.entrySet()) {
//            Date date = entry.getKey();
//            if (!setDays.contains(this.getDay(date))) {
//                continue;
//            }
//
//            ScheduleDef schedule = entry.getValue();
//
//            if (mapDatePriority.containsKey(date)) {
//                if (pPriorityInsert.isOverWrite()) {
//                    ScheduleDef item = mapDatePriority.get(date);
//                    item.setPriority(null);
//                    scheduleDao.updateSchedule(item);
//
//                    if (validateHalfAnHourSpot(schedule, pPriorityDf)) {
//                        schedule.setPriority(pPriorityDf);
//                        scheduleDao.updateSchedule(schedule);
//                    }
//                }
//
//            } else {
//                if (validateHalfAnHourSpot(schedule, pPriorityDf)) {
//                    schedule.setPriority(pPriorityDf);
//                    scheduleDao.updateSchedule(schedule);
//                }
//            }
//
//        }
//
//        return true;
//    }

    public Message InsertPriority(PriorityInsertView pPriorityInsert) {
        Message message = new Message();
        message.setFlag(true);
        message.setMessage("Successfully save");

        System.out.println(pPriorityInsert.getDtEndDate() + " " + pPriorityInsert.getiCluster());
        if (pPriorityInsert.getiTimebeltID() == -111 || pPriorityInsert.getiCluster() == -111) {
            message.setFlag(false);
            message.setMessage("Please select time belt and cluster");
            return message;
        }

        TimeBelts tBelt = timeBeltDao.getTimeBeltbyID(pPriorityInsert.getiTimebeltID());
        Date dtStart = pPriorityInsert.getDtStratDate();
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            DateFormat timeFormat = new SimpleDateFormat("1970-01-01 HH:mm:ss");

            Date today = (Date) dateFormat.parse(dateFormat.format(new Date()));
            Date timeBeltStart = (Date) timeFormat.parse(timeFormat.format(new Date()));
            System.out.println(today + " " + timeBeltStart);

            if (today.after(pPriorityInsert.getDtStratDate())) {
                dtStart = today;
            }

            if ((dtStart.equals(today)) && timeBeltStart.after(tBelt.getStartTime())) {
                Calendar gcal = Calendar.getInstance();
                ;
                gcal.setTime(dtStart);
                gcal.add(Calendar.DATE, 1);
            }
            System.out.println(dtStart);
            pPriorityInsert.setDtStratDate(dtStart);

        } catch (ParseException ex) {
            Logger.getLogger(PriorityService.class.getName()).log(Level.SEVERE, null, ex);
        }

        HashSet<Integer> setDays = new HashSet<>();
        String stDays = pPriorityInsert.getStDays();
        for (int i = 0;
             i < stDays.length();
             i++) {
            setDays.add(Character.getNumericValue(stDays.charAt(i)));
        }

        if (pPriorityInsert.isOverWrite()) {
            List<PriorityDef> priorityDefList = priorityDao.getPriorityListbyTimeBelt(pPriorityInsert.getiTimebeltID());
            List<ScheduleDef> scheduledSpotList = new ArrayList<>();
            for (PriorityDef priorityDef : priorityDefList) {
                scheduledSpotList.addAll(scheduleDao.getSchedulePriorityMapList(priorityDef, pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate()));
            }

            for (ScheduleDef scheduleDef : scheduledSpotList) {
                if(pPriorityInsert.getiAdvertisementID()==scheduleDef.getAdvertid().getAdvertid()) {
                    scheduleDef.setPriority(null);
                    scheduleDao.updateSchedule(scheduleDef);
                }
            }
        }

        PriorityDef pPriorityDf = priorityDao.getPriorityDefbyPriority(pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiCluster(), pPriorityInsert.getiPriority());
        System.out.println(pPriorityDf.getPriorityId());

        List<ScheduleDef> lstScheduleMap = scheduleDao.getSchedulePriorityMapList(pPriorityDf, pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate());
        System.out.println(lstScheduleMap.size());

        List<TimeSlotScheduleMap> lstSchedules = scheduleDao.getSchedulesbyTimebeltAdvert(pPriorityInsert.getiWorkOrderID(), pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiAdvertisementID(), pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate());
        System.out.println(lstSchedules.size());

        if (lstSchedules.size() == 0) {
            message.setFlag(false);
            message.setMessage("There are scheduled spot for the selected time belt.");
        }

        HashMap<Date, ScheduleDef> mapDatePriority = new HashMap<Date, ScheduleDef>();
        for (ScheduleDef item : lstScheduleMap) {
            mapDatePriority.put(item.getDate(), item);
        }

        HashMap<Date, ScheduleDef> mapDateSchedules = new HashMap<Date, ScheduleDef>();
        for (TimeSlotScheduleMap item : lstSchedules) {
            mapDateSchedules.put(item.getDate(), item.getScheduleid());
        }

        for (Map.Entry<Date, ScheduleDef> entry
                : mapDateSchedules.entrySet()) {
            Date date = entry.getKey();
            if (!setDays.contains(this.getDay(date))) {
                continue;
            }

            ScheduleDef schedule = entry.getValue();

            if (mapDatePriority.containsKey(date)) {
                ScheduleDef item = mapDatePriority.get(date);
                if (validateHalfAnHourSpot(item, pPriorityDf)) {
                    if (currentHourValidation(schedule, pPriorityDf)) {
                        item.setPriority(null);
                        scheduleDao.updateSchedule(item);
                    } else {
                        message.setMessage(message.getMessage() + " Today spots schedule in invalid time belt.");
                    }
                } else {
                    message.setFlag(false);
                    message.setMessage("Invalid cluster.");
                }
            }

            if (validateHalfAnHourSpot(schedule, pPriorityDf)) {
                if (currentHourValidation(schedule, pPriorityDf)) {
                    schedule.setPriority(pPriorityDf);
                    scheduleDao.updateSchedule(schedule);
                } else {
                    message.setMessage(message.getMessage() + " Today spots schedule in invalid time belt.");
                }
            } else {
                message.setFlag(false);
                message.setMessage("Invalid cluster.");
            }
        }

        logger.debug("/////////////setPriorityAdvertisementWidgetData////////");
        //priorityAdvertisementWidgetService.setPriorityAdvertisementWidgetData();
        logger.debug("/////////////finished setting PriorityAdvertisementWidgetData////////");

        return message;
    }

    private Boolean validateHalfAnHourSpot(ScheduleDef scheduleDef, PriorityDef priorityDef) {
        Date startDate = scheduleDef.getSchedulestarttime();
        Date endDate = scheduleDef.getScheduleendtime();

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        int clusterCount= priorityDao.getSelectedTimebeltClusterCount(priorityDef.getTimebeltId().getTimeBeltId());
        clusterCount--;

        int startMinute = startCal.get(Calendar.MINUTE);
        int startScheduleHour = startCal.get(Calendar.HOUR_OF_DAY);
        int endMinute = endCal.get(Calendar.MINUTE);
        int endScheduleHour = endCal.get(Calendar.HOUR_OF_DAY);
        if(endScheduleHour!=23) {
            endScheduleHour++;
        }

        Calendar selectedStartCal = Calendar.getInstance();
        selectedStartCal.setTime(priorityDef.getTimebeltId().getStartTime());

        Calendar selectedEndCal = Calendar.getInstance();
        selectedEndCal.setTime(priorityDef.getTimebeltId().getEndTime());

        int selectedTimeBeltStartHour = selectedStartCal.get(Calendar.HOUR_OF_DAY);
        int selectedTimeBeltEndHour = selectedEndCal.get(Calendar.HOUR_OF_DAY);

        if (startScheduleHour == selectedTimeBeltStartHour && startMinute == 30 && priorityDef.getCluster() == 0) {
            return false;
        } else if (endScheduleHour == selectedTimeBeltEndHour && endMinute == 30 && priorityDef.getCluster() == clusterCount) {
            return false;
        } else {
            return true;
        }
    }

    private Boolean currentHourValidation(ScheduleDef scheduleDef, PriorityDef priorityDef) {

        Boolean statue = Boolean.TRUE;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String today = dateFormat.format(new Date());
        String scheduleDate = scheduleDef.getDate().toString();
        if (today.equals(scheduleDate)) {
            Calendar currentHourCal = Calendar.getInstance();
            currentHourCal.setTime(new Date());
            int currentHour = currentHourCal.get(Calendar.HOUR_OF_DAY);

            Calendar selectedHourCal = Calendar.getInstance();
            selectedHourCal.setTime(priorityDef.getTimebeltId().getEndTime());
            int selectedHour = selectedHourCal.get(Calendar.HOUR_OF_DAY);
            if (currentHour == selectedHour) {
                statue = Boolean.FALSE;
            }
        }
        return statue;
    }

    private int getDay(Date _date) {
        Calendar c = Calendar.getInstance();
        c.setTime(_date);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    public List<PriorityPreviewView> getPriorityList(PriorityInsertView pPriorityInsert) {
        List<TimeSlotScheduleMap> lstSchedules = scheduleDao.getSchedulePriorityMapListbyTimeBelt(pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiCluster(), pPriorityInsert.getiPriority(), pPriorityInsert.getiWorkOrderID(), pPriorityInsert.getiAdvertisementID(), pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate(), pPriorityInsert.getIchannelID());
        ArrayList<PriorityPreviewView> listView = new ArrayList<>();

        ArrayList<Date> datesList = new ArrayList<>();
        Calendar gcal = Calendar.getInstance();
        ;
        TimeZone.setDefault(TimeZone.getTimeZone("IST"));
        gcal.setTime(pPriorityInsert.getDtStratDate());
        while (!gcal.getTime().after(pPriorityInsert.getDtEndDate())) {
            datesList.add(gcal.getTime());
            gcal.add(Calendar.DATE, 1);
        }

        HashSet<Integer> setDays = new HashSet<>();
        String stDays = pPriorityInsert.getStDays();
        for (int i = 0; i < stDays.length(); i++) {
            setDays.add(Character.getNumericValue(stDays.charAt(i)));
        }
        HashMap<TimeBelts, HashMap<Date, List<PriorityPreviewItem>>> mapPriority = new HashMap<>();

        for (TimeSlotScheduleMap schedule_map : lstSchedules) {
            TimeBelts tBelt = schedule_map.getTimeBelt();
            ScheduleDef schedule = schedule_map.getScheduleid();
            Advertisement advert = schedule.getAdvertid();
            Date date = schedule_map.getDate();

            if (!setDays.contains(this.getDay(date))) {
                continue;
            }

            if (!mapPriority.containsKey(tBelt)) {
                mapPriority.put(tBelt, new HashMap<>());
                HashMap<Date, List<PriorityPreviewItem>> _mapItems = mapPriority.get(tBelt);
                for (Date _date : datesList) {
                    _mapItems.put(_date, new ArrayList<>());
                }
                ;
            }

            HashMap<Date, List<PriorityPreviewItem>> mapItems = mapPriority.get(tBelt);
            if (!mapItems.containsKey(date)) {
                mapItems.put(date, new ArrayList<>());
            }
            PriorityPreviewItem item = new PriorityPreviewItem();
            item.setAdvertId(advert.getAdvertid());
            item.setAdvertName(advert.getAdvertname());
            item.setWorkOrderId(schedule_map.getWorkOrderId());
            item.setCluster(schedule.getPriority().getCluster());
            item.setPriority(schedule.getPriority().getPriority());
            mapItems.get(date).add(item);
        }

        for (Map.Entry<TimeBelts, HashMap<Date, List<PriorityPreviewItem>>> entry : mapPriority.entrySet()) {
            TimeBelts tBelt = entry.getKey();
            HashMap<Date, List<PriorityPreviewItem>> valueList = entry.getValue();

            PriorityPreviewView view = new PriorityPreviewView();
            view.setTimeBelt(tBelt);
            for (Map.Entry<Date, List<PriorityPreviewItem>> subEntry : valueList.entrySet()) {
                view.addPriorityPreviewDate(subEntry.getKey(), subEntry.getValue());
            }
            view.sortPriorityPreviewDates();
            listView.add(view);
        }

        return listView;
    }

    public boolean removePriorities(PriorityInsertView pPriorityInsert) {
        TimeBelts tBelt = timeBeltDao.getTimeBeltbyID(pPriorityInsert.getiTimebeltID());
        Date dtStart = pPriorityInsert.getDtStratDate();
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            DateFormat timeFormat = new SimpleDateFormat("1970-01-01 HH:mm:ss");
            Date today;
            today = (Date) dateFormat.parse(dateFormat.format(new Date()));
            Date timeBeltStart = (Date) timeFormat.parse(timeFormat.format(new Date()));

            if (today.after(pPriorityInsert.getDtStratDate())) {
                dtStart = today;
            }

            TimeZone.setDefault(TimeZone.getTimeZone("IST"));
            if ((dtStart.equals(today)) && timeBeltStart.after(tBelt.getStartTime())) {
                Calendar gcal = Calendar.getInstance();
                ;
                gcal.setTime(dtStart);
                gcal.add(Calendar.DATE, 1);
            }
            pPriorityInsert.setDtStratDate(dtStart);
        } catch (ParseException ex) {
            Logger.getLogger(PriorityService.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<TimeSlotScheduleMap> lstSchedules = scheduleDao.getSchedulePriorityMapListbyTimeBelt(pPriorityInsert.getiTimebeltID(), pPriorityInsert.getiCluster(), pPriorityInsert.getiPriority(), pPriorityInsert.getiWorkOrderID(), pPriorityInsert.getiAdvertisementID(), pPriorityInsert.getDtStratDate(), pPriorityInsert.getDtEndDate(), pPriorityInsert.getIchannelID());

        for (TimeSlotScheduleMap item : lstSchedules) {
            ScheduleDef removeItem = item.getScheduleid();
            removeItem.setPriority(null);
            scheduleDao.updateSchedule(removeItem);
        }
        logger.debug("/////////////setPriorityAdvertisementWidgetData////////");
        //priorityAdvertisementWidgetService.setPriorityAdvertisementWidgetData();
        logger.debug("/////////////finished setting PriorityAdvertisementWidgetData////////");
        return true;
    }
}
