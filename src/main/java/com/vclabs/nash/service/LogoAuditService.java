package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.LogoAuditDAO;
import com.vclabs.nash.model.entity.LogoAudit;
import com.vclabs.nash.model.entity.PlayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-28.
 */
@Service
@Transactional("main")
public class LogoAuditService {

    @Autowired
    private LogoAuditDAO logoAuditDAO;

    public LogoAudit save(LogoAudit logoAudit) {
        return logoAuditDAO.save(logoAudit);
    }

    public void setSelectedPlaylistItemStatus(PlayList playList) {

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(playList.getScheduleStartTime());
        calendar.get(Calendar.HOUR_OF_DAY);
        int startHour = calendar.get(Calendar.HOUR_OF_DAY);
        calendar.setTime(playList.getScheduleEndTime());
        int endHour = calendar.get(Calendar.HOUR_OF_DAY);
        int playedCount = 0;

        for (int i = startHour; i <= endHour; i++) {
            List<LogoAudit> logoAuditList = logoAuditDAO.getSelectedPlaylistId(playList.getPlaylistId(), i);
            if (!logoAuditList.isEmpty()) {
                playedCount++;
            }
        }

        if ((endHour - startHour) < playedCount) {
            playList.setStatus("10");
        } else if ((endHour - startHour) == playedCount) {
            playList.setStatus("2");
        }
    }
}
