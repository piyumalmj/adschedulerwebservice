/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Field;
import java.util.Calendar;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;

import com.vclabs.nash.model.process.dto.ClientWoTableDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.process.ClientDetailInfo;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class ClientDetailService {

    @Autowired
    private ClientDetailDAO clientDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private SchedulerDAO schedulerDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientDetailService.class);

    public int saveClient(String newModel) {
        try {
            Boolean modelUpdated = false;
            ObjectMapper m = new ObjectMapper();
            ClientDetailInfo dataModel = m.readValue(newModel, ClientDetailInfo.class);

            ClientDetails model = new ClientDetails();
            if (dataModel.getClientId() != 0) {
                model = clientDao.getSelectedClient(dataModel.getClientId());
                modelUpdated = true;
            }
            model.setClientname(dataModel.getClientName());
            model.setClienttype(dataModel.getClientType());
            model.setAddress(dataModel.getAddress());
            model.setContactdetails(dataModel.getContact_details());
            model.setEmailaddress(dataModel.getEmail());
            model.setTelephonenum(dataModel.getTelephone_num());
            model.setVat_no(dataModel.getVat());
            model.setContact_person(dataModel.getContact_person());
            model.setCx_cade(dataModel.getCx_cade());
            if (dataModel.getBalack_list() == 1) {
                model.setEnabled(true);
                model.setStatus(ClientDetails.Status.ACTIVE);
            } else {
                model.setEnabled(false);
                model.setStatus(ClientDetails.Status.BLACKLIST);
            }

            if (modelUpdated) {
                if (setAuditForClientDetails(model)) {
                    return clientDao.saveAndUpdateClient(model);
                } else {
                    return -1;
                }
            } else {
                return clientDao.saveAndUpdateClient(model);
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in ClientDetailService while trying to save the client: {}", e.getMessage());
            return -1;
        }
    }

    public List<ClientDetails> getAllClient() {

        List<ClientDetails> allClientList = clientDao.getAllClient();
        Collections.sort(allClientList);
        return allClientList;
    }

    public List<ClientDetails> getAllClientOrderByName() { //tested
           return clientDao.getAllClientOrderByName();
    }

    public ClientDetails getSelectedClient(int clientId) {
        ScheduleDef scheduleDef = schedulerDao.getMaximumSpotInClient(clientId);
        ClientDetails clientDetails = clientDao.getSelectedClient(clientId);

        if (scheduleDef == null) {
            clientDetails.setCanDelete(Boolean.TRUE);
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(scheduleDef.getDate());
            c.add(Calendar.DATE, 1);
            if (c.getTime().before(new Date())) {
                clientDetails.setCanDelete(Boolean.TRUE);
            } else {
                clientDetails.setCanDelete(Boolean.FALSE);
            }
        }

        return clientDetails;
    }

    public Boolean setAuditForClientDetails(ClientDetails model) {

        try {
            ClientDetails dataModel = clientDao.getSelectedClient(model.getClientid());

            for (Field field : (ClientDetails.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {
                    GeneralAudit auditModel = new GeneralAudit("client_details", model.getClientid());
                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }
            return true;
        } catch (IllegalAccessException e) {
            LOGGER.debug("Exception in ClientDetailService while trying to set Audit for Client Details: {}", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception in ClientDetailService while trying to set Audit for Client Details: {}", e.getMessage());
            throw e;
        }
        return false;
    }

    public boolean disableClient(int clientID) {
        try {
            ClientDetails model = clientDao.getSelectedClient(clientID);
            model.setEnabled(false);
            model.setStatus(ClientDetails.Status.DELETE);

            if (setAuditForClientDetails(model) && clientDao.saveAndUpdateClient(model) != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in ClientDetailService while trying to disable client: {}", e.getMessage());
            return false;
        }
    }

    public List<ClientWoTableDto> getClientForWoTable(){
        return clientDao.getAllClientForWoTable();
    }
}
