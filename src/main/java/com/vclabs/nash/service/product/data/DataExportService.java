package com.vclabs.nash.service.product.data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.model.entity.product.Product;
import com.vclabs.nash.service.inventory.impl.InventoryServiceImpl;
import com.vclabs.nash.service.product.BrandService;
import com.vclabs.nash.service.product.CodeMappingService;
import com.vclabs.nash.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

/**
 * Created by dperera on 29/11/2019.
 */
@Service
public class DataExportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataExportService.class);

    @Value(value = "classpath:code-mapping-v1.json")
    private Resource codeMappings;

    @Value(value = "classpath:code-mapping-v2.json")
    private Resource codeMappings2;

    @Autowired
    private ProductService productService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private ClientDetailDAO clientDetailDAO;

    @Autowired
    private CodeMappingService codeMappingService;

    private static int counter;

    @Transactional
    public void export(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<CMData> list =  objectMapper.readValue(codeMappings.getInputStream(),
                    new TypeReference<List<CMData>>(){});
            for(CMData cmData : list){
                LOGGER.debug("Inserting a new code mapping : " +cmData.getClientId()+ ", " + cmData.getCompany()+ ", " + cmData.getProduct()+
                        ", " +cmData.getBrand()+ ", " + cmData.getCode());
                saveCodeMapping(cmData);
            }
        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }

        try {
            List<CMData> list =  objectMapper.readValue(codeMappings2.getInputStream(),
                    new TypeReference<List<CMData>>(){});
            for(CMData cmData : list){
                LOGGER.debug("Inserting a new code mapping : " +cmData.getClientId()+ ", " + cmData.getCompany()+ ", " + cmData.getProduct()+
                        ", " +cmData.getBrand()+ ", " + cmData.getCode());
                saveCodeMapping(cmData);
            }
        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }
    }

    private void saveCodeMapping(CMData cmData){
        CodeMapping codeMapping = new CodeMapping();
        Product product = productService.findByName(cmData.getProduct().trim());
        if(product == null){
            product = new Product();
            product.setName(cmData.getProduct().trim());
            productService.save(product);
        }
        codeMapping.setProduct(product);

        Brand brand = brandService.findByName(cmData.getBrand().trim());
        if(brand == null){
            brand = new Brand();
            brand.setName(cmData.getBrand().trim());
            brandService.save(brand);
        }
        codeMapping.setBrand(brand);
        codeMapping.setClient(clientDetailDAO.getSelectedClient(Integer.parseInt(cmData.getClientId())));
        codeMapping.setCode(cmData.getCode().trim());
        codeMappingService.save(codeMapping);
    }
}
