package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.AuthorityDAO;
import com.vclabs.nash.model.entity.SecurityAuthoritiesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2019-08-07.
 */
@Service
@Transactional("main")
public class ReportingURLService {

    @Autowired
    private AuthorityDAO authorityDao;

    public String selectedUserAccessPage(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<SecurityAuthoritiesEntity> authoritiList = authorityDao.getAuthority(user.getUsername());
        List<String> authoriitStringLis = new ArrayList<>();
        authoriitStringLis.add("ROLE_DummyCut");
        authoriitStringLis.add("ROLE_CommercialAvailability");
        authoriitStringLis.add("ROLE_ScheduleReports");
        authoriitStringLis.add("ROLE_RevenueReport");
        authoriitStringLis.add("ROLE_SalesReport");
        authoriitStringLis.add("ROLE_PaymentDue");
        authoriitStringLis.add("ROLE_Media");
        authoriitStringLis.add("ROLE_MissedSpotReport");
        authoriitStringLis.add("ROLE_DashboardReport");
        authoriitStringLis.add("ROLE_UserWise");

        for(int i=0;i<authoriitStringLis.size();i++ ) {
            Boolean hasNotAuthority = true;
            for (SecurityAuthoritiesEntity securityAuthoritiesEntity : authoritiList) {
                if (securityAuthoritiesEntity.getAuthority().equals(authoriitStringLis.get(i))) {
                    hasNotAuthority = false;
                    break;
                }
            }

            if (hasNotAuthority) {
                authoriitStringLis.remove(authoriitStringLis.get(i));
                i--;
            }
        }

        String url="";
        for (String authority : authoriitStringLis) {
            switch (authority){
                case "ROLE_DummyCut":
                    url="/reporting/dummy-cut";
                    break;
                case "ROLE_CommercialAvailability":
                    url="/reporting/commercial-availability-hourly";
                    break;
                case "ROLE_ScheduleReports":
                    url="/reporting/client-revenue-report";
                    break;
                case "ROLE_SalesReport":
                    url="/reporting/sales-report";
                    break;
                case "ROLE_PaymentDue":
                    url="/reporting/paymentdue";
                    break;
                case "ROLE_Media":
                    url="/reporting/material-analysis";
                    break;
                case "ROLE_MissedSpotReport":
                    url="/reporting/missed-spot-count";
                    break;
                case "ROLE_DashboardReport":
                    url="/reporting/dashboard-auto";
                    break;
                case "ROLE_UserWise":
                    url="/reporting/user-wise-report";
                    break;
                case "ROLE_RevenueReport":
                    url="/reporting/client-revenue-report";
                    break;
                default:
            }
            if(!url.equals("")){
                break;
            }
        }

        if(url.equals("")){
            url = "home";
        }
        return url;
    }
}
