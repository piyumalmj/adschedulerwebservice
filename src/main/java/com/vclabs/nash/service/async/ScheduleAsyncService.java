package com.vclabs.nash.service.async;

import com.vclabs.nash.model.entity.ScheduleDef;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by dperera on 23/04/2019.
 */
public interface ScheduleAsyncService {

    int saveScheduleListAsync(List<ScheduleDef> scheduleDefList) throws ExecutionException, InterruptedException;

}
