package com.vclabs.nash.service.async;

/**
 * Created by dperera on 29/03/2019.
 */
public interface WorkOrderStatusCheckAsyncService {

    void processWorkOrders();

}
