/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.view.ZeroAndFillerPlayListBackendView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vclabs.nash.model.dao.*;
import com.vclabs.nash.model.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vclabs.nash.model.view.PlayListBackendView;
import com.vclabs.nash.model.view.ScheduleView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class PlayListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayListService.class);

    static int LastFillerIndex = 0;
    @Autowired
    private PlayListDAO playListDao;
    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private AdvertisementDAO advertismentDao;
    @Autowired
    private ChannelDetailDAO channelDetailsDao;
    @Autowired
    private SchedulerDAO scheduleDao;
    @Autowired
    private PriorityDefDAO priorityDefDao;
    @Autowired
    private SpotSpreadService spotSpreadService;
    @Autowired
    private LogoAuditService logoAuditService;

    /// get each hour playlist by insertion
    public ScheduleView getPlayList_BackEnd(int _channelId, int _hour) throws Exception {
        try {
            ScheduleView schedule_view = new ScheduleView();

            List<PlayListBackendView> out = new ArrayList<>();
            List<PlayList> list = playListDao.getScheduledPlayList(_channelId, _hour);
            TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, 0);
            if (time_belt != null) {
                schedule_view.setTotalClusterCount(time_belt.getClusterCount());
            }
            schedule_view.setScheduleHour(_hour);

            for (PlayList item : list) {
                PlayListBackendView tmp = new PlayListBackendView();

                tmp.setPlaylistid(item.getPlaylistId());
                tmp.setScheduleHour(item.getScheduleHour());
                tmp.setScheduleStartTime(item.getScheduleStartTime());
                tmp.setScheduleEndTime(item.getScheduleEndTime());
                tmp.setTimeBeltStartTime(item.getTimeBeltStartTime());
                tmp.setTimeBeltEndTime(item.getTimeBeltEndTime());
                tmp.setActualStartTime(item.getActualStartTime());
                tmp.setActualEndTime(item.getActualEndTime());
                tmp.setStatus(item.getStatus());
                tmp.setRetryCount(item.getRetryCount());

                tmp.setPlayCluster(item.getPlayCluster());
                tmp.setPlayOrder(item.getPlayOrder());
                tmp.setClusterPriority(item.getClusterPriority());

                Advertisement advert = item.getAdvert();
                tmp.setAdvertId(advert.getAdvertid());
                tmp.setAdvertName(advert.getAdvertname());
                tmp.setAdvertType(advert.getAdverttype());
                tmp.setAdvertPath(advert.getAdvertpath());
                tmp.setAdvertCommercialCategory(advert.getCommercialcategory());
                tmp.setAdvertDuration(advert.getDuration());
                tmp.setLogoContainerId(advert.getLogoContainerId());
                tmp.setConflictingSchedules(item.getConflicting_schedules());

                //            tmp.setAdvertXPosition(advert.getPlaylistid());
                //            tmp.setAdvertYPosition(advert.getPlaylistid());
                //            tmp.setAdvertWidth(advert.getPlaylistid());
                //            tmp.setAdvertHeight(advert.getPlaylistid());
                //            tmp.setAdvertOpacity(advert.getPlaylistid());
                out.add(tmp);
            }
            schedule_view.setPlayList(out);
            return schedule_view;
        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListService in getPlayList_BackEnd : {}", e.getMessage());
            throw e;
        }
    }

    /// get each hour playlist by insertion
    public ScheduleView getPlayList_BackEnd(int _channelId, int _hour, int _period) throws Exception {
        try {
            ScheduleView schedule_view = new ScheduleView();

            List<PlayListBackendView> out = new ArrayList<>();
            TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, _period);

            List<PlayList> list = playListDao.getScheduledPlayList(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
            if (time_belt != null) {
                schedule_view.setTotalClusterCount(time_belt.getClusterCount());
            }
            schedule_view.setScheduleHour(_hour);

            for (PlayList item : list) {
                PlayListBackendView tmp = new PlayListBackendView();

                tmp.setPlaylistid(item.getPlaylistId());
                tmp.setScheduleHour(item.getScheduleHour());
                tmp.setScheduleStartTime(item.getScheduleStartTime());
                tmp.setScheduleEndTime(item.getScheduleEndTime());
                tmp.setTimeBeltStartTime(item.getTimeBeltStartTime());
                tmp.setTimeBeltEndTime(item.getTimeBeltEndTime());
                tmp.setActualStartTime(item.getActualStartTime());
                tmp.setActualEndTime(item.getActualEndTime());
                tmp.setStatus(item.getStatus());
                tmp.setRetryCount(item.getRetryCount());

                tmp.setPlayCluster(item.getPlayCluster());
                tmp.setPlayOrder(item.getPlayOrder());

                Advertisement advert = item.getAdvert();
                tmp.setAdvertId(advert.getAdvertid());
                tmp.setAdvertName(advert.getAdvertname());
                tmp.setAdvertType(advert.getAdverttype());
                tmp.setAdvertPath(advert.getAdvertpath());
                tmp.setAdvertCommercialCategory(advert.getCommercialcategory());
                tmp.setAdvertDuration(advert.getDuration());
                tmp.setLogoContainerId(advert.getLogoContainerId());
                tmp.setConflictingSchedules(item.getConflicting_schedules());

                //            tmp.setAdvertXPosition(advert.getPlaylistid());
                //            tmp.setAdvertYPosition(advert.getPlaylistid());
                //            tmp.setAdvertWidth(advert.getPlaylistid());
                //            tmp.setAdvertHeight(advert.getPlaylistid());
                //            tmp.setAdvertOpacity(advert.getPlaylistid());
                out.add(tmp);
            }
            schedule_view.setPlayList(out);
            return schedule_view;
        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListService in getPlayList_BackEnd : {}", e.getMessage());
            throw e;
        }
    }

    //@Async("dailyTaskExecutor")
   // @Scheduled(cron = "0 0 0/1 1/1 * ?") MTNL
    @Transactional
    public void autoScheduleGenerateCallerAsync() {
        try {
            LOGGER.debug("Started autoScheduleGenerateCallerAsync cron job | (cron = 0 0 0/1 1/1 * ?) | ( current time : {} )", new Date().toString());
            int hour = LocalDateTime.now().getHour();
            int minuts = 0;
            List<ChannelDetails> channelList = channelDetailsDao.getChannelList();
            for (ChannelDetails channel : channelList) {
                spotSpreadService.generatePlayListForChannel(channel, hour, minuts);
            }
            LOGGER.debug("Completed autoScheduleGenerateCallerAsync cron job | (cron = 0 0 0/1 1/1 * ?) | ( current time : {} )", new Date().toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    //generate hourly playlist
    public boolean generatePlayList_BackEnd(int _channelId, int _hour, int period) {

        //get start end times
        TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, period);
        if (time_belt == null) {
            return false;
        }

        //get valid cuts for schedule
        ArrayList<PlayList> play_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);

        for (PlayList playList : play_logos) {
           setLogoAudit(playList);
        }

        //update not played cuts in previous schedule
        playListDao.updateSchedulesValidity(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);

        ScheduleGenerate generator = new ScheduleGenerate(_channelId, _hour, time_belt.getStartTime(), time_belt.getEndTime(), time_belt.getClusterCount(), advertismentDao.getFillerAdvertisements());

        Date today = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            today = (Date) dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage());
        }
        List<ScheduleDef> lstScheduleList = scheduleDao.getSchedulePrioritybyTimeBelt(time_belt.getTimeBeltId(), today);
        List<PriorityDef> lstPriorities = priorityDefDao.getPriorityListbyTimeBelt(time_belt.getTimeBeltId());
        generator.setPrioritySchedulesList(lstScheduleList, lstPriorities);
        boolean bFlag = false;

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_adverts = (ArrayList<PlayList>) playListDao.getPlayListAdvert(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);

        //generate playlist adverts
        bFlag = generator.generatePlayListAdvert(play_list_adverts);
        if (!bFlag) {
            return false;
        }

        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<ArrayList<PlayList>> lst_scheduled_adverts = generator.getLst_Schedule();
        for (int i = 0; i < lst_scheduled_adverts.size(); i++) {
            ArrayList<PlayList> lst_cluster = lst_scheduled_adverts.get(i);

            for (int j = 0; j < lst_cluster.size(); j++) {
                PlayList playList = lst_cluster.get(j);
                playList.setPlayCluster(i);
                playList.setPlayOrder(j);
                playList.setStatus("1");
                playList.setComment("schedule_generated");
                playListDao.updatePlayList(playList);
            }
        }

        //get valid cuts for schedule
        ArrayList<PlayList> play_list_crawlers = (ArrayList<PlayList>) playListDao.getPlayListCrawler(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);
        //generate playlist crawler
        ChannelDetails pChannel = channelDetailsDao.getChannel(_channelId);

        if (!pChannel.isIsManual()) {

            bFlag = generator.generatePlayListCrawler(play_list_crawlers);
            if (!bFlag) {
                return false;
            }
        } else {
            play_list_crawlers = generator.shufflePlayListCrawler(play_list_crawlers);
        }

        int iOrder = 0;
        for (PlayList playList : play_list_crawlers) {
            playList.setStatus("1");
            playList.setPlayOrder(iOrder++);
            playList.setComment("schedule_generated");
            playListDao.updatePlayList(playList);
        }
        //get valid cuts for schedule
        ArrayList<PlayList> play_list_logos = (ArrayList<PlayList>) playListDao.getPlayListLogo(_channelId, time_belt.getStartTime(), time_belt.getEndTime(), _hour);

        for (PlayList playList : play_list_logos) {
            playList.setStatus("1");
            playList.setComment("schedule_generated");
            logoAuditService.setSelectedPlaylistItemStatus(playList);
            LOGGER.debug("Cluster : {} Advert : {}", playList.getTimeBeltStartTime(), playList.getAdvert().getAdvertname());
            playListDao.updatePlayList(playList);
        }

        time_belt.setIsScheduleGenerated(true);
        time_belt.setScheduleGeneratedTime(new Date());
        timeBeltDao.updateTimeBelt(time_belt);
        return true;
    }

    public void setLogoAudit(PlayList playList) {
        LogoAudit logoAudit = new LogoAudit();
        logoAudit.setPlaylist(playList);
        logoAudit.setStartTime(playList.getActualStartTime());
        logoAudit.setEndTime(playList.getActualEndTime());
        logoAudit.setStatus(playList.getStatus());
        logoAudit.setHour(playList.getScheduleHour());
        logoAuditService.save(logoAudit);
    }

    //update_played schedules from insertion backend
    public boolean updatePlayList_BackEnd(int _id, Date actualStartTime, Date actualEndTime, String state) {
        PlayList pList = playListDao.getPlayList(_id);
        pList.setActualStartTime(actualStartTime);
        pList.setActualEndTime(actualEndTime);
        pList.setStatus(state);

        return playListDao.updatePlayList(pList);
    }

    //write test_schedules
    public boolean writePlayList_BackEnd(int _channelId, int _hour) {
        int iScheduleCount = 20 * (24 - _hour);
        int iAdvertCount = 10;
        int min_hour = _hour;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        ArrayList<TimeBelts> time_belt = (ArrayList<TimeBelts>) timeBeltDao.getTimeBelts(_channelId);
        for (TimeBelts item : time_belt) {
            try {
                item.setIsScheduleGenerated(false);
                item.setScheduleGeneratedTime(format.parse(default_format.format(new Date())));
                timeBeltDao.updateTimeBelt(item);
            } catch (ParseException ex) {
                LOGGER.error(ex.getMessage());
            }
        }

        Random rand = new Random();

        playListDao.clearTable();
        for (int i = 0; i < iScheduleCount; i++) {
            try {
                int s_hour = rand.nextInt(24 - min_hour) + min_hour;  //7.00 to 24.00
                int e_hour = rand.nextInt(2) + s_hour + 1;

                String s_min = (rand.nextInt(2) == 1) ? "30" : "00";
                String e_min = (rand.nextInt(2) == 1) ? "30" : "00";

                int advert_id = rand.nextInt(iAdvertCount) + 1;

                PlayList playList = new PlayList();
                playList.setScheduleEndTime(format.parse("1970-01-01 " + e_hour + ":" + e_min + ":00"));
                playList.setScheduleStartTime(format.parse("1970-01-01 " + s_hour + ":" + s_min + ":00"));
                playList.setTimeBeltEndTime(format.parse("1970-01-01 " + e_hour + ":" + e_min + ":00"));
                playList.setTimeBeltStartTime(format.parse("1970-01-01 " + s_hour + ":" + s_min + ":00"));
                playList.setActualEndTime(format.parse("1970-01-01 " + e_hour + ":" + e_min + ":00"));
                playList.setActualStartTime(format.parse("1970-01-01 " + s_hour + ":" + s_min + ":00"));
                playList.setScheduleHour(s_hour);
                playList.setChannel(new ChannelDetails(_channelId));
                playList.setAdvert(new Advertisement(advert_id));
                playList.setSchedule(new ScheduleDef(1));
                playList.setWorkOrder(new WorkOrder(1));
                playList.setDate(format.parse(default_format.format(new Date())));
                playList.setStatus("0");
                playList.setPlayCluster(-1);
                playList.setPlayOrder(-1);
                playList.setRetryCount(0);
                playList.setComment("random_generated");

                playListDao.updatePlayList(playList);
            } catch (ParseException ex) {
                LOGGER.error(ex.getMessage());
            }
        }
        return true;
    }

    public boolean isScheduleGenerated(int _channelId, int _hour) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        TimeBelts time_belt = timeBeltDao.getTimeBelt(_channelId, _hour, 0);
        if (time_belt == null) {
            return false;
        }

        Date current_date = null;
        Date generated_date = null;
        try {
            current_date = format.parse(default_format.format(new Date()));
            generated_date = format.parse(default_format.format(time_belt.getScheduleGeneratedTime()));
            return (time_belt.getIsScheduleGenerated() && current_date.equals(generated_date));

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListService in isScheduleGenerated() : {}", e.getMessage());
            return false;
        }
    }

    public ZeroAndFillerPlayListBackendView GetFillerSchedule(int _channelId, int _hour) {
        ArrayList<Advertisement> lst_Fillers = (ArrayList<Advertisement>) advertismentDao.getFillerAdvertisements();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        if (lst_Fillers.size() == 0) {
            return null;
        }
        Random rand = new Random();

        try {
            LastFillerIndex++;
            if (LastFillerIndex >= lst_Fillers.size()) {
                LastFillerIndex = 0;
            }
            int advert = LastFillerIndex;

            PlayList playList = new PlayList();
            playList.setScheduleEndTime(format.parse("1970-01-01 00:00:00"));
            playList.setScheduleStartTime(format.parse("1970-01-01 00:00:00"));
            playList.setTimeBeltEndTime(format.parse("1970-01-01 00:00:00"));
            playList.setTimeBeltStartTime(format.parse("1970-01-01 00:00:00"));
            playList.setActualEndTime(format.parse("1970-01-01 00:00:00"));
            playList.setActualStartTime(format.parse("1970-01-01 00:00:00"));
            playList.setScheduleHour(_hour);
            playList.setChannel(new ChannelDetails(_channelId));
            playList.setAdvert(new Advertisement(lst_Fillers.get(advert).getAdvertid()));
            playList.setSchedule(new ScheduleDef(1));
            playList.setWorkOrder(new WorkOrder(1));
            playList.setDate(format.parse(default_format.format(new Date())));
            playList.setStatus("0");
            playList.setPlayCluster(-1);
            playList.setPlayOrder(-1);
            playList.setRetryCount(0);
            playList.setComment("filler_generated");

            playListDao.updatePlayList(playList);

            ZeroAndFillerPlayListBackendView tmp = new ZeroAndFillerPlayListBackendView();
            tmp.setPlaylistid(playList.getPlaylistId());
            tmp.setScheduleHour(playList.getScheduleHour());
            tmp.setScheduleStartTime(format.parse("1970-01-01 00:00:00"));
            tmp.setScheduleEndTime(playList.getScheduleEndTime());
            tmp.setTimeBeltStartTime(playList.getTimeBeltStartTime());
            tmp.setTimeBeltEndTime(playList.getTimeBeltEndTime());
            tmp.setActualStartTime(playList.getActualStartTime());
            tmp.setActualEndTime(playList.getActualEndTime());
            tmp.setStatus(playList.getStatus());
            tmp.setRetryCount(playList.getRetryCount());

            tmp.setPlayCluster(playList.getPlayCluster());
            tmp.setPlayOrder(playList.getPlayOrder());

            Advertisement _advert = lst_Fillers.get(advert);
            tmp.setAdvertId(_advert.getAdvertid());
            tmp.setAdvertName(_advert.getAdvertname());
            tmp.setAdvertType(_advert.getAdverttype());
            tmp.setAdvertPath(_advert.getAdvertpath());
            tmp.setAdvertCommercialCategory(_advert.getCommercialcategory());
            tmp.setAdvertDuration(_advert.getDuration());
            tmp.setLogoContainerId(_advert.getLogoContainerId());

            return tmp;

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListService in GetFillerSchedule() : {}", e.getMessage());
        }

        return null;
    }

    public PlayList getSelectedPlayList(int playListId){
        return playListDao.getPlayList(playListId);
    }
}
