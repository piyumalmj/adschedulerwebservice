package com.vclabs.nash.service.exception;

/**
 * Created by dperera on 18/09/2018.
 */
public class NashException extends Exception {

    public NashException(String message){
        super(message);
    }

    public NashException(Throwable cause){
        super(cause);
    }
}
