package com.vclabs.nash.service.security.impl;

import com.vclabs.nash.model.entity.SecurityAuthoritiesEntity;
import com.vclabs.nash.model.entity.SecurityUsersEntity;
import com.vclabs.nash.model.dao.UserDetailsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 14/08/2017.
 */
@Service
public class ApplicationUserServiceImpl implements UserDetailsService {



   @Autowired
   private PasswordEncoder passwordEncoder;

   @Autowired
   private UserDetailsDAO userDetailsDAO;


/* @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      ApplicationUser user = repository.findByUsernameAndStatus(username, ApplicationUser.Status.ACTIVE);
      if(user != null){
         return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                 true, true, true, true, getGrantedAuthorities(user));
      }
      throw new UsernameNotFoundException(String.format("No user registered with username %s", username));
   }

   private List<GrantedAuthority> getGrantedAuthorities(ApplicationUser user){
      List<GrantedAuthority> authorities = new ArrayList<>();
      for(UserRole role : user.getUserRoles()){
         authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getType()));
      }
      return authorities;
   }*/

   @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      SecurityUsersEntity user = userDetailsDAO.getUserDetails(username,1);
      if(user != null){
         return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                 true, true, true, true, getGrantedAuthorities(username));
      }
      throw new UsernameNotFoundException(String.format("No user registered with username %s", username));
   }

   private List<GrantedAuthority> getGrantedAuthorities(String username){
      List<GrantedAuthority> authorities = new ArrayList<>();
      List<SecurityAuthoritiesEntity> sAuthorities = userDetailsDAO.getSecurityAuthoritiesByUserName(username);
      for(SecurityAuthoritiesEntity role : sAuthorities){
         authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
      }
      return authorities;
   }

}
