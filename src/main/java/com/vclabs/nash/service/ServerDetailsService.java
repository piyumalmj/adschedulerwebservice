/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import com.vclabs.nash.dashboard.service.ServerControlWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.ServerDetailsDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ServerDetails;
import com.vclabs.nash.model.process.ServerBasicInfo;
import com.vclabs.nash.model.process.ServerDynamicInfo;

/**
 * @author Sanira Nanayakkara
 */
@Service
@Transactional("main")
public class ServerDetailsService {

    @Autowired
    private ServerDetailsDAO serverDetailsDao;
    @Autowired
    private ChannelDetailDAO channelDetailsDao;
//    @Autowired
//    private ServerControlWidgetService serverControlWidgetService;

    private static final Logger logger = LoggerFactory.getLogger(ServerDetailsService.class);

    public List<ServerDetails> getServerList() {
        List<ServerDetails> serverDetails = serverDetailsDao.getServerList();
        return serverDetails;
    }

    public ServerDetails getServerDetails(int iServerId) {
        ServerDetails serverDetails = serverDetailsDao.getServerDetails(iServerId);
        /*=============DashBoard================
        serverControlWidgetService.setServerControlData();
        =============DashBoard================*/
        return serverDetails;
    }

    public int insertServerDetails(String modelData) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ServerBasicInfo serverData = mapper.readValue(modelData, ServerBasicInfo.class);

            ServerDetails serverModel = new ServerDetails(null);
            serverModel.setCardPort(serverData.getChannelPort());
            serverModel.setChannelDetails(null);
            serverModel.setCommand("");
            serverModel.setErrorReport("");
            serverModel.setFreeDiskSpace(BigInteger.ZERO);
            serverModel.setFreeMemory(BigInteger.ZERO);
            serverModel.setInCard(serverData.getInCard());
            serverModel.setOutCard(serverData.getOutCard());
            serverModel.setServerIP(serverData.getServerIP());
            serverModel.setServerPort(serverData.getServerPort());
            serverModel.setServicePath(serverData.getServicePath());
            serverModel.setStatus(0);
            int id = serverDetailsDao.insertServerDetails(serverModel);
            /*=============DashBoard================
            serverControlWidgetService.setServerControlData();
            /*=============DashBoard================*/
            return id;

        } catch (IOException e) {
            logger.debug("Exception: {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception: {}", e.getMessage());
            throw e;
        }
        return -1;
    }

    public boolean updateBasicServerDetails(String modelData) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ServerBasicInfo serverData = mapper.readValue(modelData, ServerBasicInfo.class);

            if (serverData.getServerID() != 0) {
                ServerDetails serverModel = serverDetailsDao.getServerDetails(serverData.getServerID());
                serverModel.setCardPort(serverData.getChannelPort());
                serverModel.setChannelDetails(null);
                serverModel.setCommand("");
                serverModel.setErrorReport("");
                serverModel.setFreeDiskSpace(BigInteger.ZERO);
                serverModel.setFreeMemory(BigInteger.ZERO);
                serverModel.setInCard(serverData.getInCard());
                serverModel.setOutCard(serverData.getOutCard());
                serverModel.setServerIP(serverData.getServerIP());
                serverModel.setServerPort(serverData.getServerPort());
                serverModel.setServicePath(serverData.getServicePath());
                serverModel.setStatus(0);

                boolean result = serverDetailsDao.updateServerDetails(serverModel);
                /*=============DashBoard================
                serverControlWidgetService.setServerControlData();
                /*=============DashBoard================*/
                return result;
            }
        } catch (IOException e) {
            logger.debug("Exception: {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception: {}", e.getMessage());
            throw e;
        }
        return false;
    }

    public boolean updateDynamicServerDetails(String modelData) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ServerDynamicInfo serverData = mapper.readValue(modelData, ServerDynamicInfo.class);

            if (serverData.getServerID() != 0) {
                ServerDetails serverModel = serverDetailsDao.getServerDetails(serverData.getServerID());
                
                Integer iChannelId = serverModel.getChannelDetails(); 
                if(iChannelId != null) 
                    updateChannelDetails(iChannelId, null);
                
                if(serverData.getChannelId() != -1)
                    serverModel.setChannelDetails(serverData.getChannelId());
                else
                    serverModel.setChannelDetails(null);                
                serverModel.setCommand(serverData.getCommand());

                boolean result = serverDetailsDao.updateServerDetails(serverModel);  
                
                if(serverModel.getChannelDetails() != null) {
                    updateChannelDetails(serverModel.getChannelDetails(), serverModel);
                }
                /*=============DashBoard================
                serverControlWidgetService.setServerControlData();
                /*=============DashBoard================*/
                return result;
            }
        } catch (IOException e) {
            logger.debug("Exception: {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception: {}", e.getMessage());
            throw e;
        }
        return false;
    }

    private boolean updateChannelDetails(int channelid, ServerDetails serverModel) {
        try {
            ChannelDetails channelDetails = channelDetailsDao.getChannel(channelid);
            if (channelDetails != null) {
                channelDetails.setServerDetails(serverModel);
                return channelDetailsDao.updateChannel(channelDetails);
            }
        } catch (Exception e) {
            logger.debug("Exception: {}", e.getMessage());
            throw e;
        }
        return false;
    }
}
