package com.vclabs.nash.controller.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/admin")
public class AdminModuleController {

    @RequestMapping(value = {"/schedule-panel"}, method = RequestMethod.GET)
    public String viewSchedulePanel () {
        return "admin/admin-schedule-panel";
    }

    @RequestMapping(value = {"/one-day-schedule"}, method = RequestMethod.GET)
    public String viewOneDaySchedulePage () {
        return "admin/one-day-schedule";
    }

    @RequestMapping(value = {"/channel/list"}, method = RequestMethod.GET)
    public String viewChannelListPage () {
        return "admin/channel-list";
    }

    @RequestMapping(value = {"/channel/update"}, method = RequestMethod.GET)
    public String viewChannelUpdatePage () {
        return "admin/channel-update";
    }

    @RequestMapping(value = {"/channel/new"}, method = RequestMethod.GET)
    public String viewChannelCreatePage () {
        return "admin/channel-add";
    }

    @RequestMapping(value = {"/client/list"}, method = RequestMethod.GET)
    public String viewClientListPage () {
        return "admin/client-list";
    }

    @RequestMapping(value = {"/client/update"}, method = RequestMethod.GET)
    public String viewClientUpdatePage () {
        return "admin/client-update";
    }

    @RequestMapping(value = {"/client/new"}, method = RequestMethod.GET)
    public String viewSaveClientPage () {
        return "admin/client-new";
    }

    @RequestMapping(value = {"/advertisement-category/list"}, method = RequestMethod.GET)
    public String viewAdvertisementCategoryListPage () {
        return "admin/advertisement-category-list";
    }

    @RequestMapping(value = {"/advertisement-category/update"}, method = RequestMethod.GET)
    public String viewAdvertisementCategoryUpdatePage () {
        return "admin/advertisement-category-update";
    }

    @RequestMapping(value = {"/advertisement-category/add"}, method = RequestMethod.GET)
    public String viewAdvertisementCategoryAddPage () {
        return "admin/advertisement-category-add";
    }

    @RequestMapping(value = {"/clusters/list"}, method = RequestMethod.GET)
    public String viewClusterListPage () {
        return "admin/all-clusters";
    }

    @RequestMapping(value = {"/priorities/list"}, method = RequestMethod.GET)
    public String viewPriorityListPage () {
        return "admin/all-priorities";
    }

    @RequestMapping(value = {"/server/list"}, method = RequestMethod.GET)
    public String viewServerListPage () {
        return "admin/all-servers";
    }

    @RequestMapping(value = {"/server/new"}, method = RequestMethod.GET)
    public String viewAddServerPage () {
        return "admin/server-new";
    }

    @RequestMapping(value = {"/server/update"}, method = RequestMethod.GET)
    public String viewServerUpdatePage (@RequestParam("id") int serverId,  ModelMap model) {
        model.addAttribute("serverId", serverId);
        return "admin/server-update";
    }

    @RequestMapping(value = {"/tax-details/list"}, method = RequestMethod.GET)
    public String viewTaxDetailsPage () {
        return "admin/tax-details";
    }

    @RequestMapping(value = {"/product/create"}, method = RequestMethod.GET)
    public String viewProductCreatePage () {
        return "admin/product-create";
    }

    @RequestMapping(value = {"/product/update"}, method = RequestMethod.GET)
    public String viewProductUpdatePage () {
        return "admin/product-update";
    }

    @RequestMapping(value = {"/product/all"}, method = RequestMethod.GET)
    public String viewAllProductPage () {
        return "admin/all-product";
    }

    @RequestMapping(value = {"/playlist-setting"}, method = RequestMethod.GET)
    public String viewPlayListSettingPage () {
        return "" +
                "admin/playlist-setting";
    }
}