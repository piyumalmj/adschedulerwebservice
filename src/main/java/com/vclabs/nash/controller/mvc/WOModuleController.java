package com.vclabs.nash.controller.mvc;


import com.vclabs.nash.model.view.UserView;
import com.vclabs.nash.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/workorder")
public class WOModuleController {

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewSigninPage(HttpServletRequest request, Model model) {
        return "workorder";
    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.GET)
    public String viewNewWorkOrderPage(HttpServletRequest request, Model model) {
        UserView userView = userDetailsService.getLoginUser();
        if(userView.getUserGroup().equals("marketing")){
            model.addAttribute("marketingExUserName", userView.getUserName());
        }
        return "new-workorder";
    }

    @RequestMapping(value = {"/update"}, method = RequestMethod.GET)
    public String updateWorkOrderPage(HttpServletRequest request, Model model) {
        return "update-workorder";
    }

}