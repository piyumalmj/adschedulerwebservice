package com.vclabs.nash.controller.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/billing")
public class BillingModuleController {

    @RequestMapping(method = RequestMethod.GET)
    public String viewBillingHomePage(HttpServletRequest request, Model model) {
        return "bill/bill-home";
    }

    @RequestMapping(value = {"/generated-bill"}, method = RequestMethod.GET)
    public String viewGeneratedBillPage(HttpServletRequest request, Model model) {
        return "bill/generated-bill";
    }

    @RequestMapping(value = {"/edit-details"}, method = RequestMethod.GET)
    public String viewEditDetailsPage(ModelMap model, @RequestParam long invoiceId) {
        model.addAttribute("invoiceId", invoiceId);
        return "bill/edit-details";
    }

    @RequestMapping(value = {"/tr-report"}, method = RequestMethod.GET)
    public String viewTRreportPage(HttpServletRequest request, Model model) {
        return "bill/transmission-report";
    }

    @RequestMapping(value = {"/detail-view"}, method = RequestMethod.GET)
    public String viewBillingDetailsPage(HttpServletRequest request, Model model) {
        return "bill/billing-view";
    }


}