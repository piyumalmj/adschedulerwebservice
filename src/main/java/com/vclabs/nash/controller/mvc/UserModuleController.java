package com.vclabs.nash.controller.mvc;


import com.vclabs.nash.model.entity.teams.TeamDetail;
import com.vclabs.nash.service.teams.TeamDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/users-v4")
public class UserModuleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserModuleController.class);

    @Autowired
    private com.vclabs.nash.service.UserDetailsService userDetailsService;

    @Autowired
    private TeamDetailService teamDetailService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewUserHomePage(HttpServletRequest request, Model model) {
        return "user/all-user";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String viewCreateAdvertisementPage(HttpServletRequest request, Model model) {
        return "user/new-user";
    }

    @RequestMapping(value = {"/group-management"}, method = RequestMethod.GET)
    public String viewCUserGroupPage(HttpServletRequest request, Model model) {
        return "user/user-group";
    }

    @RequestMapping(value = {"/change-profile"}, method = RequestMethod.GET)
    public String changeProfilePage(HttpServletRequest request, Model model) {
        String user = userDetailsService.getLoginUser().getUserName();
        model.addAttribute("current_user", user);
        return "user/change-profile";
    }

    @RequestMapping(value = {"/team-details"}, method = RequestMethod.GET)
    public String viewTeamDetailsPage(@RequestParam String teamId, Model model) {
        model.addAttribute("teamId", teamId);
        model.addAttribute("teamName", teamDetailService.findById(Integer.valueOf(teamId)).getName());
        return "user/team-details";
    }

    @RequestMapping(value = {"/team-management"}, method = RequestMethod.GET)
    public String viewTeamPage(HttpServletRequest request, Model model) {
        return "user/teamManagement";
    }

    @ResponseBody
    @RequestMapping(value = "/save-team-detail", method = RequestMethod.POST, produces = "application/json")
    public TeamDetail saveTeamDetail(@RequestBody TeamDetail teamDetail) {
        return teamDetailService.save(teamDetail);
    }

    @ResponseBody
    @RequestMapping(value = "/update-team-detail", method = RequestMethod.POST, produces = "application/json")
    public TeamDetail updateTeamDetail(@RequestBody TeamDetail teamDetail) {
        try {
            return teamDetailService.update(teamDetail);
        }catch (Exception e){
            LOGGER.debug("Exception in TeamDetailServiceImpl : ", e.getMessage());
            return null;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/load-all-teams", method = RequestMethod.GET)
    public List<TeamDetail> getAllTeams() {
        return teamDetailService.findAllTeams();
    }

    @ResponseBody
    @RequestMapping(value = "/load-all-team-members", method = RequestMethod.GET)
    public List<String> getAllTeamMembers(@RequestParam String teamId) {
        return teamDetailService.findAllTeamMembers(Integer.valueOf(teamId));
    }

    @ResponseBody
    @RequestMapping(value = "/load-team-admins", method = RequestMethod.GET)
    public List<String> getAllAdmins(@RequestParam String teamId) {
        return teamDetailService.findAllTeamAdmins(Integer.valueOf(teamId));
    }

    @ResponseBody
    @RequestMapping(value = "/deleteMemberUrl", method = RequestMethod.GET)
    public Boolean deleteMember(@RequestParam String teamId, @RequestParam String username) {
        return teamDetailService.deleteTeamMember(Integer.valueOf(teamId), username);
    }

    @ResponseBody
    @RequestMapping(value = "/deleteAdminUrl", method = RequestMethod.GET)
    public Boolean deleteAdmin(@RequestParam String teamId, @RequestParam String username) {
        return teamDetailService.deleteTeamAdmin(Integer.valueOf(teamId), username);
    }

    @ResponseBody
    @RequestMapping(value = "/checkTeamName", method = RequestMethod.GET)
    public Boolean checkTeamName(@RequestParam String teamName) {
        return teamDetailService.checkTeamName(teamName);
    }


}