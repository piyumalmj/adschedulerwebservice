package com.vclabs.nash.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/ops-ui")
public class OpsModelController {

    @RequestMapping(value = {"/import-deal"}, method = RequestMethod.GET)
    public String opsDealImportPage () {
        return "admin/ops";
    }
}
