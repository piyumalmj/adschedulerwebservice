package com.vclabs.nash.controller.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/schedule")
public class ScheduleModuleController {

    @RequestMapping(method = RequestMethod.GET)
    public String viewScheduleHomePage() {
        return "schedulehome";
    }

    @RequestMapping(value = {"/missedspot-reschedule"}, method = RequestMethod.GET)
    public String missedSpotReschedulePage() {
        return "missedspot-rescheduling";
    }

    @RequestMapping(value = {"/priority-setting"}, method = RequestMethod.GET)
    public String prioritySettingPage() {
        return "priority-setting";
    }

    @RequestMapping(value = {"/current-schedule"}, method = RequestMethod.GET)
    public String viewServerUpdatePage (@RequestParam("work_id") int woId, @RequestParam("work_name") String woName,
                                        @RequestParam("start_date") String startDate,  @RequestParam("end_date") String endDate, ModelMap model) {
        model.addAttribute("work_id", woId);
        model.addAttribute("work_name", woName);
        model.addAttribute("start_date", startDate);
        model.addAttribute("end_date", endDate);
        return "current-schedule";
    }

    @RequestMapping(value = {"/schedule-history"}, method = RequestMethod.GET)
    public String scheduleHistoryPage(@RequestParam("work_id") int woId, @RequestParam("work_name") String woName,
                                      @RequestParam("start_date") String startDate,  @RequestParam("end_date") String endDate, ModelMap model) {
        model.addAttribute("work_id", woId);
        model.addAttribute("work_name", woName);
        model.addAttribute("start_date", startDate);
        model.addAttribute("end_date", endDate);
        return "schedule-history";
    }

    @RequestMapping(value = {"/zero-ads"}, method = RequestMethod.GET)
    public String zeroAdsPage(){
        return "zeroAds";
    }

    @RequestMapping(value = {"/zero-ads-manual"}, method = RequestMethod.GET)
    public String zeroAdsManualPage(){
        return "zeroAds-manual";
    }

    @RequestMapping(value = {"/zero-ads-setting"}, method = RequestMethod.GET)
    public String zeroAdsSettingPage(){
        return "zeroAds-setting";
    }

}
