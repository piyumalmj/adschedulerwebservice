package com.vclabs.nash.controller;


import com.vclabs.nash.model.entity.AdvertSetting;
import com.vclabs.nash.service.AdvertSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by dperera on 20/09/19.
 */

@Controller
@RequestMapping("/playlist-setting")
public class PlayListSettingController {

    @Autowired
    private AdvertSettingService advertSettingService;

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<AdvertSetting> saveAdvertSetting(@RequestBody AdvertSetting setting) {
        AdvertSetting result = advertSettingService.save(setting);
        if(result != null){
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(setting, HttpStatus.PRECONDITION_FAILED);
    }

    @ResponseBody
    @RequestMapping(value = "/load-by-channel", method = RequestMethod.GET, produces = "application/json")
    public List<AdvertSetting> finByChannelId(@RequestParam("cid") int id) {
        return advertSettingService.finByChannelId(id);
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<AdvertSetting> deleteSetting(@RequestBody AdvertSetting setting) {
        advertSettingService.delete(setting);
        return new ResponseEntity<>(setting, HttpStatus.OK);
    }
}