/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.vclabs.nash.controller.dto.WorkOrderDto;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.dto.WODropDownDataDto;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.view.OrderAdvertListView;
import com.vclabs.nash.model.view.ScheduleHomeView;
import com.vclabs.nash.model.view.WorkOrderChannelListView;
import com.vclabs.nash.model.view.WorkOrderUpdateView;
import com.vclabs.nash.model.view.WorkOrderView;
import com.vclabs.nash.service.WorkOrderService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/workOrder")
public class WorkOrderController {

    @Autowired
    private WorkOrderService oredrService;

    static final Logger logger = Logger.getLogger("MyLog");

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(WorkOrderController.class);

    @RequestMapping(value = "/workOrderList/jsonp")
    public @ResponseBody
    String workOrderList(@RequestParam("callback") String callBack) throws Exception {
        return new ObjectMapper().writeValueAsString(new JSONPObject(callBack, this.workOrderList()));

    }

    /*Read*/
    @RequestMapping(value = "/allWorkOrderList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<WODropDownDataDto> allWorkOrderList() {
        return oredrService.getWODropDownData();
    }

    /*Read*/
    //Active workOrder details are returned *Test=pass*
    @RequestMapping(value = "/workOrderList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<WODropDownDataDto> workOrderList() {
        return oredrService.getWODropDownData();
    }

    /*Read*/
    //Specipic user's All workOrder details are returned *Test=pass*
    @RequestMapping(value = "/workOrderList_User/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleHomeView> workOrderList_User(@RequestParam("userName") String username) {
        return oredrService.WorkOrederList_userName(username);
    }

    //tested
    /*Read*/
    //Specipic workOrder details are returned *Test=pass*
    @RequestMapping(value = "/selectedWorkOrder/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<WorkOrderUpdateView> selectedWorkOrder(@RequestParam("workOrderId") int workOrderId) {
        return oredrService.getSelectedWorkOrder(workOrderId);
    }

    //User can save hold workOrder 
    @RequestMapping(value = "/holdWorkOrder/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean holdWorkOrder(@RequestParam("workOrderId") int workOrderId) throws Exception {
        return oredrService.holdWorkOrder(workOrderId);
    }

    //User can save hold workOrder
    @RequestMapping(value = "/resumeWorkOrder/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    WorkOrder resumeWorkOrder(@RequestParam("workOrderId") int workOrderId) throws Exception {
        return oredrService.resumingWorkOrder(workOrderId);
    }

    @ResponseBody
    @RequestMapping(value = "/workOrderSave/json", method = RequestMethod.POST, produces = "application/json")
    public int workOrderSave(@RequestBody WorkOrderDto wo) throws Exception {
        return oredrService.setWorkOrder(wo.getStartDate(), wo.getEndDate(), wo.getWorkOrderName(), wo.getClientName(), wo.getSeller(), wo.getVisibility(), wo.getComment(), wo.getExdata(), wo.getChannelList(), wo.getAdvertList());

    }

    //User can update selected workorder *Test=pass*
    /**
     *
     * @param workOrderData
     * @param channelData
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/workOrderUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean workOrderUpdate(@RequestParam("workOrderData") String workOrderData, @RequestParam("channelData") String channelData, @RequestParam("advertList") String advertList) throws Exception {
        try {
            return oredrService.updateWorkOrder(workOrderData, channelData, advertList);
        }catch (Exception e){
            LOGGER.debug("Exception in WorkOrderController while updating Work Order: {}", e.getMessage());
        }
        return null;
    }

    //User can update workorder Permissionstatus  *Test=fail*
    /**
     *
     * @param workOrderId
     * @param permissionstatus
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/workOrderUpdatePermissionstatus/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean workOrderUpdatePermissionstatus(@RequestParam("workOrderId") int workOrderId, @RequestParam("permissionstatus") int permissionstatus) {

        return oredrService.updateWorkOrderPermissionstatus(workOrderId, permissionstatus);
    }

    //User can delete selected workorder *Test=fail*
    /**
     *
     * @param workOrderId
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/workOrderDelete/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean workOrderDelete(@RequestParam("workOrderId") int workOrderId) {

        return oredrService.deleteWorkOrder(workOrderId);
    }

    //tested
    /*Read*/
    @RequestMapping(value = "/workOrderChannelList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<WorkOrderChannelListView> workOrderChannelList(@RequestParam("workOrderId") int workOrderId) {
        return oredrService.getSelectedOrderChannelList(workOrderId);
    }

    //User can update workOrderChannelList *Test=pass*
    /**
     *
     * @param orderedchannellistid
     * @param workOrderId
     * @param channelId
     * @param numberOfSpot
     * @param bonusspot
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/workOrderChannelListUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean workOrderChannelListUpdate(@RequestParam("orderedchannellistid") int orderedchannellistid, @RequestParam("workOrderId") int workOrderId, @RequestParam("channelId") int channelId, @RequestParam("numberOfSpot") int numberOfSpot, @RequestParam("bonusspot") int bonusspot) {
        return oredrService.updateWorkOrderChannelList(orderedchannellistid, workOrderId, channelId, numberOfSpot, bonusspot);
    }

    //User can detele selected workOrderChannelList *Test=fail*
    /**
     *
     * @param orderedchannellistid
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/workOrderChannelListDelete/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean workOrderChannelListDelete(@RequestParam("orderedchannellistid") int orderedchannellistid) {
        return oredrService.deleteWorkOrderChannelList(orderedchannellistid);
    }

    //All orderAdvertList details are returned *Test=pass*
    /**
     *
     * @param workOrderId
     * @return
     */
    /*Read*/
    @RequestMapping(value = "/orderAdvertList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<OrderAdvertListView> orderAdvertList(@RequestParam("workOrderId") int workOrderId) {
        return oredrService.getSelectedOrderAdvertList(workOrderId);
    }

    //All orderAdvertList details are returned *Test=pass*
    /**
     *
     * @return
     */
    /*Read*/
    @RequestMapping(value = "/orderAdvertListAll/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<OrderAdvertListView> orderAdvertListAll() {
        return oredrService.getOrderAdvertList();
    }

    /**
     *
     * @param workOrderAdvertList
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/orderAdvertListSave/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int orderAdvertListSave(@RequestParam("workOrderAdvertList") String workOrderAdvertList) {
        return oredrService.saveOrderAdvertList(workOrderAdvertList);
    }

    /**
     *
     * @param Orderadvertlistid
     * @param workOrderId
     * @param advertId
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/orderAdvertListUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean orderAdvertListUpdate(@RequestParam("OrderAdvertListId") int Orderadvertlistid, @RequestParam("workOrderId") int workOrderId, @RequestParam("advertId") int advertId) {
        return oredrService.updateOrderAdvertList(Orderadvertlistid, workOrderId, advertId);
    }

    /**
     *
     * @param Orderadvertlistid
     * @return
     */
    /*Write*/
    @RequestMapping(value = "/orderAdvertListDelete/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean orderAdvertListDelete(@RequestParam("OrderAdvertListId") int Orderadvertlistid) {
        return oredrService.deleteOrderAdvertList(Orderadvertlistid);
    }

    //tested
    /*Read*/
    @RequestMapping(value = "/allClientDetails/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ClientDetails> allClientDetails() {
        return oredrService.getClientDetails();
    }

    @RequestMapping(value = "/checkExistScheduleRef/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean checkExistScheduleRef(@RequestParam("scheduleRefe") String scheduleRefe) {
        return oredrService.checkExistScheduleRef(scheduleRefe);
    }

    //tested
    @RequestMapping(value = "/checkExistScheduleRefForUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean checkExistScheduleRef(@RequestParam("scheduleRefe") String scheduleRefe, @RequestParam("workOrderId") int workOrderId) {
        return oredrService.checkExistScheduleRefForUpadte(scheduleRefe, workOrderId);
    }

    @RequestMapping(value = "/workOrderList/page")
    public Page<WorkOrderView> getWorkOrders(@RequestParam Map<String, String> filterRequest) {
        return oredrService.getWorkOrders(filterRequest);
    }

    //tested
    @ResponseBody
    @RequestMapping(value = "/workOrderListForSchedule/page")
    public Page<WorkOrderView> getWorkOrdersForSchedule(@RequestParam Map<String, String> filterRequest) {
        return oredrService.getWorkOrdersForSchedule(filterRequest);
    }

    //tested
    @ResponseBody
    @RequestMapping(value = "/workOrderListForBilling/page")
    public Page<WorkOrderView> getWorkOrdersForBilling(@RequestParam Map<String, String> filterRequest) {
        return oredrService.getWorkOrdersForBilling(filterRequest);
    }
}
