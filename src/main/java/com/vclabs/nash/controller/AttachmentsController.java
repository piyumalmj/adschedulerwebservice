/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.vclabs.nash.model.entity.Attachments;
import com.vclabs.nash.service.AttachmentService;

/**
 *
 * @author Sanira Nanayakkara
 */
@RestController
@RequestMapping("/attachments")
public class AttachmentsController {

    @Autowired
    AttachmentService attachmentService;

    @RequestMapping(value = "/getAttachments/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Attachments> getAttachmentList(@RequestParam("table") String table, @RequestParam("id") int id) {
        return attachmentService.getAttachmentList(table, id);
    }

    @RequestMapping(value = "/upload_file", method = RequestMethod.POST)
    public @ResponseBody
    Integer handleFileUpload(MultipartHttpServletRequest request) throws Exception {
        return attachmentService.uploadAttachment(request);
    }

    @RequestMapping(value = "/removeAttachment", method = RequestMethod.GET)
    public @ResponseBody
    Boolean deleteFile(@RequestParam("id") int id) {
        return attachmentService.removeAttachment(id);
    }

    @RequestMapping(value = "/downloadAttachment", method = RequestMethod.GET)
    public void downloadAttachment(@RequestParam("id") int id, HttpServletResponse response) {
        attachmentService.downloadAttachment(id, response);

    }
}
