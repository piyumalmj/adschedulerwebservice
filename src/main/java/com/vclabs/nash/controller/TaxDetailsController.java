package com.vclabs.nash.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vclabs.nash.model.entity.TaxDef;
import com.vclabs.nash.service.TaxDetailsService;

/**
 * @author Sanira Nanayakkara
 */
@RestController
@RequestMapping("/taxDetails")
public class TaxDetailsController {

    @Autowired
    private TaxDetailsService taxDetailsService;

    @RequestMapping(value = "/list/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaxDef> getAlllist() {
        return taxDetailsService.getAllTaxDetails();
    }
    
    @RequestMapping(value = "/update/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    boolean updateTaxDetails(@RequestParam("taxData") String jsonInput) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<TaxDef> dataList = mapper.readValue(jsonInput, new TypeReference<List<TaxDef>>() {});        
        return taxDetailsService.updateTaxDetails(dataList);
    }
}
