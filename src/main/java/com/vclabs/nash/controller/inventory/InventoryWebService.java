package com.vclabs.nash.controller.inventory;

import com.vclabs.nash.controller.inventory.dto.InventoryContainer;
import com.vclabs.nash.model.entity.inventory.Inventory;

import java.util.Date;

/**
 * Created by Nalaka on 2018-09-26.
 */
public interface InventoryWebService {

    InventoryContainer loadByChannelAndDate(int channelId, Date date);

    InventoryContainer loadByChannel(int channelId);

    Inventory createInventory(Inventory originalInventory, Date date);

    void save(InventoryContainer inventoryContainer);
}
