package com.vclabs.nash.controller.product;

import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.service.product.BrandService;
import com.vclabs.nash.service.product.CodeMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nalaka on 2019-08-29.
 */
@RestController
@RequestMapping("/brand")
public class BrandController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrandController.class);

    @Autowired
    BrandService brandService;

    @Autowired
    CodeMappingService codeMappingService;

    @RequestMapping(value = "/allBrand/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Brand> brandstAllList() {
        return brandService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/saveBrand", method = RequestMethod.POST, produces = "application/json")
    public CodeMapping saveBrand(@RequestBody CodeMapping codeMapping) {
        return codeMappingService.save(codeMapping);
    }

    @ResponseBody
    @RequestMapping(value = "/updateBrand", method = RequestMethod.POST, produces = "application/json")
    public CodeMapping updateBrand(@RequestBody CodeMapping codeMapping) {
        return codeMappingService.save(codeMapping);
    }

    @ResponseBody
    @RequestMapping(value = "/validateCode", method = RequestMethod.GET, produces = "application/json")
    public Boolean validateCode(@RequestParam("code") String code) {
        return codeMappingService.vailateCocde(code);
    }
}
