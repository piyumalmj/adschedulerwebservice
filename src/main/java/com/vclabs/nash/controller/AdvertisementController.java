/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.vclabs.nash.model.process.dto.AdvertisementDropDownDataDto;
import com.vclabs.nash.model.view.GeneralAuditView;
import com.vclabs.nash.model.view.Message;
import com.vclabs.nash.service.exception.NashException;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.vclabs.nash.model.entity.AdvertCategories;

import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.view.AdvertisementDefView;
import com.vclabs.nash.service.AdvertisementService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/advertisement")
public class AdvertisementController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AdvertisementController.class);

    @Autowired
    private AdvertisementService advertService;

    @RequestMapping(value = "/getbyid/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Advertisement getAdvertisement(@RequestParam("id") int advertId) {
        return advertService.getSelectedAdvertisement(advertId);
    }

    @RequestMapping(value = "/advertisementAllList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> advertisementAllList() {
        return advertService.getAdvertisementAllList();
    }
    
    @ResponseBody
    @RequestMapping(value = "/advertisementAllList/page")
    public Page<Advertisement> getAllAdvertList(@RequestParam Map<String, String> filterRequest) {
       return advertService.getAllAdvertList(filterRequest);
    }

    @RequestMapping(value = "/advertisementAllListOrderByName/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdvertisementDropDownDataDto> advertisementAllListOrderByName() {
        return advertService.getAdvertisementAllListOrderByName();
    }

    @RequestMapping(value = "/advertisementAllListValid/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> advertisementAllListValid() {
        return advertService.getAdvertisementAllListValid();
    }

    @RequestMapping(value = "/advertisementFilterList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> advertisementFilterList(@RequestParam("fiterData") String fiterData) {
        return advertService.getAdvertisementFilterList(fiterData);
    }

    @RequestMapping(value = "/advertisementListForZeroAds/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> advertisementListForZeroAds(@RequestParam("fiterData") String fiterData) {
        return advertService.getAdvertisementForTheZeroManualList(fiterData);
    }

    @RequestMapping(value = "/advertisementClientList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdvertisementDefView> advertisementClientList(@RequestParam("clieanId") int clieanId) {
        return advertService.getClientAdvertisementList(clieanId);
    }

    @RequestMapping(value = "/advertisementClientListValid", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdvertisementDefView> advertisementClientListValid(@RequestParam("clieanId") int clieanId) {
        return advertService.getClientAdvertisementListValid(clieanId);
    }

    @RequestMapping(value = "/advertisementSelectedClientList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> advertisementSelectedClientList(@RequestParam("clieanOne") int clieanIdOne, @RequestParam("clieanTwo") int clieanIdTwo) {
        return advertService.getClientAdvertisement(clieanIdOne, clieanIdTwo);
    }

    @RequestMapping(value = "/advertisementSave/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Integer advertisementSave(@RequestParam("advertData") String advertData) {
        return advertService.saveAdvertisement(advertData);
    }

    @RequestMapping(value = "/advertisementMetaDataUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementMetaDataUpdate(@RequestParam("advertData") String advertData) {
        return advertService.updateAdvertisementMetaData(advertData);
    }

    @RequestMapping(value = "/advertisementDelete/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementDelete(@RequestParam("advertid") int advertid) {
        return advertService.deleteAdvertisement(advertid);
    }

    @RequestMapping(value = "/advertisementDeleteByStatus/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementDeleteByStatus(@RequestParam("advertid") int advertid, HttpServletRequest request) {
        return advertService.deleteAdvertisementByStatus(advertid, request);
    }

    @RequestMapping(value = "/advertisementSuspend/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementSuspend(@RequestParam("advertid") int advertid) {
        try {
            return advertService.suspendAdvert(advertid);
        }catch (Exception e){
            LOGGER.debug("Exception in AdvertisementService in suspendAdvert : {}", e.getMessage());
            return false;
        }
    }

    @RequestMapping(value = "/advertisementResume/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementResume(@RequestParam("advertid") int advertid) throws Exception {
        return advertService.resumeAdvert(advertid);
    }

    @RequestMapping(value = "/upload_file", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> handleFileUpload(MultipartHttpServletRequest request)  {
        try {
            Advertisement result = advertService.fileUploader(request);
            return  new ResponseEntity(result, HttpStatus.OK);
        } catch (NashException e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/advertisementCategoryAllList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdvertCategories> advertisementCategoryAllList() {
        return advertService.getAllAdvertCategory();
    }

    @RequestMapping(value = "/advertisementCategorySave/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Message advertisementCategorySave(@RequestParam("category") String category) {
        return advertService.saveAdvertisementCategory(category);
    }

    @RequestMapping(value = "/advertisementCategoryUpdate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementCategoryUpdate(@RequestParam("category_id") int category_id, @RequestParam("category") String category) {
        return advertService.updateAdvertisementCategory(category_id, category);
    }

    @RequestMapping(value = "/getAdvertHistory/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<GeneralAuditView> getAdvertHistory(@RequestParam("advertId") int advertId) {
        return advertService.getAdvertHistory(advertId);
    }

}
