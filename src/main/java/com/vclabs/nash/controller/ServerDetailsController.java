/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vclabs.nash.model.entity.ServerDetails;
import com.vclabs.nash.service.ServerDetailsService;

/**
 * @author Sanira Nanayakkara
 */

@RestController
@RequestMapping("/serverdetails")
public class ServerDetailsController {
    
    @Autowired
    private ServerDetailsService serverDetailsService;
    
    @RequestMapping(value = "/list/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<ServerDetails> getServerList() {
        return serverDetailsService.getServerList();
    }

    @RequestMapping(value = "/getServerbyId/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ServerDetails getServerDetails(@RequestParam("serverId")int iServerId) {
        return serverDetailsService.getServerDetails(iServerId);
    }
    
    @RequestMapping(value = "/insertServer/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Integer insertServerDetails(@RequestParam("serverData")String modelData) {
        return serverDetailsService.insertServerDetails(modelData);
    }
   
    @RequestMapping(value = "/updateServerBasicData/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody boolean updateBasicServerDetails(@RequestParam("serverData")String modelData){
        return serverDetailsService.updateBasicServerDetails(modelData);
    }
    
    @RequestMapping(value = "/updateServerDynamicData/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody boolean updateDynamicServerDetails(@RequestParam("serverData")String modelData){
         return serverDetailsService.updateDynamicServerDetails(modelData);
    }
}
