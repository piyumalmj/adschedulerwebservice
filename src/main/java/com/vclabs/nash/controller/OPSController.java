package com.vclabs.nash.controller;

import com.vclabs.nash.service.OPSBiddingService;
import com.vclabs.nash.service.OPSCustomScheduleService;
import com.vclabs.nash.service.OPSPromotionService;
import com.vclabs.nash.service.OPSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping("/ops")
public class OPSController {

    @Autowired
    OPSService opsService;
    @Autowired
    OPSCustomScheduleService opsCustomScheduleService;
    @Autowired
    OPSBiddingService opsBiddingService;
    @Autowired
    OPSPromotionService opsPromotionService;

    @ResponseBody
    @RequestMapping(value = "/workorderTest", method = RequestMethod.GET)
    public void TestWorkOrder() throws ParseException {
        opsService.saveWorkOrder();
    }

    @ResponseBody
    @RequestMapping(value = "/importCustomSchedules", method = RequestMethod.GET)
    public int importCustomSchedules() {
        try {
            return opsCustomScheduleService.saveCustomDeal();
        } catch (ParseException ex) {
            System.out.println(ex);
            return 0;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/importDeal", method = RequestMethod.GET)
    public int importDeal() throws ParseException {
        return opsService.saveWorkOrder();
    }

    @ResponseBody
    @RequestMapping(value = "/importBidItem", method = RequestMethod.GET)
    public int importBidItems() {
        try {
            return opsBiddingService.saveBitItems();
        } catch (ParseException ex) {
            System.out.println(ex);
            return 0;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/importPromotion", method = RequestMethod.GET)
    public int importPromotion() {
        try {
            return opsPromotionService.savePromotion();
        } catch (ParseException ex) {
            System.out.println(ex);
            return 0;
        }
    }
}
