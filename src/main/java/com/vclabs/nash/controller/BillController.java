/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vclabs.nash.model.entity.InvoiceData;

import com.vclabs.nash.model.entity.TaxDef;
import com.vclabs.nash.model.view.BillingView;
import com.vclabs.nash.model.view.InvoiceView;
import com.vclabs.nash.service.BillingService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/bill")
public class BillController {

    @Autowired
    private BillingService billingService;

    @RequestMapping(value = "/getBillSpotDetails/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<BillingView> getBillSpotDetails(@RequestParam("workOrderId") int workOrderId) {
        return billingService.getBillingDetails(workOrderId);
    }

    //tested
    @RequestMapping(value = "/getAllTaxDetails/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaxDef> getAllTaxDetails() {
        return billingService.getAllTaxDetails();
    }

    //tested
    @RequestMapping(value = "/saveInvoice/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int saveInvoice(@RequestParam("invoiceData") String invoiceData, HttpServletRequest request) throws Exception {
        return billingService.saveInvoice(invoiceData, request);
    }

    //tested
    @RequestMapping(value = "/getAllInvoice/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<InvoiceView> getAllInvoice() {
        return billingService.getAllInvoice();
    }

    @RequestMapping(value = "/getAgencyCommission/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    TaxDef getAgencyCommission() {
        return billingService.getAgencyCommission();
    }

    @RequestMapping(value = "/downloadBill/json", method = RequestMethod.GET)
    public void downloadPDF(HttpServletRequest request, HttpServletResponse response) {
        billingService.downloadBillPDF(request, response);
    }

    @RequestMapping(value = "/downloadSap/json", method = RequestMethod.GET)
    public void downloadSap(HttpServletResponse response, @RequestParam("invoiceId") int invoiceId) {
        billingService.downloadSAPFile(response, invoiceId);
    }

    //tested
    @RequestMapping(value = "/generatedBill/json", method = RequestMethod.GET)
    public Boolean generatedBill(HttpServletRequest request, @RequestParam("invoiceId") int invoiceId) {
        return billingService.getSelectedInvoiceAndPrint(request, invoiceId);
    }

    //tested
    @RequestMapping(value = "/getInvoiceDetails/json", method = RequestMethod.GET)
    public InvoiceData getInvoiceDetails(@RequestParam("invoiceId") int invoiceId) {
        return billingService.getInvoiceData(invoiceId);
    }

    @RequestMapping(value = "/saveInvoiceDetails/json", method = RequestMethod.GET)
    public InvoiceData saveInvoiceDetails(@RequestParam("invoiceData") String invoiceData) throws IOException {
        return billingService.saveInvoiceDetails(invoiceData);
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void saveInvoiceDetails()  {
        billingService.updateSapFilePath();
    }
}
