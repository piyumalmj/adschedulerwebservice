/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.util.List;

import com.vclabs.nash.model.process.dto.ChannelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.service.ChannelDetailService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/channeldetail")
public class ChannelDetailController {

    @Autowired
    private ChannelDetailService channelService;

    @RequestMapping(value = "/list/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ChannelDetails> getAlllist() {
        return channelService.ChannelList();
    }
    
    @RequestMapping(value = "/getAllChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ChannelDetails> getAllChanel() {
        return channelService.AllChannelList();
    }

    //tested
    @RequestMapping(value = "/listOrderByName/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ChannelDetails> listOrderByName() {
        return channelService.ChannelListOrderByName();
    }

    @RequestMapping(value = "/saveAdnUpdateChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int saveAdnUpdateChannel(@RequestParam("channelData") String channelData) {
        return channelService.saveAndUpdateChannel(channelData);
    }

    @RequestMapping(value = "updateChannelProperties/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    boolean updateChannelProperties(@RequestParam("channelData") String channelData) {
        return channelService.updateChannelProperties(channelData);
    }

    //Insertion
    @RequestMapping(value = "/getSelectedChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ChannelDetails getSelectedChannel(@RequestParam("channelId") int channelId) {
        return channelService.getChannel(channelId);
    }

    @RequestMapping(value = "/setSelectedChannelTimeBelt/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean setSelectedChannelTimeBelt(@RequestParam("channelId") int channelId) {
        return channelService.setChannelTimeBelt(channelId);
    }

    @RequestMapping(value = "/disableChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    boolean disableChannel(@RequestParam("channelId") int channelId) {
        return channelService.disableChannel(channelId);
    }

    @RequestMapping(value = "/enableChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    boolean enableChannel(@RequestParam("channelId") int channelId) {
        return channelService.enableChannel(channelId);
    }

    @RequestMapping(value = "/getAllAutoCahnnels/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ChannelDetails> getAutoChannels() {
        return channelService.getAllAutoChannels();
    }

    @ResponseBody
    @RequestMapping(value = "/createNewChannel", method = RequestMethod.POST, produces = "application/json")
    public Integer createNewChannel(@RequestBody ChannelDto channelDto) {
        return channelService.createNewChannel(channelDto.getChannelname());
    }

    @RequestMapping(value = "/getAllChannels", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ChannelDto> getAllChannels() {
        return channelService.getAllChannels();
    }
}
