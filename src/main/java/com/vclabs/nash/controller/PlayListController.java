/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.util.Date;

import com.vclabs.nash.model.view.ZeroAndFillerPlayListBackendView;
import com.vclabs.nash.service.PlayListService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import com.vclabs.nash.model.view.PlayListBackendView;
import com.vclabs.nash.model.view.ScheduleView;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/playlist")
public class PlayListController {

    @Autowired
    private PlayListService playListService;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PlayListController.class);

    @RequestMapping(value = "/backend/getplaylist", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ScheduleView getPlayList_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour) throws Exception {
        logger.info("Call to getPlayList_BackEnd 1: channelId: {} hour: {}", _channelId,_hour);
        return playListService.getPlayList_BackEnd(_channelId, _hour);
    }

    @RequestMapping(value = "/backend/getplaylist_byperiod", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ScheduleView getPlayList_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour, @RequestParam("period") int _period) throws Exception {
        logger.info("Call to getPlayList_BackEnd 2: channelId: {} hour: {}", _channelId,_hour);
        return playListService.getPlayList_BackEnd(_channelId, _hour, _period);
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean advertisementUpdate_BackEnd(@RequestParam("id") int _id, @RequestParam("start_time") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.S") Date actualStartTime, @RequestParam("end_time") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.S") Date actualEndTime, @RequestParam("state") String state) {
        logger.info("Call to advertisementUpdate_BackEnd: advertId: {}", _id);
        return playListService.updatePlayList_BackEnd(_id, actualStartTime, actualEndTime, state);
    }

    @RequestMapping(value = "/backend/write_playlist", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean writeTestPlayList_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour) {
        logger.info("Call to writeTestPlayList_BackEnd: channelId: {} hour: {}", _channelId,_hour);
        return playListService.writePlayList_BackEnd(_channelId, _hour);
    }

    @RequestMapping(value = "/backend/generate_playlist", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean generatePlayList_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour) {
        logger.info("Call to generatePlayList_BackEnd: channelId: {} hour: {}", _channelId,_hour);
        return playListService.generatePlayList_BackEnd(_channelId, _hour, 0);
    }

    @RequestMapping(value = "/backend/is_schedule_generated", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean isScheduleGenerated_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour) {
        //logger.info("Call to isScheduleGenerated_BackEnd: channelId: {} hour: {}", _channelId,_hour);
        return playListService.isScheduleGenerated(_channelId, _hour);
    }

    @RequestMapping(value = "/backend/get_filler", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ZeroAndFillerPlayListBackendView getFiller_BackEnd(@RequestParam("channel_id") int _channelId, @RequestParam("hour") int _hour) throws Exception {
        logger.info("Call to getFiller_BackEnd: channelId: {} hour: {}", _channelId,_hour);
        ZeroAndFillerPlayListBackendView playList = playListService.GetFillerSchedule(_channelId, _hour);
        if (playList == null) {
            throw new Exception("Null Filler");
        }
        return playList;
    }

    @RequestMapping(value = "/backend/test", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean test() throws Exception {
        playListService.autoScheduleGenerateCallerAsync();
        return true;
    }
}
