/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.io.IOException;
import java.util.List;

import com.vclabs.nash.model.process.MoshDetails;
import com.vclabs.nash.model.view.MoshMsg;
import com.vclabs.nash.service.ChannelAdvertMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vclabs.nash.model.entity.PageName;
import com.vclabs.nash.model.view.UserGroupView;
import com.vclabs.nash.model.view.UserView;
import com.vclabs.nash.service.UserDetailsService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/userdetaills")
public class UserDetailsController {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsController.class);

    @Autowired
    private UserDetailsService userdetailservice;

    @RequestMapping(value = "/login/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    MoshMsg login(@RequestParam("username") String userName, @RequestParam("password") String passWord,@RequestParam("ipAddress") String ipAddress,@RequestParam("channelId") int channnelId) {
        logger.debug("Started Mosh login request processing. [ user = {} ] ", userName);
        MoshDetails moshDetails = new MoshDetails();
        moshDetails.setPassWord(passWord);
        moshDetails.setUser(userName);
        moshDetails.setIpAddress(ipAddress);
        moshDetails.setChannelId(channnelId);
        return userdetailservice.getLoginPermission(userName, passWord, moshDetails);
    }

    @RequestMapping(value = "/logout/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    void logout(@RequestParam("id") int id) {
        MoshDetails moshDetails = new MoshDetails();
        moshDetails.setId(id);
        userdetailservice.moshUserLogout(moshDetails);
    }

    @RequestMapping(value = "/newuser/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Boolean newUser(@RequestParam("username") String userName, @RequestParam("userdetails") String details, @RequestParam("dashboardGroups")String dashboardGroups) throws Exception {
        try {
            return userdetailservice.setUserDetails(userName, details, dashboardGroups);
        } catch (Exception e) {
            throw e;
        }
    }

    @RequestMapping(value = "/allUser/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List<UserView> getAllUser() throws Exception {
        try {
            return userdetailservice.getAllUser();
        } catch (Exception e) {
            throw e;
        }
    }

    //tested
    @RequestMapping(value = "/getMeUser/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List<UserView> getMEUser() throws Exception {
        try {
            return userdetailservice.getMEUser();
        } catch (Exception e) {
            throw e;
            //return false;
        }
    }

    @RequestMapping(value = "/updateuser/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    int updateUser(@RequestParam("oldUsername") String oldUserName, @RequestParam("oldPassword") String oldPassword, @RequestParam("newUsername") String newUserName, @RequestParam("newPassword") String newPassword) {
        String group = "No_Group";
        int status = userdetailservice.updateUser(oldUserName, newUserName, oldPassword, newPassword, group);

        if (status == 1) {
            return 1;
        } else if (status == 0) {
            return 0;
        } else {
            return -1;
        }
    }

    //tested
    @RequestMapping(value = "/updateuserinfo/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Boolean updateUserInfo(@RequestParam("username") String userName, @RequestParam("userdetails") String userInfo
            , @RequestParam("dashboardGroups")String dashboardGroups) throws IOException {
        return userdetailservice.updateUserInfo(userName, userInfo, dashboardGroups);
    }

    @RequestMapping(value = "/getSelecteduser/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    UserView getSelectedUser(@RequestParam("username") String userName) {
        return userdetailservice.getSelectedUser(userName);
    }

    @RequestMapping(value = "/test/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    UserView test(@RequestParam("username") String userName) {

        return userdetailservice.getSelectedUser(userName);
    }

    @RequestMapping(value = "/setStatus/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean setStatus(@RequestParam("username") String userName, @RequestParam("status") int status) {

        return userdetailservice.setUserStatus(userName, status);
    }

    @RequestMapping(value = "/getChecker/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int getChecker(@RequestParam("username") String userName) {
        return userdetailservice.getCheckar(userName);
    }

    @RequestMapping(value = "/getAllPage/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<PageName> getAllPage() throws Exception {
        return userdetailservice.getAllPage();
    }

    @RequestMapping(value = "/getAllUserGroup/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<UserGroupView> getAllUserGroup() throws Exception {

        return userdetailservice.getAllUserGroup();
    }

    @RequestMapping(value = "/saveUserGroup/json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean saveUserGroup(@RequestParam("groupData") String groupData) throws Exception {

        return userdetailservice.saveUserGroup(groupData);
    }

    @RequestMapping(value = "/allTeamlessUser/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List<UserView> getTeamlessUser() throws Exception {
            return userdetailservice.getTeamlessUser();
    }
}
