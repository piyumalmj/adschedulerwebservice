// worder_data
var workOrderId = -1;
var workOrderName = -1;
var startDate;
var endDate = '';
var workOrderStatus = -1; /// wo is new?
var url_data = ''; /// page url

var startTimeDrop = [];
var endTimeDrop = [];
var default_tr_left = '';
var default_tr_data = '';
var tableData = '';
var json_data_view = {}; /// original schedule data for filtering
var addedAdverts = {};

/// inplace_edit ///////////////////////////////////
var iRowID = 0;
var iColumnMaxID = 0;
var _RowIDIncrement = 1000;
var mapAdverts = {};
var lstAdverts = [];
var suspendLstAdverts = []
var mapChannels = {};
var lstChannels = [];
var mapChannelAvailableSpot = {};
var mapScheduleMetaData = {}; // table left
var mapScheduleMetaDataCurrent = {}; // table left
var mapEditBoxData = {};
var mapEditBoxScheduleID = {};
var mapEditBoxDataCurrent = {}; // not saved schedule data
var mapEditBoxDate = {};
var selectedChannelId = -999; // in insert row
var selectedAdvertId = -1; // in insert row
var iInsertCount = 0; /// spot count in insert row

var mapColumnCount = {}; /// column spot count
var mapRowCount = {};
var fetchDone;
var _channelDrop;
var _advertDropDown;
$(document).ready(function () {
    showDialog();
    fetchDone = _.after(6, function(){
        setDataToAddRow();
        closeDialog();
    });
    workOrderId = stoi(getParameterByNamefromUrl('work_id'));
    createUpdateScheduleView();
    setWorkOrderDetails();
    createScheduleTable();
    setChannelDetails();
    setAdvertTable();
    setTimeDropDown();
});

function setDataToAddRow(){
    $('#channelDropdown_div').html(_channelDrop);
    $("#schedule_channelDropdown").val(-999);
    $('#advertDropdown_div').html(_advertDropDown);
}

/*-------------------------------------WorkOrder----------------------------------------------------*/
function setWorkOrderDetails() //
{
    workOrderId = stoi(getParameterByNamefromUrl('work_id'));
    startDate = getParameterByNamefromUrl('start_date');
    endDate = getParameterByNamefromUrl('end_date');
    workOrderName = getParameterByNamefromUrl('work_name');
    url_data = '?work_id=' + workOrderId + '&work_name=' + workOrderName + '&start_date=' + startDate + '&end_date=' + endDate;
    $('#work_order_id').html(workOrderId);
    $('#start_date').html(startDate);
    $('#end_date').html(endDate);
    getWODetails(workOrderId);
}

function getWODetails(wo_id) {
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.selectedWorkOrderUrl(),
        data: {
            workOrderId: wo_id
        },
        success: function (data)
        {
            setAdvertisementDetails(data[0].clientName, data[0].billingClient);
            workOrderStatus = data[0].workOrderStatus;
            document.getElementById("lbl_agency").innerHTML = data[0].agencyByName;
            document.getElementById("lbl_client").innerHTML = data[0].clientByName;
            document.getElementById("schedule_value").innerHTML = getDecimal(data[0].totalbudget);
            document.getElementById("lbl_comment").innerHTML = data[0].comment;
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    });
}
/*-------------------------------------End workOrder------------------------------------------------*/


/*----------------------------Advertisement--------------------------------*/
function addAdvertisement()/// advert_selecting_popup
{
    var advert = document.getElementById("advertDropdown_pop");
    var advertId = advert.options[advert.selectedIndex].id;
    var advertDuration = advert.options[advert.selectedIndex].innerHTML;
    var advertName = (advert.options[advert.selectedIndex].value).split("_");
    var advertTable_p = document.getElementById("advertisement_table_pop");
    var rowLength = advertTable_p.rows.length;
    for (i = 1; i < rowLength; i++)
    {
        var oCells = advertTable_p.rows.item(i).cells;
        if (oCells.item(0).id == advertId) {
            alert("This advertisement alredy added");
            return;
        }
    }

    if (advertId != -9) {
        var advertTable = document.getElementById("advertisement_table_pop");
        var rowCount = advertTable.rows.length;
        var row = advertTable.insertRow();
        row.id = rowCount + "row_pop";
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        cell1.id = advertId;
        cell1.innerHTML = '<a href="#" id="' + advertId + '" onclick="getId(this.id)">' + advertId + '</a>';
        cell2.innerHTML = advertDuration;
        cell3.innerHTML = advertName[0];
        cell4.innerHTML = getAdvertType(advertName[1]);
        cell5.innerHTML = '<button type="submit" id="' + rowCount + '" onclick="deleteAdvert((this.id))" class="btn btn-default sche_advert_delete">Delete</button>';
        advert.selectedIndex = 0;
    }
}

function saveAdvertisement()// advert_selecting_popup
{
    var advertArray = '[';
    var advertTable_p = document.getElementById("advertisement_table_pop");
    var rowLength = advertTable_p.rows.length;
    var advertId_P = "";
    var workOrderId_p = workOrderId;
    var count = 0;
    for (i = 1; i < rowLength; i++)
    {
        var oCells = advertTable_p.rows.item(i).cells;
        var cellLength = oCells.length;
        for (var j = 0; j < cellLength; j++)
        {
            if (j == 0)
            {
                advertId_P = oCells.item(j).id;
            }
        }
        if (cellLength != 0)
        {
            if (count != 0)
            {
                advertArray += ',';
            }
            advertArray += '{"advertisementId":' + advertId_P + ',"workOrderId":' + workOrderId_p + '}';
            count++;
        }
    }
    advertArray += ']';
    if (advertArray == '[]') {
        advertArray = '[{"advertisementId":0,"workOrderId":' + workOrderId_p + '}]';
    }
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.orderAdvertListSaveUrl(),
        data: {
            workOrderAdvertList: advertArray
        },
        success: function (data)
        {
            closeDialog();
            if (data == 1) {
               // window.location = web_page_path + "/CurrentSchedule.html" + url_data;
               window.location = URI.scheduler.getCurrentSchedulePageUrl(url_data);
            } else if (data == 3) {
                alert("Invalid operation, this advertisement is alredy scheduled");
            } else {
                alert("Error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
    $('#addAdvertisementModal').modal('toggle');
}

function setAdvertisementDetails(clientOne, ClientTwo)// advert_selecting_popup
{
    var v_clieanId = 1;
    var advertDrop = '<select id="advertDropdown_pop" name="channelName" class="form-control selectpicker" data-live-search="true"><option id="-9" value="30">Select Advertsement</option>';
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSelectedClientListUrl(),
        data: {
            clieanOne: clientOne,
            clieanTwo: ClientTwo
        },
        success: function (data)
        {
            for (var i in data)
            {
                advertDrop += '<option id=' + data[i].advertid + ' data-tokens="' + data[i].advertname + '" value="' + data[i].duration + '_' + data[i].adverttype + '_' + data[i].advertid + '">' + data[i].advertid + '-' + data[i].advertname + '</option>';
                duration[data[i].duration] = '<option value=' + data[i].duration + '>' + data[i].duration + '</option>';
            }
            advertDrop += '</select>';
            $('#advert_list_pop_div').html(advertDrop);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    });
}
advertCategoryMap = {};
function setAdvertTable() // create all_advert dropdown in schedule table and all advert table
{
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.clientOrderAdvertListUrl(),
        data: {
            workOrderId: workOrderId,
            advertType: 'All'
        },
        success: function (data)
        {
            var filter_adverDuration = '';//<option value="-999" selected>All Duaration</option>
            var filter_advertDrop = '';//<option value="-999" selected="selected">All Advertisement</option>
            var advertDropDown = '<select id="schedule_advertDropdown" onchange="changeAdvert()" class="form-control Padding0px"><option id="-9_advert" value="-1">Select Advert</option>';
            var advertTableMain = '<table style="margin-top: 12px;  margin-left: 12px;" id="advert_main_table" class="table table-bordered table-workorder"><thead><tr><th>ID</th><th>Name</th><th>Duration(Sec)</th><th>Type</th></tr></thead><tbody>';
            $.each(data, function (i, item) {
                mapAdverts[item.advertId] = item;

                var advert = data[i];
                addedAdverts[i] = advert;
                if (item.enabled === 1) {
                    lstAdverts.push(item);

                    if (!item.isFileAvailable) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "dummy-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="dummy-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="dummy-avert">' + item.duration + '</option>';
                        advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '"  class="dummy-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="dummy-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="dummy-avert">' + item.advertName + '</td><td class="dummy-avert">' + item.duration + '</td><td class="dummy-avert">' + getAdvertType(item.type) + '</td></tr>';
                    } else if (item.enabled === 0 && item.status === 1) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "expir-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="expir-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="expir-avert">' + item.duration + '</option>';
                        advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '" class="expir-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="expir-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="expir-avert">' + item.advertName + '</td><td class="expir-avert">' + item.duration + '</td><td class="expir-avert">' + getAdvertType(item.type) + '</td></tr>';
                    } else if (item.enabled === 0 && item.status === 2) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "suspend-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="suspend-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="suspend-avert">' + item.duration + '</option>';
                        advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '" class="suspend-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="suspend-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="suspend-avert">' + item.advertName + '</td><td class="suspend-avert">' + item.duration + '</td><td class="suspend-avert">' + getAdvertType(item.type) + '</td></tr>';
                    } else {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + '>' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + '>' + item.duration + '</option>';
                        advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '">' + item.advertId + '-' + item.advertName + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td>' + item.advertName + '</td><td>' + item.duration + '</td><td>' + getAdvertType(item.type) + '</td></tr>';
                    }
                   // advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '">' + item.advertId + '-' + item.advertName + '</option>';
                } else {
                    if (!item.isFileAvailable) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "dummy-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="dummy-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="dummy-avert">' + item.duration + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="dummy-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="dummy-avert">' + item.advertName + '</td><td class="dummy-avert">' + item.duration + '</td><td class="dummy-avert">' + getAdvertType(item.type) + '</td></tr>';
                    } else if (item.enabled === 0 && item.status === 1) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "expir-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="expir-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="expir-avert">' + item.duration + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="expir-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="expir-avert">' + item.advertName + '</td><td class="expir-avert">' + item.duration + '</td><td class="expir-avert">' + getAdvertType(item.type) + '</td></tr>';
                    } else if (item.enabled === 0 && item.status === 2) {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, class: "suspend-avert", text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + ' class="suspend-avert">' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + ' class="suspend-avert">' + item.duration + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '" class="suspend-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td class="suspend-avert">' + item.advertName + '</td><td class="suspend-avert">' + item.duration + '</td><td class="suspend-avert">' + getAdvertType(item.type) + '</td></tr>';
                        suspendLstAdverts.push(item);
                    } else {
                        $('#original_advertDropdown_pop').append($('<option>', {value: item.advertId, text: item.advertId + '-' + item.advertName}));
                        filter_advertDrop += '<option value=' + item.advertId + '>' + item.advertId + '-' + item.advertName + '</option>';
                        filter_adverDuration += '<option value=' + item.duration + '>' + item.duration + '</option>';
                        advertTableMain += '<tr><td id="' + item.advertId + '"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td>' + item.advertName + '</td><td>' + item.duration + '</td><td>' + getAdvertType(item.type) + '</td></tr>';
                    }
                }
               // filter_advertDrop += '<option value=' + item.advertId + '>' + item.advertId + '-' + item.advertName + '</option>';
               // filter_adverDuration += '<option value=' + item.duration + '>' + item.duration + '</option>';
               // advertTableMain += '<tr><td id="' + item.advertId + '" class="suspend-avert"><a href="#" id="' + item.advertId + '" onclick="getId(this.id)">' + item.advertId + '</a></td><td>' + item.advertName + '</td><td>' + item.duration + '</td><td>' + getAdvertType(item.type) + '</td></tr>';
                $("#advertDropdown_pop option[value='" + item.duration + '_' + getAdvertType(item.type) + '_' + item.advertId + "']").remove();
                advertCategoryMap[item.type] = item.type;
            });
            advertDropDown += '</select>';
            filter_adverDuration += '</select>';
            advertTableMain += '</tbody></table>';
            _advertDropDown = advertDropDown;
            //$('#advertDropdown_div').html(advertDropDown);
            $('#advert_main_table_div').html(advertTableMain);
            $('#advert_list_table').hide();
            $('#advertTypeDropdown').val("ADVERT");
            $('#filter_AdvertDrop').html(filter_advertDrop);
            $('#filter_AdvertDuratinoDrop').html(filter_adverDuration);
            setAdvertCategory();
            configurFilter();
            $('#filter_AdvertDrop').selectpicker('refresh');
            $('#filter_AdvertDuratinoDrop').selectpicker('refresh');
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function setAdvertCategory() {
    var advCategory = '<select id="filter_Advertcategory" onchange="filter_AdvertCatagoryChange()" class="form-control" >';
    advCategory += '<option value="-999" selected>All Categories</option>';
    for (var key in advertCategoryMap) {
        advCategory += '<option value="' + advertCategoryMap[key] + '">' + getAdvertType(advertCategoryMap[key]) + '</option>';
    }
    advCategory += '< /select>';
    $('#advert_category_div').html(advCategory);
}

function getId(id) {
    window.open(URI.advertisement.updateAdvertisementUrl(id));
}

function deleteAdvert(id)// advert_selecting_popup
{
    var ret = confirm("Do you want to delete this Advertisement?");
    if (!ret)
        return;
    var row = document.getElementById(id + "row_pop");

    var advert = addedAdverts[id];
    if(advert !== undefined){
        if( advert.canDelete){
            row.parentNode.removeChild(row);
        }else{
            viewMessageToastTyped("This advertisement is alredy scheduled.", "Warning");
        }
    }else{

    }
}

function changeAdvert()//schedule table advert change
{
    if ($("#schedule_advertDropdown").val() == -1) {
        $("#schedule_advertDropdown").val(selectedAdvertId);
        return;
    }

    var advert_id = $("#schedule_advertDropdown").val();
    var advert = mapAdverts[advert_id];
    var type = advert.type;
    var iDuration = advert.duration;
    if (type == 'L' || type == 'V') {
        type += '_SHAPE';
    }
    $('#advertTypeDropdown').val(type);
    $("#language").val(setLanguage_returnFistLetter(advert.language));
    $("#duration").val(iDuration);
    var channel_id = $("#schedule_channelDropdown").val();
    if (channel_id != -999) {
        var row_advert_count = getInsertRowAdvertCount();
        spotCountChanged(channel_id, selectedAdvertId, -row_advert_count);
        spotCountChanged(channel_id, advert_id, row_advert_count);
    }
    setSpotTable(channel_id);
    selectedAdvertId = advert_id;
}

function advertSelectionChange()//
{
    var advert_type = $("#advertTypeDropdown").val();
    var advertDropDown = '<select id="schedule_advertDropdown" onchange="changeAdvert()" class="form-control Padding0px"><option id="-9_advert" value="-1">Select Advert</option>';
    $.each(lstAdverts, function (i, item)
    {
        if (item.type == advert_type)
            advertDropDown += '<option id="' + i + '_advert" value="' + item.advertId + '">' + item.advertId + '-' + item.advertName + '</option>';
    });
    advertDropDown += '</select>';
    $('#advertDropdown_div').html(advertDropDown);
}

function setPopUpAdvertTable() {
    var advetTable = '<table id="advertisement_table_pop" class="table table-bordered table-workorder">';
    advetTable += '<thead class="tb_align_center"><tr><th>ID</th><th>Name</th><th>Duration(Sec)</th><th>Type</th><th></th></tr></thead><tbody>';
    for (var i in lstAdverts) {
        advetTable += '<tr id="' + i + 'row_pop" class="tb_align_center"><td id="' + lstAdverts[i].advertId + '"><a href="#" id="' + lstAdverts[i].advertId + '" onclick="getId(this.id)">' + lstAdverts[i].advertId + '</a></td><td>' + lstAdverts[i].advertName + '</td><td>' + lstAdverts[i].duration + '</td><td>' + getAdvertType(lstAdverts[i].type) + '</td><td><button type="submit" id="' + i + '" onclick="deleteAdvert((this.id))" class="btn btn-default sche_advert_delete">Delete</button></td></tr>';
    }
    advetTable += '</tbody></table>';
    $('#advertisement_table_pop_div').html(advetTable);
}
/*---------------------------end Advertisement-----------------------------*/


/*---------------------------Date--------------------------------------------*/
function addDays(date, days, type) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    date = yyyy + '/' + mm + '/' + dd;
    if (type == 2)
    {
        return mm + '-' + dd;
    } else if (type == 3) {
        return dd;
    } else {
        return yyyy + '-' + mm + '-' + dd;
    }
}

function getMonthL() {
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    var date = '';
    var mont = '';
    var m = '';
    var months = '';
    for (var i = 0; i <= timeDiff; i++)
    {
        date = addDays(startDate, i, 1);
        mont = getMonth(date);
        if (m != mont) {
            if (months == '') {
                months = mont;
            } else {
                months = months + "/" + mont;
            }
            m = mont;
        }
    }

    return months;
}

function getMonth(date) {

    var result = new Date(date);
    result.setDate(result.getDate());
    var mm = result.getMonth();
    var month = '';
    switch (mm) {
        case 0:
            month = "January";
            break;
        case 1:
            month = "February";
            break;
        case 2:
            month = "March";
            break;
        case 3:
            month = "April";
            break;
        case 4:
            month = "May";
            break;
        case 5:
            month = "June";
            break;
        case 6:
            month = "July";
            break;
        case 7:
            month = "August";
            break;
        case 8:
            month = "September";
            break;
        case 9:
            month = "October";
            break;
        case 10:
            month = "November";
            break;
        case 11:
            month = "December";
            break;
    }

    return month;
}

function getDifferanBetweenDay(dt1, dt2) {
    var date1 = new Date(dt1);
    var date2 = new Date(dt2);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
}

function getDay(date) {
    var d = new Date(date);
    var num = d.getDay();
    var day = '';
    switch (num) {
        case 0:
            day = "Su";
            break;
        case 1:
            day = "Mo";
            break;
        case 2:
            day = "Tu";
            break;
        case 3:
            day = "We";
            break;
        case 4:
            day = "Th";
            break;
        case 5:
            day = "Fr";
            break;
        case 6:
            day = "Sa";
    }
    return day;
}

function getDate() {
    var yyyy = new Date().getFullYear();
    var mm = new Date().getMonth() + 1;
    var dd = new Date().getDate();
    if (dd < 10)
        dd = '0' + dd;
    if (mm < 10)
        mm = '0' + mm;
    var today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function getTime() {
    var h = new Date().getHours(); // 0-24 format
    if (h < 10)
        h = '0' + h;
    var now = h + ':00:00';
    return now;
}

function getTimeWithMinutes(){
    var h = new Date().getHours(); // 0-24 format
    var m = new Date().getMinutes();
    if (h < 10){
        h = '0' + h;
    }
    if(m<10){
        m = '0' + m;
    }
    var now = h + ':'+m+':00';
    return now;
}
/*---------------------------end Date--------------------------------------------*/


/*----------------------------Scheduler------------------------------------------*/
async function createScheduleTable() // set data row header and add empty data row
{

    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    var dataTable = ' <table id="table_data" class="table table-responsive  table-workorder special-table" >';
    var tableHead = '<thead><tr>';
    var monthDiv = '<div class="row-month">';
    var tableBody = '<tbody id="tbody_data"><tr id="tbdata_default">';
    var tableColum = '<tr id="tbdata_count">';
    var count = 0;
    var totalCount = 0;
    for (var i = 0; i <= timeDiff; i++)
    {
        var day = getDay(addDays(startDate, i, 1));
        var className = '';
        if (day == "Su" || day == "Sa")
        {
            //className = "data-cell-blue_th";
            className = "form-control-weekend";
        } else {
            className = "";
        }
        var d = addDays(startDate, i, 3);
        tableHead += '<th class=' + className + '><span class="date-l">' + day + '</span><br><span class="date-n">' + addDays(startDate, i, 3) + '</span> </th>';
        if (d == 1 || i == timeDiff) {

            if (i == timeDiff) {
                if (d == 1) {
                    monthDiv += '<div class="col-' + count + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';
                    monthDiv += '<div class="col-1" style="left:' + (27 * (totalCount + count)) + 'px">' + getMonth(addDays(startDate, i, 1)) + '</div>';
                } else {
                    monthDiv += '<div class="col-' + (count + 1) + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';
                }
            }
            else {
                if (i != 0) {
                    monthDiv += '<div class="col-' + count + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';
                }
            }
            totalCount += count;
            count = 0;
        }
        count++;
        tableBody += '<td ><input type="text" class="form-control ' + className + '" id="' + addDays(startDate, i, 1) + '_txt" name="count" value="" onkeyup="onCellEditInsert(this.id)" required="" title="Please enter Advertisement count" ></td>';
        tableColum += '<td class="data-cell-light_orange" id="col_count_' + i + '"></td>';
    }
    tableHead += '<th class="last-th"></th></tr></thead>';
    tableBody += '</tr>';
    tableColum += '</tr></tbody>';
    dataTable += tableHead + tableBody + tableColum + '</table>';
    monthDiv += '</div>';
    iColumnMaxID = timeDiff;
    $('#div_table_data').html(monthDiv + dataTable);
    default_tr_data = $('#tbody_data').html();
    fetchDone();
}

function disableDataRowCells(startDate, limitDate) {
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    for (var i = 0; i <= timeDiff; i++)
    {
        var dt = addDays(startDate, i, 1);
        if (dt < limitDate) {
            $('#' + dt + '_txt').attr('readonly', true);
            $('#' + dt + '_txt').parent('td').addClass('disable');
        }
        else {
            $('#' + dt + '_txt').attr('readonly', false);
            $('#' + dt + '_txt').parent('td').removeClass('disable');
        }
    }
}

function startTimeDropDownChange(time) //
{
    var res = time.replace("30", "00");
    if (res > getTime())
        disableDataRowCells(startDate, getDate());
    else
        disableDataRowCells(startDate, addDays(getDate(), 1, 1));
    setTimeSlotsUtilization();
}

function endTimeDropDownChange(time) //
{
    var startTime = document.getElementById("schedule_startTimeDropdown").value;
    if (time <= startTime) {
        alert("Invalid time range");
        document.getElementById("schedule_endTimeDropdown").value = '00:00:00';
    }
    setTimeSlotsUtilization();
}

async function createUpdateScheduleView() // fill schedule data
{
    var v_workOrderId = workOrderId;
    var tableRow_Left = '';
    var tableRow_Data = '';
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getWOScheduleViewUrl(),
        data: {
            workOrderId: v_workOrderId,
            channelId: selectedChannelId
        },
        success: function (json_data)
        {
            buildUpdateScheduleView(json_data);
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

var checkFiler = true;
function buildUpdateScheduleView(json_data) ///refactored
{
    json_data_view = json_data;
    var jsonData = json_data.timebeltSchedules;
    resetParameters();
    mapColumnCount = {}; /// column spot count
    selectedChannelId = -999;
    selectedAdvertId = -1;
    tableRow_Left = '';
    tableRow_Data = '';
    $('#tbody_left').html("");
    $('#tbody_data').html("");
    var data = [];
    for (var i in jsonData) {
        data.push(jsonData[i]);
    }
    tableData = data;
    data = filterChannel(data);
    data = filterAdvert(data);
    data = filterDurationAdvert(data);
    data = filterStartTime(data);
    data = filterEndTime(data);
    data = filterCategoryAdvert(data);
    var channelName = '';
    var addedDateToColumCopy = true;
    $('#from-date-drop').empty();
    $('#to-date-drop').empty();
    for (var i in data)
    {
        tableRow_Left += '<tr id="tbleft_' + iRowID + '">';
        tableRow_Data += '<tr id="tbdata_' + iRowID + '">';
        channelName = data[i].channelName;
        if (channelName != null)
        {
            tableRow_Left += '<td ><input type="checkbox" value="' + i + '" /></td>';
            tableRow_Left += '<td ><button type="button" class="btn-empty" aria-label="Priority" onclick="rederectToPriorityPage(' + iRowID + ')"><span class="glyphicon glyphicon-star-empty"></span></button></td>';
            tableRow_Left += '<td ><buttsaveon type="button" class="btn-empty" aria-label="Close" onclick="removeRow(' + iRowID + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
            tableRow_Left += '<td><button type="button" class="btn-empty" aria-label="Update" onclick="updateRow(' + iRowID + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
            tableRow_Left += '<td><div data-toggle="tooltip" data-placement="right" title="' + data[i].channelName + '">' + data[i].channelName + '</div></td>';
            tableRow_Left += '<td><div align="left" style=" padding-left: 5px">' + data[i].advertName + '</div></td><td><b>' + setLanguage_returnFistLetter(data[i].adverLang) + '</b></td><td>' + data[i].advertDuration + '</td><td>' + data[i].revenueline + '</td><td>' + RemoveSec(data[i].schedulestarttime) + '</td><td>' + RemoveSec(data[i].scheduleendtime) + '</td>';
        }
        var statTime = '{"start":"' + data[i].schedulestarttime + '"}';
        var objStart = JSON.parse(statTime);
        var startFlag = true;
        var endTime = '{"end":"' + data[i].scheduleendtime + '"}';
        var objEnd = JSON.parse(endTime);
        var endFlag = true;
        if (startTimeDrop.length == 0) {
            startTimeDrop.push(objStart);
        } else {
            for (var a = 0; a < startTimeDrop.length; a++) {
                if (startTimeDrop[a].start == objStart.start) {
                    startFlag = false;
                }
            }
            if (startFlag) {
                startTimeDrop.push(objStart);
            }
        }
        if (endTimeDrop.length == 0) {
            endTimeDrop.push(objEnd);
        } else {
            for (var b = 0; b < endTimeDrop.length; b++) {
                if (endTimeDrop[b].end == objEnd.end) {
                    endFlag = false;
                }
            }
            if (endFlag) {
                endTimeDrop.push(objEnd);
            }
        }

        var today = getDate();
        var time = getTime();
        var dateList = data[i].scheduleItem;
        var spotCount = 0;
        mapScheduleMetaData[iRowID] = {workOrderId: workOrderId, channelId: data[i].channelid, advertId: data[i].advertid, startTime: data[i].schedulestarttime, endTime: data[i].scheduleendtime, programm: data[i].program, revenueLine: data[i].revenueline, month: data[i].month};
        mapScheduleMetaDataCurrent[iRowID] = {workOrderId: workOrderId, channelId: data[i].channelid, advertId: data[i].advertid, startTime: data[i].schedulestarttime, endTime: data[i].scheduleendtime, programm: data[i].program, revenueLine: data[i].revenueline, month: data[i].month};
        for (var j in dateList)
        {
            var int_j = stoi(j);
            var iCellID = iRowID + int_j;
            var advCount = dateList[j].advertCount;
            spotCount += advCount;
            if (!(int_j in mapColumnCount))
                mapColumnCount[int_j] = 0;
            mapColumnCount[int_j] = mapColumnCount[int_j] + advCount;
            mapEditBoxData[iCellID] = advCount;
            mapEditBoxDataCurrent[iCellID] = advCount;
            mapEditBoxScheduleID[iCellID] = dateList[j].scheduleid;
            mapEditBoxDate[iCellID] = dateList[j].date;
            if (advCount === 0)
                advCount = "";
            var dayofweek = getDay(dateList[j].date);
            var className = 'disable';
            var txtColor = '';
            if (dayofweek === "Su" || dayofweek === "Sa") {
                className = "data-cell-blue";
                txtColor = "form-control-weekend";
            }

            if (data[i].isDummyCut) {
                className = 'dummy';
                txtColor = 'dummy';
            }
             var selectedTime = data[i].schedulestarttime.replace("30", "00");
            if (dateList[j].date < today) {
                tableRow_Data += '<td class=' + className + '><input type="hidden" id="cell_' +iCellID + '" value="' + advCount + '">' + advCount + '</td>';
                if (addedDateToColumCopy) {
                    $('#from-date-drop').append('<option value="' + iCellID + '">' + dateList[j].date + '</option>');
                }
            } else if (dateList[j].date === today &&  selectedTime <= time) {
                tableRow_Data += '<td class=' + className + '><input type="hidden" id="cell_' +iCellID + '" value="' + advCount + '">' + advCount + '</td>';
                if (addedDateToColumCopy) {
                    $('#from-date-drop').append('<option value="' + iCellID + '">' + dateList[j].date + '</option>');
                    $('#to-date-drop').append('<option value="' + iCellID + '">' + dateList[j].date + '</option>');
                }
            } else {
                if (className === 'disable') {
                    className = '';
                }
                if (addedDateToColumCopy) {
                    $('#from-date-drop').append('<option value="' + iCellID + '">' + dateList[j].date + '</option>');
                    $('#to-date-drop').append('<option value="' + iCellID + '">' + dateList[j].date + '</option>');
                }
                tableRow_Data += '<td class=' + className + '><input type="text" class="form-control ' + txtColor + '" id="cell_' +
                        iCellID + '" value="' + advCount + '" onkeyup="onCellEdit(this.id)"></td>';
            }
        }
        addedDateToColumCopy = false;
        if (channelName != null)
            tableRow_Left += '<td id="row_count_' + iRowID + '"class="data-cell-light_orange_left">' + spotCount + '</td>';
        mapRowCount[iRowID] = spotCount;
        iRowID += _RowIDIncrement;
        tableRow_Left += '</tr>';
        tableRow_Data += '</tr>';
    }
    tableRow_Left += default_tr_left;
    tableRow_Data += default_tr_data;
    $('#tbody_left').html(tableRow_Left);
    $('#tbody_data').html(tableRow_Data);
    $(window).trigger('resize');

    for (var key in mapColumnCount)
        $('#col_count_' + key).html(mapColumnCount[key]);
    disableDataRowCells(startDate, addDays(getDate(), 1, 1));
    $("#month").val(getCurrentMonth());
    $("#schedule_channelDropdown").val(selectedChannelId);
    if (selectedChannelId != -999)
        changeTimeDropdownData(selectedChannelId);
    setAvailableSpot(selectedChannelId, json_data.woChannelSpotsCount); ///refactored
    setTimeBeltUtilization(json_data.woTimeBeltUtilization); ///refactored

//---------------------------------------------
    setMessageToast(json_data.message);
    if (checkFiler) {
        setTimeForFilter();
    }
    checkFiler = false;
    $('[data-toggle="tooltip"]').tooltip();
    getGrandTotal();
    setDataToAddRow();
}
/*--------------------------------------End Scheduler-----------------------------------------------*/


/*------------------------------------Remove_Update_Row---------------------------------------------*/
function removeRow(row_id) {
    var retVal = confirm("Do you want to remove the selected row?");
    if (retVal == false) {
        return;
    }

    var channel_id = mapScheduleMetaDataCurrent[row_id].channelId;
    var advert_id = mapScheduleMetaDataCurrent[row_id].advertId;
    var startTime = mapScheduleMetaDataCurrent[row_id].startTime;
    var currentDate = getDate();
    var currentTime = getTime();
    var spot_count = 0;
    for (var i = 0; i <= iColumnMaxID; i++)
    {
        var id = row_id + i;
        var input_date = mapEditBoxDate[id];
        if (input_date > currentDate || (input_date == currentDate && startTime > currentTime)) {
            $('#cell_' + id).val('');
            $('#cell_' + id).disabled = true;
            spot_count += mapEditBoxDataCurrent[id];
            var col_id = i;
            mapColumnCount[col_id] = mapColumnCount[col_id] - mapEditBoxDataCurrent[id];
            $('#col_count_' + col_id).html(mapColumnCount[col_id]);
            mapEditBoxDataCurrent[id] = 0;
        }
    }
    spotCountChanged(channel_id, advert_id, -spot_count);
    mapRowCount[row_id] = mapRowCount[row_id] - spot_count;
    $('#row_count_' + row_id).html(mapRowCount[row_id]);
    colorEditedCells(row_id, true);
}

function updateRow(row_id) {
    var count = document.getElementById("row_count_"+row_id).innerHTML;
    var metaData = mapScheduleMetaDataCurrent[row_id];
    var channel_id = metaData.channelId;
    var advert_id = metaData.advertId;
    var advert = mapAdverts[advert_id];
    var channelDrop = '<select id="cmbUpdateChannel_' + row_id + '" onchange="inplaceEditMetaDataChanged(' + row_id + ')" class="form-control Padding0px">';
    channelDrop += '<option value="-999">--Select Channel---</option>';
    $.each(lstChannels, function (i, item) {
        channelDrop += '<option value="' + item.channelid + '">' + item.channelname + '</option>';
    });
    channelDrop += '</select>';
    var startDrop = '<select id="cmbStartTime' + row_id + '" class="form-control" onchange="inplaceEditMetaDataChanged(' + row_id + ')"></select>';
    var endDrop = '<select id="cmbEndTime' + row_id + '" class="form-control" onchange="inplaceEditMetaDataChanged(' + row_id + ')"></select>';
    var tableRow_Left = '';
    tableRow_Left += '<td ></td>';
    tableRow_Left += '<td ></td>';
    tableRow_Left += '<td ></td>';
    tableRow_Left += '<td ><buttsaveon type="button" class="btn-empty" aria-label="Close" onclick="resetEditRow(' + row_id + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
    tableRow_Left += '<td><div>' + channelDrop + '</div></td>';
    tableRow_Left += '<td><div align="left" style=" padding-left: 5px">' + advert.advertName + '</div></td><td><b>' + setLanguage_returnFistLetter(advert.language) + '</b></td><td>' + advert.duration + '</td><td>' + metaData.revenueLine + '</td>';
    tableRow_Left += '<td><div>' + startDrop + '</div></td><td><div>' + endDrop + '</div></td>';
    tableRow_Left += '<td id="row_count_'+row_id+'" class="data-cell-light_orange_left">'+count+'</td>';
    $('#tbleft_' + row_id).html(tableRow_Left);
    $('#cmbStartTime' + row_id).html(mapChannelTimeBeltsStart[channel_id]);
    $('#cmbEndTime' + row_id).html(mapChannelTimeBeltsEnd[channel_id]);
    $('#cmbUpdateChannel_' + row_id).val(metaData.channelId);
    $('#cmbStartTime' + row_id).val(metaData.startTime);
    $('#cmbEndTime' + row_id).val(metaData.endTime);
}

function inplaceEditMetaDataChanged(row_id) {
    var channelId = $('#cmbUpdateChannel_' + row_id).val();
    var advert_id = mapScheduleMetaDataCurrent[row_id].advertId;
    var startTime = $('#cmbStartTime' + row_id).val();
    var endTime = $('#cmbEndTime' + row_id).val();
    var metaData = mapScheduleMetaDataCurrent[row_id];
    var spotCount = getSpotCountbyRowID(row_id);
    if (channelId != metaData.channelId) {
        spotCountChanged(metaData.channelId, advert_id, -spotCount);
        spotCountChanged(channelId, advert_id, spotCount);
    }
    metaData.channelId = channelId;
    metaData.startTime = startTime;
    metaData.endTime = endTime;
    colorEditedCells(row_id, true);
}

function resetEditRow(iRowID) {
     var count = document.getElementById("row_count_"+iRowID).innerHTML;
    var metaData = mapScheduleMetaData[iRowID];
    var channel = mapChannels[metaData.channelId];
    var advert = mapAdverts[metaData.advertId];
    var tableRow_Left = '';
    tableRow_Left += '<td ><input type="checkbox" value="' + (stoi(iRowID)/1000) + '" /></td>';
    tableRow_Left += '<td ><button type="button" class="btn-empty" aria-label="Priority" onclick="rederectToPriorityPage(' + iRowID + ')"><span class="glyphicon glyphicon-star-empty"></span></button></td>';
    tableRow_Left += '<td ><buttsaveon type="button" class="btn-empty" aria-label="Close" onclick="removeRow(' + iRowID + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
    tableRow_Left += '<td><button type="button" class="btn-empty" aria-label="Update" onclick="updateRow(' + iRowID + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
    tableRow_Left += '<td><div data-toggle="tooltip" data-placement="right" title="' + channel.channelname + '">' + channel.channelname + '</div></td>';
    tableRow_Left += '<td><div align="left" style=" padding-left: 5px">' + advert.advertName + '</div></td><td><b>' + setLanguage_returnFistLetter(advert.language) + '</b></td><td>' + advert.duration + '</td><td>' + metaData.revenueLine + '</td><td>' + RemoveSec(metaData.startTime) + '</td><td>' + RemoveSec(metaData.endTime) + '</td>';
    tableRow_Left += '<td id="row_count_'+iRowID+'" class="data-cell-light_orange_left">'+count+'</td>';
    $('#tbleft_' + iRowID).html(tableRow_Left);
}

function resetRow(isSpotCountChanged) {
    resetData(isSpotCountChanged);
    $('#schedule_channelDropdown').val(-999);
    $("#schedule_advertDropdown").val(-1);
    if (isSpotCountChanged)
        changeAdvert();
    $("#schedule_startTimeDropdown").val('00:00:00');
    $("#schedule_endTimeDropdown").val('01:00:00');
}

function resetData(isSpotCountChanged) {
    var row_advert_count = getInsertRowAdvertCount();
    var channel_id = $('#schedule_channelDropdown').val();
    if (channel_id == -999)
        return;
    var advert_id = $("#schedule_advertDropdown").val();
    if (advert_id == -1)
        return;
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    for (var i = 0; i <= timeDiff; i++)
    {
        var cell_id = addDays(startDate, i, 1) + "_txt";
        $('#' + cell_id).val('');
    }

    if (isSpotCountChanged)
        spotCountChanged(channel_id, advert_id, -row_advert_count);
    iInsertCount = 0;
}

function saveData()//save new row
{
    showDialog();
    var channel_id = $('#schedule_channelDropdown').val();
    if (channel_id == -999) {
        closeDialog();
        viewMessageToastTyped("Invalid Channel", "Error");
        return;
    }

    var programm = "No program";
    var advert_id = $('#schedule_advertDropdown').val();
    var revenue_line = $('#revenue_line').val();
    var month = "No month";
    var startTime = $('#schedule_startTimeDropdown').val();
    var endTime = $('#schedule_endTimeDropdown').val();
    if (endTime == '24:00:00') {
        endTime = '23:59:00';
    }

    var i_end_time = stoi(endTime.replace(/:/g, ""));
    var i_start_time = stoi(startTime.replace(/:/g, ""));
    if (i_start_time >= i_end_time) {
        closeDialog();
        viewMessageToastTyped("Invalid end time.", "Error");
        return;
    }

    lstSchedulData = [];
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    for (var i = 0; i <= timeDiff; i++)
    {
        var date = addDays(startDate, i, 1);
        var cell_id = date + "_txt";
        var data = $('#' + cell_id).val();
        data = stoi(data);
        var schedule = {workOrderId: workOrderId, channelId: channel_id, advertId: advert_id, startTime: startTime, endTime: endTime, schedulDate: date, advertCount: data, programm: programm, scheduleIds: [], comment: "", revenueLine: revenue_line, month: month};
        lstSchedulData.push(schedule);
    }
    var stScheduleData = JSON.stringify(lstSchedulData);
    resetRow(false);
    var serverURL = URI.scheduler.scheduleSaveUrl();
    if (workOrderStatus == 0) {
        serverURL = URI.scheduler.scheduleSaveAndWorkOrderUpdateUrl();
    }

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
       // async: true,
        type: 'POST',
        dataType: "json",
        url: serverURL,
        data: {
            scheduleData: stScheduleData
        },
        success: function (data)
        {
            if (data != null) {
                buildUpdateScheduleView(data);
            }
            TimeBeltUtilization();
            viewMessageToastTyped("Schedule saved successfully", "Success");
            setDataToAddRow();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function updateData()
{
    showDialog();
    console.log(new Date());
    if (getInsertRowAdvertCount() > 0) {
        closeDialog();
        saveData();
        return;
    }

    if (!isDataUpdated()) {
        closeDialog();
        return;
    }

    var lstSchedulData = [];
    for (var key in mapScheduleMetaDataCurrent) {
        if (mapScheduleMetaDataCurrent.hasOwnProperty(key)) {
            var isMetaUpdated = false;
            if (JSON.stringify(mapScheduleMetaData[key]) != JSON.stringify(mapScheduleMetaDataCurrent[key])) {
                isMetaUpdated = true;
            }

            var metaData = mapScheduleMetaDataCurrent[key];
            if (metaData.channelId == -999) {
                closeDialog();
                viewMessageToastTyped("Invalid channel at row " + (Math.floor(key / _RowIDIncrement) + 1), "Error");
                return;
            }
            if (metaData.endTime == '24:00:00') {
                metaData.endTime = '23:59:00';
            }
            var iEndTime = stoi(metaData.endTime.replace(/:/g, ""));
            var iStartTime = stoi(metaData.startTime.replace(/:/g, ""));
            if (iStartTime >= iEndTime) {
                closeDialog();
                viewMessageToastTyped("Invalid end time at row " + (Math.floor(key / _RowIDIncrement) + 1), "Error");
                return;
            }

            for (var i = 0; i <= iColumnMaxID; i++) {
                var cell = stoi(key) + i;
                if (isMetaUpdated) {
                    var schedule = {workOrderId: metaData.workOrderId, channelId: metaData.channelId, advertId: metaData.advertId, startTime: metaData.startTime, endTime: metaData.endTime, schedulDate: mapEditBoxDate[cell], advertCount: mapEditBoxDataCurrent[cell], programm: metaData.programm, scheduleIds: mapEditBoxScheduleID[cell], comment: "", revenueLine: metaData.revenueLine, month: metaData.month};
                    if(mapEditBoxDate[cell]>=getDate()){
                        lstSchedulData.push(schedule);
                    }
                }
                else if (mapEditBoxData[cell] != mapEditBoxDataCurrent[cell]) {
                    var schedule = {workOrderId: metaData.workOrderId, channelId: metaData.channelId, advertId: metaData.advertId, startTime: metaData.startTime, endTime: metaData.endTime, schedulDate: mapEditBoxDate[cell], advertCount: mapEditBoxDataCurrent[cell], programm: metaData.programm, scheduleIds: mapEditBoxScheduleID[cell], comment: "", revenueLine: metaData.revenueLine, month: metaData.month};
                    lstSchedulData.push(schedule);
                }
            }
        }
    }
    for (var n = 0; n < lstSchedulData.length; n++) {

        if (lstSchedulData[n].advertCount == 0) {
            var object = lstSchedulData[n];
            lstSchedulData.splice(n, 1);
            lstSchedulData.unshift(object);
        } else if (lstSchedulData[n].scheduleIds.length != null) {
            if (lstSchedulData[n].scheduleIds.length > lstSchedulData[n].advertCount) {
                var object = lstSchedulData[n];
                lstSchedulData.splice(n, 1);
                lstSchedulData.unshift(object);
            }
        }
    }
    var stScheduleData = JSON.stringify(lstSchedulData);
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.scheduler.scheduleSaveAndUpdateUrl() ,
        data: {
            scheduleData: stScheduleData
        },
        success: function (data)
        {
            if (data != null) {
                buildUpdateScheduleView(data);
            }
            closeDialog();
            viewMessageToast();
            TimeBeltUtilization();
            viewMessageToastTyped("Schedule updated successfully", "Success");
            setDataToAddRow();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            viewMessageToastTyped("Error has occurred while updating the schedule", "Error");
        }
    }
    );
}

function TimeBeltUtilization()//
{
    disableDataRowCells(startDate, addDays(endDate, 1, 1));
    $.ajax({
        type: 'GET',
        async: true,
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getTimeUtilizationUrl(),
        data: {
            workOrderId: workOrderId
        },
        success: function (data)
        {
            if (data != null) {
                setTimeBeltUtilization(data);
                disableDataRowCells(startDate, getDate());
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function isTableDirty() {
    return (isDataUpdated() || getInsertRowAdvertCount() > 0);
}

function checkTableDirty() {
    if (isTableDirty()) {
        viewMessageToastTyped("Please save data before continue.", "Info");
        return true;
    }
    return false;
}

function isDataUpdated() {
    for (var key in mapEditBoxDataCurrent) {
        if (mapEditBoxDataCurrent.hasOwnProperty(key)) {
            if (mapEditBoxData[key] != mapEditBoxDataCurrent[key]) {
                return true;
            }
        }
    }

    for (var key in mapScheduleMetaDataCurrent) {
        if (mapScheduleMetaDataCurrent.hasOwnProperty(key)) {
            if (JSON.stringify(mapScheduleMetaData[key]) != JSON.stringify(mapScheduleMetaDataCurrent[key])) {
                return true;
            }
        }
    }
    return false;
}

function colorEditedCells(iRowId, isAllCells) {
    if (JSON.stringify(mapScheduleMetaData[iRowId]) != JSON.stringify(mapScheduleMetaDataCurrent[iRowId])) {
        isAllCells = true;
    }

    var startTime = mapScheduleMetaDataCurrent[iRowId].startTime;
    var currentDate = getDate();
    var currentTime = getTime();
    for (var i = 0; i <= iColumnMaxID; i++) {
        var cellId = (iRowId + i);
        var input_date = mapEditBoxDate[cellId];
        if (input_date > currentDate || (input_date == currentDate && startTime > currentTime)) {
            if (isAllCells || (mapEditBoxData[cellId] != mapEditBoxDataCurrent[cellId])) {
                $('#cell_' + cellId).addClass('form-control-edited');
            } else {
                $('#cell_' + cellId).removeClass('form-control-edited');
            }
        }
    }
}

function getSpotCountbyRowID(row_id) {
    var startTime = mapScheduleMetaData[row_id].startTime; ///todo:disable_cell

    var currentDate = getDate();
    var currentTime = getTime();
    var spot_count = 0;
    for (var i = 0; i <= iColumnMaxID; i++)
    {
        var id = row_id + i;
        var input_date = mapEditBoxDate[id];
        if (input_date > currentDate || (input_date == currentDate && startTime > currentTime)) {
            spot_count += mapEditBoxDataCurrent[id];
        }
    }
    return spot_count;
}

function getInsertRowAdvertCount() {
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    var row_advert_count = 0;
    for (var i = 0; i <= timeDiff; i++)
    {
        var cell_id = addDays(startDate, i, 1) + "_txt";
        row_advert_count += stoi($('#' + cell_id).val());
    }
    return row_advert_count;
}
/*------------------------------------End Remove_Update_Row-----------------------------------------*/


/// filters ////////////////////////////////////////////////////////////////////

$(function () {

    $('#filter_AdvertDrop').on('change',async function () {
        filter_AdvertChange();
    });
});

function filter_AdvertChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
}

function filter_AdvertDurationChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
}

function filter_channelDropDownChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
    var fiter_channel = document.getElementById("schedule_filterChannelDropdown");
    var fiter_channel_id = fiter_channel.options[fiter_channel.selectedIndex].value;
}

function filter_AdvertCatagoryChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
}

function filter_startTimeChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
}

function filter_endTimeChange() {
    if (checkTableDirty())
        return;
    buildUpdateScheduleView(json_data_view);
}

function filterAdvert(data) {
    if (checkTableDirty()) {
        return;
    } else {
        var fiter_advert_ids = [];
        var fiter_advertdrop = $('#filter_AdvertDrop').find("option:selected");
        if (typeof fiter_advertdrop === "undefined") {
                    return;
        }
        fiter_advertdrop.each(function () {
            fiter_advert_ids.push($(this).val());
        });
        var remove = true;
        if (fiter_advert_ids.length !== 0 && fiter_advert_ids[0] !== "-999") {
            for (var i = 0; i < data.length; i++)
            {
                for (var j = 0; j < fiter_advert_ids.length; j++) {
                    if (data[i].advertid === stoi(fiter_advert_ids[j])) {
                        remove = false;
                    }
                }
                if (remove) {
                    data.splice(i, 1);
                    i--;
                }
                remove = true;
            }
            return data;
        } else {
            return data;
        }
    }
}

function filterDurationAdvert(data) {
    if (checkTableDirty()) {
        return;
    } else {
        var fiter_advertDuration_ids = [];
        var fiter_advertDurationdrop = $('#filter_AdvertDuratinoDrop').find("option:selected");
        if (typeof fiter_advertDurationdrop === "undefined") {
            return;
        }
        fiter_advertDurationdrop.each(function () {
            fiter_advertDuration_ids.push($(this).val());
        });
        var remove = true;
        if (fiter_advertDuration_ids.length !== 0 && fiter_advertDuration_ids[0] !== "-999") {
            for (var i = 0; i < data.length; i++)
            {
                for (var j = 0; j < fiter_advertDuration_ids.length; j++) {
                    if (data[i].advertDuration === stoi(fiter_advertDuration_ids[j])) {
                        remove = false;
                    }
                }
                if (remove) {
                    data.splice(i, 1);
                    i--;
                }
                remove = true;
            }
            return data;
        } else {
            return data;
        }
    }
}

function filterChannel(data) {
    if (checkTableDirty())
        return;
    var fiter_channel = document.getElementById("schedule_filterChannelDropdown");
    if (typeof fiter_channel === "undefined") {
        return;
    }
    var fiter_channel_id = fiter_channel.options[fiter_channel.selectedIndex].value;
    if (fiter_channel_id != -999 && parseInt(fiter_channel_id) != -111) {

        for (var i = 0; i < data.length; i++)
        {
            if (fiter_channel_id != data[i].channelid) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterCategoryAdvert(data) {
    if (checkTableDirty())
        return;
    var fiter_advertCategorydrop = document.getElementById("filter_Advertcategory");
    if (typeof fiter_advertCategorydrop === "undefined") {
            return;
    }
    var fiter_advertCategory = fiter_advertCategorydrop.options[fiter_advertCategorydrop.selectedIndex].value;
    if (fiter_advertCategory != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (fiter_advertCategory != data[i].adverCategory) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterStartTime( data) {
    if (checkTableDirty())
        return;
    var fiter_Startdrop = document.getElementById("startTime_filter");
    if (typeof fiter_Startdrop === "undefined") {
         return;
    }
    var fiter_Start_time = fiter_Startdrop.options[fiter_Startdrop.selectedIndex].value;
    if (fiter_Start_time != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (!(data[i].schedulestarttime >= fiter_Start_time)) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterEndTime(data) {
    if (checkTableDirty())
        return;
    var fiter_enddrop = document.getElementById("endTime_filter");
    if (typeof fiter_enddrop === "undefined") {
         return;
    }
    var fiter_end_time = -999
    if(fiter_enddrop.selectedIndex !=-1){
        fiter_end_time = fiter_enddrop.options[fiter_enddrop.selectedIndex].value;
    }

    if (fiter_end_time != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (!(data[i].scheduleendtime <= fiter_end_time)) {
                data.splice(i, 1);
                i--;
            } else {

            }
        }
        return data;
    } else {
        return data;
    }
}

function configurFilter() {
    $('#filter_AdvertDrop').selectpicker({});
}

function clearFilter() {

    $("#filter_AdvertDrop").selectpicker('val', "-999");
    $("#filter_AdvertDuratinoDrop").selectpicker('val', "-999");
    document.getElementById("schedule_filterChannelDropdown").value = "-999";
    document.getElementById("filter_Advertcategory").value = "-999";
    document.getElementById("startTime_filter").value = "-999";
    document.getElementById("endTime_filter").value = "-999";
    if (checkTableDirty()) {
        return;
    }
    buildUpdateScheduleView(json_data_view);
}

/*------------------------------------Channel-------------------------------------------------------*/
function setChannelDetails() //
{
    var v_workOrderId = workOrderId;
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderChannelListUrl(),
        data: {
            workOrderId: v_workOrderId
        },
        success: function (data)
        {
            var filterChannelDrop = '<select id="schedule_filterChannelDropdown" onchange="filter_channelDropDownChange()" name="channellllName" class="form-control">';
            var channelDrop = '<select id="schedule_channelDropdown" name="channelName" onchange="channelDropDownChange(this.value)" class="form-control Padding0px">';
            channelDrop += '<option id="-999_ch" value="-999">--Select Channel---</option>';
            filterChannelDrop += '<option id="-999_ch" value="-999" selected>All Channels</option>';
            for (var i in data)
            {
                channelDrop += '<option id="' + i + '_ch" value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                filterChannelDrop += '<option id="' + i + '_ch" value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                /// pattern fill model
                $('#cmb_pt_channel_select').append($('<option>', {
                    value: data[i].channelid,
                    text: data[i].channelname
                }));
                /// replace model
                $('#cmb_rp_channel_select').append($('<option>', {
                    value: data[i].channelid,
                    text: data[i].channelname
                }));
                /// rows copy model
                $('#cmb_cr_channel_select').append($('<option>', {
                    value: data[i].channelid,
                    text: data[i].channelname
                }));
                //get available spots
                loadAvailableSpot(data[i].channelid);
                lstChannels.push(data[i]);
                mapChannels[data[i].channelid] = data[i];
            }
            channelDrop += '</select>';
            filterChannelDrop += '</select>';
            _channelDrop = channelDrop;
//            $('#channelDropdown_div').html(channelDrop);
//            $("#schedule_channelDropdown").val(-999);
            $('#filterChannelDropdown_div').html(filterChannelDrop);
            $("#filterChannelDropdown_div").val(-999);
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function setAuditData() {

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.audit.getScheduleAuditUrl(),
        data: {
            workOrderId: workOrderId
        },
        success: function (data)
        {
            closeDialog();

            var trHTML = '';
            trHTML += '<thead><th class="col-md-1">Date and Time</th><th class="col-md-4">Status</th><th class="col-md-2">Count</th></tr></thead>';
            trHTML += '<tbody>';


            for (var i in data)
            {
                trHTML += '<tr><td>' + data[i].time + '</td><td>' + data[i].action + '</td><td>' + data[i].spotCount + '</td></tr>';
            }

            trHTML += '</tbody>';

            $('#audit-data').html(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
/*------------------------------------End channel--------------------------------------------------*/

/**
 * Comment
 */
function Reload() {
    window.location = web_page_path + "/CurrentSchedule.html" + url_data;
}

function resetParameters() {
    iRowID = 0;
    mapScheduleMetaData = {};
    mapScheduleMetaDataCurrent = {};
    mapEditBoxData = {};
    mapEditBoxScheduleID = {};
    mapEditBoxDataCurrent = {};
    mapEditBoxDate = {};
    selectedChannelId = -999;
    selectedAdvertId = -1;
    iInsertCount = 0;
}
