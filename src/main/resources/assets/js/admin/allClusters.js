/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var pData;

$(document).ready(function () {
    loadChannelList();
});

function loadChannelList() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            closeDialog();
            $.each(data, function (index, value) {
                $('#channelDropdown').append($('<option/>', {
                    value: value.channelid,
                    text: value.channelname
                }));
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function channelSelectionChanged(channelId) {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.timeBelts.getListbyChannelUrl(),
        data: {
            channelId: channelId,
        },
        success: function (data)
        {
            closeDialog();
            pData = data;
            var table_data = '';
            $.each(data, function (i, item)
            {
                table_data += '<tr id="tblrow_' + i + '">';
                table_data += '<td id="edit_cell_' + i + '"><button type="button"  style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + i + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
                table_data += '<td>' + item.timeBeltId + '</td>';
                table_data += '<td>' + item.startTime + ' - ' + item.endTime + '</td>';
                table_data += '<td id="cluster_cell_' + i + '">' + item.clusterCount + '</td>';
                table_data += '<td id="maxtime_cell_' + i + '">' + item.totalScheduleTime + '</td>';
                table_data += '</tr>';

            });
            $('#timebelt_table_data').html(table_data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateRow(cell_id) {
    var cellData = '<input type="text" class="form-control Padding0px" id="edit_input_' + cell_id + '" name="cluster_count" value="' + $('#cluster_cell_' + cell_id).html() + '" required="" title="Please enter Cluster Count" >';
    var cellEdit = '<td><button type="button" class="close close-icon" style="float:none;" aria-label="Close" onclick="removeRow(' + cell_id + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
    $('#cluster_cell_' + cell_id).html(cellData);
    $('#edit_cell_' + cell_id).html(cellEdit);

    var cellTimeData = '<input type="text" class="form-control Padding0px" id="edit_time_input_' + cell_id + '" value="' + $('#maxtime_cell_' + cell_id).html() + '" required="" title="Please enter Max Time" >';
    $('#maxtime_cell_' + cell_id).html(cellTimeData);
}

function removeRow(cell_id) {
    var cellEdit = '<td id="edit_cell_' + cell_id + '"><button type="button" style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + cell_id + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
    $('#cluster_cell_' + cell_id).html(pData[cell_id].clusterCount);
    $('#maxtime_cell_' + cell_id).html(pData[cell_id].totalScheduleTime);
    $('#edit_cell_' + cell_id).html(cellEdit);
}

function saveData() {
    showDialog();

    var updatedList = [];
    $.each(pData, function (i, item)
    {
        if ($('#edit_input_' + i).length && (($('#edit_input_' + i).val() != item.clusterCount) || ($('#edit_time_input_' + i).val() != item.totalScheduleTime))) {
            var updatedObject = {};
            updatedObject["id"] = item.timeBeltId;
            updatedObject["clusterCount"] = stoi($('#edit_input_' + i).val());
            updatedObject["maxScheduleTime"] = stoi($('#edit_time_input_' + i).val());
            updatedList.push(updatedObject);
        }
    });
    var sendData = JSON.stringify(updatedList);
    console.log(sendData);

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.timeBelts.updateClusterCountListUrl(),
        data: {
            clusterList: sendData,
        },
        success: function (data)
        {
            closeDialog();
            var channel = $('#channelDropdown').val();
            channelSelectionChanged(channel);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}
