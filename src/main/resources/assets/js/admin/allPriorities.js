/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var pData;

$(document).ready(function () {
    loadChannelList();
});

function loadChannelList() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            closeDialog();
            $.each(data, function (index, value) {
                $('#channelDropdown').append($('<option/>', {
                    value: value.channelid,
                    text: value.channelname
                }));
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function channelSelectionChanged(channelId) {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.getListbyChannelUrl(),
        data: {
            channelId: channelId,
        },
        success: function (data)
        {
            closeDialog();
            pData = data;
            var table_data = '';
            var tbelt_id = 0;
            $.each(data, function (i, item)
            {
                if (tbelt_id != item.timebeltId) {
                    tbelt_id = item.timebeltId;
                    table_data += '<tr id="tblrow_' + i + '"  style="border-top: solid medium lightgray;">';
                }
                else
                    table_data += '<tr id="tblrow_' + i + '">';
                table_data += '<td id="edit_cell_' + i + '"><button type="button"  style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + i + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
                table_data += '<td>' + item.timebeltId + '</td>';
                table_data += '<td>' + item.startTime + ' - ' + item.endTime + '</td>';
                table_data += '<td>' + (item.cluster + 1) + '</td>';
                table_data += '<td id="priority_cell_' + i + '">' + item.priorityCount + '</td>';
                table_data += '</tr>';

            });
            $('#priority_table_data').html(table_data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateRow(cell_id) {
    var cellData = '<input type="text" class="form-control Padding0px" id="edit_input_' + cell_id + '" name="cluster_count" value="' + $('#priority_cell_' + cell_id).html() + '" required="" title="Please enter Cluster Count" >';
    var cellEdit = '<td><button type="button" class="close close-icon" style="float:none;" aria-label="Close" onclick="removeRow(' + cell_id + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
    $('#priority_cell_' + cell_id).html(cellData);
    $('#edit_cell_' + cell_id).html(cellEdit);
}

function removeRow(cell_id) {
    var cellEdit = '<td id="edit_cell_' + cell_id + '"><button type="button" style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + cell_id + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
    $('#priority_cell_' + cell_id).html(pData[cell_id].priorityCount);
    $('#edit_cell_' + cell_id).html(cellEdit);
}

function saveData() {
    showDialog();

    var updatedList = [];
    $.each(pData, function (i, item)
    {
        if ($('#edit_input_' + i).length && ($('#edit_input_' + i).val() != item.priorityCount)) {
            var updatedObject = {};
            updatedObject["first"] = item.priorityId;
            updatedObject["second"] = stoi($('#edit_input_' + i).val());
            updatedList.push(updatedObject);
        }
    });
    var sendData = JSON.stringify(updatedList);
    console.log(sendData);

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.updatePriorityCountListUrl(),
        data: {
            priorityList: sendData,
        },
        success: function (data)
        {
            closeDialog();
            var channel = $('#channelDropdown').val();
            channelSelectionChanged(channel);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}
