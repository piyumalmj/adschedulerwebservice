$(document).ready(function () {
    showDialog();
    var channel_id = localStorage.getItem("Channel_id");
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.getSelectedChannelUrl(),
        data: {
            channelId: channel_id,
        },
        success: function (data)
        {
            closeDialog();
            document.getElementById("txt_channel_id").value = data.channelid;
            document.getElementById("txt_channel_long").value = data.channelname;
            document.getElementById("txt_channel_short").value = data.shortname;
            document.getElementById("txt_video_format").value = data.videoformat;
            document.getElementById("txt_remarks").value = data.remarks;
            document.getElementById("txt_ops_id").value = data.opsChannelId;
            if (data.enabled) {
                var btn_dact = ' <button class="btn btn_inactive btn-block add admin_add" type="button" onclick="disableChannel()">Inactive</button>';
                $('#act_dact_btn').html(btn_dact);
            } else {
                var btn_act = ' <button class="btn btn_active btn-block add admin_add" type="button" onclick="enableChannel()">Active</button>';
                $('#act_dact_btn').html(btn_act);
            }
            
            if (!data.isManual) {
                $('#rd_Auto').prop("checked", true);                
                $('#id_manualdelay').hide();
            }
            
            $('#txt_delay').val(data.manualDelay);
            
            var arPos = data.logoPosition.split("x");
            if (arPos.length == 2) {
                $('#txt_logoPositon_x').val(arPos[0]);
                $('#txt_logoPositon_y').val(arPos[1]);
            }           

        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
});

function updateChannel() {
    var channel_id = document.getElementById("txt_channel_id").value;
    var long_name = document.getElementById("txt_channel_long").value;
    var short_name = document.getElementById("txt_channel_short").value;
    var video_format = document.getElementById("txt_video_format").value;
    var remarks = document.getElementById("txt_remarks").value;
    
    var isManual = $('#rd_Manual').is(':checked');
    var manualDelay = stoi($('#txt_delay').val());
    var opsId = stoi($('#txt_ops_id').val());
    var logoPosition_x = $('#txt_logoPositon_x').val();
    var logoPosition_y = $('#txt_logoPositon_y').val();

    var v_channelData = '{"channelId":' + channel_id + ',"longName":"' + long_name + '","shortName":"' + short_name 
            + '","vFormat":"' + video_format + '","remarks":"' + remarks 
            + '","isManual":' + isManual + ',"manualDelay":' + manualDelay 
            + ',"opsId":' + opsId+',"logoPosition":"' + logoPosition_x + 'x' + logoPosition_y
            + '"}';
    
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.saveAdnUpdateChannelUrl(),
        data: {
            channelData: v_channelData,
        },
        success: function (data)
        {
            closeDialog();
            if (data == -2) {
                alert("successfully channel update");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function disableChannel() {
    var ret = confirm("Are you sure you want to inactivate this Channel?");
    if (!ret)
        return;
    showDialog();

    var channelId = localStorage.getItem("Channel_id");
    $.ajax({type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.disableChannelUrl(),
        data: {
            channelId: channelId,
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.channeldetail.channelListPageUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function enableChannel() {
    var ret = confirm("Are you sure you want to activate this Channel?");
    if (!ret)
        return;
    showDialog();

    var channelId = localStorage.getItem("Channel_id");
    $.ajax({type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.enableChannelUrl(),
        data: {
            channelId: channelId,
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.channeldetail.channelListPageUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}


function toggleFrameDelay(flag){
    if(flag)
        $('#id_manualdelay').show();
    else
        $('#id_manualdelay').hide();
}

function updateChannelAfterValidation(form) {
    if(validateInputFields(form)){
        updateChannel();
    }
}