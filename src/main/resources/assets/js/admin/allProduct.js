$(document).ready(function () {
    loadProduct();
    setClientDrop();
});

function setClientDrop() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            closeDialog();
            var clientDrop = '<span class="input-group-addon">Clients</span><select id="client_list" name="client_id" class="form-control">';
            clientDrop += '<option VALUE="-111">--All clients--</option>';
            for (var i in data)
            {
                if (data[i].clienttype == 'Client') {
                    clientDrop += '<option VALUE="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
            }
            clientDrop += '</select>';
            $('#clientList_div').html(clientDrop);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function getId(id) {
    window.open('./update?mapID='+id);
}

function loadProduct() {
    var searchResultsTable = $('#all_product_table').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.brand.allProductListUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#all_product_table').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.productId = numberValidation($("#productId").val()) ? $("#productId").val() : '-111';
                d.productName = $("#productName").val();
                d.client_id = $("#client_list").val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "clientId",
                "orderable": false,
                "render": function (data, type, full) {
                    return  $('<a/>').attr('href', '#').attr('id', full.mapId ).attr('onclick', 'getId(this.id)').append($('<span>' + full.mapId + '</span>')).wrap('<div/>').parent()
                            .html();
                }
            },
            {"width": "5%",
                "data": "productName",
                "orderable": false
            },
            {"width": "15%",
                "data": "clientName",
                "orderable": false
            },
            {"width": "5%",
                "data": "brandName",
                "orderable": false
            },
            {
                "width": "5%",
                "data": "code",
                "orderable": false
            }
        ]

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}
