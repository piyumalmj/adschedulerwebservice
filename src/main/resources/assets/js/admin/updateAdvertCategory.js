$(document).ready(function () {
    showDialog();
    var category_details = localStorage.getItem("Advrt_category_id");
    var data = category_details.split("~");

    document.getElementById("txt_advert_category_id").value = data[0];
    document.getElementById("txt_advert_category").value = data[1];

});

function updateAdvertCategory()
{
    var advert_category_id = document.getElementById("txt_advert_category_id").value;
    var advert_category = document.getElementById("txt_advert_category").value;

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategoryUpdateUrl(),
        data: {
            category_id: advert_category_id,
            category: advert_category,
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                alert("successfully updated");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateAdvertCategoryAfterValidation(form){
    if(validateInputFields(form)){
        updateAdvertCategory();
    }
}
