function clearFilter() {
    $("#wo-type").selectpicker('val', "-111");
    $("#agency-list").selectpicker('val', "-111");
    $("#client-list").selectpicker('val', "-111");
    $("#me-list").selectpicker('val', "-111");
    $("#month-list").selectpicker('val', "-111");
    $("#year-list").selectpicker('val', "-111");
}

$(document).ready(function () {
    loadClients();
});

function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
            for (var i in data) {
                if (data[i].clienttype == "Agency") {
                    $('#agency-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                } else {
                    $('#client-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                }
            }
            $("#client-list").selectpicker("refresh");
            $("#agency-list").selectpicker("refresh");
            closeDialog();
            loadMe();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function loadMe() {
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    $.ajax({
        type: 'POST',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.userDetails.getMeUserUrl(),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data)
        {
            for (var i in data) {
                $('#me-list').append('<option value="' + data[i].userName + '">' + data[i].name + '</option>');
            }
            closeDialog();
            $("#me-list").selectpicker("refresh");
            loadYear();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function loadYear() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.report.getYearsValueUrl(),
        success: function (data)
        {
            for (var key in data) {
                $('#year-list').append('<option value="' + key + '">' + data[key] + '</option>');
            }
            closeDialog();
            $("#year-list").selectpicker("refresh");
            $('.selectpicker').selectpicker({
                size: 10
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function setSellerTable() {
    showDialog();

    var woType_ids = '';
    var woTypeDrop = $('#wo-type').find("option:selected");
    woTypeDrop.each(function () {
        woType_ids += ',' + $(this).val();
    });

    var meList_ids = '';
    var meDrop = $('#me-list').find("option:selected");
    meDrop.each(function () {
        meList_ids += ',' + $(this).val();
    });

    var monthList_ids = '';
    var monthDrop = $('#month-list').find("option:selected");
    monthDrop.each(function () {
        monthList_ids += ',' + $(this).val();
    });

    var yearList_ids = '';
    var yearDrop = $('#year-list').find("option:selected");
    yearDrop.each(function () {
        yearList_ids += ',' + $(this).val();
    });

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var agencyList_ids = '';
    var agencyDrop = $('#agency-list').find("option:selected");
    agencyDrop.each(function () {
        agencyList_ids += ',' + $(this).val();
    });

    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.report.getSellerReportUrl(),
        data: {
            woType: woType_ids,
            meList: meList_ids,
            monthList: monthList_ids,
            yearList: yearList_ids,
            clientList: clientList_ids,
            agencyList: agencyList_ids
        },
        success: function (data)
        {
            var tableBody = '';
            for (var i in data) {
                tableBody += '<tr>';

                tableBody += '<td>' + data[i].workOrderId + '</td>';
                tableBody += '<td>' + data[i].agencyName + '</td>';
                tableBody += '<td>' + data[i].clientName + '</td>';
                tableBody += '<td>' + data[i].workOrderType + '</td>';
                tableBody += '<td>' + data[i].invoiceType + '</td>';
                tableBody += '<td>' + data[i].month + '</td>';
                tableBody += '<td>' + data[i].me + '</td>';
                tableBody += '<td>' + data[i].amount + '</td>';
                tableBody += '<td>' + data[i].primeSpotCount + '</td>';
                tableBody += '<td>' + data[i].nonPrimeSpotCount + '</td>';
                tableBody += '<td>' + data[i].rate + '</td>';
                tableBody += '<td>' + data[i].workOrderStatus + '</td>';
                if (data[i].invoiceDate !== null) {
                    tableBody += '<td>' + data[i].invoiceDate + '</td>';
                } else {
                    tableBody += '<td></td>';
                }
                tableBody += '<td>' + data[i].invoiceNo + '</td>';
                tableBody += '<td>' + data[i].payment + '</td>';

                tableBody += '</tr>';
            }
            $('#sellers_table_body').html(tableBody);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function getSellerReport() {
    setSellerTable();
}

function generateExcel() {
    showDialog();

    var woType_ids = '';
    var woTypeDrop = $('#wo-type').find("option:selected");
    woTypeDrop.each(function () {
        woType_ids += ',' + $(this).val();
    });

    var meList_ids = '';
    var meDrop = $('#me-list').find("option:selected");
    meDrop.each(function () {
        meList_ids += ',' + $(this).val();
    });

    var monthList_ids = '';
    var monthDrop = $('#month-list').find("option:selected");
    monthDrop.each(function () {
        monthList_ids += ',' + $(this).val();
    });

    var yearList_ids = '';
    var yearDrop = $('#year-list').find("option:selected");
    yearDrop.each(function () {
        yearList_ids += ',' + $(this).val();
    });

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var agencyList_ids = '';
    var agencyDrop = $('#agency-list').find("option:selected");
    agencyDrop.each(function () {
        agencyList_ids += ',' + $(this).val();
    });
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.report.writeSellerReportUrl(),
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: {
            woType: woType_ids,
            meList: meList_ids,
            monthList: monthList_ids,
            yearList: yearList_ids,
            clientList: clientList_ids,
            agencyList: agencyList_ids
        },
        success: function (data)
        {
            if (data) {
                window.location = URI.report.downloadSellerReportUrl();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}