$(document).ready(function () {
    setDummyutTable();
});

function setDummyutTable() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getDummyCutReportUrl(),
        success: function (data)
        {
            var trHTML = '<thead><tr><th>WO ID</th><th>Client Name</th><th>Product</th><th>ME</th><th>Cut ID</th><th>Create date</th><th>Duration</th><th>Lan</th><th>No of Spots</th><th>Spots Airing Date Range</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                trHTML += '<tr>';
                trHTML += '<td>' + data[i].woId + '</td>';
                trHTML += '<td>' + data[i].clientName + '</td>';
                trHTML += '<td>' + data[i].product + '</td>';
                trHTML += '<td>' + data[i].me + '</td>';
                trHTML += '<td>' + data[i].cutID + '</td>';
                trHTML += '<td>' + data[i].createDate + '</td>';
                trHTML += '<td>' + data[i].duration + '</td>';
                trHTML += '<td>' + data[i].language + '</td>';
                trHTML += '<td>' + data[i].numberOfSpot + '</td>';
                trHTML += '<td>' + data[i].airingDateRange + '</td>';
                trHTML += '</tr>';
            }
            trHTML += '</tbody>';

            $('#dummycut_table').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function generateExcel() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.writeDummyCutReportUrl(),
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.report.downloadDummyCutReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

