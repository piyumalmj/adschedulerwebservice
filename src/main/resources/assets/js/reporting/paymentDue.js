function clearFilter() {
    $("#agency-list").selectpicker('val', "-111");
    $("#client-list").selectpicker('val', "-111");
    $("#month-list").selectpicker('val', "-111");
    $("#year-list").selectpicker('val', "-111");
}

$(document).ready(function () {
    loadClients();
});

function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.client.clientForWoTabaleUrl(),
        success: function (data)
        {
            for (var i in data) {
                if (data[i].clientType == "Agency") {
                    $('#agency-list').append('<option value="' + data[i].clientId + '">' + data[i].clientName + '</option>');
                } else {
                    $('#client-list').append('<option value="' + data[i].clientId + '">' + data[i].clientName + '</option>');
                }
            }
            $("#agency-list").selectpicker("refresh");
            $("#client-list").selectpicker("refresh");
            closeDialog();
            loadYear();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function loadYear() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.report.getYearsValueUrl(),
        success: function (data)
        {
            for (var key in data) {
                $('#year-list').append('<option value="' + key + '">' + data[key] + '</option>');
            }
             $("#year-list").selectpicker("refresh");
            closeDialog();
            $('.selectpicker').selectpicker({
                size: 10
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function setTable() {
    showDialog();

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var agencyList_ids = '';
    var agencyDrop = $('#agency-list').find("option:selected");
    agencyDrop.each(function () {
        agencyList_ids += ',' + $(this).val();
    });

    var monthList_ids = '';
    var monthDrop = $('#month-list').find("option:selected");
    monthDrop.each(function () {
        monthList_ids += ',' + $(this).val();
    });

    var yearList_ids = '';
    var yearDrop = $('#year-list').find("option:selected");
    yearDrop.each(function () {
        yearList_ids += ',' + $(this).val();
    });

    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.report.getPaymentDueReportUrl(),
        data: {
            monthList: monthList_ids,
            yearList: yearList_ids,
            clientList: clientList_ids,
            agencyList: agencyList_ids
        },
        success: function (data)
        {
            var tableBody = '';
            for (var i in data) {
                tableBody += '<tr>';

                tableBody += '<td>' + data[i].workOrderId + '</td>';
                tableBody += '<td>' + data[i].agencyName + '</td>';
                tableBody += '<td>' + data[i].clientName + '</td>';
                tableBody += '<td>' + data[i].workOrderType + '</td>';
                tableBody += '<td>' + data[i].scheduleref + '</td>';
                tableBody += '<td>' + data[i].marketingExecutive + '</td>';
                tableBody += '<td>' + data[i].packegeAmount + '</td>';
                tableBody += '<td>' + data[i].workOrderStatus + '</td>';
                if (data[i].invoiceDate !== null) {
                    tableBody += '<td>' + data[i].invoiceDate + '</td>';
                } else {
                    tableBody += '<td></td>';
                }
                tableBody += '<td>' + data[i].invoiceNo + '</td>';
                tableBody += '<td>' + data[i].invoiceAmount + '</td>';

                tableBody += '</tr>';
            }
            $('#payment_due_table_body').html(tableBody);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function getPaymentDueReport() {
    setTable();
}

function generateExcel() {
    showDialog();

    var monthList_ids = '';
    var monthDrop = $('#month-list').find("option:selected");
    monthDrop.each(function () {
        monthList_ids += ',' + $(this).val();
    });

    var yearList_ids = '';
    var yearDrop = $('#year-list').find("option:selected");
    yearDrop.each(function () {
        yearList_ids += ',' + $(this).val();
    });

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var agencyList_ids = '';
    var agencyDrop = $('#agency-list').find("option:selected");
    agencyDrop.each(function () {
        agencyList_ids += ',' + $(this).val();
    });

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.report.writePaymentDueReportUrl(),
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: {
            monthList: monthList_ids,
            yearList: yearList_ids,
            clientList: clientList_ids,
            agencyList: agencyList_ids
        },
        success: function (data)
        {
            if (data) {
                window.location = URI.report.downloadPaymentDueReportUrl();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}