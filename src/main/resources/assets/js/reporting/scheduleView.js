var channelCount = 0;
var advertCount = 0;
var selectedChannelId = -999;

var workOrderId = 0;
var startDate = '';
var endDate = '';
var workOrderName = '';

var startTimeDrop = [];
var endTimeDrop = [];

var default_tr_left = '';
var default_tr_data = '';

var mapChannelAvailableSpot = {};
var columCountArray = {};
$(document).ready(function () {
    setWorkOrderDetails();
});

/*-----01-----*/
function setWorkOrderDetails() //
{
    showDialog();
    workOrderId = stoi(getParameterByNamefromUrl('work_id'));
    startDate = getParameterByNamefromUrl('start_date');
    endDate = getParameterByNamefromUrl('end_date');
    workOrderName = getParameterByNamefromUrl('work_name');

    url_data = '?work_id=' + workOrderId + '&work_name=' + workOrderName + '&start_date=' + startDate + '&end_date=' + endDate;

    $('#work_order_id').html(workOrderId);
    $('#start_date').html(startDate);
    $('#end_date').html(endDate);
    closeDialog();
    getWODetails(workOrderId);
}

/*-----02-----*/
function getWODetails(wo_id) {
    showDialog();
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.selectedWorkOrderUrl(),
        data: {
            workOrderId: wo_id,
        },
        success: function (data)
        {
            closeDialog();
            setAdvertisementDetails(data[0].clientName, data[0].billingClient);
            workOrderStatus = data[0].workOrderStatus;
            document.getElementById("lbl_agency").innerHTML = data[0].agencyByName;
            document.getElementById("lbl_client").innerHTML = data[0].clientByName;
            document.getElementById("schedule_value").innerHTML = getDecimal(data[0].totalbudget);
            document.getElementById("lbl_me").innerHTML = data[0].seller;
            document.getElementById("lbl_wo_type").innerHTML = data[0].woTyep;
            document.getElementById("lbl_schedule_ref").innerHTML = data[0].scheduleRef;
            document.getElementById("lbl_product").innerHTML = data[0].productName;
            document.getElementById("lbl_Commission").innerHTML = data[0].commission + "%";

            var arr = data[0].channelList;
            var drop_channelList = '<select id="selected_ChannelDropdown" name="channelName" onchange="setSpotTable(this.value)" class="form-control middle_select"><option value="-999">Select Channel</option>';
            for (var i in arr) {
                drop_channelList += '<option value="' + arr[i].channelid + '">' + arr[i].channelname + '</option>';
            }
            drop_channelList += '</select>';
            $('#selectedChannelList').html(drop_channelList);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    })
}

/*-----03-----*/
function setAdvertisementDetails(clientOne, ClientTwo)//
{
    showDialog();
    var filter_advertDrop = '<option value="-999">All advert</option>';
    var filter_advertDurationDrop = '<option value="-999">All Duaration</option>';
    var duration = {};
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSelectedClientListUrl(),
        data: {
            clieanOne: clientOne,
            clieanTwo: ClientTwo
        },
        success: function (data)
        {
            closeDialog();
            ;
            for (var i in data)
            {
                filter_advertDrop += '<option value=' + data[i].advertid + '>' + data[i].advertname + '</option>';
                duration[data[i].duration] = '<option value=' + data[i].duration + '>' + data[i].duration + '</option>';
            }
            $('#filter_AdvertDrop').html(filter_advertDrop);

            for (var j in duration) {
                filter_advertDurationDrop += duration[j];
            }
            filter_advertDurationDrop += '</select>';
            $('#filter_AdvertDuratinoDrop').html(filter_advertDurationDrop);
            setAdvertTable();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    })
}

/*-----04-----*/
function setAdvertTable() // create all_advert dropdown in schedule table and all advert table
{
    showDialog();
    console.log(workOrderId);
    var v_workOrderId = workOrderId;
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.clientOrderAdvertListUrl(),
        data: {
            workOrderId: v_workOrderId,
            advertType: 'All',
        },
        success: function (data)
        {
            closeDialog();
            var advertTableMain = '<table style="margin-top: 12px;  margin-left: 12px; white-space: nowrap !important; overflow: hidden;" id="advert_main_table" class="table table-bordered table-workorder"><thead><tr><th>ID</th><th>Name</th><th>Duration(Sec)</th><th>Type</th></tr></thead><tbody>';

            for (var i in data)
            {
                advertTableMain += '<tr><td><a href="#" id="' + data[i].advertId + '" onclick="getId(this.id)">' + data[i].advertId + '</a></td><td>' + data[i].advertName + '</td><td>' + data[i].duration + '</td><td>' + getAdvertType(data[i].type) + '</td></tr>';
            }
            advertTableMain += '</tbody></table>';
            $('#advert_main_table_div').html(advertTableMain);
            setChannelDetails();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*-----05-----*/
function setChannelDetails() //
{
    showDialog();
    var v_workOrderId = workOrderId;
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderChannelListUrl(),
        data: {
            workOrderId: v_workOrderId,
        },
        success: function (data)
        {
            closeDialog();
            var filterChannelDrop = '<select id="schedule_filterChannelDropdown" onchange="filter_channelDropDownChange()" name="channellllName" class="form-control">';
            filterChannelDrop += '<option id="-999_ch" value="-999">All Channels</option>';
            for (var i in data)
            {
                filterChannelDrop += '<option id="' + i + '_ch" value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                loadAvailableSpot(data[i].channelid);
            }
            filterChannelDrop += '</select>';
            $('#filterChannelDropdown_div').html(filterChannelDrop);
            $("#filterChannelDropdown_div").val(-999);
            loadAvailableSpot(-999);
            setSpotTable(-999)
            createScheduleTable();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*-----06-----*/
function loadAvailableSpot(channelID) {
    showDialog();
    var chID = channelID;
    var v_workOrderId = workOrderId;
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.scheduler.selectedScheduleAvailableSpotsCountUrl(),
        data: {
            workOrderId: v_workOrderId,
            channelId: chID,
        },
        success: function (data)
        {
            mapChannelAvailableSpot[chID] = data;
            //setSpotTable(data);
            closeDialog();
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            return false;
        }
    })
}

/*-----07-----*/
function setSpotTable(channel_id) {
    var data = mapChannelAvailableSpot[channel_id];
    spotArray = data;
    var spotTable = '<table id="spot_table" ><thead>';
    var spotThesd = '<tr><th>Duration</th>';
    var spotTbody = '<tbody><tr><td>TVC</td>';
    var count = 1;
    for (var key in spotArray.tvcBreackdownMap) {
        var AvailableSpot = 0;
        if (key in spotArray.tvcBreackdownMapAvailable) {
            AvailableSpot = spotArray.tvcBreackdownMapAvailable[key];
            spotThesd += '<th id="' + key + '">' + key + '</th>';
            spotTbody += '<td>' + (spotArray.tvcBreackdownMap[key] - spotArray.tvcBreackdownMapAvailable[key]) + '/' + spotArray.tvcBreackdownMap[key] + '</td>';
            // spotTable += '<tr><td id="' + key + '">' + key + '</td><td>' + (spotArray.tvcBreackdownMap[key] - spotArray.tvcBreackdownMapAvailable[key]) + '/' + spotArray.tvcBreackdownMap[key] + '</td><td></td><td></td><td></td><td></td></tr>';
            count++;
        }
    }

    spotThesd += '</thead>'
    spotTbody += '</tr>';
    var notTVC = '';
    if (data.logoSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Logo</td><td colspan="' + count + '">' + (spotArray.logoSpots - spotArray.availableLogoSpots) + '/' + spotArray.logoSpots + '</td></tr>';
    }
    if (data.crowlerSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Crowler</td><td colspan="' + count + '">' + (spotArray.crowlerSpots - spotArray.availableCrowlerSpots) + '/' + spotArray.crowlerSpots + '</td></tr>';
    }
    if (data.v_sqeezeSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Vsqeeze</td><td colspan="' + count + '">' + (spotArray.v_sqeezeSpots - spotArray.availableV_SqeezeSpots) + '/' + spotArray.v_sqeezeSpots + '</td></tr>';
    }
    if (data.l_sqeezeSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Lsqeeze</td><td colspan="' + count + '">' + (spotArray.l_sqeezeSpots - spotArray.availableL_SqeezeSpots) + '/' + spotArray.v_sqeezeSpots + '</td></tr>';
    }

    spotTbody += notTVC + '</tbody>';
    spotTable += spotThesd + spotTbody + '</table>';
    $('#spot_table_div').html(spotTable);
}

/*-----08-----*/
function createScheduleTable() // set data row header and add empty data row
{
    // console.log(startDate);
    var timeDiff = getDifferanBetweenDay(startDate, endDate);

    var dataTable = ' <table id="table_data" class="table table-responsive  table-workorder special-table" >';
    var tableHead = '<thead><tr>';
    var monthtableHead = '<thead class="blank_head"><tr>';
    var monthDiv = '<div class="row-month">';
    var tableBody = '<tbody id="tbody_data"><tr id="tbdata_default">';
    var tableColum = '<tr id="tbdata_count">'
    var count = 0;
    var totalCount = 0;
    for (var i = 0; i <= timeDiff; i++)
    {
        var day = getDay(addDays(startDate, i, 1));
        var className = ''
        if (day == "Su" || day == "Sa")
        {
            className = "data-cell-blue_th";
        }
        var d = addDays(startDate, i, 3);
        tableHead += '<th class=' + className + '><span class="date-l">' + day + '</span><br><span class="date-n">' + addDays(startDate, i, 3) + '</span> </th>';
        if (d == 1 || i == timeDiff) {

            if (i == timeDiff) {
                if (d == 1) {
                    monthDiv += '<div class="col-' + count + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';
                    monthDiv += '<div class="col-1" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i, 1)) + '</div>';

                    monthtableHead += '<th style= width:' + 27 * (count) + 'px>' + getMonth(addDays(startDate, i - 1, 1)) + '</th>';
                    monthtableHead += '<th style= width:' + 27 + 'px>' + getMonth(addDays(startDate, i, 1)) + '</th>';
                } else {
                    monthDiv += '<div class="col-' + (count + 1) + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';

                    monthtableHead += '<th style= width:' + 27 * (count + 1) + 'px>' + getMonth(addDays(startDate, i - 1, 1)) + '</th>';
                }
            }
            else {
                if (i != 0) {
                    monthDiv += '<div class="col-' + count + '" style="left:' + (27 * totalCount) + 'px">' + getMonth(addDays(startDate, i - 1, 1)) + '</div>';

                    monthtableHead += '<th style= width:' + 27 * (count) + 'px>' + getMonth(addDays(startDate, i - 1, 1)) + '</th>';
                }
            }
            totalCount += count;
            count = 0;
        }
        count++;
        className = className.replace("_th", "");
        //tableBody += '<td class=' + className + '><input type="text" class="form-control" id="' + addDays(startDate, i, 1) + '_txt" name="count" value="" onkeyup="scheduleCount(this.id)" required="" title="Please enter Advertisement count" ></td>';
        tableColum += '<td class="data-cell-light_orange" id="' + addDays(startDate, i, 1) + '_count"></td>';
    }
    tableHead += '<th class="last-th"></th></tr></thead>';
    monthtableHead += '</tr></thead>';
    tableBody += '</tr>';
    tableColum += '</tr></tbody>';
    monthDiv += '</div>';
    dataTable += tableHead + tableBody + tableColum + '</table>';

    $('#div_table_data').html(monthDiv + dataTable);
    default_tr_data = $('#tbody_data').html();
    createUpdateScheduleView();
}

/*-----09-----*/
function createUpdateScheduleView() // fill schedule data
{
    showDialog();

    fillBody = false;
    var v_workOrderId = workOrderId;
    var tableRow_Left = '';
    var tableRow_Data = '';

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getWOScheduleViewUrl(),
        data: {
            workOrderId: v_workOrderId,
            channelId: -999,
        },
        success: function (json_data)
        {
            buildUpdateScheduleView(json_data);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*-----10-----*/
var checkFiler = true;
function buildUpdateScheduleView(json_data) ///refactored
{
    json_data_view = json_data;
    var jsonData = json_data.timebeltSchedules;
    var data = [];
    isRowCopping = false;

    for (var i in jsonData) {
        data.push(jsonData[i]);
    }
    tableData = data;
    tableRow_Left = '';
    tableRow_Data = '';

    $('#tbody_left').html("");
    $('#tbody_data').html("");
    data = filterChannel(data);
    data = filterAdvert(data);
    data = filterDurationAdvert(data);
    data = filterStartTime(data);
    data = filterEndTime(data);

    for (var i in data)
    {
        tableRow_Left += '<tr id="tbleft_' + i + '">';
        tableRow_Data += '<tr id="tbdata_' + i + '">';
        var channelName = data[i].channelName;
        if (channelName != null)
        {
            tableRow_Left += '<td ><input type="checkbox" value="' + i + '" /></td>';
            tableRow_Left += '<td ><button type="button" class="btn-empty" aria-label="Priority" onclick="rederectToPriorityPage(' + i + ')"><span class="glyphicon glyphicon-star-empty"></span></button></td>';
            tableRow_Left += '<td ><buttsaveon type="button" class="btn-empty" aria-label="Close" onclick="removeRow(' + i + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
            tableRow_Left += '<td><button type="button" class="btn-empty" aria-label="Update" onclick="updateRow(' + i + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
            tableRow_Left += '<td><div data-toggle="tooltip" data-placement="right" title="' + data[i].channelName + '">' + data[i].channelName + '</div></td>';
            tableRow_Left += '<td><div align="left" style=" padding-left: 5px">' + data[i].advertName + '</div></td><td><b>' + setLanguage_returnFistLetter(data[i].adverLang) + '</b></td><td>' + data[i].advertDuration + '</td><td>' + data[i].revenueline + '</td><td>' + RemoveSec(data[i].schedulestarttime) + '</td><td>' + RemoveSec(data[i].scheduleendtime) + '</td>';
        }
        var statTime = '{"start":"' + data[i].schedulestarttime + '"}';
        var objStart = JSON.parse(statTime);
        var startFlag = true;

        var endTime = '{"end":"' + data[i].scheduleendtime + '"}';
        var objEnd = JSON.parse(endTime);
        var endFlag = true;

        if (startTimeDrop.length == 0) {
            startTimeDrop.push(objStart);
        } else {

            for (var a = 0; a < startTimeDrop.length; a++)
            {
                if (startTimeDrop[a].start == objStart.start) {
                    startFlag = false;
                }
            }
            if (startFlag) {
                startTimeDrop.push(objStart);
            }
        }

        if (endTimeDrop.length == 0) {
            endTimeDrop.push(objEnd);
        } else {

            for (var b = 0; b < endTimeDrop.length; b++)
            {
                if (endTimeDrop[b].end == objEnd.end) {
                    endFlag = false;
                }
            }
            if (endFlag) {
                endTimeDrop.push(objEnd);
            }
        }

        var today = getDate();
        var time = getTime();
        var dateList = data[i].scheduleItem;
        var count = 0;
        for (var j in dateList)
        {
            var toColor = getDay(dateList[j].date)
            var className = ''
            if (toColor == "Su" || toColor == "Sa")
            {
                className = "data-cell-blue";
            } else {
                className = "disable";
            }
            count += dateList[j].advertCount;
            var advCount = dateList[j].advertCount;
            if (advCount == 0) {
                advCount = "";
            }
            if (dateList[j].date < today) {

                tableRow_Data += '<td class=' + className + '>' + advCount + '</td>';
            } else if (dateList[j].date == today && data[i].schedulestarttime <= time) {
                tableRow_Data += '<td class=' + className + '>' + advCount + '</td>';
            } else {
                if (className == 'disable') {
                    className = '';
                }
                tableRow_Data += '<td class=' + className + '>' + advCount + '</td>';
            }

            if (i == 0) {
                columCountArray[dateList[j].date] = dateList[j].advertCount;
            } else {
                var columCOunt = columCountArray[dateList[j].date];
                var newCount = columCOunt + dateList[j].advertCount;
                columCountArray[dateList[j].date] = newCount;
            }
        }
        if (dateList.length != 0) {
            tableRow_Left += '<td class="data-cell-light_orange_left">' + count + '</td>';
        }
        tableRow_Left += '</tr>';
        tableRow_Data += '</tr>';
    }
    tableRow_Left += '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
    tableRow_Data += default_tr_data;
    $('#tbody_left').html(tableRow_Left);
    $('#tbody_data').html(tableRow_Data);
    $(window).trigger('resize');

    disableDataRowCells(startDate, addDays(getDate(), 1, 1));
    $("#month").val(getCurrentMonth());
    $("#schedule_channelDropdown").val(selectedChannelId);

    var totalSpotCount = 0;
    for (var key in columCountArray) {
        var id = key + '_count';
        var adCount = columCountArray[key];
        totalSpotCount += adCount;
        if (adCount == 0) {
            adCount = "";
        }
        document.getElementById(id).innerHTML = adCount;
    }

    if (checkFiler) {
        setTimeForFilter();
    }
    checkFiler = false;
    $('[data-toggle="tooltip"]').tooltip();
}

function setTimeForFilter() {
    var startDrop = '<option value="-999">All Sta</option>';
    var endDrop = '<option value="-999">All Sta</option>';

    for (var i = 0; i < startTimeDrop.length; i++)
    {
        startDrop += '<option value="' + startTimeDrop[i].start + '">' + RemoveSec(startTimeDrop[i].start) + '</option>';
    }

    for (var i = 0; i < endTimeDrop.length; i++)
    {
        endDrop += '<option value="' + endTimeDrop[i].end + '">' + RemoveSec(endTimeDrop[i].end) + '</option>';
    }

    $('#startTime_filter').html(startDrop);
    $('#endTime_filter').html(endDrop);
}

function filter_channelDropDownChange() {
    buildUpdateScheduleView(json_data_view);
    var fiter_channel = document.getElementById("schedule_filterChannelDropdown");
    var fiter_channel_id = fiter_channel.options[fiter_channel.selectedIndex].value;
}

function filter_startTimeChange() {
    buildUpdateScheduleView(json_data_view);
}

function filter_endTimeChange() {
    buildUpdateScheduleView(json_data_view);
}

function filter_AdvertChange() {
    buildUpdateScheduleView(json_data_view);
}

function filter_AdvertDurationChange() {
    buildUpdateScheduleView(json_data_view);
}

function filterChannel(data) {
    var fiter_channel = document.getElementById("schedule_filterChannelDropdown");
    var fiter_channel_id = fiter_channel.options[fiter_channel.selectedIndex].value;
    if (fiter_channel_id != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (fiter_channel_id != data[i].channelid) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterStartTime(data) {
    var fiter_Startdrop = document.getElementById("startTime_filter");
    var fiter_Start_time = fiter_Startdrop.options[fiter_Startdrop.selectedIndex].value;
    if (fiter_Start_time != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (!(data[i].schedulestarttime >= fiter_Start_time)) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterEndTime(data) {
    var fiter_enddrop = document.getElementById("endTime_filter");
    var fiter_end_time = fiter_enddrop.options[fiter_enddrop.selectedIndex].value;
    if (fiter_end_time != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (!(data[i].scheduleendtime <= fiter_end_time)) {
                data.splice(i, 1);
                i--;
            } else {

            }
        }
        return data;
    } else {
        return data;
    }
}

function filterAdvert(data) {
    var fiter_advertdrop = document.getElementById("filter_AdvertDrop");
    var fiter_advertID = fiter_advertdrop.options[fiter_advertdrop.selectedIndex].value;
    if (fiter_advertID != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (fiter_advertID != data[i].advertid) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}

function filterDurationAdvert(data) {
    var fiter_advertDurationdrop = document.getElementById("filter_AdvertDuratinoDrop");
    var fiter_advertduration = fiter_advertDurationdrop.options[fiter_advertDurationdrop.selectedIndex].value;
    if (fiter_advertduration != -999) {

        for (var i = 0; i < data.length; i++)
        {
            if (fiter_advertduration != data[i].advertDuration) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    } else {
        return data;
    }
}
/*---------------------------Date--------------------------------------------*/
function addDays(date, days, type)//
{
    var result = new Date(date);
    result.setDate(result.getDate() + days);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = yyyy + '/' + mm + '/' + dd;

    if (type == 2)
    {
        return mm + '-' + dd;
    } else if (type == 3) {
        return dd;
    } else {
        return yyyy + '-' + mm + '-' + dd;
    }
}

function getDifferanBetweenDay(dt1, dt2)
{
    var date1 = new Date(dt1);
    var date2 = new Date(dt2);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    return diffDays;
}

function getDay(date)
{
    var d = new Date(date);
    var num = d.getDay();
    var day = '';
    switch (num) {
        case 0:
            day = "Su";
            break;
        case 1:
            day = "Mo";
            break;
        case 2:
            day = "Tu";
            break;
        case 3:
            day = "We";
            break;
        case 4:
            day = "Th";
            break;
        case 5:
            day = "Fr";
            break;
        case 6:
            day = "Sa";
    }
    return day;
}

function getMonth(date) {

    var result = new Date(date);
    result.setDate(result.getDate());

    var mm = result.getMonth();

    var month = '';
    switch (mm) {
        case 0:
            month = "January";
            break;
        case 1:
            month = "February";
            break;
        case 2:
            month = "March";
            break;
        case 3:
            month = "April";
            break;
        case 4:
            month = "May";
            break;
        case 5:
            month = "June";
            break;
        case 6:
            month = "July";
            break;
        case 7:
            month = "August";
            break;
        case 8:
            month = "September";
            break;
        case 9:
            month = "October";
            break;
        case 10:
            month = "November";
            break;
        case 11:
            month = "December";
            break;
    }

    return month;
}

function getDate() {
    var yyyy = new Date().getFullYear();
    var mm = new Date().getMonth() + 1;
    var dd = new Date().getDate();
    if (dd < 10)
        dd = '0' + dd;
    if (mm < 10)
        mm = '0' + mm;

    var today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function getTime() {
    var h = new Date().getHours(); // 0-24 format
    if (h < 10)
        h = '0' + h;

    var now = h + ':00:00';
    return now;
}

function disableDataRowCells(startDate, limitDate) {
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    for (var i = 0; i <= timeDiff; i++)
    {
        var dt = addDays(startDate, i, 1);
        if (dt < limitDate) {
            $('#' + dt + '_txt').attr('readonly', true);
            $('#' + dt + '_txt').parent('td').addClass('disable');
        }
        else {
            $('#' + dt + '_txt').attr('readonly', false);
            $('#' + dt + '_txt').parent('td').removeClass('disable');
        }
    }
}

function getCurrentMonth() {
    var d = new Date();
    var month = d.getMonth();

    switch (month) {
        case 0:
            return "Jan"
            break;

        case 1:
            return "Feb"
            break;

        case 2:
            return "Mar"
            break;

        case 3:
            return "Apr"
            break;

        case 4:
            return "May"
            break;

        case 5:
            return "Jun"
            break;

        case 6:
            return "Jul"
            break;

        case 7:
            return "Aug"
            break;

        case 8:
            return "Sep"
            break;

        case 9:
            return "Oct"
            break;

        case 10:
            return "Nov"
            break;

        case 11:
            return "Dec"
            break;

        default:
            return month;
            break;
    }
}

function getId(id) {
    window.open(URI.advertisement.updateAdvertisementUrl(id));
}

function printSchedule() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.writeSchedulePrintReportUrl(),
        data: {
            workOrderId: workOrderId
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location =URI.report.downloadSchedulePrintReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

/*-------------------------------------WorkOrder----------------------------------------------------*/

function getParameterByNamefromUrl(name) {
    url = window.location.href;
    console.log(url);

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
/*-------------------------------------End workOrder------------------------------------------------*/
