function clearFilter() {
    $("#cut-id-list").selectpicker('val', "-111");
    $("#client-list").selectpicker('val', "-111");
}

$(document).ready(function () {
    loadClients();
});

function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
            for (var i in data) {
                if (data[i].clienttype !== "Agency") {
                    $('#client-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                }
            }
            closeDialog();
            $("#client-list").selectpicker("refresh");
            loadAdvertIDs();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function loadAdvertIDs() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.report.getAdvertIDsUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#cut-id-list').append('<option value="' + data[i] + '">' + data[i] + '</option>');
            }
            $("#cut-id-list").selectpicker("refresh");
            closeDialog();

            $('.selectpicker').selectpicker({
                size: 10
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function setMaterialAnalysisTable() {
    showDialog();

    /*var woType_ids = '';
     var woTypeDrop = $('#wo-type').find("option:selected");
     woTypeDrop.each(function () {
     woType_ids += ',' + $(this).val();
     });
     
     var meList_ids = '';
     var meDrop = $('#me-list').find("option:selected");
     meDrop.each(function () {
     meList_ids += ',' + $(this).val();
     });
     
     var monthList_ids = '';
     var monthDrop = $('#month-list').find("option:selected");
     monthDrop.each(function () {
     monthList_ids += ',' + $(this).val();
     });
     
     var yearList_ids = '';
     var yearDrop = $('#year-list').find("option:selected");
     yearDrop.each(function () {
     yearList_ids += ',' + $(this).val();
     });*/

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var cut_ids = '';
    var cutIdsDrop = $('#cut-id-list').find("option:selected");
    cutIdsDrop.each(function () {
        cut_ids += ',' + $(this).val();
    });

    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.report.getMaterialAnalysisReportUrl(),
        data: {
            clientList: clientList_ids,
            cutIds: cut_ids
        },
        success: function (data)
        {
            var tableBody = '';
            for (var i in data) {
                tableBody += '<tr>';

                tableBody += '<td>' + data[i].cutId + '</td>';
                tableBody += '<td>' + data[i].advertisementName + '</td>';
                tableBody += '<td>' + data[i].agency + '</td>';
                tableBody += '<td>' + data[i].client + '</td>';
                tableBody += '<td>' + data[i].captureDate + '</td>';
                if (data[i].lastAiredDate === null) {
                    tableBody += '<td>Not aired</td>';
                } else {
                    tableBody += '<td>' + data[i].lastAiredDate + '</td>';
                }
                tableBody += '<td>' + data[i].expiredDate + '</td>';
                tableBody += '<td>' + data[i].status + '</td>';

                tableBody += '</tr>';
            }
            $('#material_table_body').html(tableBody);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function getMaterialAnalysisReport() {
    setMaterialAnalysisTable();
}

function generateExcel() {
    showDialog();

    var clientList_ids = '';
    var clientDrop = $('#client-list').find("option:selected");
    clientDrop.each(function () {
        clientList_ids += ',' + $(this).val();
    });

    var cut_ids = '';
    var cutIdsDrop = $('#cut-id-list').find("option:selected");
    cutIdsDrop.each(function () {
        cut_ids += ',' + $(this).val();
    });
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.report.writeMaterialAnalysisReportUrl(),
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: {
            clientList: clientList_ids,
            cutIds: cut_ids
        },
        success: function (data)
        {
            if (data) {
                window.location = URI.report.downloadMaterialAnalysisReportUrl();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}