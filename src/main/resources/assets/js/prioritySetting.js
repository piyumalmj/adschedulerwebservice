/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lstTimeBelts;
var lstPriorities;
var mapPriority = new Object();
var tableData;
var iPopupCellID;

$(document).ready(function () {

    $('#divFromDate').datetimepicker({
        pickTime: false
    });
    $('#divToDate').datetimepicker({
        pickTime: false
    });

    loadChannelList();

    iworkOrder = stoi(getParameterByNamefromUrl('wo_id'));
    if (iworkOrder == 0) {
        iworkOrder = -1;
    }
    dtStartDate = getParameterByNamefromUrl('s_date');
    dyEndDate = getParameterByNamefromUrl('e_date');
    iChannelId = stoi(getParameterByNamefromUrl('channel'));
    if (iChannelId == 0) {
        iChannelId = -1;
    }
    iAdvertId = stoi(getParameterByNamefromUrl('advert'));
    if (iAdvertId == 0) {
        iAdvertId = -1;
    }
    tStartTime = getParameterByNamefromUrl('s_time');

    $('#fromDate').val(dtStartDate);
    $('#toDate').val(dyEndDate);
    $('#channelDropdown').val(iChannelId);
    setWODetails(iworkOrder);
    channelSelectionChanged(iChannelId);
    $('#advertDropdown').val(iAdvertId);

    $.each(lstTimeBelts, function (index, item) {
        if (item.startTime == tStartTime) {
            $('#timebeltDropdown').val(item.timeBeltId);
            loadClusterList(item.timeBeltId);
        }
    });
});

function setWODetails(id) {
    $('#workOrderDropdown').val(id);
    loadAdvertList(id);
}

function loadChannelList() {
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.channelDetailUrl(),
        success: function (data)
        {
            $.each(data, function (index, value) {
                $('#channelDropdown').append($('<option/>', {
                    value: value.channelid,
                    text: value.channelname
                }));
            });
            loadingClose();
            loadWorkOrderList();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
    })
}

function channelSelectionChanged(iChannelID) {
    loadTimeBeltList(iChannelID);
    loadPriorityData(iChannelID);
}

function loadWorkOrderList() {
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            $.each(data, function (index, value) {
                $('#workOrderDropdown').append($('<option/>', {
                    value: value.workorderid,
                    text: value.workorderid + " - " + value.ordername
                }));
            });
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
    })
}

function loadTimeBeltList(iChannelID) {
    loadingOpen();
    $('#timebeltDropdown').empty();
    $('#timebeltDropdown').append($('<option/>', {
        value: -111,
        text: "------ All ------"
    }));

    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.timeBelts.getListbyChannelUrl(),
        data: {
            channelId: iChannelID,
        },
        success: function (data)
        {
            loadingClose();
            lstTimeBelts = data;
            $.each(data, function (index, value) {
                $('#timebeltDropdown').append($('<option/>', {
                    value: value.timeBeltId,
                    text: value.startTime + ' - ' + value.endTime
                }));
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
    })
}

function loadClusterList(iTimeBeltID) {
    loadingOpen();
    $('#clusterDropdown').empty();
    $('#clusterDropdown').append($('<option/>', {
        value: -111,
        text: "------ All ------"
    }));

    for (var index in lstTimeBelts) {
        item = lstTimeBelts[index];
        if (item.timeBeltId == iTimeBeltID) {
            for (var i = 1; i <= item.clusterCount; i++) {
                $('#clusterDropdown').append($('<option/>', {
                    value: i - 1,
                    text: "Cluster " + i
                }));
            }
        }
    }
    loadingClose();
}

function loadPriorityData(iChannelID) {
    loadingOpen();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.getListbyChannelUrl(),
        data: {
            channelId: iChannelID,
        },
        success: function (data)
        {
            loadingClose();
            lstPriorities = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
    })
}

function loadPriorityList() {
    loadingOpen();
    iTimeBeltID = $('#timebeltDropdown').val();
    iClusterID = $('#clusterDropdown').val();

    $('#priorityDropdown').empty();
    $('#priorityDropdown').append($('<option/>', {
        value: -1,
        text: "------ None ------"
    }));

    if (iTimeBeltID == -1 || iClusterID == -1)
        return;

    for (var index in lstPriorities) {
        item = lstPriorities[index];
        if (item.timebeltId == iTimeBeltID && item.cluster == iClusterID) {
            for (var i = 1; i <= item.priorityCount; i++) {
                $('#priorityDropdown').append($('<option/>', {
                    value: i,
                    text: "Priority " + i
                }));
            }
        }
    }
    loadingClose();
}

function loadAdvertList(iWOrderID) {
    $('#advertDropdown').empty();
    $('#advertDropdown').append($('<option/>', {
        value: -1,
        text: "------ None ------"
    }));
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.clientOrderAdvertListUrl(),
        data: {
            workOrderId: iWOrderID,
            advertType: 'All',
        },
        success: function (data)
        {
            $.each(data, function (index, value) {
                $('#advertDropdown').append($('<option/>', {
                    value: value.advertId,
                    text: value.advertName
                }));
            });
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
    })
}

function savePriority() {
    loadingOpen();

    viTimeBeltID = $('#timebeltDropdown').val();
    viCluster = $('#clusterDropdown').val();
    viWorkOrder = $('#workOrderDropdown').val();
    viAdvertID = $('#advertDropdown').val();
    viPriority = $('#priorityDropdown').val();
    vdtStartDate = $('#fromDate').val();
    vdtEndDate = $('#toDate').val();
    vbOverWrite = $('#ckbxOverWrite').prop('checked');//$('#timebeltDropdown').val();
    vstDays = getCheckedDates();//$('#timebeltDropdown').val();

    var currentDate = new Date();
    var currentTime = currentDate.getHours();
    currentTime = currentTime+":00:00";
    var today = getDate();
    var selectedTimeBelt = "";
    var timeBeltId = stoi(viTimeBeltID);
    for (var index in lstTimeBelts) {
        item = lstTimeBelts[index];
        console.log(item.timeBeltId);
        if (item.timeBeltId == timeBeltId) {
            selectedTimeBelt = item.startTime;
        }
    }

    if(vdtStartDate == today){
        if(currentTime>selectedTimeBelt){
            loadingClose();
            viewMessageToastTyped("You can't chenge current hour spots", "Error");
            return;
        }
    }

    if(viAdvertID=="-1"){
        loadingClose();
        viewMessageToastTyped("Please select Advertisement", "Error");
        return;
    }

    if(viWorkOrder=="-1"){
        loadingClose();
        viewMessageToastTyped("Please select Workorder", "Error");
        return;
    }

    if(viPriority=="-1"){
        loadingClose();
        viewMessageToastTyped("Please select priority", "Error");
        return;
    }

    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.insertPriorityUrl(),
        data: {
            iTimebeltID: viTimeBeltID,
            iCluster: viCluster,
            iWorkOrderID: viWorkOrder,
            iAdvertisementID: viAdvertID,
            iPriority: viPriority,
            dtStratDate: vdtStartDate,
            dtEndDate: vdtEndDate,
            stDays: vstDays,
            overWrite: vbOverWrite,
        },
        success: function (data)
        {
            loadingClose();
            if(data.flag){
                viewMessageToastTyped(data.message, "Success");
                loadPriorityView();
            }else{
                viewMessageToastTyped(data.message, "Error");
            }
            //console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getCheckedDates() {// java calender day of week
    retString = '';
    if ($('#ckbx_Mon').prop('checked'))
        retString += '2';
    if ($('#ckbx_Tue').prop('checked'))
        retString += '3';
    if ($('#ckbx_Wed').prop('checked'))
        retString += '4';
    if ($('#ckbx_Thu').prop('checked'))
        retString += '5';
    if ($('#ckbx_Fri').prop('checked'))
        retString += '6';
    if ($('#ckbx_Sat').prop('checked'))
        retString += '7';
    if ($('#ckbx_Sun').prop('checked'))
        retString += '1';

    return retString;
}


////////////////////////////////////////////////////////////
function loadPriorityView() {

    viChannelId = $('#channelDropdown').val();
    viTimeBeltID = $('#timebeltDropdown').val();
    viCluster = $('#clusterDropdown').val();
    viWorkOrder = $('#workOrderDropdown').val();
    viAdvertID = $('#advertDropdown').val();
    viPriority = $('#priorityDropdown').val();
    vdtStartDate = $('#fromDate').val();
    vdtEndDate = $('#toDate').val();
    vstDays = getCheckedDates();
    if (vdtStartDate == "" || vdtEndDate == "")
    {
        alert("Please select date range.")
        return;
    }
    if (viChannelId == -1) {
        alert("Please select channel")
        return;
    }
    loadingOpen();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.viewPriorityListUrl(),
        data: {
            iTimebeltID: viTimeBeltID,
            iCluster: viCluster,
            iWorkOrderID: viWorkOrder,
            iAdvertisementID: viAdvertID,
            iPriority: viPriority,
            dtStratDate: vdtStartDate,
            dtEndDate: vdtEndDate,
            stDays: vstDays,
            overWrite: false,
            ichannelID: viChannelId,
        },
        success: function (data)
        {
            console.log(JSON.stringify(data));
            tblHead = '';
            tblBody = '';

            var weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            var from = vdtStartDate.split("-");
            var stDate = new Date(from[0], from[1] - 1, from[2]);
            var to = vdtEndDate.split("-");
            var edDate = new Date(to[0], to[1] - 1, to[2]);

            tblHead += '<tr>';
            tblHead += '<th class="time-belt-cell"><div style="width:150px">Time Belt</div></th>';
            for (var date = stDate; date <= edDate; date.setDate(date.getDate() + 1)) {
                tblHead += '<th><div style="width:45px">';
                tblHead += date.getDate() + ' / ' + (date.getMonth() + 1) + '  ' + weekdays[date.getDay()];
                tblHead += '</div></th>';
            }
            tblHead += '</tr>';
            $('#priorityData thead').html(tblHead);

            tableData = data;
            $.each(data, function (i, value_i) {
                timeBelt = value_i.timeBelt;
                tblBody += '<tr>';
                tblBody += '<td class="time-belt-cell">';
                tblBody += timeBelt.startTime + ' - ' + timeBelt.endTime;
                tblBody += '</td>';
                $.each(value_i.priorityList, function (j, value_j) {
                    //date = value_j.
                    tblBody += '<td>';
                    $.each(value_j.priorityItemList, function (k, value_k) {
                        var cellID = i + '_' + j + '_' + k;
                        tblBody += '<div id="' + cellID + '" onclick="viewPriorityData(this.id)" class="" data-toggle="modal" data-target="#priority_view_popup">';
                        tblBody += 'C' + (value_k.cluster + 1) + ' - P' + value_k.priority + '</div>';
                    });
                    //mapPriority[mapID++] = value_j.priorityItemList;
                    tblBody += '</td>';
                });
                tblBody += '<tr>';
            });
            $('#priorityData tbody').html(tblBody);
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

function viewPriorityData(id) {
    var spl = id.split('_');
    dataItem = tableData[stoi(spl[0])].priorityList[stoi(spl[1])].priorityItemList[stoi(spl[2])];
    $('#pupWO').html(dataItem.workOrderId);
    $('#pupAdvert').html(dataItem.advertName);
    $('#pupCluster').html(dataItem.cluster + 1);
    $('#pupPriority').html(dataItem.priority);
    iPopupCellID = id;
}

function removePriorities() {
    loadingOpen();

    var spl = iPopupCellID.split('_');
    var cellItem = tableData[stoi(spl[0])].priorityList[stoi(spl[1])].priorityItemList[stoi(spl[2])];

    viTimeBeltID = tableData[stoi(spl[0])].timeBelt.timeBeltId;
    viCluster = cellItem.cluster;
    viWorkOrder = cellItem.workOrderId;
    viAdvertID = cellItem.advertId;
    viPriority = cellItem.priority;
    vdtStartDate = tableData[stoi(spl[0])].priorityList[stoi(spl[1])].date;
    vdtEndDate = tableData[stoi(spl[0])].priorityList[stoi(spl[1])].date;
    viChannelId = $('#channelDropdown').val();

    if ($('#ckbx_DeleteAll').prop('checked')) {
        vdtStartDate = $('#fromDate').val();
        vdtEndDate = $('#toDate').val();
    }

    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.priority.removePrioritiesUrl(),
        data: {
            iTimebeltID: viTimeBeltID,
            iCluster: viCluster,
            iWorkOrderID: viWorkOrder,
            iAdvertisementID: viAdvertID,
            iPriority: viPriority,
            dtStratDate: vdtStartDate,
            dtEndDate: vdtEndDate,
            stDays: '',
            overWrite: false,
            ichannelID: viChannelId
        },
        success: function (data)
        {
            loadPriorityView();
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getParameterByNamefromUrl(name) {
    url = window.location.href;
    console.log(url);

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function changeChannel(value){
    if(channelSelectionChanged(value)){
        loadPriorityList();
    }
}

function savePriorityAfterValidation(form){
    if(validateInputFields(form)){
        savePriority();
    }
}

function getDate() {
    var yyyy = new Date().getFullYear();
    var mm = new Date().getMonth() + 1;
    var dd = new Date().getDate();
    if (dd < 10)
        dd = '0' + dd;
    if (mm < 10)
        mm = '0' + mm;
    var today = yyyy + '-' + mm + '-' + dd;
    return today;
}
