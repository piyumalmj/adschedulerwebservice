var mapClients = {};
var fetchDone;
$(document).ready(function () {
    fetchDone = _.after(3, function(){
        loadWorkOrderTable();
    });
    showDialog();
    loadClients();
    setUser();
    loadClientsForWoTable();
});

$(function () {
    $('#div-modify-date').datetimepicker({
        pickTime: false
    });
});

$(function () {
    $('#div-end-date').datetimepicker({
        pickTime: false
    });
});
$(function () {
    $('#div-start-date').datetimepicker({
        pickTime: false
    });
});

function loadClientsForWoTable() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url : URI.client.clientForWoTabaleUrl(),
        success: function (data)
        {
            for (var i in data) {
                mapClients[data[i].clientId] = data[i].clientName;
            }
            mapClients[0] = "No agent";
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function loadClients() {
    var agentDrp = '<select  id="txt-agent" name="txt-agent" class="form-control selectpicker" data-live-search="true"><option value="">Select Agent</option>';
    var clientDrp = '<select  id="txt-client" name="txt-client" class="form-control selectpicker" data-live-search="true"><option value="">Select Client</option>';
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url : URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
            for (var i in data) {
                if (data[i].clienttype == "Agency") {
                    agentDrp += '<option data-tokens="' + data[i].clientname + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                } else {
                    clientDrp += '<option data-tokens="' + data[i].clientname + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
            }
            agentDrp += '</select>';
            clientDrp += '</select>';
            $('#drp-agent').html(agentDrp);
            $('#drp-client').html(clientDrp);
            $("#txt-agent").selectpicker('refresh');
            $("#txt-client").selectpicker('refresh');
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function setUser() {

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.userDetails.getMeUserUrl(),
        success: function (data)
        {
            var meDrp = '<select  id="txt-me" name="txt-me" class="form-control selectpicker" data-live-search="true"><option value="">Select ME</option>';
            for (var i in data) {
                meDrp += '<option data-tokens="' + data[i].userName + '" value="' + data[i].userName + '">' + data[i].userName + '</option>';
            }
            $('#drp-me').html(meDrp);
            $("#txt-me").selectpicker('refresh');
            fetchDone();
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function getId(id)
{
    var work_id = document.getElementById(id).innerHTML;

    window.location.href = "./UpdateWorkOrder.html?work_id=" + work_id;

}

function loadWorkOrderTable() {
    var searchResultsTable = $('#tb-all-workOrder').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.workOrder.workOrderListUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#tb-all-workOrder').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.woid = $('#txt-workOrder-id').val();
                d.agent = $('#txt-agent').val();
                d.client = $('#txt-client').val();
                d.product = $('#txt-product').val();
                d.scheduleRef = $('#txt-schedule-ref').val();
                d.me = $('#txt-me').val();
                d.start = $('#txt-start').val();
                d.end = $('#txt-end').val();
                d.modifyDate = $('#txt-modify-date').val();
                d.status = $('#txt-status').val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "workorderid",
                "orderable": false,
                "render": function (data) {
                    return  $('<a/>').attr('href', './workorder/update?work_id=' + data).append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                            .html();
                }
            },
            {"width": "15%",
                "data": "agent",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }
            },
            {"width": "15%",
                "data": "client",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }
            },
            {"width": "10%",
                "data": "ordername",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "scheduleRef",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "seller",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "spotCount",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "startdate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "enddate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "lastModifydate",
                "orderable": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return  date.getFullYear() + "-" + month + "-" + date.getDate();
                }
            },
            {
                "width": "10%",
                "data": "status",
                "orderable": false
            }
        ],
        "createdRow": function (row, data, index) {
            if (data.permissionstatus == 4) {
                //Suspend workOrder
                $('td', row).addClass('suspend-work-order');
            } else if (data.permissionstatus == 3) {
                //Closed workOrder
                $('td', row).addClass('closed-work-order');
            } else if (data.notMatchWithBudget) {
                $('td', row).addClass('budget-missed-match');
            }
        }

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });

}

function clearFilter() {
    document.getElementById("txt-workOrder-id").value = "";
    $("#txt-agent").selectpicker('val', "");
    $("#txt-client").selectpicker('val', "");
    document.getElementById("txt-product").value = "";
    document.getElementById("txt-schedule-ref").value = "";
//    document.getElementById("txt-me").value = "";
    $("#txt-me").selectpicker('val', "");
    document.getElementById("txt-start").value = "";
    document.getElementById("txt-end").value = "";
    document.getElementById("txt-modify-date").value = "";
    document.getElementById("txt-status").value = "select";
}