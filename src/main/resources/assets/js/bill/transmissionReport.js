$(document).ready(function () {
    var fr = document.getElementById("from");
    fr.value = addDays(0);
    $('#from').datepicker({
        format: "yyyy-mm-dd",
        pickTime: false,
    });

    var t = document.getElementById("to");
    t.value = addDays(0);
    $('#to').datepicker({
        format: "yyyy-mm-dd",
        pickTime: false,
    });

//    $('#instFrom').datepicker({
//        format: "yyyy-mm-dd"
//    });
//
//    $('#instTo').datepicker({
//        format: "yyyy-mm-dd"
//    });

    createWorkOrderDropdown();
});

function createChannelsDropdown() {

    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <select id="channelDropdown" name="channelName" class="form-control">';
            trHTML += ' <option VALUE="-111" >All</option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].channelid + '">' + data[i].channelname + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span id="snp_msg" class="text-danger"></span>';
            trHTML += '<span class="help-block"></span>';

            $('#channelList').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
;

function createWorkOrderDropdown() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.allWorkOrderListUrl(),
        success: function (data)
        {
            var trHTML = '<span class="input-group-addon">Work Oder</span>';
            trHTML += ' <select id="workOrderList" name="workOrderId" class="form-control" onchange="workOrderSelectionChange()">';
            trHTML += ' <option value="-111" >All</option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].workorderid + '_' + data[i].startDate + '_' + data[i].endDate + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span id="snp_msg" class="text-danger"></span>';
            trHTML += '<span class="help-block"></span>';

            $('#workOrderList_div').html(trHTML);
            createChannelsDropdown();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
;

function workOrderSelectionChange() {
    var v_workOrderId = document.getElementById("workOrderList");
    var endAndStarDate = v_workOrderId.options[v_workOrderId.selectedIndex].value;
    if (endAndStarDate == -111) {
        return;
    }
    endAndStarDate = endAndStarDate.split('_');
    var fromdate = new Date(parseFloat(endAndStarDate[1]));
    var fromMonth = fromdate.getMonth() + 1;
    var fromDt = fromdate.getDate();
    if (fromMonth < 10) {
        fromMonth = '0' + fromMonth
    }
    if (fromDt < 10) {
        fromDt = '0' + fromDt
    }
    document.getElementById("from").value = fromdate.getFullYear() + "-" + fromMonth + "-" + fromDt;

    var toDate = new Date(parseFloat(endAndStarDate[2]));
    var toMonth = toDate.getMonth() + 1;
    var toDt = toDate.getDate()
     if (toDt < 10) {
        toDt = '0' + toDt
     }
     if (toMonth < 10) {
        toMonth = '0' + toMonth
     }

    var result = new Date();
    // if (date.getTime() > result.getTime()) {
    //    document.getElementById("to").value = addDays(0);
    // } else {
    document.getElementById("to").value = toDate.getFullYear() + "-" + toMonth + "-" +toDt ;
    //}
}
// When the document is ready


function addDays(days) {
    var result = new Date();
    result.setDate(result.getDate() + days);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    return yyyy + '-' + mm + '-' + dd;
}

function filter() {

    showDialog();
    var DateValiFlag = 0;

    var inp = $("#from");
    var fromDate_value = inp.val();
    if (fromDate_value == "")
    {
        //valiFlag = 1;
        DateValiFlag = 1;
        $('#fromDateVali').html('<h5 style="color:red;">Date can not be empty</h5>');
    }
    else
    {
        $('#fromDateVali').html("");
        var arr = fromDate_value.split('-');

        var fromDaysString = arr[0] + arr[1] + arr[2];
        var fromDays = parseInt(fromDaysString);
        //var fromDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
        //var a=parseInt((arr[0]-1)*365);
        //var aa=parseInt((arr[1]-1)*30);
        //var aaa=parseInt(arr[2]);
        //alert (parseInt(arr[2]-1));
        //alert (parseInt(arr[0]));
        //alert(fromDays);

    }

    var inp = $("#to");
    var toDate_value = inp.val();
    if (toDate_value == "")
    {
        //valiFlag = 1;
        DateValiFlag = 1;
        $('#toDateVali').html('<h5 style="color:red;">Date can not be empty</h5>');
    }
    else
    {
        $('#toDateVali').html("");
        var arr = toDate_value.split('-');

        var todayString = arr[0] + arr[1] + arr[2];
        var toDays = parseInt(todayString);
        //var toDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
        //var a=parseInt((arr[0]-1)*365);
        //var aa=parseInt((arr[1]-1)*30);
        //var aaa=parseInt(arr[2]);
        //alert (parseInt(arr[2]-1));
        //alert (parseInt(arr[0]));
        //alert(toDays);

    }
    if (DateValiFlag == 0)
    {
        if (toDays < fromDays)
        {
            //valiFlag = 1;
            DateValiFlag = 1;
            $('#fromDateVali').html('<h5 style="color:red;">invalid Duration</h5>');
            $('#toDateVali').html('<h5 style="color:red;">invalid Duration</h5>');
        }
    }
    if (DateValiFlag == 1)
    {
        return false;
    }
    else
    {
        event.preventDefault();
        var v_channelId = document.getElementById("channelList").value;
        var v_workOrderId = document.getElementById("workOrderList").value;
        v_workOrderId = v_workOrderId.split('_');
        var v_to = document.getElementById("to").value;
        var v_from = document.getElementById("from").value;
        var date = new Date(v_to);

        var result = new Date();
        if (date.getTime() > result.getTime()) {
            if (v_workOrderId == -111) {
                document.getElementById("to").value = addDays(0);
                v_to = document.getElementById("to").value;
            }
        }
        var count = 0;

        $.ajax({
            type: 'GET',
            dataType: "json",
            async: true,
            contentType: 'application/json',
            url: URI.report.transemissionReportUrl(),
            data: {
                channelId: v_channelId,
                workOrderId: v_workOrderId[0],
                to: v_to,
                from: v_from
            },
            success: function (data)
            {
                var tbHTML = '';


                for (var i in data)
                {
                    var statusString = '';
                    var airedTimeString = '';
                    if (data[i].status == 'Played') {
                        statusString = 'Aired';
                        airedTimeString = data[i].actualTime;
                    }
                    tbHTML += '<tr><td class="tb_align_center">' + data[i].client + '</td><td class="tb_align_center">' + data[i].channelName + '</td><td class="tb_align_center">' + data[i].workOrderId + '</td><td class="tb_align_center">' + data[i].advertName + '</td><td class="tb_align_center">' + data[i].date + '</td><td class="tb_align_center">' + RemoveSec(data[i].scheduleStartTime) + '&nbsp;-&nbsp;' + RemoveSec(data[i].scheduleEndTime) + '</td><td class="tb_align_center">' + airedTimeString + '</td><td class="tb_align_center">' + statusString + '</td></tr>';
                    count++
                }
                $('#trancemissionReportTableBody').html(tbHTML);
                $('#downloadPDF_div').html('');
                closeDialog();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
        return true;
    }

}

function printReport() {
    var v_channelId = document.getElementById("channelList").value;
    var v_workOrderId = document.getElementById("workOrderList").value;
    v_workOrderId = v_workOrderId.split('_');
    var v_to = document.getElementById("to").value;
    var v_from = document.getElementById("from").value;

    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.report.transemissionReportPrintUrl(),
                data: {
                    channelId: v_channelId,
                    workOrderId: v_workOrderId[0],
                    to: v_to,
                    from: v_from
                },
                success: function (data)
                {
                    $('#downloadPDF_div').html('<button onclick="downloadPDF()" type="submit" class="btn btn-primary pull-left">&nbsp;&nbsp;&nbsp;&nbsp; downloadPDF &nbsp;&nbsp;&nbsp;&nbsp;</button>');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert("error");
                }
            });
}
function printReportExcell() {
    var v_channelId = document.getElementById("channelList").value;
    var v_workOrderId = document.getElementById("workOrderList").value;
    v_workOrderId = v_workOrderId.split('_');
    var v_to = document.getElementById("to").value;
    var v_from = document.getElementById("from").value;

    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.report.transemissionReportPrintXMSUrl(),
                data: {
                    channelId: v_channelId,
                    workOrderId: v_workOrderId[0],
                    to: v_to,
                    from: v_from
                },
                success: function (data)
                {
                    $('#downloadPDF_div').html('<button onclick="downloadExcell()" type="submit" class="btn btn-primary pull-left">&nbsp;&nbsp;&nbsp;&nbsp; downloadExcell &nbsp;&nbsp;&nbsp;&nbsp;</button>');
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert("error");
                }
            });
}

function downloadPDF() {
    window.location = URI.report.downloadPDF1Url();
}

function downloadExcell() {
    window.location = URI.report.downloadExcelUrl();
}
