var jsonChannelList = [];
var AllChannelJson = '';
var lstAverts = [];
var mapAdverts = {};


var viTotalSecondsNBonus = 0;
var viTotalSecondsBonus = 0;

var usedAmount_pub = 0;
var totalBudget_pub = 0;
var selectedChannelValue_pub = 0;


$(function () {
    $('#form-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(function () {
    $('#to-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});


$(document).ready(function () {
    closeDialog();
    channelDetailCreater();
    setUser();
    spotCountValidation();
    document.getElementById("revenue-month").value = setCurrentMonth();
});

function setUser() {
    var username = localStorage.getItem("current_user");

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    var marketingUser = $('#marketingExUserName').val();

    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.userDetails.getMeUserUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <div class="dropdown">';

            if(marketingUser == ""){
                trHTML += '<select id="user_name" name="wo_type" class="form-control" >';
                trHTML += '<option value="">Select Marketing Ex</option>';
                for (var i in data) {
                    trHTML += '<option value="' + data[i].userName + '">' + data[i].userName + '</option>';
                }
                trHTML += '</select></div><span class="help-block"></span>';
                $('#user_drop').html(trHTML);
            }else{
                trHTML += '<input type="text" id="user_name" name="wo_type" class="form-control"/>';
                trHTML += '</select></div><span class="help-block"></span>';
                $('#user_drop').html(trHTML);
                $('#user_name').val(marketingUser);
                $('#user_name').prop('disabled',true);
            }
            //document.getElementById("user_name").value = username;
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function setBillingClient() {
    document.getElementById("billClientDropdown").value = document.getElementById("clientDropdown").value;
    setAdvertisementDetails();
}

function clientDetailsCreater() {
    agencyClientList = '<div class="dropdown"><select id="agencyDropdown" name="clientName" class="form-control"><option id="-1" value="">Select Agency</option>';
    clientList = '<div class="dropdown"><select id="clientDropdown" onchange="setBillingClient()" name="clientName" class="form-control" onchange="setAdvertisementDetails()"><option id="-1" value="">Select Client</option>';
    allClientList = '<div class="dropdown"><select id="allclientDropdown" name="clientName" class="form-control" onchange="setAdvertisementDetails()"><option id="-1" value="">Select Client</option>';
    billClientList = '<div class="dropdown"><select id="billClientDropdown" name="clientName" class="form-control" onchange="setAdvertisementDetails()" ><option id="-1" value="">Select Client</option>';
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            for (var i in data)
            {
                if (data[i].clienttype == 'Agency')
                {
                    agencyClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
                else if (data[i].clienttype == 'Client')
                {
                    clientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
                allClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                billClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
            }
            agencyClientList += '	</select></div><span class="help-block"></span>';
            clientList += '</select></div><span class="help-block"></span>';
            allClientList += '</select></div><span class="help-block"></span>';
            billClientList += '</select></div><span class="help-block"></span>';
            $('#agency').html(agencyClientList);
            $('#client').html(clientList);
            $('#billing_client').html(billClientList);
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function ClientClick(vale) {
    var clientDro = '';
    if (vale == 2)
    {
        clientDro = '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="client">Client:</label><div class="control-label col-sm-8 col-md-8 col-lg-8 input_width" id ="client_all">' + allClientList + '<span class="help-block"></span></div></div>';
    }
    else if (vale == 1)
    {
        clientDro = '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="agency">Agency:</label><div class="control-label col-sm-8 col-md-8 col-lg-9 input_width" id ="agency">' + agencyClientList + '<span class="help-block"></span></div></div>';
        clientDro += '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="client">End Client:</label><div class="control-label col-sm-8 col-md-8 col-lg-9 input_width" id ="client">' + clientList + '<span class="help-block"></span></div></div>';
    }
    $('#client_div').html(clientDro);

}


function channelDetailCreater() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <select id="channelDropdown" name="channelName" onchange="changeChannels(this.value)"  class="form-control middle_select">';
            trHTML += ' <option VALUE="-111" >Select Channel</option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].channelid + '|' + data[i].channelname + '">' + data[i].channelname + '</option>';
                var channelData = '{"channelId":' + data[i].channelid + ',"channelname":"' + data[i].channelname + '"}';
                if (AllChannelJson == '') {
                    channelData = '[' + channelData + ']';
                    AllChannelJson = JSON.parse(channelData);
                } else {
                    var obj = JSON.parse(channelData);
                    AllChannelJson.push(obj);
                }
            }
            trHTML += '</select>';
            $('#channelList').html(trHTML);
            clientDetailsCreater();
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function addChannel() {

    var selectedChannel = $('#channelDropdown').val().split('|');
    var channelName = selectedChannel[1];
    var channelId = selectedChannel[0];

    hasError($('#channelDropdown'), false, 'error');
    if (channelId != -111)
    {
        var tvc_spots = stoi($('#txt_tvc_spots').val());
        var tvc_spots_b = stoi($('#txt_tvc_spots_b').val());
        var logo_spots = stoi($('#txt_logo_spots').val());
        var logo_spots_b = stoi($('#txt_logo_spots_b').val());
        var crawler_spots = stoi($('#txt_Crawler_spots').val());
        var crawler_spots_b = stoi($('#txt_Crawler_spots_b').val());
        var v_squeeze_spots = stoi($('#txt_v_squeeze_spots').val());
        var v_squeeze_spots_b = stoi($('#txt_v_squeeze_spots_b').val());
        var l_squeeze_spots = stoi($('#txt_l_squeeze_spots').val());
        var l_squeeze_spots_b = stoi($('#txt_l_squeeze_spots_b').val());

        var tvc_package_val = parseFloat($('#txt_tvc_spots_packege').val());
        if (isNaN(tvc_package_val))
        {
            tvc_package_val = 0;
        }
        var logo_package_val = parseFloat($('#txt_logos_spots_packege').val());
        if (isNaN(logo_package_val))
        {
            logo_package_val = 0;
        }
        var crawler_package_val = parseFloat($('#txt_crawler_spots_packege').val());
        if (isNaN(crawler_package_val))
        {
            crawler_package_val = 0;
        }
        var l_squeeze_package_val = parseFloat($('#txt_l_squeeze_spots_packege').val());
        if (isNaN(l_squeeze_package_val))
        {
            l_squeeze_package_val = 0;
        }
        var v_squeeze_package_val = parseFloat($('#txt_v_squeeze_spots_packege').val());
        if (isNaN(v_squeeze_package_val))
        {
            v_squeeze_package_val = 0;
        }
        var package_value = parseFloat($('#txt_package').val());
        if (isNaN(package_value))
        {
            package_value = 0;
        }
        var average_package_value = parseFloat($('#txt_average_package').val());
        if (isNaN(average_package_value))
        {
            average_package_value = 0;
        }

        var workOrderId_p = 70;
        var channelArray = '{"channelId":' + channelId + ',"workOrderId":' + workOrderId_p + ',"tvc_numOfSpots":'
                + tvc_spots + ',"tvc_bonusSpots":' + tvc_spots_b + ',"tvc_package":' + tvc_package_val + ',"logo_numOfSpots":'
                + logo_spots + ',"logo_bonusSpots":' + logo_spots_b + ',"logo_package":' + logo_package_val
                + ',"crowler_numOfSpots":' + crawler_spots + ',"crowler_bonusSpots":' + crawler_spots_b + ',"crowler_package":'
                + crawler_package_val + ',"v_sqeeze_numOfSpots":' + v_squeeze_spots + ',"v_sqeeze_bonusSpots":'
                + v_squeeze_spots_b + ',"v_sqeeze_package":' + v_squeeze_package_val + ',"l_sqeeze_numOfSpots":'
                + l_squeeze_spots + ',"l_sqeeze_bonusSpots":' + l_squeeze_spots_b + ',"l_sqeeze_package":'
                + l_squeeze_package_val + ',"package":' + package_value + ',"ave_package":' + average_package_value
                + ',"tvc_brkdown":{}'
                + ',"tvc_brkdown_b":{}'
                + '}';

        var obj = JSON.parse(channelArray);
        if (channelId in map_ChannelSpotCountDuration_NonBonus)
            obj.tvc_brkdown = map_ChannelSpotCountDuration_NonBonus[channelId];
        if (channelId in map_ChannelSpotCountDuration_Bonus)
            obj.tvc_brkdown_b = map_ChannelSpotCountDuration_Bonus[channelId];

        usedAmount_pub = 0;
        for (var i = 0; i < jsonChannelList.length; i++)
        {
            if (jsonChannelList[i].channelId == obj.channelId) {
                jsonChannelList.splice(i, 1);
                i--;
            } else {
                usedAmount_pub = usedAmount_pub + jsonChannelList[i].package;
            }
        }
        usedAmount_pub = usedAmount_pub + obj.package;
        usedAmount_pub = usedAmount_pub.toFixed(2);
        jsonChannelList.push(obj);
        setSelectedChannelDrop();

        calculateTotalBuget();
        setSelectedChannelDrop();

        document.getElementById("txt_tvc_spots").value = '';
        document.getElementById("txt_tvc_spots_b").value = '';
        document.getElementById("txt_logo_spots").value = '';
        document.getElementById("txt_logo_spots_b").value = '';
        document.getElementById("txt_Crawler_spots").value = '';
        document.getElementById("txt_Crawler_spots_b").value = '';
        document.getElementById("txt_v_squeeze_spots").value = '';
        document.getElementById("txt_v_squeeze_spots_b").value = '';
        document.getElementById("txt_l_squeeze_spots").value = '';
        document.getElementById("txt_l_squeeze_spots_b").value = '';
        document.getElementById("txt_package").value = '';
        document.getElementById("txt_average_package").value = '';
        document.getElementById("txt_tvc_spots_packege").value = '';
        document.getElementById("txt_logos_spots_packege").value = '';
        document.getElementById("txt_crawler_spots_packege").value = '';
        document.getElementById("txt_v_squeeze_spots_packege").value = '';
        document.getElementById("txt_l_squeeze_spots_packege").value = '';

        document.getElementById("channelDropdown").value = '-111';

        $('#txt_TotalSeconds').val('');
    } else {
        hasError($('#channelDropdown'), true, 'error');
    }
    getTotalDurationfromTVCBreckdown(channelId);
}

function changeChannels(channelvalue) {
    var channelId = channelvalue.split("|");

    document.getElementById("txt_tvc_spots").value = '';
    document.getElementById("txt_tvc_spots_b").value = '';
    document.getElementById("txt_logo_spots").value = '';
    document.getElementById("txt_logo_spots_b").value = '';
    document.getElementById("txt_Crawler_spots").value = '';
    document.getElementById("txt_Crawler_spots_b").value = '';
    document.getElementById("txt_v_squeeze_spots").value = '';
    document.getElementById("txt_v_squeeze_spots_b").value = '';
    document.getElementById("txt_l_squeeze_spots").value = '';
    document.getElementById("txt_l_squeeze_spots_b").value = '';
    document.getElementById("txt_package").value = '';
    document.getElementById("txt_average_package").value = '';
    document.getElementById("txt_tvc_spots_packege").value = '';
    document.getElementById("txt_logos_spots_packege").value = '';
    document.getElementById("txt_crawler_spots_packege").value = '';
    document.getElementById("txt_v_squeeze_spots_packege").value = '';
    document.getElementById("txt_l_squeeze_spots_packege").value = '';

    b_IsFound = false;
    selectedChannelValue_pub = 0;
    if (jsonChannelList != '') {
        for (var i in jsonChannelList) {
            if (jsonChannelList[i].channelId == channelId[0]) {
                document.getElementById("txt_tvc_spots").value = jsonChannelList[i].tvc_numOfSpots;
                document.getElementById("txt_tvc_spots_b").value = jsonChannelList[i].tvc_bonusSpots;
                document.getElementById("txt_logo_spots").value = jsonChannelList[i].logo_numOfSpots;
                document.getElementById("txt_logo_spots_b").value = jsonChannelList[i].logo_bonusSpots;
                document.getElementById("txt_Crawler_spots").value = jsonChannelList[i].crowler_numOfSpots;
                document.getElementById("txt_Crawler_spots_b").value = jsonChannelList[i].crowler_bonusSpots;
                document.getElementById("txt_v_squeeze_spots").value = jsonChannelList[i].v_sqeeze_numOfSpots;
                document.getElementById("txt_v_squeeze_spots_b").value = jsonChannelList[i].v_sqeeze_bonusSpots;
                document.getElementById("txt_l_squeeze_spots").value = jsonChannelList[i].l_sqeeze_numOfSpots;
                document.getElementById("txt_l_squeeze_spots_b").value = jsonChannelList[i].l_sqeeze_bonusSpots;
                document.getElementById("txt_package").value = jsonChannelList[i].package;
                document.getElementById("txt_average_package").value = jsonChannelList[i].ave_package;
                document.getElementById("txt_tvc_spots_packege").value = jsonChannelList[i].tvc_package;
                document.getElementById("txt_logos_spots_packege").value = jsonChannelList[i].logo_package;
                document.getElementById("txt_crawler_spots_packege").value = jsonChannelList[i].crowler_package;
                document.getElementById("txt_v_squeeze_spots_packege").value = jsonChannelList[i].v_sqeeze_package;
                document.getElementById("txt_l_squeeze_spots_packege").value = jsonChannelList[i].l_sqeeze_package;

                b_IsFound = true;
                selectedChannelValue_pub = jsonChannelList[i].package;
                break;
            }
        }
    }

    if (!b_IsFound) {
        ///tvc spots breakdown => Not Saved channels /////////////////////////
        map_ChannelSpotCountDuration_Bonus[stoi(channelId[0])] = {};
        map_ChannelSpotCountDuration_NonBonus[stoi(channelId[0])] = {};
    }

    ///
    if (!(channelId[0] in map_ChannelSpotCountDuration_Bonus)) {
        map_ChannelSpotCountDuration_Bonus[stoi(channelId[0])] = {};
    }
    if (!(channelId[0] in map_ChannelSpotCountDuration_NonBonus)) {
        map_ChannelSpotCountDuration_NonBonus[stoi(channelId[0])] = {};
    }

    /// set total seconds //
    getTotalDurationfromTVCBreckdown(channelId[0]);
    $('#txt_TotalSeconds').val(viTotalSecondsNBonus + viTotalSecondsBonus);
}

function setSelectedChannelDrop() {
    var drop_channelList = '<select id="selected_ChannelDropdown" name="channelName" onchange="selectedChannelsChange(this.value)" class="form-control middle_select"><option value="-1">Select Channel</option>';
    for (var i in jsonChannelList)
    {
        for (var j in AllChannelJson)
        {
            if (jsonChannelList[i].channelId == AllChannelJson[j].channelId) {
                drop_channelList += '<option value="' + AllChannelJson[j].channelId + '">' + AllChannelJson[j].channelname + '</option>';
            }
        }
    }
    drop_channelList += '</select>';
    $('#selectedChannelList').html(drop_channelList);
    selectedChannelsChange(-1);
}

function selectedChannelsChange(channelid) {
    if (channelid == -1) {
        table = '<table class="table table-bordered"><thead><tr><th></th><th>Spots</th><th>Bonus</th><th>Value(Rs)</th></tr></thead><tbody>';
        table += '<tr><td>TVC</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>Logo</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>Crawler</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>V Squeeze</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>L Squeeze</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td class="cspan" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Total value</label></td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td class="cspan2" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Average value</label></td><td align="right" class="currency-input">000.00</td></tr>';
        table += '</tbody></table>';
        $('#selectedChannelDetails').html(table);

        return;
    }
    for (var i in jsonChannelList)
    {
        if (jsonChannelList[i].channelId == channelid) {
            var table = '<table class="table table-bordered"><thead><tr><th></th><th>Spots</th><th>Bonus</th><th>Value(Rs)</th></tr></thead><tbody>';
            table += '<tr><td>TVC</td><td>' + jsonChannelList[i].tvc_numOfSpots + '</td><td>' + jsonChannelList[i].tvc_bonusSpots + '</td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].tvc_package).toFixed(2) + '</td></tr>';
            table += '<tr><td>Logo</td><td>' + jsonChannelList[i].logo_numOfSpots + '</td><td>' + jsonChannelList[i].logo_bonusSpots + '</td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].logo_package).toFixed(2) + '</td></tr>';
            table += '<tr><td>Crawler</td><td>' + jsonChannelList[i].crowler_numOfSpots + '</td><td>' + jsonChannelList[i].crowler_bonusSpots + '</td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].crowler_package).toFixed(2) + '</td></tr>';
            table += '<tr><td>V Squeeze</td><td>' + jsonChannelList[i].v_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].v_sqeeze_bonusSpots + '</td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].v_sqeeze_package).toFixed(2) + '</td></tr>';
            table += '<tr><td>L Squeeze</td><td>' + jsonChannelList[i].l_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].l_sqeeze_bonusSpots + '</td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].l_sqeeze_package).toFixed(2) + '</td></tr>';
            table += '<tr><td class="cspan" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Total value</label></td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].package).toFixed(2) + '</td></tr>';
            table += '<tr><td class="cspan2" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Average value</label></td><td align="right" class="currency-input">' + parseFloat(jsonChannelList[i].ave_package).toFixed(2) + '</td></tr>';
            table += '</tbody></table>';
            $('#selectedChannelDetails').html(table);
            break;
        }
    }
}

function deleteChannel() {
    var ret = confirm("Are you sure you want to remove this Channel?");
    if (!ret)
        return;

    var channelId = document.getElementById("selected_ChannelDropdown").value;
    if (channelId != -1) {
        for (var i in jsonChannelList)
        {
            if (jsonChannelList[i].channelId == channelId) {
                usedAmount_pub = usedAmount_pub - jsonChannelList[i].package;
                jsonChannelList.splice(i, 1);
            }
        }
    }
    usedAmount_pub = usedAmount_pub.toFixed(2);
    setSelectedChannelDrop();
    calculateTotalBuget();
}

function setCurrentMonth() {
    var d = new Date();
    var n = d.getMonth();
    var month = '';
    switch (n) {
        case 0:
            month = "January";
            break;
        case 1:
            month = "February";
            break;
        case 2:
            month = "March";
            break;
        case 3:
            month = "April";
            break;
        case 4:
            month = "May";
            break;
        case 5:
            month = "June";
            break;
        case 6:
            month = "July";
            break;
        case 7:
            month = "August";
            break;
        case 8:
            month = "September";
            break;
        case 9:
            month = "October";
            break;
        case 10:
            month = "November";
            break;
        case 11:
            month = "December";
            break;
    }

    return month;
}


//////////////////////////////////////////////////////////
function woSave() {
    showDialog();
    if (!validateForm()){
        closeDialog();
        return;
    }

    if (jsonChannelList == "") {
        closeDialog();
        alert("Please add a Channel.");
        return;
    }

    var v_clienName = '';
    var v_agencyClien = '';
    if (document.getElementById('client_radio').checked) {
        v_agencyClien = $('#agencyDropdown').val();
        v_clienName = $('#clientDropdown').val();
    } else {
        v_clienName = $('#allclientDropdown').val();
        v_agencyClien = 0;
    }

    var v_seller = $('#user_name').val();
    var V_billClient = $('#billClientDropdown').val();
    var v_productName = $('#product-name').val();
    var v_startDate = $('#fromDate').val();
    var v_endDate = $('#toDate').val();
    var v_comment = $('#comment').val();
    v_comment = v_comment.split("\n").join("\\n");

    var v_wo_type = $('#wo_type_Dropdown').val();
    var v_total_budget = $('#txt_total_budget').val().replace(/,/g, "");
    var v_schedule_ref = $('#txt_schedule_ref').val();
    var v_user = $('#user_name').val();
    var v_commission = '';
    var v_revenue_month = $('#revenue-month').val();
    if (document.getElementById('commison_15').checked) {
        v_commission = 15;
    } else {
        v_commission = 0;
    }

    var channel_List = JSON.stringify(jsonChannelList);
    var v_visibility = '0';
    if (document.getElementById('visible').checked)
        v_visibility = 1;
    else
        v_visibility = 2;
    var v_advert_List = JSON.stringify(lstAverts);

    var dataArray = '{"userName":"' + v_user + '","workOrderId":0,"productName":"' + v_productName + '","startDate":"' + v_startDate + '","endDate":"' + v_endDate + '","comment":"' + v_comment + '","visibility":' + v_visibility + ',"client":' + v_clienName + ',"agency":' + v_agencyClien + ',"billClient":' + V_billClient + ',"woType":"' + v_wo_type + '","scheduleRef":"' + v_schedule_ref + '","commission":' + v_commission + ',"totalBudget":' + v_total_budget + ',"revenueMonth":"' + v_revenue_month + '"}';

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    var postData = {
                        startDate: v_startDate,
                        endDate: v_endDate,
                        workOrderName: v_productName,
                        clientName: v_clienName + '-' + v_agencyClien + '-' + V_billClient,
                        seller: v_seller,
                        visibility: v_visibility,
                        comment: v_comment,
                        exdata: dataArray,
                        channelList: channel_List,
                        advertList: v_advert_List
                   };

    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        url: URI.workOrder.workOrderSaveUrl(),
        success: function (data)
        {
            closeDialog();
            if (data != -1) {
                viewMessageToastTyped("Successfully save workorder ","Success");
                window.location = URI.workOrder.updateWorkOrderUrl(data);
            } else {
                viewMessageToastTyped("WorkOrder Saving Failed..","Error");
            }
        },
        error: function (jqXHR, textStatus, thrownError) {
            closeDialog();
            if (thrownError === 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function validateForm() {

    var checker = false;
    if ($('#user_name').val() == '') {
        hasError($("#user_name"), true, 'Select Marketing Ex.');
        checker = true;
    }
    if ($('#product-name').val() == '') {
        hasError($("#product-name"), true, 'Enter Product Name.');
        checker = true;
    }
    if ($('#txt_schedule_ref').val() == '') {
        hasError($("#txt_schedule_ref"), true, 'Enter Schedule ref.');
        checker = true;
    }
    if ($('#txt_total_budget').val() == '') {
        hasError($("#txt_total_budget"), true, 'Enter Total budget.');
        checker = true;
    }
    if ($('#wo_type_Dropdown').val() == '') {
        hasError($("#wo_type_Dropdown"), true, 'Select WO type.');
        checker = true;
    }
    if ($('#billClientDropdown').val() == '') {
        hasError($("#billClientDropdown"), true, 'Please Select Client.');
        checker = true;
    }
    if ($('#fromDate').val() == '') {
        hasError($("#form-date"), true, 'Select Start Date.');
        checker = true;
    }
    if ($('#toDate').val() == '') {
        hasError($("#to-date"), true, 'Select End Date.');
        checker = true;
    }
    if (checkExistScheduleRef()) {
        hasError($("#txt_schedule_ref"), true, 'There is exist schedule ref.');
        viewMessageToastTyped("There is exist schedule ref.", "error");
        checker = true;
    }
    if (checker) {
        return false;
    }
    if ($('#client_radio').is(':checked')) {
        hasError($("#agencyDropdown"), ($('#agencyDropdown').val() == ''), 'Please Select Agency.');
        hasError($("#clientDropdown"), ($('#clientDropdown').val() == ''), 'Please Select Client.');
        if ($('#clientDropdown').val() == '' || $('#agencyDropdown').val() == '')
            return false;
    }
    else {
        hasError($("#allclientDropdown"), ($('#allclientDropdown').val() == ''), 'Please Select Client.');
        if ($('#allclientDropdown').val() == '')
            return false;
    }

    if (new Date($('#fromDate').val()).getTime() > new Date($('#toDate').val()).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        return false;
    }

    if ($('#billClientDropdown').val() == '' || $('#fromDate').val() == '' || $('#toDate').val() == '' ||
            $('#user_name').val() == '' || $('#product-name').val() == '')
        return false;

    return true;
//
}


var agencyClientList = '';
var clientList = '';
var allClientList = '';
var billClientList = '';

function channelDetailsChanged(id) {
    if (!spotCountValidation()) {
        return;
    }

    var total_package = calculatePackageTotal();

    if (!checkBuggetLimit(total_package, id)) {
        total_package = calculatePackageTotal();
        var tvc_package = parseFloat($('#txt_tvc_spots_packege').val());
        var average_package = calculateAverageValue(tvc_package);

        $('#txt_average_package').val(parseFloat(average_package).toFixed(2));
        $('#txt_package').val(parseFloat(total_package).toFixed(2));
    } else {
        var tvc_package = stoi($('#txt_tvc_spots_packege').val());
        var average_package = calculateAverageValue(tvc_package);

        $('#txt_average_package').val(parseFloat(average_package).toFixed(2));
        $('#txt_package').val(parseFloat(total_package).toFixed(2));
    }
}

function calculateAverageValue(total_value) {
    var tvc_spots = parseFloat($('#txt_tvc_spots').val());
    if (isNaN(tvc_spots)) {
        tvc_spots = 0;
    }
//    var tvc_spots_b = stoi($('#txt_tvc_spots_b').val());
//    var logo_spots = stoi($('#txt_logo_spots').val());
//    var logo_spots_b = stoi($('#txt_logo_spots_b').val());
//    var crawler_spots = stoi($('#txt_Crawler_spots').val());
//    var crawler_spots_b = stoi($('#txt_Crawler_spots_b').val());
//    var v_squeeze_spots = stoi($('#txt_v_squeeze_spots').val());
//    var v_squeeze_spots_b = stoi($('#txt_v_squeeze_spots_b').val());
//    var l_squeeze_spots = stoi($('#txt_l_squeeze_spots').val());
//    var l_squeeze_spots_b = stoi($('#txt_l_squeeze_spots_b').val());
//    stoi($('#txt_TotalSeconds').val());

    var total_spots = tvc_spots;// + tvc_spots_b + logo_spots + logo_spots_b + crawler_spots + crawler_spots_b + v_squeeze_spots + v_squeeze_spots_b + l_squeeze_spots + l_squeeze_spots_b;


    if (total_spots > 0) {
        return (total_value / total_spots);
    } else {
        return 0;
    }
}

function checkBuggetLimit(total_value, id) {
    if (totalBudget_pub < (usedAmount_pub - selectedChannelValue_pub + total_value)) {
        alert("Budget over limit");
        document.getElementById(id).value = "";
        return false;
    } else {
        return true;
    }
}

function calculatePackageTotal() {
    var tvc_p_va = $('#txt_tvc_spots_packege').val();
    var tvc_p = 0;
    if (tvc_p_va != "") {
        tvc_p = parseFloat(tvc_p_va);
    }
    var logo_p_va = $('#txt_logos_spots_packege').val()
    var logo_p = 0;
    if (logo_p_va != "") {
        logo_p = parseFloat(logo_p_va);
    }
    var crow_p_va = $('#txt_crawler_spots_packege').val();
    var crow_p = 0;
    if (crow_p_va != "") {
        crow_p = parseFloat(crow_p_va);
    }
    var l_sqeeze_p_va = $('#txt_v_squeeze_spots_packege').val();
    var l_sqeeze_p = 0;
    if (l_sqeeze_p_va != "") {
        l_sqeeze_p = parseFloat(l_sqeeze_p_va);
    }
    var v_sqees_p_va = $('#txt_l_squeeze_spots_packege').val();
    var v_sqees_p = 0;
    if (v_sqees_p_va != "") {
        v_sqees_p = parseFloat(v_sqees_p_va);
    }
    var newoTalValue = tvc_p + logo_p + crow_p + l_sqeeze_p + v_sqees_p;
    return newoTalValue;
}

function spotCountValidation() {
    $('input', '#divChannelDetails').each(function () {
        hasError($(this), false, '')

        var vlaue = $(this).val();
        if (vlaue == null || vlaue == "")
            $(this).val('');
        else if (isNaN(vlaue)) {
            hasError($(this), true, 'error')
            $(this).val('');
        }
    });
    return true;
}

function stoi(value) {
    if (value == null || value == "" || isNaN(value))
        return 0;
    else
        return parseInt(value);
}


//task_139
$(document).ready(function () {
});

function advetListCollapse(element) {
    if ($(element).text() == String.fromCharCode(9650)) {
        $('#advert_list_table').hide();
        $(element).text(String.fromCharCode(9660));
    }
    else {
        $('#advert_list_table').show();
        $(element).text(String.fromCharCode(9650));
    }
}

function setAdvertisementDetails()//
{
    var v_clienName = '';
    if (document.getElementById('client_radio').checked) {
        v_clienName = document.getElementById("clientDropdown");
    } else {
        v_clienName = document.getElementById("allclientDropdown");

    }
    var clientID = v_clienName.options[v_clienName.selectedIndex].id;

    var v_billClient = document.getElementById("billClientDropdown");
    var billClientID = v_billClient.options[v_billClient.selectedIndex].id;

    if (clientID == -1 && billClientID == -1) {
        return;
    }

    showDialog();
    var advertDrop = '<select id="advertDropdown_pop" name="channelName" class="form-control selectpicker" data-live-search="true"><option id="-1" value="-1">Select Advertsement</option>';
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSelectedClientListUrl(),
        data: {
            clieanOne: clientID,
            clieanTwo: billClientID,
        },
        success: function (data)
        {
            closeDialog();
            for (var i in data) {
                advertDrop += '<option id=' + data[i].advertid + ' data-tokens="' + data[i].advertname + '"  value="' + data[i].advertid + '">' + data[i].advertid + '-' + data[i].advertname + '</option>';
                mapAdverts[data[i].advertid] = data[i];
            }

            advertDrop += '</select></div>';
            $('#advert_list_pop_div').html(advertDrop);
            $('#advert_list_table').hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    });
    $('.selectpicker').selectpicker({
        size: 10
    });

}

function setAdvertTable() {
    advertTableMain = '';
    advert_count = 0;
    lstAverts.forEach(function (id) {
        advert = mapAdverts[id];
        advertTableMain += '<tr><td id="' + advert.advertid + '"><a href="#" id="' + advert.advertid + '" onclick="getId(this.id)">' + advert.advertid + '</a></td><td>' + advert.advertname + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.adverttype) + '</td><td>' + advert.lastModifydate + '</td> </tr>';
        console.log(id);
        advert_count++;
    });
    $('#advert_main_table tbody').html(advertTableMain);
    $('#lbl_advert_count').html(advert_count + ' Advertisements');
}

function addAdvertisement() {
    advert_id = $("#advertDropdown_pop").val();
    if (advert_id == -1)
        return;

    advert = mapAdverts[advert_id];
    row_html = '<tr id=tr_' + advert.advertid + '><td id="' + advert.advertid + '"><a href="#" id="' + advert.advertid + '" onclick="getId(this.id)">' + advert.advertid + '</a></td><td>' + advert.advertname + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.adverttype) + '</td><td>' + advert.lastModifydate + '</td><td style="width:20px;"><button type="button" class="close close-icon" aria-label="Close" style="font-size:16px;color:red;" onclick="removeAdvert(' + advert.advertid + ')"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
    $('#advertisement_table_pop tbody').append(row_html);

    $("#advertDropdown_pop option[value='" + advert_id + "']").remove();
    $('#advertDropdown_pop').selectpicker('refresh');
    //$("#advertDropdown_pop").val(-1);
}

function getId(id) {
    window.open(URI.advertisement.updateAdvertisementUrl(id));
}

function removeAdvert(id) {
    $('#tr_' + id).remove();
    advert = mapAdverts[id];
    $('#advertDropdown_pop').append($('<option>', {value: advert.advertid, text: advert.advertname}));
    $('#advertDropdown_pop').selectpicker('refresh');
}

function saveAdvertisements() {
    lstAverts = [];

    $('#advertisement_table_pop tr').each(function (i, row) {
        if (i > 0) {
            cell = row.cells[0];
            advert_id = stoi(cell.id);
            console.log(advert_id);
            lstAverts.push(advert_id);
        }
    });
    setAdvertTable();
    viewMessageToastTyped("Advertisement saved successfully", "Success");
    $('#addAdvertisementModal').modal('toggle');
}


///Task_141 //////////////////////////////////////////
var b_IsBonusTVCSpotPopup = false;
var map_ChannelSpotCountDuration_NonBonus = {};
var map_ChannelSpotCountDuration_Bonus = {};
var map_SpotCountDuration = {};

function onclickTVCSpotCount() {
    iChannelId = stoi($('#channelDropdown').val().split("|")[0]);
    if (iChannelId == -111)
        return;

    b_IsBonusTVCSpotPopup = false;
    map_SpotCountDuration = map_ChannelSpotCountDuration_NonBonus[iChannelId];
    $('#spotcount_view_popup').modal('toggle');
    createSpotCountTable();
}

function onclickTVCSpotCountBonus() {
    iChannelId = $('#channelDropdown').val().split("|")[0];
    if (iChannelId == -111)
        return;

    b_IsBonusTVCSpotPopup = true;
    map_SpotCountDuration = map_ChannelSpotCountDuration_Bonus[iChannelId];
    $('#spotcount_view_popup').modal('toggle');
    createSpotCountTable();
}

function addSpotCount() {
    iDuration = stoi($('#txt_duration').val());
    iSpotCount = stoi($('#txt_spots').val());

    if (iDuration in map_SpotCountDuration)
        map_SpotCountDuration[iDuration] = map_SpotCountDuration[iDuration] + iSpotCount;
    else
        map_SpotCountDuration[iDuration] = iSpotCount;
    createSpotCountTable();
}

function removeRow(id) {
    delete map_SpotCountDuration[id];
    createSpotCountTable();
}

function createSpotCountTable() {
    tblData = '';
    i_TotalSeconds = 0;
    for (var key in map_SpotCountDuration) {
        tblData += '<tr id="row_spot_' + key + '"><td>' + key + '</td><td>' + map_SpotCountDuration[key] + '</td><td style="width:20px;"><button type="button" class="close close-icon" aria-label="Close" style="font-size:16px;color:red;" onclick="removeRow(' + key + ')"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';

        iDuration = key;
        iSpotCount = map_SpotCountDuration[key];

        iSeconds = iDuration * iSpotCount;
        i_TotalSeconds += iSeconds;
    }
    $('#tblSpotCount tbody').html(tblData);
    $('#lbl_TotalCount').html(Math.ceil(i_TotalSeconds / 30));
}

function saveSpotCount() {
    i_TotalSeconds = 0;
    for (var key in map_SpotCountDuration) {
        iDuration = key;
        iSpotCount = map_SpotCountDuration[key];

        iSeconds = iDuration * iSpotCount;
        i_TotalSeconds += iSeconds;
    }
    i_SpotCount = Math.ceil(i_TotalSeconds / 30);

    if (b_IsBonusTVCSpotPopup) {
        $('#txt_tvc_spots_b').val(i_SpotCount);
        viTotalSecondsBonus = i_TotalSeconds;
    }
    else {
        $('#txt_tvc_spots').val(i_SpotCount);
        viTotalSecondsNBonus = i_TotalSeconds;
    }
    $('#spotcount_view_popup').modal('toggle');
    $('#txt_TotalSeconds').val(viTotalSecondsNBonus + viTotalSecondsBonus);

    tvc_package = stoi($('#txt_tvc_spots_packege').val());
    $('#txt_average_package').val(parseFloat(calculateAverageValue(tvc_package)).toFixed(2));

}

function numberValidation_el(element) {
    hasError($(element), false, '')

    var vlaue = $(element).val();
    if (vlaue == null || vlaue == "") {
        $(element).val('');
        return false;
    }
    else if (isNaN(vlaue)) {
        hasError($(element), true, 'error')
        $(element).val('');
        return false;
    }
    return true;
}

function getTotalDurationfromTVCBreckdown(iChannelId) {
    viTotalSecondsNBonus = 0;
    viTotalSecondsBonus = 0;

    mapSpotCount = map_ChannelSpotCountDuration_NonBonus[iChannelId];
    for (var key in mapSpotCount) {
        iDuration = stoi(key) * mapSpotCount[key];
        viTotalSecondsNBonus += iDuration;
    }

    mapSpotCount = map_ChannelSpotCountDuration_Bonus[iChannelId];
    for (var key in mapSpotCount) {
        iDuration = stoi(key) * mapSpotCount[key];
        viTotalSecondsBonus += iDuration;
    }
}

/*--------Task 137-------*/
function setBudget(value) {
    totalBudget_pub = parseFloat(value).toFixed(2);
    calculateTotalBuget();
}

function calculateTotalBuget() {
    var budgetLbl = '<label class="col-md-9 lbl_total_budget">Total budget : ' + getDecimal(parseFloat(usedAmount_pub)) + '/' + getDecimal(parseFloat(totalBudget_pub)) + '</label>';
    $('#lbl_total_budget').html(budgetLbl);
}
/*------Task 137 end-----*/

/*--------Task 138-------*/
/*function channelSummery() {
 
 var summeryTable = '<table id="channel_summery_table_pop" class="table table-bordered table-workorder">';
 summeryTable += '<thead><tr><th>Channel</th><th>Type</th><th>Spot</th><th>Bonus</th><th>Value</th></tr></thead><tbody>';
 
 
 for (var i in jsonChannelList)
 {
 if (jsonChannelList[i].tvc_numOfSpots != 0 || jsonChannelList[i].tvc_bonusSpots != 0) {
 summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>TVC</td><td>' + jsonChannelList[i].tvc_numOfSpots + '</td><td>' + jsonChannelList[i].tvc_bonusSpots + '</td><td class="currency-input">' + jsonChannelList[i].tvc_package + '</td></tr>';
 }
 if (jsonChannelList[i].logo_numOfSpots != 0 || jsonChannelList[i].logo_bonusSpots != 0) {
 summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Logo</td><td>' + jsonChannelList[i].logo_numOfSpots + '</td><td>' + jsonChannelList[i].logo_bonusSpots + '</td><td class="currency-input">' + jsonChannelList[i].logo_package + '</td></tr>';
 }
 if (jsonChannelList[i].crowler_numOfSpots != 0 || jsonChannelList[i].crowler_bonusSpots != 0) {
 summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Crowler</td><td>' + jsonChannelList[i].crowler_numOfSpots + '</td><td>' + jsonChannelList[i].crowler_bonusSpots + '</td><td class="currency-input">' + jsonChannelList[i].crowler_package + '</td></tr>';
 }
 if (jsonChannelList[i].v_sqeeze_numOfSpots != 0 || jsonChannelList[i].v_sqeeze_bonusSpots != 0) {
 summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Vsqueeze</td><td>' + jsonChannelList[i].v_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].v_sqeeze_bonusSpots + '</td><td class="currency-input">' + jsonChannelList[i].v_sqeeze_package + '</td></tr>';
 }
 if (jsonChannelList[i].l_sqeeze_numOfSpots != 0 || jsonChannelList[i].l_sqeeze_bonusSpots != 0) {
 summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Lsqueeze</td><td>' + jsonChannelList[i].l_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].l_sqeeze_bonusSpots + '</td><td class="currency-input">' + jsonChannelList[i].l_sqeeze_package + '</td></tr>';
 }
 
 }
 summeryTable += '</tbody></table>';
 $('#channel_summery_table_pop_div').html(summeryTable);
 }*/
function channelSummery() {

    var summeryTable = '<table id="channel_summery_table_pop" class="table table-bordered table-workorder">';
    summeryTable += '<thead><tr><th>Channel</th><th>Type</th><th>Duration</th><th>Spot</th><th>Bonus</th><th>Value</th></tr></thead><tbody>';

    var totalbuget = 0.0;
    for (var i in jsonChannelList)
    {
        if (jsonChannelList[i].tvc_numOfSpots !== 0 || jsonChannelList[i].tvc_bonusSpots !== 0) {
            var tvcBreakDown = jsonChannelList[i].tvc_brkdown;
            var tvcBreakDown_bons = jsonChannelList[i].tvc_brkdown_b;
            var rowCount = 0;
            var firstRow = '';
            var otherRow = '';
            for (var key in tvcBreakDown) {
                var bonusSpot = tvcBreakDown_bons[key];
                if (bonusSpot === undefined) {
                    bonusSpot = 0;
                }
                if (rowCount === 0) {
                    summeryTable += '<tr><td rowspan="';
                    firstRow += '">' + getChannelName(jsonChannelList[i].channelId) + '</td><td>TVC</td><td>' + key + '</td><td>' + tvcBreakDown[key] + '</td><td>' + bonusSpot + '</td><td rowspan="';
                    otherRow += ' class="currency-input">' + getDecimal(jsonChannelList[i].tvc_package) + '</td></tr>';
                } else {
                    otherRow += '<tr><td>TVC</td><td>' + key + '</td><td>' + tvcBreakDown[key] + '</td><td>' + bonusSpot + '</td></tr>';
                }
                rowCount++;
            }
            for (var key_b in tvcBreakDown_bons) {
                var spot = tvcBreakDown[key_b];
                if (spot === undefined) {
                    if (rowCount === 0) {
                        summeryTable += '<tr><td rowspan="';
                        firstRow += '">' + getChannelName(jsonChannelList[i].channelId) + '</td><td>TVC</td><td>' + key_b + '</td><td>0</td><td>' + tvcBreakDown_bons[key_b] + '</td><td rowspan="';
                        otherRow += ' class="currency-input">' + getDecimal(jsonChannelList[i].tvc_package) + '</td></tr>';
                    } else {
                        otherRow += '<tr><td>TVC</td><td>' + key_b + '</td><td>0</td><td>' + tvcBreakDown_bons[key_b] + '</td></tr>';
                    }
                    rowCount++;
                }
            }
            totalbuget += jsonChannelList[i].tvc_package;
            summeryTable += rowCount + firstRow + rowCount + otherRow;
        }
        if (jsonChannelList[i].logo_numOfSpots !== 0 || jsonChannelList[i].logo_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Logo</td><td>0</td><td>' + jsonChannelList[i].logo_numOfSpots + '</td><td>' + jsonChannelList[i].logo_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonChannelList[i].logo_package) + '</td></tr>';
            totalbuget += jsonChannelList[i].logo_package;
        }
        if (jsonChannelList[i].crowler_numOfSpots !== 0 || jsonChannelList[i].crowler_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Crowler</td><td>0</td><td>' + jsonChannelList[i].crowler_numOfSpots + '</td><td>' + jsonChannelList[i].crowler_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonChannelList[i].crowler_package) + '</td></tr>';
            totalbuget += jsonChannelList[i].crowler_package;
        }
        if (jsonChannelList[i].v_sqeeze_numOfSpots !== 0 || jsonChannelList[i].v_sqeeze_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Vsqueeze</td><td>0</td><td>' + jsonChannelList[i].v_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].v_sqeeze_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonChannelList[i].v_sqeeze_package) + '</td></tr>';
            totalbuget += jsonChannelList[i].v_sqeeze_package;
        }
        if (jsonChannelList[i].l_sqeeze_numOfSpots !== 0 || jsonChannelList[i].l_sqeeze_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonChannelList[i].channelId) + '</td><td>Lsqueeze</td><td>0</td><td>' + jsonChannelList[i].l_sqeeze_numOfSpots + '</td><td>' + jsonChannelList[i].l_sqeeze_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonChannelList[i].l_sqeeze_package) + '</td></tr>';
            totalbuget += jsonChannelList[i].l_sqeeze_package;
        }
    }
    summeryTable += '<tr><td colspan="5">Total value</td><td>' + getDecimal(totalbuget) + '</td></tr>';
    summeryTable += '</tbody></table>';
    $('#channel_summery_table_pop_div').html(summeryTable);
}

function getChannelName(channelId) {

    for (var i in AllChannelJson)
    {
        if (AllChannelJson[i].channelId == channelId) {
            return AllChannelJson[i].channelname;
        }
    }
}
/*------Task 138 end-----
 * <td class="cspan2" colspan="3">
 * */

///Task_140 //////////////////////////////////////////
iCommission = 0;
iNBT = 0;
iVAT = 0;
iTeleLvy = 0;
iOtherLvy = 0;

function onclickTotalBudgetTxt(val) {
    setTaxDetails(val);
    $('#budgetplan_view_popup').modal('toggle');
}

function setTotalBudget() {
    iTotalBudget = parseFloat($('#txt_WithoutTax').val().replace(",", "")).toFixed(2);
    if (iTotalBudget == "NaN")
        iTotalBudget = 0;

    if (usedAmount_pub > iTotalBudget) {
        alert("The budget less than used amount");
        return;
    }
    $('#txt_total_budget').val(getDecimal(parseFloat(iTotalBudget)));

    $('#txt_WithoutTax').val("");
    $('#txt_WithTax').val("");
    $('#budgetplan_view_popup').modal('toggle');
    setBudget(iTotalBudget);
}

function setTaxDetails(val) {
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.bill.getAllTaxDetailsUrl(),
        success: function (data)
        {
            data.forEach(function (item) {
                if (item.taxcategory == "NBT")
                    iNBT = item.rate;
                else if (item.taxcategory == "VAT")
                    iVAT = item.rate;
                else if (item.taxcategory == "Telecommunication Levy")
                    iTeleLvy = item.rate;
                else if (item.taxcategory == "Other Government Levy")
                    iOtherLvy = item.rate;
            });

            if ($("#commison_15").prop("checked"))
                iCommission = 15;
            else
                iCommission = 0;

            $('#lbl_Commission').html(iCommission + '%');
            $('#lbl_NBT').html(iNBT + '%');
            $('#lbl_VAT').html(iVAT + '%');
            $('#lbl_TeleLevy').html(iTeleLvy + '%');
            $('#lbl_OtherLevy').html(iOtherLvy + '%');

            $("#txt_WithoutTax").val(val);
            var withoutTax = calculateTotalWithTax(parseFloat(val.replace(",","")));
            $('#txt_WithTax').val(withoutTax.toFixed(2));

            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function  txt_WOTaxChanged() {
    if (!numberValidation_el($('#txt_WithoutTax'))) {
        $('#txt_WithTax').val("");
        return;
    }
    iTotal = calculateTotalWithTax(parseFloat($('#txt_WithoutTax').val()));
    $('#txt_WithTax').val(iTotal.toFixed(2));
}

function  txt_WithTaxChanged() {
    if (!numberValidation_el($('#txt_WithTax'))) {
        $('#txt_WithoutTax').val("");
        return;
    }
    iTotal = calculateTotalWOTax(parseFloat($('#txt_WithTax').val()));
    $('#txt_WithoutTax').val(iTotal.toFixed(2));
}

function calculateTotalWOTax(iTotal) {
    iTotal = iTotal / (1 + (iVAT + iTeleLvy + iOtherLvy) / 100);
    iTotal = iTotal / (1 + iNBT / 100);
    iTotal = iTotal / (1 - iCommission / 100);

    return iTotal;
}

function calculateTotalWithTax(iTotal) {
    iTotal -= (iTotal * iCommission) / 100;
    iTotal += (iTotal * iNBT) / 100;
    iTotal += (iTotal * (iVAT + iTeleLvy + iOtherLvy)) / 100;
    return iTotal;
}

function checkExistScheduleRef() {
    var ref = $('#txt_schedule_ref').val();
    var respons = false;
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.checkExistScheduleRefUrl(),
        data: {
            scheduleRefe: ref
        },
        success: function (data)
        {
            respons = data;
        },
        error: function (jqXHR, textStatus, thrownError) {
            respons = false;
        }
    })

    return respons;
}


function setBudgetAfterValidation(value){
    if(numberValidation(value)){
       setBudget(value);
       closeDialog();
    }
}