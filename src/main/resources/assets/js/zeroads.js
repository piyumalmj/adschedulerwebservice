//Added channel id list
var channelIdMap = {};
//Not Added advert id list
var newChannelIdMap = {};
//Added advert id list
var advertIdMap = {};
//Not Added advert id list
var newAdvertIdMap = {};
//channel advet map
var channelAdvertmap = {};

var channelAdvertmapJSON = [];
var dataTableLoaded = false;
$(document).ready(function () {
// setAllChannels();
//setAllAdvert();
    setChannelAdvertData();
});

//Add advert to table
function addAdvert() {
    for (var advertkey in newAdvertIdMap) {
        if (document.getElementById(advertkey) != null && document.getElementById(advertkey).checked) {
            advertIdMap[advertkey] = newAdvertIdMap[advertkey];
        }
    }
    buildTable();
    setAllAdvert();
    $('#addAdvertisement').modal('toggle');
}

//Delete Channel_ads_map table selected row
function deleteAdvert(id)
{
    var ret = confirm("Do you want to delete this row?");
    if (!ret) {
        return;
    } else {
        delete advertIdMap[id];
        for (var key in channelIdMap) {
            delete channelAdvertmap[key + '_' + id];
        }
        buildTable();
        setAllAdvert();
    }
}

//Get all advertisemant
function setAllAdvert() {
    showDialog();
    newAdvertIdMap = {};
    var clientID = "-111";
    var adverName = "-111";
    var advertCategory = "-111";
    var advertID =  "-111";
    var f_data = clientID + "," + advertCategory + "," + adverName + "," + advertID;

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementFilterForZeroManualUrl(),
        data: {
            fiterData: f_data
        },
        success: function (data)
        {
            var numRows = 0;
            var trHTML = '<thead><tr><th>ID</th><th>Advertisement</th><th>Type</th><th></th></tr></thead><tbody>';
            for (var i in data)
            {
                var advrtData = advertIdMap[data[i].advertid];
                if (advrtData == null) {
                    newAdvertIdMap[data[i].advertid] = data[i].advertname;
                    trHTML += '<tr id="' + data[i].advertid + '_row">';
                    trHTML += '<td>' + data[i].advertid + '</td>';
                    trHTML += '<td>' + data[i].advertname + '</td>';
                    trHTML += '<td>' + getAdvertType(data[i].adverttype) + '</td>';
                    trHTML += '<td><input id="' + data[i].advertid + '" type = "checkbox" name = "checkbox" value = ""></td>';
                    trHTML += '</tr>';
                }
            }
            trHTML += '</tbody>';
            $('#tbl_all_advert').html(trHTML);
            setDataTable();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            } else {
                alert("Error");
            }
        }
    });

}

function setDataTable() {
    if(dataTableLoaded){
        $('#tbl_all_advert').DataTable().destroy();
    }
    dataTableLoaded = true;
    $('#tbl_all_advert').DataTable(
    {   "bFilter": false,
        "responsive": true,
        "processing": true,
    });
}

function advertNameChange() {

    var clientID = "-111";
    var adverName = $("#txt_search").val();
    var advertCategory = "-111";
    if (adverName == "" || adverName == " ") {
        adverName = "-111"
    }
    var advertID = $("#txt_advertId").val();
    if (advertID == "" || advertID == " ") {
        advertID = "-111"
    }
    var f_data = clientID + "," + advertCategory + "," + adverName + "," + advertID;
    getFilterData(f_data);
}

function getFilterData(p_fiterData) {
    showDialog();
    newAdvertIdMap = {};
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementFilterForZeroManualUrl(),
        data: {
            fiterData: p_fiterData
        },
        success: function (data)
        {
            var numRows = 0;
            var trHTML = '<thead><tr><th>ID</th><th>Advertisement</th><th>Type</th><th></th></tr></thead><tbody>';
            for (var i in data)
            {
                var advrtData = advertIdMap[data[i].advertid];
                if (advrtData == null) {
                    newAdvertIdMap[data[i].advertid] = data[i].advertname;
                    trHTML += '<tr id="' + data[i].advertid + '_row">';
                    trHTML += '<td>' + data[i].advertid + '</td>';
                    trHTML += '<td>' + data[i].advertname + '</td>';
                    trHTML += '<td>' + getAdvertType(data[i].adverttype) + '</td>';
                    trHTML += '<td><input id="' + data[i].advertid + '" type = "checkbox" name = "checkbox" value = ""></td>';
                    trHTML += '</tr>';
                }
            }
            trHTML += '</tbody>';
            $('#tbl_all_advert').html(trHTML);
            setDataTable();
            closeDialog();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    })
}

//Get all advertisemant
function setChannelAdvertData() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.zero.getAllChannelAdvertMapUrl(),
        success: function (data)
        {
            channelIdMap = data.channelIdMap;
            advertIdMap = data.advertIdMap;
            channelAdvertmap = data.channelAdvertMap;
            buildTable();
            setAllAdvert();
            setAllChannels();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

//Get all manual channels and create channel table
function setAllChannels() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.getAllAutoCahnnelsUrl(),
        success: function (data)
        {
            newChannelIdMap = {};
            var trHTML = '<thead> <tr><th>ID</th><th>Channel</th><th></th></tr></thead><tbody>';
            for (var i in data)
            {
                newChannelIdMap[data[i].channelid] = data[i].channelname;
                trHTML += '<tr id="' + data[i].channelid + '_row">';
                trHTML += '<td> ' + data[i].channelid + ' </td>';
                trHTML += '<td> ' + data[i].channelname + ' </td>';
                if (channelIdMap[data[i].channelid] == null) {
                    trHTML += '<td> <input type = "checkbox" id = "' + data[i].channelid + '_chan" name = "checkbox" value = "' + data[i].channelid + '" ></td></tr>';
                } else {
                    trHTML += '<td> <input type = "checkbox" id = "' + data[i].channelid + '_chan" name = "checkbox" value = "' + data[i].channelid + '" checked></td></tr>';
                }
            }
            trHTML += '</tbody>';
            $('#tbl_all_channel').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getDropDownData(){
 if(copyChannelList()){
    toChannelList()
 }
}

function copyChannelList() {
    var cmb = '<option value = "-1" > - Select - </option>';
    for (var key in channelIdMap) {
        cmb += '<option value = "' + key + '" >' + channelIdMap[key] + '</option>';
    }
    $('#cmb_copy_list').html(cmb);
    return true;
}

function toChannelList() {
    var cmb = '<option value = "-1" > - Select - </option>';
    for (var key in newChannelIdMap) {
        if (key !== "1") {
            cmb += '<option value = "' + key + '" >' + newChannelIdMap[key] + '</option>';
        }
    }
    $('#cmb_to_list').html(cmb);
}

function copyChannel() {

    var toChannelID = $('#cmb_to_list').val();
    var copyChannelID = $('#cmb_copy_list').val();
    if (copyChannelID == -1) {
        viewMessageToastTyped("Please select copy channel", "warning");
        return;
    }
    if (toChannelID == -1) {
        viewMessageToastTyped("Please select to channel", "warning");
        return;
    }

    var newChannel = true;
    for (var channdlkey in channelIdMap) {
        if (channdlkey == toChannelID) {
            newChannel = false;
        }
    }

    if (newChannel) {
        channelIdMap[toChannelID] = newChannelIdMap[toChannelID];
    } else {
        var ret = confirm("Alredy added channel. Are you sure to copy this channel?");
        if (!ret) {
            return;
        } else {
            for (var Ad_key in advertIdMap) {
                if (channelAdvertmap[toChannelID + '_' + Ad_key] != null) {
                    delete channelAdvertmap[ toChannelID + '_' + Ad_key];
                }
            }
        }
    }

    for (var Ad_key in advertIdMap) {
        if (channelAdvertmap[copyChannelID + '_' + Ad_key] != null) {
            channelAdvertmap[ toChannelID + '_' + Ad_key] = channelAdvertmap[copyChannelID + '_' + Ad_key];
        }
    }

    buildTable();
    setAllChannels();
    $('#copyChannel').modal('toggle');
    saveMap();
}

function addOrRemoveChannel() {

    //Delete not checked channels
    for (var channdlkey in newChannelIdMap) {
        if (!document.getElementById(channdlkey + '_chan').checked) {
            delete newChannelIdMap[channdlkey];
        }
    }

    var notUpdate = true;
    for (var oldkey in channelIdMap) {
        notUpdate = true;
        for (var newkey in newChannelIdMap) {
            if (oldkey == newkey) {
                notUpdate = false;
                delete newChannelIdMap[newkey];
            }
        }

        if (notUpdate) {
            delete channelIdMap[oldkey];
            for (var advertkey in advertIdMap) {
                delete channelAdvertmap[oldkey + '_' + advertkey];
            }
        }
    }

    for (var key in newChannelIdMap) {
        channelIdMap[key] = newChannelIdMap[key];
    }
    buildTable();
    setAllChannels();
    $('#addChannel').modal('toggle');
}


function buildTable() {

    var channelkey = '';
    var table = '<thead><tr><th></th>';
    for (var key in channelIdMap) {
        if (key !== "1") {
            table += '<th>' + channelIdMap[key] + '</th>';
        }
    }
    table += '<th></th></tr></thead><tbody>';
    for (var keyAdvert in advertIdMap) {
        table += '<tr id="' + keyAdvert + '_row">';
        table += '<td>' + advertIdMap[keyAdvert] + '</td>';
        for (var keyChannel in channelIdMap) {
            channelkey = keyChannel;
            if (channelkey !== "1") {
                table += '<td><input id="' + keyChannel + '_' + keyAdvert + '" type="text" class="txt-input font-A-14" value="" onkeyup="changePercentage(this.id)"></td>';
            }
        }
        table += '<td><button id="' + keyAdvert + '" onclick="deleteAdvert(this.id)"><span class="glyphicon glyphicon-remove"></span></button></td>';
        table += '</tr>';
    }
    table += ' </tbody>';
    $('#tbl_zero_ads').html(table);
    for (var ch_Ad_key in channelAdvertmap) {
        var ch_ad_arry = ch_Ad_key.split("_");
        if (ch_ad_arry[0] !== "1") {
            document.getElementById(ch_Ad_key).value = channelAdvertmap[ch_Ad_key];
        }
    }
}

function saveMap() {
    channelAdvertmapJSON = [];
    for (var ch_Ad_key in channelAdvertmap) {
        if (channelAdvertmap[ch_Ad_key] != null) {
            var ch_ad_arry = ch_Ad_key.split("_");
            var data = {channelId: ch_ad_arry[0], advertId: ch_ad_arry[1], percentage: channelAdvertmap[ch_Ad_key]};
            channelAdvertmapJSON.push(data);
        }
    }
    if (channelAdvertmapJSON.length != 0) {
            var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
            var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            type: 'POST',
            dataType: "json",
            url: URI.zero.saveChannelAdvertMapUrl(),
            data: {
                mapData: JSON.stringify(channelAdvertmapJSON),
            },
            success: function (data)
            {
                if (data) {
                    viewMessageToastTyped("successfully", "success");
                }
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
    } else {
        viewMessageToastTyped("No data", "success");
    }
}

function changePercentage(id) {
    var channelId = id.split('_');
    var total = 0;
    for (var key in advertIdMap) {
        var txt_id = channelId[0] + '_' + key;
        var value = document.getElementById(txt_id).value;
        if (value != "") {
            total = total + stoi(value);
        }
    }
    if (total <= 100) {
        channelAdvertmap[id] = document.getElementById(id).value;
    } else {
        viewMessageToastTyped("Greater than 100%", "warning");
        document.getElementById(id).value = "";
    }
}