var spotArray = [];
function rederectToPriorityPage(iRowID) {
    iWoId = $('#work_order_id').html();
    dtStartDate = $('#start_date').html();
    dyEndDate = $('#end_date').html();
    row_left = mapScheduleMetaData[iRowID];
    iChannelId = row_left.channelId;
    iAdvertId = row_left.channelId;
    tStartTime = row_left.startTime;
    window.location.href = URI.priority.settingPageUrl() + "?wo_id=" + iWoId + "&channel=" + iChannelId + "&advert=" + iAdvertId + "&s_date=" + dtStartDate + "&e_date=" + dyEndDate + "&s_time=" + tStartTime;
}

function updateTVCBreakdownTooltip() {
    iTotalAvailableSpots = 0;
    iTotalSpots = 0;
    stToolTip = '';
    for (var key in all_TVCBreakdown) {
        iAvailableSpot = 0;
        if (key in availableTVCBreakdown)
            iAvailableSpot = availableTVCBreakdown[key];
        stToolTip += key + ' > ';
        stToolTip += iAvailableSpot + ' / ' + all_TVCBreakdown[key];
        stToolTip += '  :  ';
        iTotalAvailableSpots += availableTVCBreakdown[key];
        iTotalSpots += all_TVCBreakdown[key];
    }

    if (stToolTip.length > 5)
        stToolTip = stToolTip.substring(0, stToolTip.length - 5);
    $('#id_Tvcbreakdown').prop('title', stToolTip);
    $('#id_Tvcbreakdown').attr('data-original-title', stToolTip);
    $('[data-toggle="tooltip"]').tooltip();
}

function changeTimeDropdownData(iChannelId) {
    var startTimeBelt = '<option data-timebelt="0" value="-01:00:00">----</option>' + mapChannelTimeBeltsStart[iChannelId];
    var endTimeBelt = '<option data-timebelt="0" value="-01:00:00">----</option>' + mapChannelTimeBeltsEnd[iChannelId];

    var selectedStartTime=$('#schedule_startTimeDropdown').val();
    var selectedEndTime=$('#schedule_endTimeDropdown').val();

    $('#schedule_startTimeDropdown').html(startTimeBelt);
    $('#schedule_endTimeDropdown').html(endTimeBelt);

    if(selectedStartTime !== null && selectedStartTime !== "-01:00:00") {
       $('#schedule_startTimeDropdown').val(selectedStartTime);
    }

    if(selectedEndTime !== null && selectedEndTime !== "-01:00:00") {
        $('#schedule_endTimeDropdown').val(selectedEndTime);
    }
    //setTimeSlotsUtilization();
    //clearTextBox();
    if (iChannelId != -999) {
        var time = $('#schedule_startTimeDropdown').val();
        startTimeDropDownChange(time);
    }
}

function setTimeForFilter() {
    startTimeDrop = _.sortBy(startTimeDrop, "start");
    endTimeDrop = _.sortBy(endTimeDrop, "end");

    var startDrop = '<select id="startTime_filter" onchange="filter_startTimeChange()" class="form-control" ><option value="-999" selected>All Start</option>';
    var endDrop = '<select id="endTime_filter" onchange="filter_endTimeChange()" class="form-control" ><option value="-999" selected>All End</option>';
    for (var i = 0; i < startTimeDrop.length; i++)
    {
        if(startTimeDrop[i].start!='null'){
            startDrop += '<option value="' + startTimeDrop[i].start + '">' + RemoveSec(startTimeDrop[i].start) + '</option>';
        }
        //startDrop += '<option value="' + startTimeDrop[i].start + '">' + RemoveSec(startTimeDrop[i].start) + '</option>';
    }

    for (var i = 0; i < endTimeDrop.length; i++)
    {
        if(endTimeDrop[i].end!='null'){
            endDrop += '<option value="' + endTimeDrop[i].end + '">' + RemoveSec(endTimeDrop[i].end) + '</option>';
        }
    }
    startDrop += '</select>';
    endDrop += '</select>';
    $('#startTime_div').html(startDrop);
    $('#endTime_div').html(endDrop);
}

function clearTextBox() {
    var timeDiff = getDifferanBetweenDay(startDate, endDate);
    for (var i = 0; i <= timeDiff; i++)
    {
        var input_date = addDays(startDate, i, 1);
        var input_id = input_date + '_txt';
        document.getElementById(input_id).value = "";
    }
}


//////////// page update ///////////////////////////////////////////////////////
function loadAvailableSpot(v_channelId) {
    var v_workOrderId = workOrderId;
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.scheduler.selectedScheduleAvailableSpotsCountUrl(),
        data: {
            workOrderId: v_workOrderId,
            channelId: v_channelId
        },
        success: function (data)
        {
            mapChannelAvailableSpot[v_channelId] = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            return false;
        }
    });
}

function setSpotTable(channel_id) {
    var data = mapChannelAvailableSpot[channel_id];
    spotArray = data;
    var spotTable = '<table id="spot_table" ><thead>';
    var spotThesd = '<tr><th>Duration</th>';
    var spotTbody = '';
    var count = 1;
    for (var key in spotArray.tvcBreackdownMap) {
        if(spotTbody==''){
            spotTbody = '<tbody><tr><td>TVC</td>';
        }
        var AvailableSpot = 0;
        if (key in spotArray.tvcBreackdownMapAvailable) {
            AvailableSpot = spotArray.tvcBreackdownMapAvailable[key];
            spotThesd += '<th id="' + key + '">' + key + '</th>';
            spotTbody += '<td>' + (spotArray.tvcBreackdownMap[key] - spotArray.tvcBreackdownMapAvailable[key]) + '/' + spotArray.tvcBreackdownMap[key] + '</td>';
            // spotTable += '<tr><td id="' + key + '">' + key + '</td><td>' + (spotArray.tvcBreackdownMap[key] - spotArray.tvcBreackdownMapAvailable[key]) + '/' + spotArray.tvcBreackdownMap[key] + '</td><td></td><td></td><td></td><td></td></tr>';
            count++;
        }
    }

    spotThesd += '</thead>';
    spotTbody += '</tr>';
    var notTVC = '';
    if (data.logoSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Logo</td><td colspan="' + count + '">' + (spotArray.logoSpots - spotArray.availableLogoSpots) + '/' + spotArray.logoSpots + '</td></tr>';
    }
    if (data.crowlerSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Crowler</td><td colspan="' + count + '">' + (spotArray.crowlerSpots - spotArray.availableCrowlerSpots) + '/' + spotArray.crowlerSpots + '</td></tr>';
    }
    if (data.v_sqeezeSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Vsqeeze</td><td colspan="' + count + '">' + (spotArray.v_sqeezeSpots - spotArray.availableV_SqeezeSpots) + '/' + spotArray.v_sqeezeSpots + '</td></tr>';
    }
    if (data.l_sqeezeSpots != 0) {
        notTVC += '<tr><td id="' + key + '">Lsqeeze</td><td colspan="' + count + '">' + (spotArray.l_sqeezeSpots - spotArray.availableL_SqeezeSpots) + '/' + spotArray.l_sqeezeSpots + '</td></tr>';
    }

    spotTbody += notTVC + '</tbody>';
    spotTable += spotThesd + spotTbody + '</table>';
    $('#spot_table_div').html(spotTable);
}

function setAvailableSpot(channel_id, data) {
    mapChannelAvailableSpot[channel_id] = data;
    setSpotTable(channel_id);
}

function onCellEditInsert(id) {
    var advert_type = $('#advertTypeDropdown').val();
    if (advert_type == 'All')
    {
        viewMessageToastTyped("Please select advertisement type.", "Error");
        $('#' + id).val('');
        return false;
    }

    var channel_id = $('#schedule_channelDropdown').val();
    if (channel_id == -999) {
        viewMessageToastTyped("Please select a channel.", "Error");
        $('#' + id).val('');
        return false;
    }

    var advert_id = $("#schedule_advertDropdown").val();
    if (advert_id == -1) {
        viewMessageToastTyped("Please select an Advertisement.", "Error");
        $('#' + id).val('');
        return false;
    }

    var pre_value = iInsertCount;
    var current_value = getInsertRowAdvertCount();
    var count_change = current_value - pre_value;
    var retValue = spotCountChanged(channel_id, advert_id, count_change);
    if (!retValue) {
        current_value = getInsertRowAdvertCount();
        count_change = current_value - pre_value;
        spotCountChanged(channel_id, advert_id, count_change);
        $('#' + id).val("");
    }
    else
    {
        iInsertCount = current_value;
        var dateDifferan = getDifferanBetweenDay(startDate, id.replace('_txt', ''));
        var mapValue = mapColumnCount[dateDifferan];
        if (mapValue === undefined ) {
            mapColumnCount[dateDifferan] = count_change;
        } else {
            mapColumnCount[dateDifferan] = mapValue + count_change;
        }
        $('#col_count_' + dateDifferan).html(mapColumnCount[dateDifferan]);
        $('#insert-row-spot').html(iInsertCount);
    }
    getGrandTotal();
    return retValue;
}

function onCellEdit(id) { /// for inplace edit
    var cell_id = id.replace('cell_', '');
    var row_id = (Math.floor(cell_id / _RowIDIncrement)) * _RowIDIncrement;
    var pre_value = mapEditBoxDataCurrent[cell_id];
    var current_value = $('#' + id).val();
    if (!isInteger(current_value)) {
        $('#' + id).val(pre_value);
        return;
    }

    current_value = (current_value == "") ? 0 : stoi(current_value);
    var count_change = current_value - pre_value;
    var channel_id = mapScheduleMetaDataCurrent[row_id].channelId;
    var advert_id = mapScheduleMetaDataCurrent[row_id].advertId;
    pre_value = (pre_value == 0) ? "" : pre_value; /// for reset value if thre is an error

    var retValye = spotCountChanged(channel_id, advert_id, count_change);
    if (retValye) {
        mapEditBoxDataCurrent[cell_id] = current_value;
        mapRowCount[row_id] = mapRowCount[row_id] + count_change;
        $('#row_count_' + row_id).html(mapRowCount[row_id]);
        var col_id = cell_id % _RowIDIncrement;
        mapColumnCount[col_id] = mapColumnCount[col_id] + count_change;
        $('#col_count_' + col_id).html(mapColumnCount[col_id]);
        colorEditedCells(row_id, false);
    }
    else {
        $('#' + id).val(pre_value);
    }
    getGrandTotal();
}

function spotCountChanged(channel_id, advert_id, count_change) {
    if (channel_id == -999 || advert_id == -1)
        return;
    var availableSpots = mapChannelAvailableSpot[channel_id];
    var advert = mapAdverts[advert_id];
    var availableSpotsCount = 0;
    switch (advert.type)
    {
        case "ADVERT":
            var iAvailableDuration = availableSpots.availableTVCDuration;
            iAvailableDuration -= count_change * advert.duration;
            if (iAvailableDuration < 0) {
                viewMessageToastTyped("Available TVC spots are finished.", "Error");
                return false;
            } else {
                availableSpots.availableTVCDuration = iAvailableDuration;
                if (!(advert.duration in availableSpots.tvcBreackdownMap)) {
                    availableSpots.tvcBreackdownMap[advert.duration] = 0;
                    availableSpots.tvcBreackdownMapAvailable[advert.duration] = 0;
                }
                availableSpots.tvcBreackdownMapAvailable[advert.duration] = availableSpots.tvcBreackdownMapAvailable[advert.duration] - count_change;
                availableSpots.availableTvcSpots = Math.floor(availableSpots.availableTVCDuration / 30);
            }
            break;
        case "LOGO":
            availableSpotsCount = availableSpots.availableLogoSpots - count_change;
            if (availableSpotsCount >= 0)
            {
                availableSpots.availableLogoSpots = availableSpotsCount;
            } else
            {
                viewMessageToastTyped("Available Logo spots are finished.", "Error");
                return false;
            }
            break;
        case "CRAWLER":
            availableSpotsCount = availableSpots.availableCrowlerSpots - count_change;
            if (availableSpotsCount >= 0)
            {
                availableSpots.availableCrowlerSpots = availableSpotsCount;
            } else
            {
                viewMessageToastTyped("Available Crawler spots are finished.", "Error");
                return false;
            }
            break;
        case "V_SHAPE":
            availableSpotsCount = availableSpots.availableV_SqeezeSpots - count_change;
            if (availableSpotsCount >= 0)
            {
                availableSpots.availableV_SqeezeSpots = availableSpotsCount;
            } else
            {
                viewMessageToastTyped("Available V-Squeeze spots are finished.", "Error");
                return false;
            }
            break;
        case "L_SHAPE":
            availableSpotsCount = availableSpots.availableL_SqeezeSpots - count_change;
            if (availableSpotsCount >= 0)
            {
                availableSpots.availableL_SqeezeSpots = availableSpotsCount;
            } else
            {
                viewMessageToastTyped("Available L-Squeeze spots are finished.", "Error");
                return false;
            }
            break;
        default :
            break;
    }
    setAvailableSpot(channel_id, availableSpots);
    return true;
}

function setTimeSlotsUtilization() {
    iStartTimeBeltId = $('#schedule_startTimeDropdown').find('option:selected').data('timebelt');
    iEndTimeBeltId = $('#schedule_endTimeDropdown').find('option:selected').data('timebelt');
    if (iStartTimeBeltId == iEndTimeBeltId) {
        mapDates = mapTimeBeltUtilization[iStartTimeBeltId];
        if (jQuery.isEmptyObject(mapDates))
            setToDefault(false);
        for (var date in mapDates) {
            iCategory = mapDates[date].ucategory;
            eParentTd = $('#' + date + '_txt').parent('td');
            if (eParentTd.hasClass("disable"))
                continue;
            $('#' + date + '_txt').attr("placeholder", "" + (mapDates[date].uRatio.toFixed(2) * 100) + "%");
            eParentTd.removeClass();
            if (iCategory == 0)
                eParentTd.addClass('timeslot_utilization_category_0');
            else if (iCategory == 1)
                eParentTd.addClass('timeslot_utilization_category_1');
            else if (iCategory == 2)
                eParentTd.addClass('timeslot_utilization_category_2');
        }
    }
    else {
        mapDates = mapTimeBeltUtilization[iStartTimeBeltId];
        if (jQuery.isEmptyObject(mapDates))
            setToDefault(true);
        for (var date in mapDates) {
            eParentTd = $('#' + date + '_txt').parent('td');
            if (!eParentTd.hasClass("disable"))
                eParentTd.removeClass();
            $('#' + date + '_txt').attr("placeholder", "");
        }
    }
}

function setToDefault(bRemoveClass) {
    stStartDate = $("#start_date").html();
    stEndDate = $("#end_date").html();
    iTimeDiff = getDifferanBetweenDay(stStartDate, stEndDate);
    for (i = 0; i <= iTimeDiff; i++) {
        date = addDays(stStartDate, i, 1);
        eParentTd = $('#' + date + '_txt').parent('td');
        if (!eParentTd.hasClass("disable")) {
            eParentTd.removeClass();
            if (!bRemoveClass) {
                eParentTd.addClass('timeslot_utilization_category_0');
                $('#' + date + '_txt').attr("placeholder", "0%");
            } else {
                $('#' + date + '_txt').attr("placeholder", "");
            }
        }
    }
}

/////// load data //////////////////////////////////////////////////////////////
var mapChannelTimeBeltsStart = {};
var mapChannelTimeBeltsEnd = {};
var mapTimeBeltUtilization = {};
function setTimeDropDown() {
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.timeBelts.getListbyWorkOrderUrl(),
        data: {
            wo_Id: workOrderId
        },
        success: function (data)
        {
            $.each(data, function (index, item) {
                if (!(item.channelID in mapChannelTimeBeltsStart))
                    mapChannelTimeBeltsStart[item.channelID] = '';
                if (!(item.channelID in mapChannelTimeBeltsEnd))
                    mapChannelTimeBeltsEnd[item.channelID] = '';
                mapChannelTimeBeltsStart[item.channelID] += '<option data-timebelt="' + item.timeBeltId + '" value="' + item.startTime + '">' + RemoveSec(item.startTime) + '</option>';
                dtStartTime = Add30MinutstoHour(item.startTime);
                mapChannelTimeBeltsStart[item.channelID] += '<option data-timebelt="" value="' + dtStartTime + '">' + RemoveSec(dtStartTime) + '</option>';
                if (item.endTime == '00:00:00')
                    item.endTime = '24:00:00';
                mapChannelTimeBeltsEnd[item.channelID] += '<option data-timebelt="' + item.timeBeltId + '" value="' + item.endTime + '">' + RemoveSec(item.endTime) + '</option>';
                dtEndTime = item.endTime;
                if (dtEndTime == '24:00:00')
                    dtEndTime = '00:30:00';
                dtEndTime = Add30MinutstoHour(dtEndTime);
                mapChannelTimeBeltsEnd[item.channelID] += '<option data-timebelt="" value="' + dtEndTime + '">' + RemoveSec(dtEndTime) + '</option>';
                mapTimeBeltUtilization[item.timeBeltId] = {};
            });
            default_tr_left = $('#tbody_left').html(); //'<tr id="table_default'+ default_data[1];
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function loadTimeBeltUtilization() {
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.timeBelts.slotUtilizationUrl(),
        data: {
            workoder_id: workOrderId
        },
        success: function (data)
        {
            setTimeBeltUtilization(data);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function setTimeBeltUtilization(data) {

    if (data !== null) {
        $.each(data, function (i, timeBelt) {
            mapTimeBelt = mapTimeBeltUtilization[timeBelt.timeBelId];
            /*--Check again--*/
            if (!mapTimeBelt) {
                mapTimeBelt = {};
                mapTimeBeltUtilization[timeBelt.timeBelId] = mapTimeBelt;
            }
            items = timeBelt.utilizationItems;

            $.each(items, function (j, item) {
                mapTimeBelt[item.date] = item;
            });
        });
    }
}


/////////////// even handlers //////////////////////////////////////////////////
function channelDropDownChange(channelId) {
    var advert_id = $("#schedule_advertDropdown").val();
    if (channelId != -999) {
        if (advert_id != -1) {
            var row_advert_count = getInsertRowAdvertCount();
            spotCountChanged(selectedChannelId, advert_id, -row_advert_count);
            spotCountChanged(channelId, advert_id, row_advert_count);
        }

        setSpotTable(channelId);
        selectedChannelId = channelId;
        changeTimeDropdownData(channelId);
    }
    else {
        $("#schedule_channelDropdown").val(selectedChannelId);
    }
}

function Add30MinutstoHour(time) {
    var realTime = time.split(":");
    return realTime[0] + ':' + "30:00";
}

function getCurrentMonth() {
    var d = new Date();
    var month = d.getMonth();
    switch (month) {
        case 0:
            return "Jan";
            break;
        case 1:
            return "Feb";
            break;
        case 2:
            return "Mar";
            break;
        case 3:
            return "Apr";
            break;
        case 4:
            return "May";
            break;
        case 5:
            return "Jun";
            break;
        case 6:
            return "Jul";
            break;
        case 7:
            return "Aug";
            break;
        case 8:
            return "Sep";
            break;
        case 9:
            return "Oct";
            break;
        case 10:
            return "Nov";
            break;
        case 11:
            return "Dec";
            break;
        default:
            return month;
            break;
    }
}

/**
 * In this function, User can replace advertisement
 */
function replaceAdvert() {

    var workOrderId_p = workOrderId;
    var originalAdvert = document.getElementById("original_advertDropdown_pop");
    var originalAdvertId = originalAdvert.options[originalAdvert.selectedIndex].id;
    var originalAdvertDuration = (originalAdvert.options[originalAdvert.selectedIndex].value).split("_");
    var replaceAdvert = document.getElementById("replace_advertDropdown_pop");
    var replaceAdvertId = replaceAdvert.options[replaceAdvert.selectedIndex].id;
    var replaceAdvertDuration = (replaceAdvert.options[replaceAdvert.selectedIndex].value).split("_");
    if (originalAdvertId == -9 || replaceAdvertId == -9) {
        alert("Please select both advertisement");
        return;
    }

    if (originalAdvertDuration[0] < replaceAdvertDuration[0]) {
        alert("First advertisements duration less than second advertisement");
        return;
    }

    if (originalAdvertDuration[0] % replaceAdvertDuration[0] != 0) {
        alert("");
        return;
    }

    var workOrderData = '{"workOrderId":' + workOrderId_p + ',"originalAdvertID":' + originalAdvertId + ',"replaceAdvertID":' + replaceAdvertId + '}';
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.advertReplaceUrl(),
        data: {
            workOrderData: workOrderData
        },
        success: function (data)
        {
            closeDialog();
            alert(data);
            return data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            return false;
        }
    });
}

function getAdvertSpot() {
    var originalAdvert = document.getElementById("original_advertDropdown_pop");
    var originalAdvertId = originalAdvert.options[originalAdvert.selectedIndex].id;
    if (originalAdvertId == -9) {
        return;
    }

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getAvailbleAdvertCountUrl(),
        data: {
            workOrderId: workOrderId,
            advertId: originalAdvertId
        },
        success: function (data)
        {
            closeDialog();
            var msg = '<label class="control-label col-sm-1 col-md-1 col-lg-1"></label><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms">Available scheduled spot:</label><label class="control-labe col-sm-3 col-md-3 col-lg-3 lable_forms">' + data[1] + ' / ' + data[0] + '</label>'
            $('#msg_lbl').html(msg);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

$(document).ready(function () {
    $('#cmb_days_pattern').multiselect();
    $('#rp_form-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});
//==== Task 243 =========================================
lst_PatternFillTimeBelts = ["00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30",
    "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30",
    "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30",
    "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30",
    "23:00", "23:30", "24:00"];
function buildPatternFillModel() {
    if (checkTableDirty())
        return;
    $('#cmb_pt_advertisement').empty();
    $('#cmb_pt_start_time').empty();
    $('#cmb_pt_end_time').empty();
    $('#txt_pt_slot_count').val("");
    $('#txt_pt_revenue_line').val("");
    for (var i = 0; i < 48; i++) {
        $('#cmb_pt_start_time').append($('<option>', {value: i, text: lst_PatternFillTimeBelts[i]}));
        $('#cmb_pt_end_time').append($('<option>', {value: i + 1, text: lst_PatternFillTimeBelts[i + 1]}));
    }

    $.each(lstAdverts, function (i, item) {
        $('#cmb_pt_advertisement').append($('<option>', {value: item.advertId, text: item.advertId + '-' + item.advertName}));
    });
    $("#cmb_pt_revenue_month").val(getCurrentMonth());
    console.log(endDate);
    var sDate = (new Date(startDate) < new Date()) ? new Date() : new Date(startDate);
    $('#form-date').datetimepicker({
        pickTime: false,
        startDate: sDate
    });
    ;
    $("#form-date").datetimepicker("setDate", sDate);
    $('#to-date').datetimepicker({
        pickTime: false,
        startDate: new Date(),
        endDate: new Date(endDate)
    });
    $("#to-date").datetimepicker("setDate", sDate);
    $("#cmb_pt_channel_select option:selected").prop("selected", false);
    $("#cmb_pt_channel_select").multiselect('refresh');
    $("#cmb_days_pattern option").prop("selected", true);
    $("#cmb_days_pattern").multiselect('refresh');
}

function preSavePatternAfterValidation(form) {
    if(validateInputFields(form)){
        preSavePattern();
    }
}

function preSavePattern() {
    showDialog();
    var lst_channel_ids = [];
    var selected = $('#cmb_pt_channel_select').find("option:selected");
    selected.each(function () {
        lst_channel_ids.push($(this).val());
    });
    if (lst_channel_ids.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select a Channel.", "Error");
        return;
    }

    var i_start_time = stoi($('#cmb_pt_start_time').val());
    var i_end_time = stoi($('#cmb_pt_end_time').val());
    if (i_start_time >= i_end_time) {
        closeDialog();
        viewMessageToastTyped("Invalid end time.", "Error");
        return;
    }

    var st_start_time = lst_PatternFillTimeBelts[i_start_time];
    var st_end_time = lst_PatternFillTimeBelts[i_end_time];
    if (st_end_time == '24:00')
        st_end_time = '23:59';
    var st_start_date = $('#pt_fromDate').val();
    if (new Date(st_start_date).getTime() < new Date(startDate).getTime()) {
        hasError($("#form-date"), true, 'Invalid Start Date.');
        closeDialog();
        return;
    }

    var st_end_date = $('#pt_toDate').val();
    if (new Date(st_start_date).getTime() > new Date(st_end_date).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }
    if (new Date(st_end_date).getTime() > new Date(endDate).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }

    var i_workorder_id = workOrderId;
    var i_advert_id = $('#cmb_pt_advertisement').val();
    var i_slot_count = $('#txt_pt_slot_count').val();
    var st_month = $('#cmb_pt_revenue_month').val();
    var st_revenue_line = $('#txt_pt_revenue_line').val();
    var lst_days = [];
    var selected_days = $('#cmb_days_pattern').find("option:selected");
    selected_days.each(function () {
        lst_days.push($(this).val());
    });
    if (lst_days.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select Days for Repeat.", "Error");
        return;
    }

    var json_data = '{"workOrderId":' + i_workorder_id + ',"lstChannelId":' + JSON.stringify(lst_channel_ids) + ',"advertId":' + i_advert_id
            + ',"startDate":"' + st_start_date + '","endDate":"' + st_end_date + '","startTime":"' + st_start_time + ':","endTime":"' + st_end_time
            + '","slotCount":' + i_slot_count + ',"lstDays":' + JSON.stringify(lst_days) + ',"revenueMonth":"' + st_month + '","revenueLine":"' + st_revenue_line + '"}';

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.scheduler.prePatternInsertUrl(),
        data: {
            scheduleData: json_data
        },
        success: function (data)
        {
            closeDialog();
            var vTableData = '';
            vTableData += '<table class="table table-bordered" style="text-align:center;margin-top:10px;margin-bottom:10px;">';
            vTableData += '<thead style="text-align:center">';
            vTableData += '<tr><th>Channel</th><th>Available Spots (' + data[0].advertDuration + 's)</th>';
            vTableData += '<th>Duration(s)(Ava./Total)</th><th>New Spots (Possible/Total)</th>';
            vTableData += '</tr></thead>';
            vTableData += '<tbody>';
            $.each(data, function (i, item) {
                vTableData += '<tr>';
                vTableData += '<td>' + item.channelName + '</td>';
                vTableData += '<td>' + item.availableSpotCount + '</td>';
                vTableData += '<td>' + item.availableScheduleTime + ' / ' + item.initialScheduleTime + '</td>';
                vTableData += '<td>' + item.validSlotCount + ' / ' + item.newSlotCount + '</td>';
                vTableData += '</tr>';
            });
            vTableData += '</thead>';
            vTableData += '</table>';
            viewConfirmToast(vTableData, 'Continue ?', savePattern);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function savePattern()
{
    showDialog();
    var lst_channel_ids = [];
    var selected = $('#cmb_pt_channel_select').find("option:selected");
    selected.each(function () {
        lst_channel_ids.push($(this).val());
    });
    if (lst_channel_ids.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select a Channel.", "Error");
        return;
    }

    var i_start_time = stoi($('#cmb_pt_start_time').val());
    var i_end_time = stoi($('#cmb_pt_end_time').val());
    if (i_start_time >= i_end_time) {
        closeDialog();
        viewMessageToastTyped("Invalid end time.", "Error");
        return;
    }

    var st_start_time = lst_PatternFillTimeBelts[i_start_time];
    var st_end_time = lst_PatternFillTimeBelts[i_end_time];
    if (st_end_time == '24:00')
        st_end_time = '23:59';
    var st_start_date = $('#pt_fromDate').val();
    if (new Date(st_start_date).getTime() < new Date(startDate).getTime()) {
        hasError($("#form-date"), true, 'Invalid Start Date.');
        closeDialog();
        return;
    }

    var st_end_date = $('#pt_toDate').val();
    if (new Date(st_start_date).getTime() > new Date(st_end_date).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }
    if (new Date(st_end_date).getTime() > new Date(endDate).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }

    var i_workorder_id = workOrderId;
    var i_advert_id = $('#cmb_pt_advertisement').val();
    var i_slot_count = $('#txt_pt_slot_count').val();
    var st_month = $('#cmb_pt_revenue_month').val();
    var st_revenue_line = $('#txt_pt_revenue_line').val();
    var lst_days = [];
    var selected_days = $('#cmb_days_pattern').find("option:selected");
    selected_days.each(function () {
        lst_days.push($(this).val());
    });
    if (lst_days.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select Days for Repeat.", "Error");
        return;
    }

    var json_data = '{"workOrderId":' + i_workorder_id + ',"lstChannelId":' + JSON.stringify(lst_channel_ids) + ',"advertId":' + i_advert_id
            + ',"startDate":"' + st_start_date + '","endDate":"' + st_end_date + '","startTime":"' + st_start_time + ':","endTime":"' + st_end_time
            + '","slotCount":' + i_slot_count + ',"lstDays":' + JSON.stringify(lst_days) + ',"revenueMonth":"' + st_month + '","revenueLine":"' + st_revenue_line + '"}';

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.scheduler.schedulePatternSaveUrl(),
        data: {
            scheduleData: json_data
        },
        success: function (data)
        {
            $('#txt_pt_slot_count').val("");
            if (data != null){
                checkFiler = true;
                buildUpdateScheduleView(data);
                viewMessageToastTyped(data.message,"Info");
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}


//==== Task 241 =========================================
var iReplaceTableRowCount = 0;
function buildReplaceModel()
{
    var currentDate = addDays(new Date(),0,1);
    if (checkTableDirty())
        return;
    console.log(endDate);

    if(new Date(currentDate)<=new Date(addDays(endDate,1,1))){
        $("#rp_fromDate").prop("disabled", false);
        $("#rp_toDate").prop("disabled", false);
        $('#rp_to-date').datetimepicker({
            pickTime: false,
            startDate: new Date(),
            endDate: new Date(endDate)
        });
    }else{
        $("#rp_fromDate").prop("disabled", true);
        $("#rp_toDate").prop("disabled", true);
    }

    $('#cmb_rp_channel_select').multiselect();
    $('#cmb_rp_channel_select').multiselect('rebuild');
}

function addReplaceAdd() {
    var advert_id = $('#replace_advertDropdown_pop').val();
    if (advert_id == -1) {
        viewMessageToastTyped("Please select a Advertisement.", "Error");
        return;
    }

    var selected_advert = null;
    $.each(lstAdverts, function (i, item) {
        if (item.advertId == advert_id) {
            selected_advert = item;
        }
    });
    if (selected_advert == null) {
        viewMessageToastTyped("Please select a Advertisement.", "Error");
        return;
    }

    var table_row = '<tr id="tr_rp_' + (++iReplaceTableRowCount) + '"><td><button type="button" class="close close-icon" aria-label="Close" onclick="removeReplaceAdvert(' + iReplaceTableRowCount + ')"><span class="glyphicon glyphicon-remove"></span></button></td><td>'
            + advert_id + '</td><td><a href="#" id="' + advert_id + '" onclick="getId(this.id)">' + selected_advert.advertName + '</a></td><td>' + selected_advert.duration + '</td><td>' + selected_advert.lastModifydate + '</td></tr>';
    $('#replace_advertisement_table_pop').append(table_row);
}

function removeReplaceAdvert(id) {
    $('#tr_rp_' + id).remove();
}

var previousReplaceAdvertType = "";
function cmbChangedRepalceAdvert() {
    var id = $('#original_advertDropdown_pop').val();
    var type = "";
    var notSetType = true;
    $.each(lstAdverts, function (i, item) {
        if (item.advertId == id) {
            type = item.type;
            notSetType = false;
        }
    });

    if(notSetType){
        $.each(suspendLstAdverts, function (i, item) {
            if (item.advertId == id) {
                type = item.type;
            }
        });
    }

    if (previousReplaceAdvertType != type) {
        $('#replace_advertDropdown_pop').empty();
        $('#replace_advertDropdown_pop').append($('<option>', {value: -1, text: 'Select Advertisement'}));
        $.each(lstAdverts, function (i, item) {
            $('#replace_advertDropdown_pop').append($('<option>', {value: item.advertId, text: item.advertId + '-' + item.advertName}));
        });
        $('#tbl_replace_advertisement_body').empty();
    }
    previousReplaceAdvertType = type;
}

function preReplaceAdvertismetsAfterValidation(form) {
    if( validateInputFields(form)) {
        preReplaceAdvertismets();
    }
}

function preReplaceAdvertismets() {
    showDialog();
    var lst_channel_ids = [];
    var selected = $('#cmb_rp_channel_select').find("option:selected");
    selected.each(function () {
        lst_channel_ids.push($(this).val());
    });
    if (lst_channel_ids.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select a Channel.", "Error");
        return;
    }

    var st_start_date = $('#rp_fromDate').val();
    var st_end_date = $('#rp_toDate').val();
    if (new Date(st_start_date).getTime() > new Date(st_end_date).getTime()) {
        hasError($("#rp_to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }

    var lst_replace_adverts = [];
    $('#replace_advertisement_table_pop > tbody  > tr').each(function ()
    {
        var advert_id = stoi($(this).find("td").eq(1).html());
        lst_replace_adverts.push(advert_id);
    });
    if (lst_replace_adverts.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select Advertisments for Replace.", "Error");
        return;
    }

    var is_selected = ($("input:radio[name ='selected']:checked").val() == "true");
    var lst_selected_scedules = [];
    if (is_selected) {
        $('#tbody_left > tr').each(function ()
        {
            var ckbx_select = $(this).find("input:checkbox");
            if (ckbx_select.is(":checked")) {
                var row_id = ckbx_select.val();
                var row_data = tableData[row_id].scheduleItem;
                $.each(row_data, function (i, item) {
                    if (item.advertCount > 0) {
                        $.each(item.scheduleid, function (j, id) {
                            lst_selected_scedules.push(id);
                        });
                    }
                });
            }
        });
        if (lst_selected_scedules.length == 0) {
            closeDialog();
            viewMessageToastTyped("Please select Schedules for Replace.", "Error");
            return;
        }
    }

    var i_workOrderId = workOrderId;
    var i_originalAdvert = $("#original_advertDropdown_pop").val();
    var e_mode = stoi($("input:radio[name ='mode']:checked").val());
    var json_data = '{"workOrderId":' + i_workOrderId + ',"lstReplaceChannelID":' + JSON.stringify(lst_channel_ids)
            + ',"originalAdvertID":' + i_originalAdvert + ',"lstReplaceAdvertID":' + JSON.stringify(lst_replace_adverts)
            + ',"startDate":"' + st_start_date + '","endDate":"' + st_end_date + '","replaceMode":' + e_mode
            + ',"isSelected":' + is_selected + ',"lstSelectedSchedules":' + JSON.stringify(lst_selected_scedules) + '}';
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.preAdvertReplaceUrl(),
        data: {
            replaceData: json_data
        },
        success: function (data)
        {
            closeDialog();
            var vTableData = '';
            vTableData += '<table class="table table-bordered" style="text-align:center;margin-top:10px;margin-bottom:10px;">';
            vTableData += '<thead style="text-align:center">';
            vTableData += '<tr><th>Channel</th>';
            vTableData += '<th>Duration(s)(Avl./Req.)</th><th>Replacements (Possible/Total)</th>';
            vTableData += '</tr></thead>';
            vTableData += '<tbody>';
            $.each(data, function (i, item) {
                vTableData += '<tr>';
                vTableData += '<td>' + item.channelName + '</td>';
                vTableData += '<td>' + item.availableDuration + ' / ' + item.requaredDuration + '</td>';
                vTableData += '<td>' + item.validReplaceCount + ' / ' + item.totalReplaceCount + '</td>';
                vTableData += '</tr>';
            });
            vTableData += '</thead>';
            vTableData += '</table>';
            viewConfirmToast(vTableData, 'Continue ?', replaceAdvertismets);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            advertReplaceLock = true;
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            return false;
        }
    });
}

function replaceAdvertismets() {
    showDialog();
    var lst_channel_ids = [];
    var selected = $('#cmb_rp_channel_select').find("option:selected");
    selected.each(function () {
        lst_channel_ids.push($(this).val());
    });
    if (lst_channel_ids.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select a Channel.", "Error");
        return;
    }

    var st_start_date = $('#rp_fromDate').val();
    var st_end_date = $('#rp_toDate').val();
    if (new Date(st_start_date).getTime() > new Date(st_end_date).getTime()) {
        hasError($("#rp_to-date"), true, 'Invalid End Date.');
        closeDialog();
        return;
    }

    var lst_replace_adverts = [];
    $('#replace_advertisement_table_pop > tbody  > tr').each(function ()
    {
        var advert_id = stoi($(this).find("td").eq(1).html());
        lst_replace_adverts.push(advert_id);
    });
    if (lst_replace_adverts.length == 0) {
        closeDialog();
        viewMessageToastTyped("Please select Advertisments for Replace.", "Error");
        return;
    }

    var is_selected = ($("input:radio[name ='selected']:checked").val() == "true");
    var lst_selected_scedules = [];
    if (is_selected) {
        $('#tbody_left > tr').each(function ()
        {
            var ckbx_select = $(this).find("input:checkbox");
            if (ckbx_select.is(":checked")) {
                var row_id = ckbx_select.val();
                var row_data = tableData[row_id].scheduleItem;
                $.each(row_data, function (i, item) {
                    if (item.advertCount > 0) {
                        $.each(item.scheduleid, function (j, id) {
                            lst_selected_scedules.push(id);
                        });
                    }
                });
            }
        });
        if (lst_selected_scedules.length == 0) {
            closeDialog();
            viewMessageToastTyped("Please select Schedules for Replace.", "Error");
            return;
        }
    }

    var i_workOrderId = workOrderId;
    var i_originalAdvert = $("#original_advertDropdown_pop").val();
    var e_mode = stoi($("input:radio[name ='mode']:checked").val());
    var json_data = '{"workOrderId":' + i_workOrderId + ',"lstReplaceChannelID":' + JSON.stringify(lst_channel_ids)
            + ',"originalAdvertID":' + i_originalAdvert + ',"lstReplaceAdvertID":' + JSON.stringify(lst_replace_adverts)
            + ',"startDate":"' + st_start_date + '","endDate":"' + st_end_date + '","replaceMode":' + e_mode
            + ',"isSelected":' + is_selected + ',"lstSelectedSchedules":' + JSON.stringify(lst_selected_scedules) + '}';
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.advertReplaceUrl(),
        data: {
            replaceData: json_data
        },
        success: function (data)
        {
            if (data != null){
                 createUpdateScheduleView();
            }
            closeDialog();
            viewMessageToastTyped("Successfully replace advertisement","Success");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            advertReplaceLock = true;
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            return false;
        }
    });
}


//==== Task 240 =========================================
var isCopyWOData = false;
function checkAllRows() {
    var chk = $('#ckbx_select_all').is(":checked");
    $('#tbody_left > tr').each(function ()
    {
        var ckbx = $(this).find("input:checkbox");
        ckbx.prop('checked', chk);
    });
}

function copyRows() {

    var today = getDate();
    var time = getTime();
    if (checkTableDirty())
        return;
    var lst_selected_scedules = [];
    $('#tbody_left > tr').each(function ()
    {
        var ckbx_select = $(this).find("input:checkbox");
        if (ckbx_select.is(":checked")) {
            var row_id = ckbx_select.val();
            lst_selected_scedules.push(row_id);
        }
    });
    if (lst_selected_scedules.length == 0) {
        viewMessageToastTyped("Please select a row.", "Error");
        return;
    }

    if (lst_selected_scedules.length == 1) {
        var id = lst_selected_scedules[0];
        var row_left = tableData[id];
        var row_data = tableData[id].scheduleItem;
        selectedAdvertId = row_left.advertid;
        selectedChannelId = row_left.channelid;
        $("#schedule_channelDropdown").val(row_left.channelid);
        changeTimeDropdownData(row_left.channelid);
        $("#programm_name").val(row_left.program);
        $("#schedule_advertDropdown").val(row_left.advertid);
        $("#language").val(row_left.adverLang);
        $("#duration").val(row_left.advertDuration);
        $("#revenue_line").val(row_left.revenueline);
        $("#month").val(row_left.month);
        $("#schedule_startTimeDropdown").val(row_left.schedulestarttime);
        $("#schedule_endTimeDropdown").val(row_left.scheduleendtime);
        var adevrtData = mapAdverts[$("#schedule_advertDropdown").val()];
        $('#advertTypeDropdown').val(adevrtData.type);
        if (row_left.schedulestarttime > getTime()){
            disableDataRowCells(startDate, getDate());
        }else{
            disableDataRowCells(startDate, addDays(getDate(), 1, 1));
        }
        for (var j in row_data)
        {
           var selectedTime = row_left.schedulestarttime.replace("30", "00");
           if (!($("#" + row_data[j].date + "_txt").is('[readonly]'))) {
                if (row_data[j].date === today &&  selectedTime > time){
                    $("#" + row_data[j].date + "_txt").val(row_data[j].advertCount);
                    if (!onCellEditInsert(row_data[j].date + "_txt")){
                        return;
                    }
                }else{
                    $("#" + row_data[j].date + "_txt").val(row_data[j].advertCount);
                    if (!onCellEditInsert(row_data[j].date + "_txt")){
                        return;
                    }
                }
            }
        }

    }
    else if (lst_selected_scedules.length > 1) {
        isCopyWOData = false;
        $('#cmb_cr_advertisement').empty();
        $('#cmb_cr_advertisement').append($('<option>', {value: -1, text: "Default"}));
        $.each(lstAdverts, function (i, item) {
            $('#cmb_cr_advertisement').append($('<option>', {value: item.advertId, text: item.advertName}));
        });
        $('#cmb_cr_start_time').empty();
        $('#cmb_cr_end_time').empty();
        $('#cmb_cr_start_time').append($('<option>', {value: -1, text: "Default"}));
        $('#cmb_cr_end_time').append($('<option>', {value: -1, text: "Default"}));
        for (var i = 0; i < 48; i++) {
            $('#cmb_cr_start_time').append($('<option>', {value: i, text: lst_PatternFillTimeBelts[i]}));
            $('#cmb_cr_end_time').append($('<option>', {value: i + 1, text: lst_PatternFillTimeBelts[i + 1]}));
        }

        $('#copyRowsModal').modal('show');
    }
}

function preCopyRowsSave() {
    if (isCopyWOData) {
        copyRowsWODataSave();
        return;
    }
    showDialog();
    var lst_selected_scedules = [];
    $('#tbody_left > tr').each(function ()
    {
        var ckbx_select = $(this).find("input:checkbox");
        if (ckbx_select.is(":checked")) {
            var row_id = ckbx_select.val();
            var row_data = tableData[row_id].scheduleItem;
            $.each(row_data, function (i, item) {
                if (item.advertCount > 0) {
                    $.each(item.scheduleid, function (j, id) {
                        lst_selected_scedules.push(id);
                    });
                }
            });
        }
    });
    if (lst_selected_scedules.length == 0) {
        viewMessageToastTyped("Please select a row.", "Error");
        return;
    }

    var i_channel_id = $('#cmb_cr_channel_select').val();
    var i_workorder_id = workOrderId;
    var i_advert_id = $('#cmb_cr_advertisement').val();
    var st_month = $('#cmb_cr_revenue_month').val();
    var st_revenue_line = $('#txt_cr_revenue_line').val();
    var st_start_time = "";
    var st_end_time = "";
    var i_start_time = stoi($('#cmb_cr_start_time').val());
    var i_end_time = stoi($('#cmb_cr_end_time').val());
    if (i_start_time != -1 && i_end_time != -1) {
        if (i_start_time >= i_end_time) {
            closeDialog();
            viewMessageToastTyped("Invalid end time.", "Error");
            return;
        }
        st_start_time = lst_PatternFillTimeBelts[i_start_time];
        st_end_time = lst_PatternFillTimeBelts[i_end_time];
        if (st_end_time == '24:00')
            st_end_time = '23:59';
    }

    var json_data = '{"workOrderId":' + i_workorder_id + ',"channelID":' + i_channel_id + ',"advertID":' + i_advert_id
            + ',"startTime":"' + st_start_time + '","endTime":"' + st_end_time
            + '","revenueMonth":"' + st_month + '","revenueLine":"' + st_revenue_line + '","lstSelectedSchedules":' + JSON.stringify(lst_selected_scedules) + '}';

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.scheduler.preCopyScheduleRowsUrl(),
        data: {
            rowsData: json_data
        },
        success: function (data)
        {
            closeDialog();
            var vTableData = '';
            vTableData += '<table class="table table-bordered" style="text-align:center;margin-top:10px;margin-bottom:10px;">';
            vTableData += '<thead style="text-align:center">';
            vTableData += '<tr><th>Channel</th>';
            vTableData += '<th>Duration(s)(Ava./Total)</th><th>New Spots (Possible/Total)</th>';
            vTableData += '</tr></thead>';
            vTableData += '<tbody>';
            $.each(data, function (i, item) {
                vTableData += '<tr>';
                vTableData += '<td>' + item.channelName + '</td>';
                vTableData += '<td>' + item.availableScheduleTime + ' / ' + item.initialScheduleTime + '</td>';
                vTableData += '<td>' + item.validSlotCount + ' / ' + item.newSlotCount + '</td>';
                vTableData += '</tr>';
            });
            vTableData += '</thead>';
            vTableData += '</table>';
            viewConfirmToast(vTableData, 'Continue ?', copyRowsSave);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function copyRowsSave() {
    showDialog();
    var lst_selected_scedules = [];
    $('#tbody_left > tr').each(function ()
    {
        var ckbx_select = $(this).find("input:checkbox");
        if (ckbx_select.is(":checked")) {
            var row_id = ckbx_select.val();
            var row_data = tableData[row_id].scheduleItem;
            $.each(row_data, function (i, item) {
                if (item.advertCount > 0) {
                    $.each(item.scheduleid, function (j, id) {
                        lst_selected_scedules.push(id);
                    });
                }
            });
        }
    });
    if (lst_selected_scedules.length == 0) {
        viewMessageToastTyped("Please select a row.", "Error");
        return;
    }

    var i_channel_id = $('#cmb_cr_channel_select').val();
    var i_workorder_id = workOrderId;
    var i_advert_id = $('#cmb_cr_advertisement').val();
    var st_month = $('#cmb_cr_revenue_month').val();
    var st_revenue_line = $('#txt_cr_revenue_line').val();
    var st_start_time = "";
    var st_end_time = "";
    var i_start_time = stoi($('#cmb_cr_start_time').val());
    var i_end_time = stoi($('#cmb_cr_end_time').val());
    if (i_start_time != -1 && i_end_time != -1) {
        if (i_start_time >= i_end_time) {
            closeDialog();
            viewMessageToastTyped("Invalid end time.", "Error");
            return;
        }
        st_start_time = lst_PatternFillTimeBelts[i_start_time];
        st_end_time = lst_PatternFillTimeBelts[i_end_time];
        if (st_end_time == '24:00')
            st_end_time = '23:59';
    }

    var json_data = '{"workOrderId":' + i_workorder_id + ',"channelID":' + i_channel_id + ',"advertID":' + i_advert_id
            + ',"startTime":"' + st_start_time + '","endTime":"' + st_end_time
            + '","revenueMonth":"' + st_month + '","revenueLine":"' + st_revenue_line + '","lstSelectedSchedules":' + JSON.stringify(lst_selected_scedules) + '}';

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.scheduler.copyScheduleRowsUrl(),
        data: {
            rowsData: json_data
        },
        success: function (data)
        {
            if (data != null){
                buildUpdateScheduleView(data);
                viewMessageToast();
            }
            setDataToAddRow();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function copyWOData() {
    if (checkTableDirty())
        return;
    var lst_selected_rows = [];
    $('#tbody_left > tr').each(function ()
    {
        var ckbx_select = $(this).find("input:checkbox");
        if (ckbx_select.is(":checked")) {
            var row_id = ckbx_select.val();
            lst_selected_rows.push(row_id);
        }
    });
    if (lst_selected_rows.length == 0) {
        viewMessageToastTyped("Please select a row.", "Error");
        return;
    }
    else if (lst_selected_rows.length == 1) {
        var id = lst_selected_rows[0];
        var row_left = tableData[id];
        var row_data = tableData[id].scheduleItem;
        $("#schedule_channelDropdown").val(row_left.channelid);
        changeTimeDropdownData(row_left.channelid);
        $("#programm_name").val(row_left.program);
        $("#schedule_advertDropdown").val(row_left.advertid);
        $("#language").val(row_left.adverLang);
        $("#duration").val(row_left.advertDuration);
        $("#revenue_line").val(row_left.revenueline);
        $("#month").val(row_left.month);
        $("#schedule_startTimeDropdown").val(row_left.schedulestarttime);
        $("#schedule_endTimeDropdown").val(row_left.scheduleendtime);
        var adevrtData = mapAdverts[row_left.advertid];
        $('#advertTypeDropdown').val(adevrtData.type);
        if (row_left.schedulestarttime > getTime())
            disableDataRowCells(startDate, getDate());
        else
            disableDataRowCells(startDate, addDays(getDate(), 1, 1));
    }
    else if (lst_selected_rows.length > 1) {
        isCopyWOData = true;
        $('#cmb_cr_advertisement').empty();
        $('#cmb_cr_advertisement').append($('<option>', {value: -1, text: "Default"}));
        $.each(lstAdverts, function (i, item) {
            $('#cmb_cr_advertisement').append($('<option>', {value: item.advertId, text: item.advertName}));
        });
        $('#cmb_cr_start_time').empty();
        $('#cmb_cr_end_time').empty();
        $('#cmb_cr_start_time').append($('<option>', {value: -1, text: "Default"}));
        $('#cmb_cr_end_time').append($('<option>', {value: -1, text: "Default"}));
        for (var i = 0; i < 48; i++) {
            $('#cmb_cr_start_time').append($('<option>', {value: i, text: lst_PatternFillTimeBelts[i]}));
            $('#cmb_cr_end_time').append($('<option>', {value: i + 1, text: lst_PatternFillTimeBelts[i + 1]}));
        }
        $('#copyRowsModal').modal('show');
    }
}

function copyRowsWODataSave() {
    showDialog();
    var lst_selected_rows = [];
    $('#tbody_left > tr').each(function ()
    {
        var ckbx_select = $(this).find("input:checkbox");
        if (ckbx_select.is(":checked")) {
            var row_id = ckbx_select.val();
            lst_selected_rows.push(row_id);
        }
    });
    if (lst_selected_rows.length == 0) {
        viewMessageToastTyped("Please select a row.", "Error");
        return;
    }

    var i_channel_id = $('#cmb_cr_channel_select').val();
    var i_advert_id = $('#cmb_cr_advertisement').val();
    var st_month = $('#cmb_cr_revenue_month').val();
    var st_revenue_line = $('#txt_cr_revenue_line').val();
    var st_start_time = "";
    var st_end_time = "";
    var i_start_time = stoi($('#cmb_cr_start_time').val());
    var i_end_time = stoi($('#cmb_cr_end_time').val());
    if (i_start_time != -1 && i_end_time != -1) {
        if (i_start_time >= i_end_time) {
            closeDialog();
            viewMessageToastTyped("Invalid end time.", "Error");
            return;
        }
        st_start_time = lst_PatternFillTimeBelts[i_start_time];
        st_end_time = lst_PatternFillTimeBelts[i_end_time];
        if (st_end_time == '24:00')
            st_end_time = '23:59';
    }

    var lst_schedules = [];
    $.each(lst_selected_rows, function (i, row_id) {
        var row_data = tableData[row_id];
        var schedule_data = {workOrderId: workOrderId, channelId: row_data.channelid, advertId: row_data.advertid
            , startTime: row_data.schedulestarttime, endTime: row_data.scheduleendtime, schedulDate: endDate
            , advertCount: 1, programm: row_data.program, revenueLine: row_data.revenueline, month: row_data.month
        };
        if (i_channel_id != -1)
            schedule_data.channelId = i_channel_id;
        if (i_advert_id != -1)
            schedule_data.advertId = i_advert_id;
        if (st_month != -1)
            schedule_data.month = st_month;
        if (st_revenue_line != "")
            schedule_data.revenueLine = st_revenue_line;
        if (st_start_time != "")
            schedule_data.startTime = st_start_time + ":00";
        if (st_end_time != "")
            schedule_data.endTime = st_end_time + ":00";
        lst_schedules.push(schedule_data);
    });
    selected_data = JSON.stringify(lst_schedules);
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.scheduler.scheduleSaveUrl(),
        data: {
            scheduleData: selected_data
        },
        success: function (data) {
            if (data != null) {
                buildUpdateScheduleView(data);
            }
            viewMessageToast();
            setDataToAddRow();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getGrandTotal() {
    var grandTotal = 0;
    for (var key in mapEditBoxDataCurrent) {
        grandTotal += mapEditBoxDataCurrent[key];
    }
    grandTotal += iInsertCount;
    $('#grand-total').html(grandTotal);
}

function columnCopy() {

    var toIdNumber = $('#to-date-drop').val();
    var fromIdNumber = $('#from-date-drop').val();
    var rowCount = Object.keys(mapRowCount).length;

    if (toIdNumber != -1 & fromIdNumber != -1) {

        for (i = 0; i < rowCount; i++) {
            var toId = 'cell_' + toIdNumber;
            var fromId = 'cell_' + fromIdNumber;
            $('#' + toId).val($('#' + fromId).val());
            onCellEdit(toId);
            toIdNumber = stoi(toIdNumber) + 1000;
            fromIdNumber = stoi(fromIdNumber) + 1000;
        }

    } else {
        alert("Please select the from and to date");
    }
}
