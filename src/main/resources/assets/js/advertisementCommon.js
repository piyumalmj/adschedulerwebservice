$(document).ready(function () {
    $('#logo_list_table').hide();
    setAdvertisementDetails();
});

var logos_list = [];

function addAdvertisement() 
{
    var advert = document.getElementById("advertDropdown_pop");
    var advertId = advert.options[advert.selectedIndex].id;
    var advertDuration = advert.options[advert.selectedIndex].innerHTML;
    var advertName = (advert.options[advert.selectedIndex].value).split("_");

    var advertTable_p = document.getElementById("advertisement_table_pop");
    var rowLength = advertTable_p.rows.length;
    for (i = 1; i < rowLength; i++)
    {
        var oCells = advertTable_p.rows.item(i).cells;
        if (oCells.item(0).innerHTML == advertId) {
            return;
        }
    }

    if (advertId != -9) {
        var advertTable = document.getElementById("advertisement_table_pop");
        var rowCount = advertTable.rows.length;
        var row = advertTable.insertRow();

        row.id = rowCount + "row_pop";
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.innerHTML = advertId;
        cell2.innerHTML = advertDuration;
        cell3.innerHTML = advertName[0];
        cell4.innerHTML = getAdvertType(advertName[1]);
        cell5.innerHTML = '<button type="submit" id="' + rowCount + '" onclick="deleteAdvert((this.id))" class="btn btn-default sche_advert_delete">Delete</button>';

        advert.selectedIndex = 0;
    }
}

function saveAdvertisement()
{
    advertTable_p = document.getElementById("advertisement_table_pop");
    rowLength = advertTable_p.rows.length;
    stAdvertListTbl = '';
    for (i = 1; i < rowLength; i++)
    {
        oCells = advertTable_p.rows.item(i).cells;
        cellLength = oCells.length;
        logos_list.push(stoi(oCells.item(0).innerHTML));
        
        stAdvertListTbl += '<tr>';
        stAdvertListTbl += '<td>' + oCells.item(0).innerHTML + '</td>';
        stAdvertListTbl += '<td>' + oCells.item(1).innerHTML + '</td>';
        stAdvertListTbl += '<td>' + oCells.item(2).innerHTML + '</td>';
        stAdvertListTbl += '</tr>';
        $('#advert_main_table tbody').html(stAdvertListTbl);
    }
    $('#addAdvertisementModal').modal('toggle');
}

function setAdvertisementDetails()//
{
    showDialog();
    var advertDrop = '<select id="advertDropdown_pop" name="channelName" class="form-control"><option id="-9" value="30">Select Advertsement</option>';
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementFilterListUrl(),
        data: {
            fiterData: "-111,LOGO,-111",
        },
        success: function (data)
        {
            closeDialog();
            for (var i in data)
            {
                if(data[i].advertpath == "LOGO_CONTAINER") continue;
                advertDrop += '<option id=' + data[i].advertid + ' value="' + data[i].duration + '_' + data[i].adverttype + '">' + data[i].advertname + '</option>';
            }
            advertDrop += '</select>';
            $('#advert_list_pop_div').html(advertDrop);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    })
}

function deleteAdvert(id)//
{
    var ret = confirm("Do you want to delete this Advertisement?");
    if (!ret)
        return;
    var row = document.getElementById(id + "row_pop");
    row.parentNode.removeChild(row);
}

function setAdvertListTable(){ ///
    
}

function saveMultipleLogos(){
    showDialog();
    stLogoIds = '';
    $.each(logos_list, function (index, item) {
        stLogoIds += item + ',';
    }); 
    if(stLogoIds.length > 0)
        stLogoIds = stLogoIds.substring(0, stLogoIds.length-1);
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.logoContainer.insertLogoListtoContainerUrl(),
        data: {
            container_id: iAdvertID,
            logo_ids: stLogoIds,
        },
        success: function (data)
        {
            closeDialog();            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}