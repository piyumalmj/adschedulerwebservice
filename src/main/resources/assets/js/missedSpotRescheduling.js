var dataArray = '';
var count = 0;
var advertList = [];
var clientList = [];
var advertSelectedValue = -999;
var clientSelectedValue = [-999];
$(function () {
    $('#form-date').datetimepicker({
        pickTime: false,
        endDate: new Date()
    });
});
$(function () {
    $('#to-date').datetimepicker({
        pickTime: false,
        endDate: new Date()
    });
});
$(function () {
    $('#form-date-pop').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(document).ready(function () {

    document.getElementById("fromDate").value = addDays(new Date(), -1);
    document.getElementById("toDate").value = addDays(new Date(), -1);
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += '<span class="input-group-addon lable_forms_trans">Channels</span> <select id="channelDropdown" style="width: 100px; name="channelName" class="selectpicker" multiple>';
//            trHTML += ' <option VALUE="-111" selected>--All--</option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].channelid + '">' + data[i].channelname + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span id="snp_msg" class="text-danger"></span>';
            trHTML += '<span class="help-block"></span>';

            $('#channelList').html(trHTML);

            $('#channelDropdown').selectpicker({
                size: 15
            });
            loadingClose();
            setMissedSpotTable();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }

    });

    loadClients();
    workOrderDetailsCreator();
});

function setMissedSpotTable() {

    console.time("setMissedSpotTable");
    loadingOpen();
    var _fromDate = document.getElementById("fromDate").value;
    var _toDate = document.getElementById("toDate").value;
    var selectedHour = document.getElementById("hourList").value;

    var fiter_channel_ids = '';
    var fiter_channel = $('#channelDropdown').find("option:selected");
    fiter_channel.each(function () {
        fiter_channel_ids += "_" + $(this).val();
    });

    var fiter_client_ids = [];
    var fiter_client = $('#clientDropdown').find("option:selected");
    fiter_client.each(function () {
        clientSelectedValue = [];
        fiter_client_ids.push(parseInt($(this).val()));
        clientSelectedValue.push(parseInt($(this).val()));
    });

    var _statusDrop = document.getElementById('status_list');
    var _statusId = _statusDrop.options[_statusDrop.selectedIndex].value;

    var advertID = $('#productDropdown').val();
    if (advertID === null) {
        advertID = -999;
    }
    advertSelectedValue = advertID;

    if (!isNaN(_fromDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }
    if (!isNaN(_toDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }

    if(_fromDate>_toDate){
        viewMessageToastTyped("To date must be greater than from date", "Warning");
        return;
    }

    if(fiter_channel.length===0){
//        viewMessageToastTyped("Please select channels", "Warning");
//        return;
      fiter_channel_ids += "_-111" ;
    }

    var data = {
        fromDate: _fromDate,
        toDate: _toDate,
        channelIDs: fiter_channel_ids,
        advertID: advertID,
        clientID: fiter_client_ids,
        hour:selectedHour
    };

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        async: true,
        contentType: "application/json",
        url: URI.scheduler.getMissedSpotUrl(),
        cache: false,
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data)
        {
            //console.time("Come");
            count = 0;
            var newRowCheker = 0;
            var trHTML = '';
            trHTML += ' <table id="missed_spot_table" class="table table-bordered table-workorder">';
            trHTML += '<thead class="tb_align_center"><tr><th class="col-md-2">Original time rule</th><th class="col-md-2">WO Name/ID</th><th class="col-md-2">Client</th><th class="col-md-1">Cut ID</th><th class="col-md-2">Product</th><th class="col-md-1">Type</th><th class="col-md-1">channel</th><th class="col-md-1">Prio</th><th class="col-md-2">Status</th><th class="col-md-1"><input type="checkbox" id="all_select_check" name="checkbox" onclick="selectAllAdvert()"></th><th class="col-md-1">Actual time belt</th><th class="col-md-1">Date</th><th class="col-md-1">Schedule End Date </th></tr></thead><tbody>';
            for (var i in data)
            {
                newRowCheker = 0;
                var rowData =data[i].itemList;
                var tableBodyHTML = '';
                var originalTime = ((data[i].timeBelt)).split("-");
                var timeBelt = '<tr class="tb_align_center"><td>' + RemoveSec(originalTime[0]) + '-' + RemoveSec(originalTime[1]) + '</td>';
                for (var j in rowData) {
                var tableRowdata = data[i].itemList[j];
                    var actualTime = (tableRowdata.actualTimeBelt).split("-");
                    advertList[tableRowdata.advertId] = tableRowdata.advertNme;
                    clientList[tableRowdata.clientId] = tableRowdata.client;
                    if (_statusId == 'all') {
                        if (j == 0) {
                            tableBodyHTML += '<td>' + tableRowdata.workOrderName + ' / ' + tableRowdata.workOrderId + '</td><td><div>' + tableRowdata.client + '</div></td><td>' + tableRowdata.advertId + '</td><td ><a href="#" id="' + tableRowdata.scheduleId + '~' + tableRowdata.channelId + '~' + tableRowdata.advertNme + '~' + tableRowdata.schedulDate + '~' + tableRowdata.actualTimeBelt + '~' + tableRowdata.workOrderId + '~' + tableRowdata.advertId + '~' + tableRowdata.schedulStartTime + '~' + tableRowdata.schedulEndTime + '~' + tableRowdata.workOrderName + '  " data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + tableRowdata.advertNme + '</a></td><td>' + getAdvertType(tableRowdata.advertType) + '</td><td>' + tableRowdata.channelName + '</td><td>' + tableRowdata.priority + '</td><td>' + tableRowdata.status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + tableRowdata.scheduleId + '~' + tableRowdata.workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + tableRowdata.schedulDate + '</td><td>' + tableRowdata.workOrderEndDate + '</td></tr>';
                            count++;
                        } else {
                            tableBodyHTML += '<td></td><td>' + tableRowdata.workOrderName + ' / ' + tableRowdata.workOrderId + '</td><td><div>' + tableRowdata.client + '</div></td><td>' + tableRowdata.advertId + '</td><td><a href="#" id="' + tableRowdata.scheduleId + '~' + tableRowdata.channelId + '~' + tableRowdata.advertNme + '~' + tableRowdata.schedulDate + '~' + tableRowdata.actualTimeBelt + '~' + tableRowdata.workOrderId + '~' + tableRowdata.advertId + '~' + tableRowdata.schedulStartTime + '~' + tableRowdata.schedulEndTime + '~' + tableRowdata.workOrderName + ' " data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + tableRowdata.advertNme + '</a></td><td>' + getAdvertType(tableRowdata.advertType) + '</td><td>' + tableRowdata.channelName + '</td><td>' + tableRowdata.priority + '</td><td>' + tableRowdata.status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + tableRowdata.scheduleId + '~' + tableRowdata.workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + tableRowdata.schedulDate + '</td><td>' + tableRowdata.workOrderEndDate + '</td></tr>';
                            count++;
                        }
                    } else {
                        if (data[i].itemList[j].status == _statusId) {
                            if (newRowCheker == 0) {
                                tableBodyHTML += '<td>' + tableRowdata.workOrderName + ' / ' + tableRowdata.workOrderId + '</td><td><div>' + tableRowdata.client + '</div></td><td>' + tableRowdata.advertId + '</td><td ><a href="#" id="' + tableRowdata.scheduleId + '~' + tableRowdata.channelId + '~' + tableRowdata.advertNme + '~' + tableRowdata.schedulDate + '~' + tableRowdata.actualTimeBelt + '~' + tableRowdata.workOrderId + '~' + tableRowdata.advertId + '~' + tableRowdata.schedulStartTime + '~' + tableRowdata.schedulEndTime + '~' + tableRowdata.workOrderName + '" data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + tableRowdata.advertNme + '</a></td><td>' + getAdvertType(tableRowdata.advertType) + '</td><td>' + tableRowdata.channelName + '</td><td>' + tableRowdata.priority + '</td><td>' + tableRowdata.status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + tableRowdata.scheduleId + '~' + tableRowdata.workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + tableRowdata.schedulDate + '</td><td>' + tableRowdata.workOrderEndDate + '</td></tr>';
                                count++;
                            } else {
                                tableBodyHTML += '<td></td><td>' + tableRowdata.workOrderName + ' / ' +tableRowdata.workOrderId + '</td><td><div>' + tableRowdata.client + '</div></td><td>' + tableRowdata.advertId + '</td><td><a href="#" id="' + tableRowdata.scheduleId + '~' + tableRowdata.channelId + '~' + tableRowdata.advertNme + '~' + tableRowdata.schedulDate + '~' + tableRowdata.actualTimeBelt + '~' + tableRowdata.workOrderId + '~' + tableRowdata.advertId + '~' + tableRowdata.schedulStartTime + '~' + tableRowdata.schedulEndTime + '~' + tableRowdata.workOrderName + '" data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + tableRowdata.advertNme + '</a></td><td>' + getAdvertType(tableRowdata.advertType) + '</td><td>' + tableRowdata.channelName + '</td><td>' + tableRowdata.priority + '</td><td>' + tableRowdata.status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + tableRowdata.scheduleId + '~' + tableRowdata.workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + tableRowdata.schedulDate + '</td><td>' + tableRowdata.workOrderEndDate + '</td></tr>';
                                count++;
                            }
                            newRowCheker++;
                        }
                    }
                }
                if (tableBodyHTML != "") {
                    trHTML += timeBelt + tableBodyHTML;
                }
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#missed_spot_div').html(trHTML);
            setAdvertDrop();
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }

    });
    console.timeEnd("setMissedSpotTable");
    setTimeDropDown();
}

function setMissedSpotTable_v1() {

    console.time("setMissedSpotTable");
    loadingOpen();
    var _fromDate = document.getElementById("fromDate").value;
    var _toDate = document.getElementById("toDate").value;
    var selectedHour = document.getElementById("hourList").value;

    var fiter_channel_ids = '';
    var fiter_channel = $('#channelDropdown').find("option:selected");
    fiter_channel.each(function () {
        fiter_channel_ids += "_" + $(this).val();
    });

    var fiter_client_ids = [];
    var fiter_client = $('#clientDropdown').find("option:selected");
    fiter_client.each(function () {
        clientSelectedValue = [];
        fiter_client_ids.push(parseInt($(this).val()));
        clientSelectedValue.push(parseInt($(this).val()));
    });

    var _statusDrop = document.getElementById('status_list');
    var _statusId = _statusDrop.options[_statusDrop.selectedIndex].value;

    var advertID = $('#productDropdown').val();
    //if (advertID === null) {
        advertID = -999;
 //   }
    advertSelectedValue = advertID;

    if (!isNaN(_fromDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }
    if (!isNaN(_toDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }

    if(_fromDate>_toDate){
        viewMessageToastTyped("To date must be greater than from date", "Warning");
        return;
    }

    if(fiter_channel.length===0){
        viewMessageToastTyped("Please select channels", "Warning");
        return;
    }

    var data = {
        fromDate: _fromDate,
        toDate: _toDate,
        channelIDs: fiter_channel_ids,
        advertID: advertID,
        clientID: fiter_client_ids,
        hour:selectedHour
    };

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        async: true,
        contentType: "application/json",
        url: URI.scheduler.getMissedSpotUrl(),
        cache: false,
        data: JSON.stringify(data),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data)
        {
            console.time("Come");
            count = 0;
            var newRowCheker = 0;
            var trHTML = '';
            trHTML += ' <table id="missed_spot_table" class="table table-bordered table-workorder">';
            trHTML += '<thead class="tb_align_center"><tr><th class="col-md-2">Original time rule</th><th class="col-md-2">WO Name/ID</th><th class="col-md-2">Client</th><th class="col-md-1">Cut ID</th><th class="col-md-2">Product</th><th class="col-md-1">Type</th><th class="col-md-1">channel</th><th class="col-md-1">Prio</th><th class="col-md-2">Status</th><th class="col-md-1"><input type="checkbox" id="all_select_check" name="checkbox" onclick="selectAllAdvert()"></th><th class="col-md-1">Actual time belt</th><th class="col-md-1">Date</th><th class="col-md-1">Schedule End Date </th></tr></thead><tbody>';
            for (var i in data)
            {
                newRowCheker = 0;
                var tableBodyHTML = '';
                var originalTime = (data[i].timeBelt).split("-");
                var timeBelt = '<tr class="tb_align_center"><td>' + RemoveSec(originalTime[0]) + '-' + RemoveSec(originalTime[1]) + '</td>';
                for (var j in data[i].itemList) {
                    var actualTime = (data[i].itemList[j].actualTimeBelt).split("-");
                    advertList[data[i].itemList[j].advertId] = data[i].itemList[j].advertNme;
                    clientList[data[i].itemList[j].clientId] = data[i].itemList[j].client;
                    if (_statusId == 'all') {
                        if (j == 0) {
                            tableBodyHTML += '<td>' + data[i].itemList[j].workOrderName + ' / ' + data[i].itemList[j].workOrderId + '</td><td><div>' + data[i].itemList[j].client + '</div></td><td>' + data[i].itemList[j].advertId + '</td><td ><a href="#" id="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].channelId + '~' + data[i].itemList[j].advertNme + '~' + data[i].itemList[j].schedulDate + '~' + data[i].itemList[j].actualTimeBelt + '~' + data[i].itemList[j].workOrderId + '~' + data[i].itemList[j].advertId + '~' + data[i].itemList[j].schedulStartTime + '~' + data[i].itemList[j].schedulEndTime + '~' + data[i].itemList[j].workOrderName + '  " data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + data[i].itemList[j].advertNme + '</a></td><td>' + getAdvertType(data[i].itemList[j].advertType) + '</td><td>' + data[i].itemList[j].channelName + '</td><td>' + data[i].itemList[j].priority + '</td><td>' + data[i].itemList[j].status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + data[i].itemList[j].schedulDate + '</td><td>' + data[i].itemList[j].workOrderEndDate + '</td></tr>';
                            count++;
                        } else {
                            tableBodyHTML += '<td></td><td>' + data[i].itemList[j].workOrderName + ' / ' + data[i].itemList[j].workOrderId + '</td><td><div>' + data[i].itemList[j].client + '</div></td><td>' + data[i].itemList[j].advertId + '</td><td><a href="#" id="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].channelId + '~' + data[i].itemList[j].advertNme + '~' + data[i].itemList[j].schedulDate + '~' + data[i].itemList[j].actualTimeBelt + '~' + data[i].itemList[j].workOrderId + '~' + data[i].itemList[j].advertId + '~' + data[i].itemList[j].schedulStartTime + '~' + data[i].itemList[j].schedulEndTime + '~' + data[i].itemList[j].workOrderName + ' " data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + data[i].itemList[j].advertNme + '</a></td><td>' + getAdvertType(data[i].itemList[j].advertType) + '</td><td>' + data[i].itemList[j].channelName + '</td><td>' + data[i].itemList[j].priority + '</td><td>' + data[i].itemList[j].status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + data[i].itemList[j].schedulDate + '</td><td>' + data[i].itemList[j].workOrderEndDate + '</td></tr>';
                            count++;
                        }
                    } else {
                        if (data[i].itemList[j].status == _statusId) {
                            if (newRowCheker == 0) {
                                tableBodyHTML += '<td>' + data[i].itemList[j].workOrderName + ' / ' + data[i].itemList[j].workOrderId + '</td><td><div>' + data[i].itemList[j].client + '</div></td><td>' + data[i].itemList[j].advertId + '</td><td ><a href="#" id="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].channelId + '~' + data[i].itemList[j].advertNme + '~' + data[i].itemList[j].schedulDate + '~' + data[i].itemList[j].actualTimeBelt + '~' + data[i].itemList[j].workOrderId + '~' + data[i].itemList[j].advertId + '~' + data[i].itemList[j].schedulStartTime + '~' + data[i].itemList[j].schedulEndTime + '~' + data[i].itemList[j].workOrderName + '" data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + data[i].itemList[j].advertNme + '</a></td><td>' + getAdvertType(data[i].itemList[j].advertType) + '</td><td>' + data[i].itemList[j].channelName + '</td><td>' + data[i].itemList[j].priority + '</td><td>' + data[i].itemList[j].status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + data[i].itemList[j].schedulDate + '</td><td>' + data[i].itemList[j].workOrderEndDate + '</td></tr>';
                                count++;
                            } else {
                                tableBodyHTML += '<td></td><td>' + data[i].itemList[j].workOrderName + ' / ' + data[i].itemList[j].workOrderId + '</td><td><div>' + data[i].itemList[j].client + '</div></td><td>' + data[i].itemList[j].advertId + '</td><td><a href="#" id="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].channelId + '~' + data[i].itemList[j].advertNme + '~' + data[i].itemList[j].schedulDate + '~' + data[i].itemList[j].actualTimeBelt + '~' + data[i].itemList[j].workOrderId + '~' + data[i].itemList[j].advertId + '~' + data[i].itemList[j].schedulStartTime + '~' + data[i].itemList[j].schedulEndTime + '~' + data[i].itemList[j].workOrderName + '" data-toggle="modal" data-target="#missedSpot_pop" onclick="getScheduleId(this.id)">' + data[i].itemList[j].advertNme + '</a></td><td>' + getAdvertType(data[i].itemList[j].advertType) + '</td><td>' + data[i].itemList[j].channelName + '</td><td>' + data[i].itemList[j].priority + '</td><td>' + data[i].itemList[j].status + '</td><td><input type="checkbox" id="' + count + '_check" name="checkbox" value="' + data[i].itemList[j].scheduleId + '~' + data[i].itemList[j].workOrderId + '"></td><td>' + RemoveSec(actualTime[0]) + '-' + RemoveSec(actualTime[1]) + '</td><td>' + data[i].itemList[j].schedulDate + '</td><td>' + data[i].itemList[j].workOrderEndDate + '</td></tr>';
                                count++;
                            }
                            newRowCheker++;
                        }
                    }
                }
                if (tableBodyHTML != "") {
                    trHTML += timeBelt + tableBodyHTML;
                }
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#missed_spot_div').html(trHTML);
            console.timeEnd("Come");
//            setAdvertDrop();
//            workOrderDetailsCreator();

            loadingClose();
 //           setTimeDropDown();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }

    });
    console.timeEnd("setMissedSpotTable");
}

function setAdvertDrop() {
    console.time("setAdvertDrop");
    loadingOpen();
    var advertDrop = '<span class="input-group-addon lable_forms_trans">Product</span><select id="productDropdown" name="advertName" class="form-control"><option VALUE="-999"> --All--</option>';
    for (var key in advertList) {
        advertDrop += '<option VALUE="' + key + '">' + advertList[key] + '</option>';
    }
    advertDrop += '</select>';
    $('#productList').html(advertDrop);
    $('#productDropdown').val(advertSelectedValue);
    //setClientDrop();
    loadingClose();
    console.timeEnd("setAdvertDrop");
}

function setClientDrop() {
    var clientDrop = '<span class="input-group-addon lable_forms_trans">Client</span><select id="clientDropdown" name="clientName" class="selectpicker" multiple><option VALUE="-999" selected=""> --All--</option>';
    for (var key in clientList) {
        clientDrop += '<option VALUE="' + key + '">' + clientList[key] + '</option>';
    }
    clientDrop += '</select>';
    $('#clientList').html(clientDrop);

     for (var value in clientSelectedValue) {
     $("#clientDropdown").selectpicker('val', '"' + value + '"');
     }

//       $('#clientDropdown').selectpicker({
//             size: 15
//         });

}

function getDays() {
    var result = new Date();

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    return yyyy + '-' + mm + '-' + dd;
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    date = yyyy + '/' + mm + '/' + dd;

    return yyyy + '-' + mm + '-' + dd;
}

function setTimeDropDown() {
    console.time("setTimeDropDown");
    var fromTimeDrop = '<span class="input-group-addon lable_forms_trans">From</span><select id="fromDropdown" name="frome" class="form-control">';
    var toTimeDrop = '<span class="input-group-addon lable_forms_trans">To</span><select id="toDropdown" name="to" class="form-control">';
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.scheduler.timeSlotListUrl(),
        success: function (data)
        {
            for (var i in data)
            {
                fromTimeDrop += '<option value="' + data[i].starttime + '">' + RemoveSec(data[i].starttime) + '</option>';
                toTimeDrop += '<option value="' + data[i].starttime + '">' + RemoveSec(data[i].starttime) + '</option>';
            }
            fromTimeDrop += '</select><span class="help-block"></span>';
            toTimeDrop += '</select><span class="help-block"></span>';
            $('#frome_time').html(fromTimeDrop);
            $('#to_time').html(toTimeDrop);
            loadingClose();
            console.timeEnd("setTimeDropDown");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getScheduleId(Id) {

    var workOrderID = 0;
    for (var i = 0; i < count; i++) {
        var id = i + '_check';
        if (document.getElementById(id).checked) {
            var valueData = document.getElementById(id).value.split("~");
            if (workOrderID == 0) {
                workOrderID = valueData[1];
            } else if (workOrderID != valueData[1]) {
                viewMessageToastTyped("You select different work orders spot.", "Warning");
                $('#missedSpot_pop').modal('toggle');
                return;
            }

        }
    }

    dataArray = Id.split("~");
    var channelId = dataArray[1];
    var adverName = dataArray[2];
    var date = dataArray[3];
    var startTime = dataArray[7];
    var endTime = dataArray[8];
    var workOrderName = dataArray[9];
    var workOrderId = dataArray[5];
    var scheduleData = '<label class="control-label col-sm-4 col-md-4 col-lg-4 miss_lable" for="ewe" id="channName">WO/ID : <font size="3">' + workOrderName + '/' + workOrderId + '</font></label><label class="control-label col-sm-4 col-md-4 col-lg-4 miss_lable" id="0">Advert :  <font size="3">' + adverName + '</font></label><label class="control-label col-sm-4 col-md-4 col-lg-4 miss_lable">Scheduled Date :  <font size="3">' + date + '</font></label>';
    $('#schedule_data').html(scheduleData);
    document.getElementById("fromDropdown").value = startTime;
    document.getElementById("toDropdown").value = endTime;
    workOrderChannels(workOrderId, channelId);
}

function workOrderChannels(wo_id, channelId) {
    console.time("workOrderChannels")
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderChannelListUrl(),
        data: {
            workOrderId: wo_id
        },
        success: function (data)
        {
            loadingClose();
            var trHTML = '';
            trHTML += '<span class="input-group-addon lable_forms_trans">Channels</span> <select id="channelList_pop" name="channelName" class="form-control">';
           // trHTML += ' <option VALUE="-111" >--All--</option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
            }
            trHTML += '</select><span class="help-block"></span>';

            $('#channelList_pop_div').html(trHTML);
            document.getElementById("channelList_pop").value = channelId;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
    console.timeEnd("workOrderChannels")
}

function saveReschedulr() {
    var newDate = document.getElementById("date_pop").value;
    if (newDate == "" || newDate == " ") {
        alert("Please select date..");
        return;
    }

    var startTime = document.getElementById("fromDropdown").value;
    var endTime = document.getElementById("toDropdown").value;
    var intStart = startTime.replace(":", "");
    var intEnd = endTime.replace(":", "");
    intStart = intStart.replace(":", "");
    intEnd = intEnd.replace(":", "");

    if (parseInt(intStart) >= parseInt(intEnd)) {
        alert("Selected time belt is invalid.");
        return;
    }
    var programm_name = localStorage.getItem("current_user");
    var schedulDate_v = '[{"workOrderId":' + dataArray[5] + ',"channelId":' + dataArray[6] + ',"advertId":' + dataArray[7] + ',"startTime":"' + startTime + '","endTime":"' + endTime + '","schedulDate":"' + newDate + '","advertCount":' + 1 + ',"programm":"' + programm_name + '","comment":"Reschedule ' + dataArray[0] + '"}]';

    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.scheduler.saveMissedSpotUrl(),
        data: {
            scheduleData: schedulDate_v,
            scheduleId: dataArray[0]
        },
        success: function (data)
        {
            loadingClose();
            if (data) {
                $('#missedSpot_pop').modal('toggle');
                setMissedSpotTable();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function saveReschedulr_v2() {
    var newDate = document.getElementById("date_pop").value;
    if (newDate == "" || newDate == " ") {
        viewMessageToastTyped("Please select date.", "Warning");
        return;
    }

    var startTime = document.getElementById("fromDropdown").value;
    var endTime = document.getElementById("toDropdown").value;
    var channelID = document.getElementById("channelList_pop").value;
    var intStart = startTime.replace(":", "");
    var intEnd = endTime.replace(":", "");
    intStart = intStart.replace(":", "");
    intEnd = intEnd.replace(":", "");

    if (parseInt(intStart) >= parseInt(intEnd)) {
        viewMessageToastTyped("Selected time belt is invalid.", "Warning");
        return;
    }
    var selectedIdList = '';
    for (var i = 0; i < count; i++) {
        var id = i + '_check';
        if (document.getElementById(id).checked) {
            var valueData = document.getElementById(id).value.split("~");
            selectedIdList += valueData[0] + '_';
        }
    }
    if (selectedIdList == '') {
        viewMessageToastTyped("Please select spot", "Warning");
        return;
    }
    var programm_name = localStorage.getItem("current_user");
    var schedulDate_v = '[{"workOrderId":' + dataArray[5] + ',"channelId":' + channelID + ',"advertId":' + dataArray[6] + ',"startTime":"' + startTime + '","endTime":"' + endTime + '","schedulDate":"' + newDate + '","advertCount":' + 1 + ',"programm":"' + programm_name + '","comment":"Reschedule ' + dataArray[0] + '"}]';

    loadingOpen();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.scheduler.saveMissedSpotV2Url(),
        data: {
            scheduleData: schedulDate_v,
            scheduleId: selectedIdList
        },
        success: function (data)
        {
            loadingClose();
            if (data.flag) {
                $('#missedSpot_pop').modal('toggle');
                setMissedSpotTable();
            }
            setMessageToast("" + data.message + "");
            viewMessageToast();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function reschedul() {
    var idList = '';
    for (var i = 0; i < count; i++) {
        var id = i + '_check';
        if (document.getElementById(id).checked) {
            var valueData = document.getElementById(id).value.split("~");
            idList += valueData[0] + '_';
        }
    }

    if (idList != '') {
        loadingOpen();
        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            type: 'POST',
            dataType: "json",
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            async: true,
            url: URI.scheduler.saveReschedulSpotUrl(),
            data: {
                scheduleIds: idList
            },
            success: function (data)
            {
                loadingClose();
                if (data.flag) {
                    setMissedSpotTable();
                }
                setMessageToast("" + data.message + "");
                viewMessageToast();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                loadingClose();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
    } else {
        setMessageToast("Please select the spots");
        viewMessageToast();
    }
}

function selectAllAdvert() {
    if (document.getElementById('all_select_check').checked) {
        for (var i = 0; i < count; i++) {
            document.getElementById(i + '_check').checked = true;
        }
    } else {
        for (var i = 0; i < count; i++) {
            document.getElementById(i + '_check').checked = false;
        }
    }
}

function generateExcel(parameters) {
    var _fromDate = document.getElementById("fromDate").value;
    var _toDate = document.getElementById("toDate").value;
    var selectedHour = document.getElementById("hourList").value;
    var fiter_channel_ids = "";
    var fiter_channel = $('#channelDropdown').find("option:selected");
    fiter_channel.each(function () {
        fiter_channel_ids += "_" + $(this).val();
    });


    if(fiter_channel.length===0){
        viewMessageToastTyped("Please select channels", "Warning");
        return;
    }

    var advertID = $('#productDropdown').val();
    if (advertID === null) {
        advertID = -999;
    }

    var fiter_client_ids = [];
    var fiter_client = $('#clientDropdown').find("option:selected");
    fiter_client.each(function () {
        fiter_client_ids.push(parseInt($(this).val()));
    });

    if (!isNaN(_fromDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }
    if (!isNaN(_toDate)) {
        viewMessageToastTyped("Invalid date", "Warning");
        return;
    }
    var data = {
        fromDate: _fromDate,
        toDate: _toDate,
        channelIDs: fiter_channel_ids,
        advertID: advertID,
        clientID: fiter_client_ids,
        hour:selectedHour
    };
    loadingOpen();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        async: true,
        contentType: "application/json",
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.scheduler.generateExcelSpotUrl(),
        data: JSON.stringify(data),
        success: function (data)
        {
            loadingClose();
            if (data) {
                window.location = URI.scheduler.downloadExcellUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }

    });
}

function releaseSelectedDummyCut() {
    var dummyIDList = '';
    for (var i = 0; i < count; i++) {
        var id = i + '_check';
        if (document.getElementById(id).checked) {
            var valueData = document.getElementById(id).value.split("~");
            dummyIDList += valueData[0] + '_';
        }
    }

    if (dummyIDList != '') {
        loadingOpen();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.scheduler.relaseDummyCutsUrl(),
            data: {
                dummyCutIds: dummyIDList,
            },
            success: function (data)
            {
                loadingClose();
                if (data.flag) {
                    setMissedSpotTable();
                }
                setMessageToast("" + data.message + "");
                viewMessageToast()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                loadingClose();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    } else {
        viewMessageToastTyped("Please selecte dummy cuts", "Warning");
    }

}

function filterChannel(data) {

    var fiter_channel_ids = [];
    var fiter_channel = $('#channelDropdown').find("option:selected");
    fiter_channel.each(function () {
        fiter_channel_ids.push($(this).val());
    });
    var remove = true;
    if (fiter_channel_ids.length !== 0 && fiter_channel_ids[0] !== "-111") {
        for (var i = 0; i < data.length; i++)
        {
            for (var j = 0; j < fiter_channel_ids.length; j++) {
                if (data[i].channelid === stoi(fiter_channel_ids[j])) {
                    remove = false;
                }
            }
            if (remove) {
                data.splice(i, 1);
                i--;
            }
            remove = true;
        }
        return data;
    } else {
        return data;
    }

}

function loadClients() {
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
         var clientListDropdown = '<span class="input-group-addon lable_forms_trans">Client</span> <select id="clientDropdown" name="clientDropdown" class="selectpicker" multiple="true">';
            for (var i in data) {
                clientListDropdown += '<option value=' +data[i].clientid + '>' + data[i].clientname + '</option>';
             }
             clientListDropdown+='</select>';
              $('#clientList').html(clientListDropdown);
              $('#clientDropdown').selectpicker({
                    size: 15
              });
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error");

        }
    });
}

function workOrderDetailsCreator()
{
    console.time("workOrderDetailsCreator");
    loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            for (var i in data)
            {
                $('#productDropdown').append('<option value="' + data[i].workorderid + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>');
            }
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error");

        }
    });
    console.timeEnd("workOrderDetailsCreator");
}