/*var server_path = 'http://localhost:8084/VCLInsertion_DBService';
 var web_page_path = 'http://localhost:8084/VCLInsertion_DBService/pages';
 
 var server_path = 'http://172.22.240.60:8080/VCLInsertion_DBService';
 var web_page_path = 'http://172.22.240.60:8080/VCLInsertion_DBService/pages';*/

var server_path = '';
var web_page_path = '';

var basePath = window.location.origin + '/';

$(document).ready(function () {
    var url = window.location.href;
    url = url.split("/pages");
    server_path = url[0];
    web_page_path = url[0] + "/pages";

});

function showDialog() {
    $("#loading").show();
}

function closeDialog() {
    $("#loading").hide();
}

function logOut() {

    $.ajax({
        type: 'POST',
        url: server_path + '/logout'

    }).done(function (data, textStatus, jqXHR) {
        localStorage.clear();
        window.location.replace("Login.html");
    }).fail(function (jqXHR, textStatus, errorThrown) {
        localStorage.clear();
        window.location.replace("Login.html");
    });

}

function logOut_other() {
    $.ajax({
        type: 'POST',
        url: server_path + '/logout'

    }).done(function (data, textStatus, jqXHR) {
        localStorage.clear();
        window.location.replace("../Login.html");
    }).fail(function (jqXHR, textStatus, errorThrown) {
        localStorage.clear();
        window.location.replace("../Login.html");
    });
}

$(document).ready(function () {
 /*   if (typeof window.USER_AUTHORITY !== 'undefined' && window.USER_AUTHORITY == 'anonymous') {
        console.log(window.USER_AUTHORITY);
        return;
    }

    var username = localStorage.getItem("current_user");
    if (username == null) {
        window.location.replace(web_page_path + "/Login.html");
    } else {
        $('#user_name_div').html(username);
    }*/
});

function validateInputFields(form) {
    flag = true;
    $('input', form).each(function () {
        if ($(this).is('[readonly]') || $(this).is('[disabled]'))
            return;

        if ($(this).is(':valid')) {
            $(this).parent().removeClass('has-error');
            $(this).siblings("span").removeClass('error-block');
            $(this).siblings("span").html('');
        }
        else {
            $(this).parent().addClass('has-error');
            $(this).siblings("span").next().addClass('error-block');
            $(this).siblings("span").next().html(this.validationMessage);
            flag = false;
        }
    });

    $('select', form).each(function () {
        $(this).parent().removeClass('has-error');
        console.log($(this).prop('required'));
        if ($(this).prop('required') && ($(this).val() == -1)) {
            $(this).parent().addClass('has-error');
        }
    });
    return flag;
}

function hasError(element, flag, error) {
    if (!flag) {
        element.parent().removeClass('has-error');
        element.siblings("span").removeClass('error-block');
        element.siblings("span").html('');
    }
    else {
        element.parent().addClass('has-error');
        element.siblings("span").addClass('error-block');
        element.siblings("span").html(error);
    }
}


function stoi(value) {
    if (value == null || value == "" || isNaN(value))
        return 0;
    else
        return parseInt(value);
}

function ChangeProfile() {
    if (typeof (Storage) !== "undefined") {
        //Store
        //localStorage.setItem("updateUser_id", localStorage.getItem("current_user"));
        localStorage.setItem("updateUser_id", $('#user_name_div span').text());
        localStorage.setItem("pageKey", "No");
        window.location.href = URI.userDetails.changeProfilePageUrl();
    }
}

function setMessageToastTyped(message, type) {
    setMessageToast(message);
    switch (type) {
        case "Info":
            $("#msg_info").addClass('info');
            break;
        case "Error":
            $("#msg_info").addClass('error');
            break;
        case "Warning":
            $("#msg_info").addClass('warning');
            break;
        case "Success":
            $("#msg_info").addClass('success');
            break;
    }
}

function isInteger(x) {
    var str = String(Math.floor(Number(x)));
    return (str == x || str == '0');
}

function setMessageToast(message) {
    $("#msg_info").html(message);
}

function viewMessageToast() {
    if ($("#msg_info").html() == "")
        return;

    $("#msg_info").addClass('show');
    // After 3 seconds, remove the show class from DIV
    setTimeout(function () {
        $("#msg_info").removeClass('show');
        $("#msg_info").html("");
    }, 3000);
}

function viewMessageToastTyped(message, type) {
    setMessageToastTyped(message, type)
    if ($("#msg_info").html() == "")
        return;
    viewMessageToast();
}

function viewConfirmToast(message, title, callback_ok) {
    mscConfirm({
        title: title,
        subtitle: message,
        onOk: callback_ok,
        onCancel: function () {
        },
        okText: 'Continue',
        cancelText: 'Cancel'
    });
}

function getDecimal(num)
{
    var originalNumber = num;
    var with2Decimals = originalNumber.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
    num = parseFloat(with2Decimals);
    if (num.length <= 3) {
        return num.toFixed(2);
    } else {
        var deciNum = num.toFixed(2);
        deciNum = deciNum.split('.');
        var value = deciNum[0];
        var number = '';
        var count = 0;
        for (var i = value.length - 1; i >= 0; i--)
        {
            if (count == 3) {
                number = value[i] + ',' + number;
                count = 0;
            } else {
                count++;
                number = value[i] + number;
            }
        }
        return number + '.' + deciNum[1];
    }
}

function getParameterByNamefromUrl(name) {
    url = window.location.href;
    console.log(url);

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var UTIL = {};

UTIL.common = UTIL.common || (function(){
    return {
        getApplicationVersion : function () {
            return $("meta[name='_applicationVersion']").attr("content");
        },
        getFileSeparator : function () {
            return $("meta[name='_filePathSeparator']").attr("content");
        }
    }
}());

//UTIL.inventory = UTIL.inventory || (function(){
//    return {
//        getInventoryByChannelAndDate:  function(channelId, date){
//            return basePath + 'inventory/view-inventory?channelId='+channelId+'&date='+date;
//        },
//        findInventoryExistDates: function(channelId, fromDate, toDate){
//            return basePath + 'inventory/exist-dates?channelId='+channelId+'&fromDate='+fromDate+'&toDate='+toDate;
//        },
//
//    }
//}());


var loadingProcessCount=0;

function loadingOpen(){
    loadingProcessCount++;
    showDialog();
}

function loadingClose(){
    loadingProcessCount--;
    if(loadingProcessCount===0){
        closeDialog();
    }
}

