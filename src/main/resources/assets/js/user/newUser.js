$(document).ready(function () {
    getUserGroupDrop();
    //loadDashboardUserGroups();
});

function addUserAfterValidation(form) {
//    if(validateInputFields(form)){
        addUser();
//    }
}

function addUser() {
    showDialog();
    var valiFlag = 0;
    var name = $("#name").val();
    var imployee_id = $("#imp_id").val();
    var userName = $("#username").val();
    var passWord = $("#password").val();
    var confirmPassWord = $("#confirmpassword").val();
    var addres = $("#address").val();
    var email = $("#e_mail").val();
    var contac = $("#contact").val();
    var wo_create = '';
    var dashboardgroups = getSelectedUserGroups();
    if (document.getElementById("wo_create_premission").checked) {
        wo_create = "1";
    } else {
        wo_create = "0";
    }

    var usrGroup = $("#groupdropdown").val();

    if (passWord != confirmPassWord)
    {
        valiFlag = 1;
        alert("Password not match with confirm password");
    }

    if (usrGroup == -111) {
        valiFlag = 1;
        alert("Please select user group");
    }

    var userdetaills = '{"name":"' + name + '","imployeid":"' + imployee_id + '","password":"' + passWord + '","address":"' + addres + '","email":"' + email + '","contact":"' + contac + '","usergroup":"' + usrGroup + '","wocreatepremission":"' + wo_create + '"}';
    if (valiFlag == 0) {

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            data: {
                username: userName,
                userdetails: userdetaills,
                dashboardGroups: "'"+dashboardgroups+"'"
            },
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            type: 'POST',
            url: URI.userDetails.newuserUrl()
        }).done(function (data, textStatus, jqXHR) {
            closeDialog();
            if (data == true) {
                document.getElementById("name").value = "";
                document.getElementById("imp_id").value = "";
                document.getElementById("username").value = "";
                document.getElementById("password").value = "";
                document.getElementById("confirmpassword").value = "";
                document.getElementById("address").value = "";
                document.getElementById("e_mail").value = "";
                document.getElementById("contact").value = "";
                document.getElementById("groupdropdown").value = -111;
                viewMessageToastTyped("Successfully added the user", "success");
            }
            else {
                alert("Invalid username");
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
            $('#pwConfVali').html("<p><code><small>Error Creating User Account.</small></code></p>");
        });
    } else {
        closeDialog();
    }
}

function getUserGroupDrop() {
    var dropdown = '<label class="control-label col-sm-3 lable_forms new" for="txt_channel_lon">User Group:</label>';
    dropdown += '<div id="group_seleter" class="control-label col-sm-8">';
    dropdown += '<select id="groupdropdown" class="form-control control">';
    dropdown += '<option VALUE="-111" >--Select Group--</option>';

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.getAllUserGroupUrl(),
        success: function (data)
        {
            closeDialog();
            var name = '';

            for (var i in data)
            {
                if (data[i].groupname != name) {
                    name = data[i].groupname;
                    dropdown += '<option VALUE="' + data[i].groupname + '" >' + data[i].groupname + '</option>';
                }
            }

            dropdown += '</select>';
            dropdown += '<span class="help-block"></span>';
            dropdown += '</div>';
            $('#userGroup_droup').html(dropdown);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

$( function() {
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var allUsers = [];
    $.ajax({
        type: 'POST',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.allUserUrl(),
        success: function (data){
            for(var i in data){
                allUsers.push(data[i].userName);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
    $( "#username" ).autocomplete({
        source: allUsers
    });
} );

function getSelectedUserGroups(){
    var selected = [];
    $('#dashboard-user-groups input:checked').each(function() {
        selected.push($(this).attr('value'));
    });
    return selected;
}

function loadDashboardUserGroups(){
    $.ajax({
        url: URI.dashboard.getAllDashboardUserGroupsUrl(),
        success: function (data){
            for(var i in data){
                $("#dashboard-user-groups").append('<label class="control-label col-sm-3 lable_forms new">' + data[i].name + '</label>' +
                '<div class="control-label col-sm-3">' +
                '<input id="' + data[i].code + '" type="checkbox" name="' + data[i].code + '" value="' + data[i].id +'"/>' +
                '<span class="help-block"></span></div>'
                );
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
}
