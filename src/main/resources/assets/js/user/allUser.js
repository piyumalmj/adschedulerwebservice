$(document).ready(function () {
    showDialog();
    loadData()
});

function loadData() {
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.allUserUrl(),
        success: function (data)
        {
            closeDialog();
            var numRows = 0;

            var trHTML = '';
            trHTML += '<table id="allUser_table" class="table table-bordered table-workorder">';
            trHTML += '	<thead><tr><th>User name</th><th>Name</th><th>Employee Id</th><th>Email</th><th>Contact</th><th>Status</th><th>User Group</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                var st = '';
                var group = '';
                if (data[i].status == 1) {
                    st = 'Active';
                } else {
                    st = 'Inactive';
                }

                if (data[i].userGroup != -111) {
                    group = data[i].userGroup;
                }
                trHTML += '<tr> <td class="tb_align_center"><a href="/users-v4/change-profile" id="' + data[i].userName + '" onclick="getId(this.id)">' + data[i].userName + '</a></td><td class="tb_align_center">' + data[i].name + '</td><td class="tb_align_center">' + data[i].employeeid + '</td><td class="tb_align_center">' + data[i].email + '</td><td class="tb_align_center">' + data[i].contact + '</td><td class="tb_align_center">' + st + '</td><td class="tb_align_center">' + group + '</td></tr>';
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#allUser_div').html(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getId(id)
{
    var user_id = document.getElementById(id).innerHTML;
    // Check browser support
    if (typeof (Storage) !== "undefined") {
        //Store
        localStorage.setItem("updateUser_id", user_id);
        localStorage.setItem("pageKey", "Admin");
    }
}
