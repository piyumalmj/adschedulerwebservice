var pageGroupList = '';

$(document).ready(function () {
    showDialog();
    loadData();
    SetJSONdat();
});

/*DB data are put the local JSON array*/
function SetJSONdat() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.getAllUserGroupUrl() ,
        success: function (data)
        {
            closeDialog();
            pageGroupList = data;
            setGroupNameDrop();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*According to DB data, Pages details dropdown is created */
function loadData() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.getAllPageUrl() ,
        success: function (data)
        {
            closeDialog();
            var dropDown = '<select id="pagedropdown" class="form-control control"><option VALUE="-111" >--Select Page--</option>';
            for (var i in data)
            {
                dropDown += '<option VALUE="' + data[i].pageId + '" >' + data[i].pageName + '</option>';
            }
            dropDown += '</select>';
            $('#page_seleter').html(dropDown);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*New group details are collected into JSON array*/
function AddData() {

    var groupName = $("#groupdropdown").val();
    var pageID = $("#pagedropdown").val();
    var pageData = document.getElementById("pagedropdown");
    var pageName = pageData.options[pageData.selectedIndex].innerHTML;
    if (groupName == -111) {
        alert("Please select group name");
        return;
    }
    if (pageID == -111) {
        alert("Please select page");
        return;
    }

    var groupObject = '{"groupname":"' + groupName + '","pagename":"' + pageName + '","pageId":' + pageID + ',"readAcc":1,"writeAcc":1}';


    if (pageGroupList == '') {
        groupObject = '[' + groupObject + ']';
        pageGroupList = JSON.parse(groupObject);
    } else {
        var obj = JSON.parse(groupObject);
        var newPage = true;
        for (var i in pageGroupList)
        {
            if (pageGroupList[i].pageId == obj.pageId && pageGroupList[i].groupname == obj.groupname) {
                //pageGroupList.splice(i, 1);
                newPage = false;
            }
        }
        if (newPage) {
            pageGroupList.push(obj);
        }
    }
    createGroupTable();
}

function AddGeoupName() {

    var groupName = $("#txt_groupName").val();

    if (groupName == '') {
        alert("Please enter group name");
        return;
    }

    var groupObject = '{"groupname":"' + groupName + '","pagename":"AddGroup","pageId":-1,"readAcc":-1,"writeAcc":-1}';


    if (pageGroupList == '') {
        groupObject = '[' + groupObject + ']';
        pageGroupList = JSON.parse(groupObject);
    } else {
        var obj = JSON.parse(groupObject);
        for (var i in pageGroupList)
        {
            if (pageGroupList[i].groupname == obj.groupname) {
                return;
                //pageGroupList.splice(i, 1);
            }
        }
        pageGroupList.push(obj);
    }
    setGroupNameDrop();
    $('#addNewGroup').modal('toggle');
}

/*User group dropdown is created*/
function setGroupNameDrop() {

    var groun_name = '';
    var groupDrop = '<select id="groupdropdown" onchange="groupNameChange()" class="form-control control"><option VALUE="-111" >--Select Group--</option>';

    for (var i in pageGroupList)
    {
        if (pageGroupList[i].groupname != groun_name) {
            groun_name = pageGroupList[i].groupname;
            groupDrop += '<option VALUE="' + groun_name + '" >' + groun_name + '</option>';
        }
    }
    groupDrop += '</select><span class="help-block"></span>';
    $('#group_seleter').html(groupDrop);
    createGroupTable();
}

function createGroupTable() {

    var groupName = $("#groupdropdown").val();

    var groupTable = '<table id="userGroup_table" class="table table-bordered table-workorder">';
    groupTable += '<thead><tr><th>Page</th><th>Read</th><th>Write</th><th></th></tr></thead><tbody>';

    if (pageGroupList != '' && groupName != -111) {

        for (var i in pageGroupList)
        {
            if (pageGroupList[i].groupname == groupName && pageGroupList[i].pageId != -1) {
                var readCheck = '';
                var writeCheck = '';

                if (pageGroupList[i].readAcc == 1) {
                    readCheck = '<input type="checkbox" id="' + pageGroupList[i].pageId + '" onclick="readAcc(this.id)" value="" checked>';
                } else {
                    readCheck = '<input type="checkbox" id="' + pageGroupList[i].pageId + '" onclick="readAcc(this.id)" value="">';
                }

                if (pageGroupList[i].writeAcc == 1) {
                    writeCheck = '<input type="checkbox"  id="' + pageGroupList[i].pageId + '" onclick="writeAcc(this.id)" value="" checked>';
                } else {
                    writeCheck = '<input type="checkbox"  id="' + pageGroupList[i].pageId + '" onclick="writeAcc(this.id)" value="" >';
                }

                groupTable += '<tr class="tb_align_center"><td>' + pageGroupList[i].pagename + '</td><td>' + readCheck + '</td><td>' + writeCheck + '</td><td><button id="' + pageGroupList[i].pageId + '" class="btn btn_color" type="button" onclick="deletePage(this.id)">Delete</button></td></tr>';
            }
        }

    }
    groupTable += '</tbody></table>';
    groupTable += '<div class="row"><button class="btn save btn_color" onclick="savaUserDroup()" type="button" >Save</button></div>';

    $('#userGroup_table_div').html(groupTable);

}

function readAcc(id) {
    var groupName = $("#groupdropdown").val();

    for (var i in pageGroupList)
    {
        if (pageGroupList[i].groupname == groupName) {
            if (pageGroupList[i].pageId == id) {
                if (pageGroupList[i].readAcc == 1) {
                    pageGroupList[i].readAcc = 0;
                } else {
                    pageGroupList[i].readAcc = 1;
                }
            }
        }
    }

}

function writeAcc(id) {
    var groupName = $("#groupdropdown").val();

    for (var i in pageGroupList)
    {
        if (pageGroupList[i].groupname == groupName) {
            if (pageGroupList[i].pageId == id) {
                if (pageGroupList[i].writeAcc == 1) {
                    pageGroupList[i].writeAcc = 0;
                } else {
                    pageGroupList[i].writeAcc = 1;
                }
            }
        }
    }

}

function deletePage(id) {

    var groupName = $("#groupdropdown").val();

    for (var i in pageGroupList)
    {
        if (pageGroupList[i].groupname == groupName) {
            if (pageGroupList[i].pageId == id) {
                pageGroupList.splice(i, 1);
            }
        }
    }

    createGroupTable();

}

function groupNameChange() {
    createGroupTable();
}

function savaUserDroup() {

    for (var i in pageGroupList)
    {
        if (pageGroupList[i].writeAcc == -1) {
            pageGroupList.splice(i, 1);
        }
    }

    var userGroupData = JSON.stringify(pageGroupList);

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.userDetails.saveUserGroupUrl(),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: {
            groupData: userGroupData,
        },
        success: function (data)
        {
            closeDialog();
            if(data){
                viewMessageToastTyped("successfully save user group", "Success");
            }else{
                viewMessageToastTyped("Try again", "Success");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}
