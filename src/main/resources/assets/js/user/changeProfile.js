var groups;
$(document).ready(function () {
    loadUser();
   // loadDashboardUserGroups();
});

function loadUser() {
    showDialog();
    var menu = '<ul class="nav navbar-nav navbar-left"><li class="nav_stn"><a  href="ChangeProfile.html">Change Profile</a></li></ul><a onclick="logOut_other()" class="logout" href="#" id="logout">Log Out</a>';
    var Name = localStorage.getItem("updateUser_id");
    var key = localStorage.getItem("pageKey");
    username = $("#current_user").val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.getSelecteduserUrl(username),
        data: {
            username: Name,
        },
        success: function (data)
        {
            closeDialog();
            document.getElementById("name").value = data.name;
            document.getElementById("emp_id").value = data.employeeid;
            document.getElementById("username").value = data.userName;
            document.getElementById("address").value = data.address;
            document.getElementById("e_mail").value = data.email;
            document.getElementById("contact").value = data.contact;
            document.getElementById("wo_create_premission").checked = data.wo_creat;
            if (key == "Admin") {
                if (data.status == 1) {
                    var button = '<button class="btn btn_inactive btn-block add admin_add" type="button" onclick="InctiveUser()">Inactive</button>';
                    $('#active_button').html(button)
                } else {
                    var button = '<button class="btn btn_active btn-block add admin_add" type="button" onclick="ActiveUser()">Active</button>';
                    $('#active_button').html(button);
                }
                var p_word = '<label class="control-label col-sm-3 lable_forms new" for="txt_channel_lon">Old Password:</label><div class="control-label col-sm-8" ><input type="password" class="form-control control" id="password" name="" value="" title="" placeholder=""><span class="help-block"></span></div>';
                $('#password_div').html(p_word);
                document.getElementById('password_div').style.display = "none"
                getUserGroupDrop(data.userGroup);
            } else {
                document.getElementById('wocreate_div').style.display = "none"
                $('#user_menu').html(menu);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function ActiveUser() {
    showDialog();
    var Name = localStorage.getItem("updateUser_id");
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.setStatusUrl(),
        data: {
            username: Name,
            status: 1,
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.userDetails.changeProfilePageUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });

}


function InctiveUser() {
    showDialog();
    var Name = localStorage.getItem("updateUser_id");
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.setStatusUrl(),
        data: {
            username: Name,
            status: 0,
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.userDetails.changeProfilePageUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function updateUserAfterValidation(form){
    if(validateInputFields(form) ){
        updateUser();
    }
}

function updateUser() {
    var dashboardgroups = getSelectedUserGroups();
    showDialog();
    var key = localStorage.getItem("pageKey");
    var valiFlag = 0;
    var name = $("#name").val();
    var employee_id = $("#emp_id").val();
    var userName = $("#username").val();
    var passWord = $("#password").val();
    var newPassWord = $("#newPassword").val();
    var confirmPassWord = $("#confirmpassword").val();
    var addres = $("#address").val();
    var email = $("#e_mail").val();
    var contac = $("#contact").val();
    var usrGroup = 'Not_admin';
    var wo_create = '';
    if (document.getElementById("wo_create_premission").checked) {
        wo_create = "1";
    } else {
        wo_create = "0";
    }

    if (key == "Admin") {
        usrGroup = $("#groupdropdown").val();
        if (usrGroup == -111) {
            valiFlag = 1;
            alert("Please select user group");
        }

    }

    if (newPassWord != "") {
        if (confirmPassWord == "") {
            valiFlag = 1;
            alert("Please enter New confirm password");
        } else if (newPassWord != confirmPassWord) {
            valiFlag = 1;
            alert("New password not match with confirm password");
        }

    }

    var userdetaills = '{"name":"' + name + '","imployeid":"' + employee_id + '","password":"' + passWord + '","newpassword":"' + newPassWord + '","address":"' + addres + '","email":"' + email + '","contact":"' + contac + '","usergroup":"' + usrGroup + '","wocreatepremission":"' + wo_create + '"}';
    if (valiFlag == 0) {

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            data: {
                username: userName,
                userdetails: userdetaills,
                dashboardGroups: "'"+dashboardgroups+"'"
            },
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            type: 'POST',
            url: URI.userDetails.updateuserinfoUrl()
        }).done(function (data, textStatus, jqXHR) {
            closeDialog();
            if (data == true) {
                if (key == 'FirstTime') {
                    localStorage.removeItem('pageKey');
                    localStorage.removeItem('updateUser_id');
                    window.location = URI.other.homePageUrl() ;
                } else {
                    window.location = URI.userDetails.changeProfilePageUrl();
                }
                //loadDashboardUserGroups()
            }
            else {
                alert("Invalid passWord");
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
            $('#pwConfVali').html("<p><code><small>Error Creating User Account.</small></code></p>");
        });
    } else {
        closeDialog();
    }
}

function getUserGroupDrop(selectedUser) {
    var dropdown = '<label class="control-label col-sm-3 lable_forms new" for="txt_channel_lon">User Group:</label>';
    dropdown += '<div id="group_seleter" class="control-label col-sm-8">';
    dropdown += '<select id="groupdropdown" class="form-control control">';
    dropdown += '<option VALUE="-111" >--Select Group--</option>';

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.userDetails.getAllUserGroupUrl(),
        success: function (data)
        {
            closeDialog();
            var name = '';

            for (var i in data)
            {
                if (data[i].groupname != name) {
                    name = data[i].groupname;
                    dropdown += '<option VALUE="' + data[i].groupname + '" >' + data[i].groupname + '</option>';
                }
            }

            dropdown += '</select>';
            dropdown += '<span class="help-block"></span>';
            dropdown += '</div>';
            $('#userGroup_droup').html(dropdown);
            document.getElementById("groupdropdown").value = selectedUser;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

function loadDashboardUserGroups(){
    username = localStorage.getItem("updateUser_id");
    loadSelectedUserGroups(username);
    $.ajax({
        url: URI.dashboard.getAllDashboardUserGroupsUrl(),
        success: function (data){
            for(var i in data){
                var checked = false;
                for(var j in groups){
                    if(data[i].name == groups[j].name){
                        checked = true;
                        break;
                    }
                }
                if(checked){
                    $("#dashboard-user-groups").append('<label class="control-label col-sm-3 lable_forms new">' + data[i].name + '</label>' +
                    '<div class="control-label col-sm-3">' +
                    '<input id="' + data[i].code + '" type="checkbox" name="' + data[i].code + '" value="' + data[i].id +'" checked/>' +
                    '<span class="help-block"></span></div>'
                    );
                }else{
                    $("#dashboard-user-groups").append('<label class="control-label col-sm-3 lable_forms new">' + data[i].name + '</label>' +
                    '<div class="control-label col-sm-3">' +
                    '<input id="' + data[i].code + '" type="checkbox" name="' + data[i].code + '" value="' + data[i].id +'"/>' +
                    '<span class="help-block"></span></div>'
                    );
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
}

function loadSelectedUserGroups(username){
    $.ajax({
        url: URI.dashboard.getDashboardUserUrl(username),
        success: function (data){
            groups = data.groups;
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
}

function getSelectedUserGroups(){
    var selected = [];
    $('#dashboard-user-groups input:checked').each(function() {
        selected.push($(this).attr('value'));
    });
    return selected;
}
