var settingMap = {};
var settingMapJSON = [];
$(document).ready(function () {
    getSettingDetails();
});


function getSettingDetails() {
    showDialog();
    newAdvertIdMap = {};
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.zero.getZeroAdsSettingUrl(),
        success: function (data)
        {
            buildTable(data);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            } else {
                alert("Error");
            }
        }
    });

}

function buildTable(data) {

    settingMap = {};
    var channelkey = '';
    var table = '<thead><tr><th>Channel</th><th>Auto PlayList</th><th>Manual PlayList</th></tr>';
    table += '</thead><tbody>';
    for (var i in data) {
        settingMap[data[i].channelId.channelid] = data[i];
        table += '<tr><td>' + data[i].channelId.channelname + '</td>';
        if (data[i].pListType === "AUTOPLAYLIST") {
            table += '<td><input id="' + data[i].channelId.channelid + '_auto" type = "checkbox" name = "checkbox" onclick="checkAutoPlayList(this.id)" checked="true"></td><td><input id="' + data[i].channelId.channelid + '_manual" type = "checkbox" name = "checkbox" value = "" onclick="checkManualPlayList(this.id)"></td></tr>';
        } else {
            table += '<td><input id="' + data[i].channelId.channelid + '_auto" type = "checkbox" name = "checkbox" onclick="checkAutoPlayList(this.id)" ></td><td><input id="' + data[i].channelId.channelid + '_manual" type = "checkbox" name = "checkbox" onclick="checkManualPlayList(this.id)" checked="true"></td></tr>';
        }
    }
    table += ' </tbody>';
    $('#tbl_zero_ads_setting').html(table);
}

function checkAutoPlayList(id) {
    var checkbox = document.getElementById(id);
    var manualId = id.split("_")[0] + '_manual';
    var channelId = id.split("_")[0];
    if (checkbox.checked != true)
    {
        settingMap[channelId].pListType = "MANUALPLAYLIST";
        document.getElementById(manualId).checked = true;
    } else {
        settingMap[channelId].pListType = "AUTOPLAYLIST";
        document.getElementById(manualId).checked = false;
    }
}

function checkManualPlayList(id) {
    var checkbox = document.getElementById(id);
    var autoId = id.split("_")[0] + '_auto';
    var channelId = id.split("_")[0];
    if (checkbox.checked != true)
    {
        settingMap[channelId].pListType = "AUTOPLAYLIST";
        document.getElementById(autoId).checked = true;
    } else {
        settingMap[channelId].pListType = "MANUALPLAYLIST";
        document.getElementById(autoId).checked = false;
    }
}

function saveMap() {
    settingMapSON = [];
    for (var key in settingMap) {
            var data = {id: settingMap[key].id, channelId: key, pListType: settingMap[key].pListType};
            settingMapSON.push(data);
    }
    if (settingMapSON.length != 0) {

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            type: 'POST',
            dataType: "json",
            url: URI.zero.saveZeroAdsSettingMapUrl(),
            data: {
                mapData: JSON.stringify(settingMapSON)
            },
            success: function (data)
            {
                if (data) {
                    viewMessageToastTyped("successfully", "success");
                }
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
    } else {
        viewMessageToastTyped("No data", "success");
    }
}